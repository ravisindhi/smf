package com.smf.daffodil.smfapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smf.daffodil.smfapp.R;

/**
 * Created by daffodil on 9/2/17.
 */

public class ImageUtils {

    public static void setprofile(final ImageView imageView, String url, final Context _mcontext)
    {

        Glide.with(_mcontext).load(url).asBitmap().centerCrop().error(R.drawable.profile_placeholder).placeholder(R.drawable.profile_placeholder).into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(_mcontext.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }
}
