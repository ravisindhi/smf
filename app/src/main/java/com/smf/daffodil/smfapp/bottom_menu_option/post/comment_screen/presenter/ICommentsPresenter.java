package com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.presenter;

import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;

/**
 * Created by daffodil on 20/2/17.
 */

public interface ICommentsPresenter {

    void getAllComments(String postid);
    void addComment(String postid,String coomenttxt);
    void updatePost(UpdatePostRequest request, int pos,String from);
}
