package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

import java.io.Serializable;

/**
 * Created by daffodil on 11/2/17.
 */

public class Services implements Serializable{

    private Password password;

    public Password getPassword ()
    {
        return password;
    }

    public void setPassword (Password password)
    {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [password = "+password+"]";
    }
}
