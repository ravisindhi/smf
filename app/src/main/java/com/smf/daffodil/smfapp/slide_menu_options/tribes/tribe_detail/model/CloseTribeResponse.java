package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model;

/**
 * Created by daffodil on 14/2/17.
 */

public class CloseTribeResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
