package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.TribesMemberSearchResponse;

/**
 * Created by daffodil on 15/2/17.
 */

public interface ITribeMemberView extends IBaseView{
    void onSuccessRemoveMember(int position);
    void onError(int errorcode,String msg);
    void onGetMember(TribesMemberSearchResponse result);
    void onAddMember();
}
