package com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.presenter;

/**
 * Created by daffodil on 9/2/17.
 */

public interface IUserProfilePresenter {
    void doBlockMember(String id);
}
