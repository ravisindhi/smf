package com.smf.daffodil.smfapp.splash;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.messaging.FirebaseMessaging;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.AuthActivity;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.push_notification.NotificationConfig;
import com.smf.daffodil.smfapp.splash.presenter.ISplashCompl;
import com.smf.daffodil.smfapp.splash.presenter.ISplashPresenter;
import com.smf.daffodil.smfapp.splash.view.iSplashView;

public class SplashActivity extends AppCompatActivity implements iSplashView{


    ISplashPresenter mISplashPresenter;
    SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /** Hiding Title bar of this activity screen */
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        /** Making this activity, full screen */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        initPresenter();

        sessionManager=new SessionManager(this);



      mISplashPresenter.starTimer();

    }

    private void initPresenter()
    {
        mISplashPresenter =new ISplashCompl(this);
    }

    @Override
    public void onTimeComplete() {
        sessionManager.checkLogin();
        finish();

    }
}
