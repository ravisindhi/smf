package com.smf.daffodil.smfapp.authentication.login.model.response;

import android.support.annotation.Nullable;

import com.smf.daffodil.smfapp.bottom_menu_option.map.model.response.SavedLocation;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.SavedLocations;

import java.io.Serializable;
import java.util.List;

/**
 * Created by daffodil on 30/1/17.
 */

public class Profile implements Serializable{

    private String twitterProfileUrl;

    private String firstName;

    private String streetAddress;

    private String fullName;

    private String relationship;



    private String gender;
    private String zipCode;

    private String lastName;
    private String searchEmail;
    private String handle;
    private String profileImage;
    private String webAddress;
    private String state;
    private String facebookProfileUrl;
    private String linkedInProfileUrl;
    private String intro;
    private String[] hiddenPosts;
    private String city;
    private String title;
    private String phoneNumber;

    private String company;
    private String dob;
    private List<SavedLocations> savedLocations;

   // private String fullName;




    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getStreetAddress ()
    {
        return streetAddress;
    }

    public void setStreetAddress (String streetAddress)
    {
        this.streetAddress = streetAddress;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getSearchEmail ()
    {
        return searchEmail;
    }

    public void setSearchEmail (String searchEmail)
    {
        this.searchEmail = searchEmail;
    }

    public String getHandle ()
    {
        return handle;
    }

    public void setHandle (String handle)
    {
        this.handle = handle;
    }

    public String getProfileImage ()
    {
        return profileImage;
    }

    public void setProfileImage (String profileImage)
    {
        this.profileImage = profileImage;
    }

    public String getWebAddress ()
    {
        return webAddress;
    }

    public void setWebAddress (String webAddress)
    {
        this.webAddress = webAddress;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getFacebookProfileUrl ()
    {
        return facebookProfileUrl;
    }

    public void setFacebookProfileUrl (String facebookProfileUrl)
    {
        this.facebookProfileUrl = facebookProfileUrl;
    }

    public String getLinkedInProfileUrl ()
    {
        return linkedInProfileUrl;
    }

    public void setLinkedInProfileUrl (String linkedInProfileUrl)
    {
        this.linkedInProfileUrl = linkedInProfileUrl;
    }

    public String getIntro ()
    {
        return intro;
    }

    public void setIntro (String intro)
    {
        this.intro = intro;
    }

    public String[] getHiddenPosts ()
    {
        return hiddenPosts;
    }

    public void setHiddenPosts (String[] hiddenPosts)
    {
        this.hiddenPosts = hiddenPosts;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getPhoneNumber ()
    {
        return phoneNumber;
    }

    public void setPhoneNumber (String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }


    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

  /*  public String getFullName ()
    {
        return fullName;
    }

    public void setFullName (String fullName)
    {
        this.fullName = fullName;
    }
*/
    public String getTwitterProfileUrl ()
    {
        return twitterProfileUrl;
    }

    public void setTwitterProfileUrl (String twitterProfileUrl)
    {
        this.twitterProfileUrl = twitterProfileUrl;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [streetAddress = "+streetAddress+", lastName = "+lastName+", searchEmail = "+searchEmail+", handle = "+handle+", profileImage = "+profileImage+", webAddress = "+webAddress+", state = "+state+", facebookProfileUrl = "+facebookProfileUrl+", linkedInProfileUrl = "+linkedInProfileUrl+", intro = "+intro+", hiddenPosts = "+hiddenPosts+", city = "+city+", title = "+title+", phoneNumber = "+phoneNumber+", company = "+company+", dob = "+dob+",  twitterProfileUrl = "+twitterProfileUrl+", firstName = "+firstName+"]";
    }

    public List<SavedLocations> getSavedLocations() {
        return savedLocations;
    }

    public void setSavedLocations(List<SavedLocations> savedLocations) {
        this.savedLocations = savedLocations;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
