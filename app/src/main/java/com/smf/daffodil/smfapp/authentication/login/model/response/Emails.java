package com.smf.daffodil.smfapp.authentication.login.model.response;

import java.io.Serializable;

/**
 * Created by daffodil on 30/1/17.
 */

public class Emails implements Serializable{

    private String _id;

    private String address;

    private String verified;

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getVerified ()
    {
        return verified;
    }

    public void setVerified (String verified)
    {
        this.verified = verified;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [_id = "+_id+", address = "+address+", verified = "+verified+"]";
    }
}
