package com.smf.daffodil.smfapp.authentication.login.presenter;

import android.content.Context;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.request.FacebookLoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.request.LoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.RequestCallback;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daffodil on 24/1/17.
 */

public class ILoginPresenterCompl implements ILoginPresenter {

    ILoginView _mLoginView;
    private Context _context;

    public ILoginPresenterCompl(Context _context, ILoginView iLoginView) {
        this._context = _context;
        this._mLoginView = iLoginView;
    }

    @Override
    public void doLogin(LoginRequest loginRequest) {

        if (validateLoginForm(loginRequest)) {
            startLoginService(loginRequest);
        }

    }

    @Override
    public void doFacebookLogin(FacebookLoginRequest facebookLoginRequest) {
        if (facebookLoginRequest != null) {
            startFacebookLoginService(facebookLoginRequest);
        }
    }

    private boolean validateLoginForm(LoginRequest _mLoginResquest) {
        boolean _mValidModel = true;
        if (_mLoginResquest.getEmail() == null || _mLoginResquest.getEmail().trim().length() <= 0) {
            _mValidModel = false;
            _mLoginView.onErrorHandleWithView(ILoginView.EMAIL_ERROR, "");
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(_mLoginResquest.getEmail()).matches()) {
            _mValidModel = false;
            _mLoginView.onErrorHandleWithSnakbar(ILoginView.EMAIL_ERROR, "");
        } else if (_mLoginResquest.getPassword() == null || _mLoginResquest.getPassword().trim().length() <= 0) {
            _mValidModel = false;
            _mLoginView.onErrorHandleWithView(ILoginView.PASSWORD_ERROR, "");
        }
        return _mValidModel;
    }

    private void startLoginService(LoginRequest loginRequest) {

        _mLoginView.showLoader(_context.getString(R.string.please_wait_message));
        if (_mLoginView.onCheckNetworkConnection()) {

            String json = new Gson().toJson(loginRequest);
            final JSONObject _mRequest;
            try {
                _mRequest = new JSONObject(json);
                SMFApplication.getRestClient().doLogin(new RequestCallback<LoginResponse>() {
                    @Override
                    public void onRestResponse(Exception e, LoginResponse result) {
                        if (_mLoginView.onSaveUserData(result)) {
                            _mLoginView.onSuccess(result);
                        }
                        _mLoginView.hideLoader();
                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        _mLoginView.hideLoader();
                        e.printStackTrace();
                        _mLoginView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR, result.getMessage());

                    }
                }, _mRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            _mLoginView.hideLoader();
            _mLoginView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR, _context.getString(R.string.internet_error_message));

        }


    }


    private void startFacebookLoginService(FacebookLoginRequest loginRequest) {
        _mLoginView.showLoader(_context.getString(R.string.please_wait_message));
        if (_mLoginView.onCheckNetworkConnection()) {


            SMFApplication.getRestClient().doFacebookLogin(new StringRequestCallback<LoginResponse>() {


                @Override
                public void onRestResponse(Exception e, LoginResponse result) {
                    if (e == null && result != null) {
                        if (_mLoginView.onSaveUserData(result)) {
                            _mLoginView.onSuccess(result);
                        }
                        _mLoginView.hideLoader();
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    _mLoginView.hideLoader();
                    e.printStackTrace();
                    _mLoginView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR, result.getMessage());

                }


            }, loginRequest);
        } else {
            _mLoginView.hideLoader();
            _mLoginView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR, _context.getString(R.string.internet_error_message));
        }
    }


}
