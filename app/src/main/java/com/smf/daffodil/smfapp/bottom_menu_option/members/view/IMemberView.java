package com.smf.daffodil.smfapp.bottom_menu_option.members.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;

/**
 * Created by daffodil on 26/1/17.
 */

public interface IMemberView extends IBaseView{

    public enum Action{
        Follow("follow"),
        Unfollow("unfollow");


        String action_type;
        private Action(String value)
        {
            action_type=value;
        }
        @Override
        public String toString() {
            return action_type;
        }
    }

    void onGetMember(MemberResponse[] membersDataList);
    void onError(int errorcode,String message);
    void onFollowUnfollowSuccess(String message);
}
