package com.smf.daffodil.smfapp.bottom_menu_option.post;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;
import com.smf.daffodil.smfapp.R;

import com.smf.daffodil.smfapp.base.BaseActivity;

import dmax.dialog.SpotsDialog;

public class FullScreenPlayerActivity extends BaseActivity {

    private VideoView videoview;
    AlertDialog dialog;
    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_player);

        videoview = (VideoView)findViewById(R.id.surface_view);

        Uri newUri= Uri.parse(getIntent().getStringExtra("URI"));
        position=getIntent().getIntExtra("currentPosition",0);
        if(newUri==null)
        {
            Toast.makeText(FullScreenPlayerActivity.this,"Video Not Available..", Toast.LENGTH_SHORT).show();
            finish();
        }

        dialog = new SpotsDialog(FullScreenPlayerActivity.this, R.style.Custom);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        videoview.setVideoURI(newUri);
        videoview.setMediaController(new MediaController(this));
        videoview.requestFocus();
        videoview.seekTo(position);
        videoview.start();

        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                if (dialog != null && dialog.isShowing()) {
                    dialog.cancel();
                }

            }
        });



    }
}
