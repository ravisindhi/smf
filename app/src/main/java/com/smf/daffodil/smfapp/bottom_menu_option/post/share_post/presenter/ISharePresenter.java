package com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.view.IShareView;

/**
 * Created by daffodil on 21/2/17.
 */

public interface ISharePresenter {

    void doSharePost(SharePostRequest request);
}
