package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.RequestCallback;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.AddMemberRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.TribesMemberSearchResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.view.ITribeMemberView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daffodil on 15/2/17.
 */

public class ITribeMemberCompl implements ITribeMemberPresneter{

    private Context context;
    private ITribeMemberView iTribeMemberView;

    public ITribeMemberCompl(Context context, ITribeMemberView iTribeMemberView) {
        this.context = context;
        this.iTribeMemberView = iTribeMemberView;
    }


    @Override
    public void removeTribeMember(final JoinTribeRequest joinTribeRequest) {
        iTribeMemberView.showLoader(context.getString(R.string.please_wait_message));

        if (iTribeMemberView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().removeTribeMember(new StringRequestCallback<CreateTribeResponse>() {
                @Override
                public void onRestResponse(Exception e, CreateTribeResponse result) {
                    iTribeMemberView.hideLoader();
                    iTribeMemberView.onSuccessRemoveMember(joinTribeRequest.getPosition());
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    iTribeMemberView.hideLoader();
                    iTribeMemberView.onError(IBaseView.SERVER_ERROR, result.getMessage());

                }
            },joinTribeRequest);

        }else
        {
            iTribeMemberView.hideLoader();
            iTribeMemberView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }
    }

    @Override
    public void searchMember(String name) {
        if(iTribeMemberView.onCheckNetworkConnection())
        {

            SMFApplication.getRestClient().groupMemberSearchByName(new StringRequestCallback<TribesMemberSearchResponse>() {

                @Override
                public void onRestResponse(Exception e, TribesMemberSearchResponse result) {
                    if(e==null && result !=null)
                    {
                        iTribeMemberView.onGetMember(result);
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    e.printStackTrace();
                    iTribeMemberView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                }

            },name);
        }else
        {
            iTribeMemberView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void addTribeMember(AddMemberRequest request) {
        iTribeMemberView.showLoader(context.getString(R.string.please_wait_message));

        if (iTribeMemberView.onCheckNetworkConnection()) {

            String json = new Gson().toJson(request);
            final JSONObject _mRequest;
            try {
                _mRequest = new JSONObject(json);

                SMFApplication.getRestClient().addMember(new RequestCallback<CreateTribeResponse>() {
                    @Override
                    public void onRestResponse(Exception e, CreateTribeResponse result) {
                        iTribeMemberView.hideLoader();
                        iTribeMemberView.onAddMember();
                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        iTribeMemberView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                        iTribeMemberView.hideLoader();
                    }
                }, _mRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            iTribeMemberView.hideLoader();
            iTribeMemberView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }
    }


}
