package com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.bottom_menu_option.post.adapter.PostAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.model.CommentsResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.utils.DateTimeAgo;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daffodil on 20/2/17.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder> {

    private List<CommentsResponse> list;
    private Activity context;
    private SessionManager sessionManager;
    private PopupWindow post_popup;
    private PostActionsListener postActionsListener;

    public CommentsAdapter(List<CommentsResponse> list, Activity context, CommentsAdapter.PostActionsListener postActionsListener) {
        this.list = list;
        this.context = context;
        this.postActionsListener=postActionsListener;
        sessionManager=new SessionManager(context);
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item_view, parent, false);
        CommentsViewHolder holder = new CommentsViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final CommentsViewHolder holder, final int position) {

        ImageUtils.setprofile(holder.commentuserImage,list.get(position).getAuthor().getProfile().getProfileImage(),context);
        holder.commentUserName.setText(list.get(position).getAuthor().getProfile().getFirstName()+" "+
        list.get(position).getAuthor().getProfile().getLastName());
        holder.textViewCommentContent.setText(list.get(position).getContent());
        holder.textViewCommentLikes.setText(String.valueOf(list.get(position).getLikedBy().size()));
        String date = list.get(position).getCreatedAt().split("\\.")[0];
        holder.textViewCommentTime.setText(DateTimeAgo.getTimeAgo2(date + "Z"));
        holder.hotview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slideToTop(holder.hotview,list.get(position),position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class CommentsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rounded_image_view_comment)
        ImageView commentuserImage;

        @BindView(R.id.txt_name_comment)
        TextView commentUserName;

        @BindView(R.id.txt_content_comment)
        TextView textViewCommentContent;

        @BindView(R.id.hot_comment_txt)
        TextView textViewCommentLikes;

        @BindView(R.id.txt_comment_time)
        TextView textViewCommentTime;

        @BindView(R.id.hot_view_comment)
        LinearLayout hotview;


        public CommentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }


    public void slideToTop(final View view, CommentsResponse postData, final int position) {

        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.emojiViewNew);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.emoji_new_layout, null);


        LinearLayout likeLayout = (LinearLayout) layout.findViewById(R.id.like_button);
        LinearLayout dislikeLayout = (LinearLayout) layout.findViewById(R.id.dislike_button);
        LinearLayout peaceLayout = (LinearLayout) layout.findViewById(R.id.peace_button);

        ImageView thumbsupImg = (ImageView) layout.findViewById(R.id.thumbs_up_count_image);
        ImageView thumbsdownImg = (ImageView) layout.findViewById(R.id.thumbs_down_count_image);
        ImageView peaceImg = (ImageView) layout.findViewById(R.id.peace_count_image);


        TextView txt_liked = (TextView) layout.findViewById(R.id.txt_liked);
        TextView txt_dislike = (TextView) layout.findViewById(R.id.txt_disliked);
        TextView txt_peace = (TextView) layout.findViewById(R.id.txt_peace);

        if (postData.getLikedBy().contains(sessionManager.getUserData().get_id()))
            thumbsupImg.setImageResource(R.drawable.ic_thumbs_up);
        else
            thumbsupImg.setImageResource(R.drawable.ic_thunbs_up_gray);

        if (postData.getDislikedBy().contains(sessionManager.getUserData().get_id()))
            thumbsdownImg.setImageResource(R.drawable.ic_thumbs_down_blue);
        else
            thumbsdownImg.setImageResource(R.drawable.ic_thumbs_down);

        if (postData.getPeaceBy().contains(sessionManager.getUserData().get_id()))
            peaceImg.setImageResource(R.drawable.ic_peace_blue);
        else
            peaceImg.setImageResource(R.drawable.ic_peace);

        txt_liked.setText(String.valueOf(postData.getLikedBy().size()));
        txt_dislike.setText(String.valueOf(postData.getDislikedBy().size()));
        txt_peace.setText(String.valueOf(postData.getPeaceBy().size()));

        YoYo.with(Techniques.RotateInUpLeft)
                .duration(700)
                .playOn(likeLayout);

        YoYo.with(Techniques.RotateInUpLeft)
                .duration(700)
                .playOn(dislikeLayout);

        YoYo.with(Techniques.RotateInUpLeft)
                .duration(700)
                .playOn(peaceLayout);


        final UpdatePostRequest request = new UpdatePostRequest();
        request.setPostId(postData.getId());

        likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request.setAction("like");
                postActionsListener.performAction(request,position);
                post_popup.dismiss();
            }
        });

        dislikeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request.setAction("dislike");
                postActionsListener.performAction(request,position);
                post_popup.dismiss();
            }
        });

        peaceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request.setAction("peace");
                postActionsListener.performAction(request,position);
                post_popup.dismiss();
            }
        });


        // Creating the PopupWindow
        post_popup = new PopupWindow(context);
        post_popup.setContentView(layout);
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 260, context.getResources().getDisplayMetrics());

        post_popup.setWidth(width);
        post_popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int OFFSET_X = -500;
        int OFFSET_Y = -70;

        //Clear the default translucent background
        post_popup.setBackgroundDrawable(new BitmapDrawable());

        int[] location = new int[2];

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        view.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        Point point = new Point();
        point.x = location[0];
        point.y = location[1];


        // Displaying the popup at the specified location, + offsets.
        post_popup.showAtLocation(layout, Gravity.NO_GRAVITY, point.x, point.y + OFFSET_Y);
        YoYo.with(Techniques.SlideInUp)
                .duration(700)
                .playOn(layout);

    }

    public interface PostActionsListener {
        public void performAction(UpdatePostRequest request, int position);
    }


}
