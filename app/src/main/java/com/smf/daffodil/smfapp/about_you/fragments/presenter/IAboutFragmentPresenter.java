package com.smf.daffodil.smfapp.about_you.fragments.presenter;

import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;

/**
 * Created by daffodil on 25/1/17.
 */

public interface IAboutFragmentPresenter {
    void getAboutYouData(AboutYouRequest aboutYouRequest);
    void checkHandleName(String handle_txt);

}
