package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response.Result;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 30/1/17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder> {

    private Context context;
    private List<Result> results;
    private SessionManager sessionManager;

    public MessagesAdapter(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
        sessionManager=new SessionManager(context);
    }


    @Override
    public MessagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_item,parent,false);

        return new MessagesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessagesViewHolder holder, int position) {
        Result data=results.get(position);

        if(data!=null){
            if(!sessionManager.getUserData().get_id().equals(data.getRecipient())){
                holder.textViewName.setText(data.getReceiverDetail()[0].getProfile().getFullName());
                ImageUtils.setprofile(holder.imageViewProfileView,data.getReceiverDetail()[0].getProfile().getProfileImage(),context);
                holder.textViewMsgContent.setText(data.getContent());
            }else
            {
                holder.textViewName.setText(data.getSenderDetail()[0].getProfile().getFullName());
                ImageUtils.setprofile(holder.imageViewProfileView,data.getSenderDetail()[0].getProfile().getProfileImage(),context);
                holder.textViewMsgContent.setText(data.getContent());
            }


        }

    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class MessagesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rounded_image_view)
        ImageView imageViewProfileView;

        @BindView(R.id.txt_name)
        TextView textViewName;

        @BindView(R.id.txt_msg_content)
        TextView textViewMsgContent;


        public MessagesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
