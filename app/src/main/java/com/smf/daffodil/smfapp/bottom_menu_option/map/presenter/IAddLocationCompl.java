package com.smf.daffodil.smfapp.bottom_menu_option.map.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.map.model.request.AddLocationRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.map.model.response.AddLocationResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.map.model.response.SavedLocation;
import com.smf.daffodil.smfapp.bottom_menu_option.map.view.IMapView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.RequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daffolap-164 on 16/2/17.
 */

public class IAddLocationCompl implements IAddLocationPresenter {

    IMapView iMapView;
    Context context;
    SessionManager sessionManager;

    public IAddLocationCompl(Context context, IMapView iMapView) {
        this.iMapView = iMapView;
        this.context = context;
        sessionManager=new SessionManager(context);
    }


    @Override
    public void addLocation(AddLocationRequest request) {
        if (request != null)
            addLocationToServer(request);

    }

    private void addLocationToServer(final AddLocationRequest request) {
        if (iMapView.onCheckNetworkConnection()) {
            iMapView.showLoader(context.getString(R.string.please_wait_message));
            String json = new Gson().toJson(request);
            final JSONObject _mRequest;
            try {
                _mRequest = new JSONObject(json);
                //_mRequest.remove("imageUri");
                SMFApplication.getRestClient().addLocation(new RequestCallback<AddLocationResponse>() {
                    @Override
                    public void onRestResponse(Exception e, AddLocationResponse result) {

                    //    sessionManager.getUserData().getProfile().setSavedLocations(result.getProfile().getSavedLocations());
                        LoginResponse loginResponse=sessionManager.getUserData();
                        loginResponse.getProfile().setSavedLocations(result.getProfile().getSavedLocations());
                        sessionManager.saveUserData(loginResponse);

                        Log.e("RESULT",new Gson().toJson(result));
                        iMapView.hideLoader();
                        iMapView.onSuccess();
                        Toast.makeText(context,"Location added successfully",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        iMapView.hideLoader();
                    }
                }, _mRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            iMapView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }
    }
}
