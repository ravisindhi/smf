package com.smf.daffodil.smfapp.slide_menu_options.change_password.presenter;

import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordRequest;

/**
 * Created by daffodil on 1/2/17.
 */

public interface IChangePasswordPresenter {

    void doChangePassword(ChangePasswordRequest request);
}
