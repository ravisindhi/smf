package com.smf.daffodil.smfapp.about_you.view;

import com.smf.daffodil.smfapp.about_you.model.AboutYouResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;

/**
 * Created by daffodil on 25/1/17.
 */

public interface IAboutYouView extends IBaseView{

    void onError(int error_code,String error_message);
    void onSuccess(AboutYouResponse aboutYouResponse);
}
