package com.smf.daffodil.smfapp.utils;

import java.io.Serializable;

/**
 * Created by Administrator on 29-Dec-15.
 * lunchbox-android
 */
public abstract class ImageCompressionCallback implements Serializable {
    public abstract void onCompressionComplete();
}
