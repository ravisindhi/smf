package com.smf.daffodil.smfapp.authentication.forgot_password.view;

import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;

/**
 * Created by daffodil on 24/1/17.
 */

public interface IForgotPasswordView extends IBaseView{

    public static int EMAIL_ERROR=1;

    void onErrorHandleWithView(int errorCode,String _errorMsg);
    void onErrorHandleWithSnakbar(int errorCode,String _errorMsg);
    void onSuccess(ForgotPasswordResponse forgotPasswordResponse);
}
