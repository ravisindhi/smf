package com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.presenter;

import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.model.SearchPostRequest;

/**
 * Created by daffodil on 10/2/17.
 */

public interface ISearchPostPresenter {

    void searchPost(SearchPostRequest request);
    void searchGroupPost(SearchPostRequest request);
}
