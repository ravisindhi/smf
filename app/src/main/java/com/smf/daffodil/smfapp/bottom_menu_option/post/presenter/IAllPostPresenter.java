package com.smf.daffodil.smfapp.bottom_menu_option.post.presenter;

import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;

/**
 * Created by daffodil on 3/2/17.
 */

public interface IAllPostPresenter {

    void getAllPosts(GetPostRequest request);

}
