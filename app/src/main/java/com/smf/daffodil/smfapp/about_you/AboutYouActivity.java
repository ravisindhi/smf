package com.smf.daffodil.smfapp.about_you;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.adapter.ViewPagerAdapter;
import com.smf.daffodil.smfapp.about_you.communication.ActivityCommunicator;
import com.smf.daffodil.smfapp.about_you.communication.FragmentCommunicator;
import com.smf.daffodil.smfapp.about_you.fragments.presenter.IAboutFragmentPresenter;
import com.smf.daffodil.smfapp.about_you.fragments.presenter.IAboutPresenterCompl;
import com.smf.daffodil.smfapp.about_you.fragments.view.IAboutFragmentView;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.about_you.model.AboutYouResponse;
import com.smf.daffodil.smfapp.about_you.presenter.IAboutYouCompl;
import com.smf.daffodil.smfapp.about_you.presenter.IAboutYouPresenter;
import com.smf.daffodil.smfapp.about_you.view.IAboutYouView;
import com.smf.daffodil.smfapp.authentication.AuthActivity;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.security.AccessController.getContext;

public class AboutYouActivity extends BaseActivity implements IAboutYouView,ViewPager.OnPageChangeListener, ActivityCommunicator {

    public FragmentCommunicator fragmentCommunicator;
    IAboutYouPresenter iAboutYouPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.viewPagerCountDots)
    LinearLayout pager_indicator;

    @BindView(R.id.btn_continue)
    Button btn_login;

    ViewPagerAdapter mAdapter;
    private int dotsCount;
    private ImageView[] dots;

    AboutYouRequest aboutYouRequest;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_you);
        ButterKnife.bind(this);
        iAboutYouPresenter=new IAboutYouCompl(this,this);

        aboutYouRequest = new AboutYouRequest();

        setupToolbar(toolbar, R.layout.toolbar_layout);
        setupToolBarName("ABOUT YOU", false, false);

        mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(this);
        viewPager.setOffscreenPageLimit(0);
        setUiPageViewController();
    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselected_item_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_dot));
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        switch (position) {
            case 0:
                setupToolBarName(getString(R.string.about_you_text), false, false);
                break;
            case 1:
                setupToolBarName(getString(R.string.profile_photo_text), false, false);
                break;
            case 2:
                setupToolBarName(getString(R.string.review_profile_text), false, false);
                break;

        }

        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselected_item_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_dot));

        if (position + 1 == dotsCount) {
            btn_login.setText("FINISH");
        } else {
            btn_login.setText("CONTINUE");
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        fragmentCommunicator.setMandetoryData();
    }

    @OnClick(R.id.btn_continue)
    public void getDataFromFragment() {
        if (btn_login.getText().equals("CONTINUE"))
            fragmentCommunicator.passDataToFragment();
        else
            iAboutYouPresenter.checkDataValidation(aboutYouRequest);
    }


    @Override
    public void passDataToActivity(AboutYouRequest aboutYouRequest) {
        this.aboutYouRequest = aboutYouRequest;
        viewPager.setCurrentItem(getItem(+1));
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public AboutYouRequest getDataToActivity() {
        return null;
    }

    @Override
    public void onError(int error_code, String error_message) {
        switch (error_code)
        {
            case IAboutFragmentView.FIRST_NAME_ERROR:
                showErrorMessage(getApplicationContext(),"Enter First Name",this);
                break;

            case IAboutFragmentView.LAST_NAME_ERROR:
                showErrorMessage(getApplicationContext(),"Enter Last Name",this);
                break;

            case IAboutFragmentView.HANDLE_ERROR:
                showErrorMessage(getApplicationContext(),"Enter Handle",this);
                break;
            case IAboutFragmentView.MISSING_ERROR:
                showErrorMessage(getApplicationContext(),"Please complete the form before finish",this);
                break;
        }
    }

    @Override
    public void onSuccess(AboutYouResponse aboutYouResponse) {
        if(!getIntent().getBooleanExtra("isFromEditProfile",false))
            launchActivity(AboutYouActivity.this,HomeActivity.class);
        finish();

    }
}
