package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model;

import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;

/**
 * Created by daffodil on 17/2/17.
 */

public class TribesMemberSearchResponse {

    private TribesMembers[] result;

    private String searchFor;

    public TribesMembers[] getResult ()
    {
        return result;
    }

    public void setResult (TribesMembers[] result)
    {
        this.result = result;
    }

    public String getSearchFor ()
    {
        return searchFor;
    }

    public void setSearchFor (String searchFor)
    {
        this.searchFor = searchFor;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", searchFor = "+searchFor+"]";
    }
}
