
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Metadata {

    @SerializedName("smsVerified")
    @Expose
    private String smsVerified;

    public String getSmsVerified() {
        return smsVerified;
    }

    public void setSmsVerified(String smsVerified) {
        this.smsVerified = smsVerified;
    }

}
