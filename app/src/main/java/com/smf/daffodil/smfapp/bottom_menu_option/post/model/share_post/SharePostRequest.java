package com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post;

/**
 * Created by daffodil on 20/2/17.
 */

public class SharePostRequest {

    private String post_id;
    private String caption;
    private String[] cordinates;

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String[] getCordinates() {
        return cordinates;
    }

    public void setCordinates(String[] cordinates) {
        this.cordinates = cordinates;
    }
}
