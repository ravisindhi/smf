package com.smf.daffodil.smfapp.slide_menu_options.my_profile.presenter;

/**
 * Created by daffodil on 25/2/17.
 */

public interface IMyProfilePresenter {

    void doProfilePrivate(String type);
}
