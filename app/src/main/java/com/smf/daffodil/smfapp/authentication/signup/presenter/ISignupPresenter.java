package com.smf.daffodil.smfapp.authentication.signup.presenter;

import com.smf.daffodil.smfapp.authentication.signup.model.SignupRequest;

/**
 * Created by daffodil on 25/1/17.
 */

public interface ISignupPresenter {

    public void doSignup(SignupRequest signupRequest);
}
