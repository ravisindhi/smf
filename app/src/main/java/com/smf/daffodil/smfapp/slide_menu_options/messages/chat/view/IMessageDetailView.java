package com.smf.daffodil.smfapp.slide_menu_options.messages.chat.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.response.MessageDetailResponse;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public interface IMessageDetailView extends IBaseView {

    void onGetMessageComment(MessageDetailResponse messageDetailResponse);
    void onError(int errorcode, String message);
    void scrollToBottom();
}
