package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response;

/**
 * Created by daffodil on 27/2/17.
 */

public class SenderDetail {
    private String _id;

    private MessageProfile profile;

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public MessageProfile getProfile ()
    {
        return profile;
    }

    public void setProfile (MessageProfile profile)
    {
        this.profile = profile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [_id = "+_id+", profile = "+profile+"]";
    }
}
