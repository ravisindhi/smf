package com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberData;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberProfile;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.presenter.IUserProfileCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.presenter.IUserProfilePresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.view.IUserProfileView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.slide_menu_options.history.HistoryActivity;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.MessagesActivity;
import com.smf.daffodil.smfapp.utils.DateTimeAgo;
import com.smf.daffodil.smfapp.utils.DebouncedOnClickListener;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.BlurTransformation;

public class UserProfileActivity extends BaseActivity implements IUserProfileView{

    SessionManager sessionManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.block_button)
    LinearLayout linearLayoutBlockMember;

    @BindView(R.id.message_button)
    LinearLayout linearLayoutMessageMemberButton;

    @BindView(R.id.follow_button)
    LinearLayout linearLayoutFollow;

    @BindView(R.id.blur_image_view)
    ImageView blurImageView;

    @BindView(R.id.rounded_image_view)
    ImageView rounded_image_view;

    @BindView(R.id.txt_name)
    TextView textViewName;

    @BindView(R.id.txt_handle)
    TextView textViewHandle;



    @BindView(R.id.txt_dob)
    TextView textViewDOB;

    @BindView(R.id.txt_gender)
    TextView textViewGender;

    @BindView(R.id.txt_relation_status)
    TextView textViewRelationStatus;

    @BindView(R.id.txt_email)
    TextView textViewEmail;

    @BindView(R.id.txt_default_address)
    TextView textViewDefaultAddress;

    @BindView(R.id.follow_txtview)
    TextView textViewFollow;

    @BindView(R.id.btn_history)
    Button materialRippleLayout;

    private MemberProfile memberProfile;
    private IUserProfilePresenter iUserProfilePresenter;
    private String user_id;
    private int clickFlag=0;
    private String email="";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ButterKnife.bind(this);

        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("USER PROFILE", true, false);

        sessionManager = new SessionManager(this);
        iUserProfilePresenter=new IUserProfileCompl(this,UserProfileActivity.this);
        memberProfile= (MemberProfile) getIntent().getSerializableExtra("member_data");
        textViewFollow.setText(getIntent().getStringExtra("follow_text"));
        user_id=getIntent().getStringExtra("user_id");
        initViews(memberProfile);



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @OnClick(R.id.message_button)
    public void messageOpen(){

        if(textViewFollow.getText().toString().trim().equalsIgnoreCase("following")){
            Intent i=new Intent(UserProfileActivity.this, MessagesActivity.class);
            i.putExtra("isFromMember",true);
            i.putExtra("userid",user_id);
            startActivity(i);
        }
    }

    @OnClick(R.id.block_button)
    public void blockUser()
    {
        iUserProfilePresenter.doBlockMember(user_id);
    }

    private void initViews(MemberProfile  profile)
    {
        textViewName.setText(profile.getFirstName() + " " + profile.getLastName());
        textViewHandle.setText("@"+profile.getHandle());

        if (profile.getDob() != null && profile.getDob().length() > 0) {

            String date = profile.getDob();
            String day = date.substring(0, 2);
            String month = date.substring(2, 4);
            String year = date.substring(4);
            textViewDOB.setText(day + "/" + month + "/" + year);
        } else {
            textViewDOB.setText("NA");
        }

        textViewGender.setText(profile.getGender());
        textViewEmail.setText(getIntent().getStringExtra("email"));
        textViewDefaultAddress.setText(profile.getCity() + " " + profile.getState());
        textViewRelationStatus.setText(profile.getRelationship());

        ImageUtils.setprofile(rounded_image_view,profile.getProfileImage(),this);
        //Glide.with(this).load(profile.getProfileImage()).error(R.drawable.dummy_rounded).into(rounded_image_view);
        Glide.with(this).load(profile.getProfileImage()).error(R.drawable.dummy_full).bitmapTransform(new BlurTransformation(this)).into(blurImageView);

    }


    @Override
    public void onError(int errorCode, String message) {
        switch (errorCode) {
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(), message, UserProfileActivity.this);
                break;
        }
    }

    @Override
    public void onSuccess() {
        Toast.makeText(UserProfileActivity.this,"Member blocked !!",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        clickFlag=0;
    }

    @OnClick(R.id.btn_history)
    public void onHistory()
    {
        if(clickFlag==0){
            clickFlag++;
            Intent intent=new Intent(UserProfileActivity.this, HistoryActivity.class);
            intent.putExtra("user_id",user_id);
            startActivity(intent);

        }


    }


}
