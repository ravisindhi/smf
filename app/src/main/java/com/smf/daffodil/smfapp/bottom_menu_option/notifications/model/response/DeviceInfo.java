
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceInfo {

    @SerializedName("android")
    @Expose
    private List<Object> android = null;
    @SerializedName("ios")
    @Expose
    private List<Object> ios = null;

    public List<Object> getAndroid() {
        return android;
    }

    public void setAndroid(List<Object> android) {
        this.android = android;
    }

    public List<Object> getIos() {
        return ios;
    }

    public void setIos(List<Object> ios) {
        this.ios = ios;
    }

}
