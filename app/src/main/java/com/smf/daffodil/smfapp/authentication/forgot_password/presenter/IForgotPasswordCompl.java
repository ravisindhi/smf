package com.smf.daffodil.smfapp.authentication.forgot_password.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordRequest;
import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordResponse;
import com.smf.daffodil.smfapp.authentication.forgot_password.view.IForgotPasswordView;
import com.smf.daffodil.smfapp.authentication.login.model.request.LoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 24/1/17.
 */

public class IForgotPasswordCompl implements IForgotPasswordPresenter{

    IForgotPasswordView iForgotPasswordView;
    Context _context;

    public IForgotPasswordCompl(Context _context,IForgotPasswordView iForgotPasswordView)
    {
        this._context=_context;
        this.iForgotPasswordView=iForgotPasswordView;
    }

    @Override
    public void doForgotPassword(ForgotPasswordRequest request) {

        if(validateForgotPasswordForm(request))
        {
            startForgotService(request);
        }

    }

    private boolean validateForgotPasswordForm(ForgotPasswordRequest _mLoginResquest)
    {
        boolean _mValidModel = true;
        if (_mLoginResquest.getEmail() == null || _mLoginResquest.getEmail().trim().length() <= 0) {
            _mValidModel = false;
            iForgotPasswordView.onErrorHandleWithView(ILoginView.EMAIL_ERROR, "");
        }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(_mLoginResquest.getEmail()).matches()){
            _mValidModel = false;
            iForgotPasswordView.onErrorHandleWithSnakbar(ILoginView.EMAIL_ERROR, "");
        }

        return _mValidModel;
    }

    private void startForgotService(ForgotPasswordRequest forgotPasswordRequest)
    {iForgotPasswordView.showLoader(_context.getString(R.string.please_wait_message));
        if(iForgotPasswordView.onCheckNetworkConnection())
        {


            SMFApplication.getRestClient().doForgotPassword(new StringRequestCallback<ForgotPasswordResponse>() {



                @Override
                public void onRestResponse(Exception e, ForgotPasswordResponse result) {
                    if(e==null && result !=null)
                    {
                        iForgotPasswordView.onSuccess(result);
                    }
                    iForgotPasswordView.hideLoader();
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iForgotPasswordView.hideLoader();
                    e.printStackTrace();
                    iForgotPasswordView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR,result.getMessage());

                }


            },forgotPasswordRequest);
        }else
        { iForgotPasswordView.hideLoader();
            iForgotPasswordView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR,_context.getString(R.string.internet_error_message));
        }
    }



}
