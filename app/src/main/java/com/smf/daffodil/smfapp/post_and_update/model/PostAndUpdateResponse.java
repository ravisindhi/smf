package com.smf.daffodil.smfapp.post_and_update.model;

/**
 * Created by daffodil on 4/2/17.
 */

public class PostAndUpdateResponse {

    private String updatedAt;

    private String content;

    private String author;

    private String[] likedBy;

    private String[] dislikedBy;

    private String _id;

    private PostAndUpdateLocation location;

    private String createdAt;

    private String hidden;

    private String __v;

    private String[] peaceBy;

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getAuthor ()
    {
        return author;
    }

    public void setAuthor (String author)
    {
        this.author = author;
    }

    public String[] getLikedBy ()
    {
        return likedBy;
    }

    public void setLikedBy (String[] likedBy)
    {
        this.likedBy = likedBy;
    }

    public String[] getDislikedBy ()
    {
        return dislikedBy;
    }

    public void setDislikedBy (String[] dislikedBy)
    {
        this.dislikedBy = dislikedBy;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public PostAndUpdateLocation getLocation ()
    {
        return location;
    }

    public void setLocation (PostAndUpdateLocation location)
    {
        this.location = location;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getHidden ()
    {
        return hidden;
    }

    public void setHidden (String hidden)
    {
        this.hidden = hidden;
    }

    public String get__v ()
    {
        return __v;
    }

    public void set__v (String __v)
    {
        this.__v = __v;
    }

    public String[] getPeaceBy ()
    {
        return peaceBy;
    }

    public void setPeaceBy (String[] peaceBy)
    {
        this.peaceBy = peaceBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updatedAt = "+updatedAt+", content = "+content+", author = "+author+", likedBy = "+likedBy+", dislikedBy = "+dislikedBy+", _id = "+_id+", location = "+location+", createdAt = "+createdAt+", hidden = "+hidden+", __v = "+__v+", peaceBy = "+peaceBy+"]";
    }
}
