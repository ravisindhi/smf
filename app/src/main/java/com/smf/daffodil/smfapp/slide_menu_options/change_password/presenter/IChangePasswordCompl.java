package com.smf.daffodil.smfapp.slide_menu_options.change_password.presenter;

import android.content.Context;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordRequest;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordResponse;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.view.IChangePasswordView;

/**
 * Created by daffodil on 1/2/17.
 */

public class IChangePasswordCompl implements IChangePasswordPresenter {

    IChangePasswordView iChangePasswordView;
    Context context;

    public IChangePasswordCompl(IChangePasswordView iChangePasswordView, Context context) {
        this.iChangePasswordView = iChangePasswordView;
        this.context = context;
    }

    @Override
    public void doChangePassword(ChangePasswordRequest request) {

        if (validateChangePasswordForm(request)) {
            startChangePasswordService(request);
        }

    }


    private boolean validateChangePasswordForm(ChangePasswordRequest changePasswordRequest) {
        boolean _mValidModel = true;
        if (changePasswordRequest.getOld_password() == null || changePasswordRequest.getOld_password().trim().length() <= 0) {
            _mValidModel = false;
            iChangePasswordView.onError(IChangePasswordView.OLD_PASSWORD_ERROR);
        } else if (changePasswordRequest.getPassword() == null || changePasswordRequest.getPassword().trim().length() <= 0) {
            _mValidModel = false;
            iChangePasswordView.onError(IChangePasswordView.NEW_PASSWORD_ERROR);
        } else if (changePasswordRequest.getConfirm_password() == null || changePasswordRequest.getConfirm_password().trim().length() <= 0) {
            _mValidModel = false;
            iChangePasswordView.onError(IChangePasswordView.CONFIRM_PASSWORD_ERROR);
        } else if (!changePasswordRequest.getConfirm_password().equals(changePasswordRequest.getPassword())) {
            _mValidModel = false;
            iChangePasswordView.onError(IChangePasswordView.PASSWORD_MISMATCH);
        }
        return _mValidModel;
    }

    private void startChangePasswordService(ChangePasswordRequest request) {

        iChangePasswordView.showLoader(context.getString(R.string.please_wait_message));
        if (iChangePasswordView.onCheckNetworkConnection()) {


            SMFApplication.getRestClient().doChangePassword(new StringRequestCallback<ChangePasswordResponse>() {
                @Override
                public void onRestResponse(Exception e, ChangePasswordResponse result) {
                    if (e == null && result != null) {

                        iChangePasswordView.onSuccess(result);
                        iChangePasswordView.hideLoader();
                    }
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iChangePasswordView.hideLoader();
                    e.printStackTrace();
                    Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    // iChangePasswordView.onErrorWithMessage(IBaseView.NETWORK_ERROR,result.getMessage());
                }
            }, request);
        } else {
            iChangePasswordView.hideLoader();
            iChangePasswordView.onErrorWithMessage(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }


}
