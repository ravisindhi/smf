package com.smf.daffodil.smfapp.about_you.communication;

import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;

/**
 * Created by daffodil on 25/1/17.
 */

public interface ActivityCommunicator {
    public void passDataToActivity(AboutYouRequest aboutYouRequest);
    public AboutYouRequest getDataToActivity();
}
