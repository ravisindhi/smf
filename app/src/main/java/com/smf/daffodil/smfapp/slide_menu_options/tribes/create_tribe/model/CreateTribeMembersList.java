package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model;

import java.io.Serializable;

/**
 * Created by daffodil on 11/2/17.
 */

public class CreateTribeMembersList implements Serializable{

    private String id;
    private String status;
    private String handle;

    public CreateTribeMembersList(String id, String status) {
        this.id = id;
        this.status = status;
    }

    public CreateTribeMembersList() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }
}
