package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 15/2/17.
 */

public class TribeMemberSearchAdapter extends ArrayAdapter<TribesMembers> {


    private List<TribesMembers> list=new ArrayList<>();
    private Context context;
    private AddMemberLisetner lisetner;



    public TribeMemberSearchAdapter(Context context, int resource, List<TribesMembers> list, AddMemberLisetner lisetner) {
        super(context, resource, list);

        this.list.addAll(list);
        this.context = context;
        this.lisetner = lisetner;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public TribesMembers getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        MemberViewHolder memberViewHolder;
        if(view==null){
            LayoutInflater inflater=((Activity)context).getLayoutInflater();
            view=inflater.inflate(R.layout.search_tribe_member_item,viewGroup,false);
            memberViewHolder=new MemberViewHolder(view);
            view.setTag(memberViewHolder);
        }else
        {
            memberViewHolder= (MemberViewHolder) view.getTag();
        }

        ImageUtils.setprofile(memberViewHolder.imageViewMemberImage,getItem(i).getProfile().getProfileImage(),context);
        memberViewHolder.textViewMemberName.setText("@"+getItem(i).getProfile().getHandle());
        memberViewHolder.buttonAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lisetner.addMember(getItem(i).get_id());
            }
        });

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    public class MemberViewHolder{

        @BindView(R.id.member_image)
        ImageView imageViewMemberImage;

        @BindView(R.id.member_name)
        TextView textViewMemberName;

        @BindView(R.id.button_add_member)
        Button buttonAddMember;


        public MemberViewHolder(View view)
        {
            ButterKnife.bind(this,view);
        }
    }

    public interface AddMemberLisetner
    {
        void addMember(String userid);
    }
}
