package com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.request;

/**
 * Created by daffolap-164 on 21/2/17.
 */

public class MessageDetailRequest {

    private String limit;
    private String skip;
    private String messageId;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }
}
