package com.smf.daffodil.smfapp.authentication.login.model.response;

import java.io.Serializable;

/**
 * Created by daffodil on 30/1/17.
 */

public class Connections implements Serializable{

    private String[] following;

    private String[] connected;

    private String[] blocked;

    private String[] favorites;

    public String[] getFollowing ()
    {
        return following;
    }

    public void setFollowing (String[] following)
    {
        this.following = following;
    }

    public String[] getConnected ()
    {
        return connected;
    }

    public void setConnected (String[] connected)
    {
        this.connected = connected;
    }

    public String[] getBlocked ()
    {
        return blocked;
    }

    public void setBlocked (String[] blocked)
    {
        this.blocked = blocked;
    }

    public String[] getFavorites ()
    {
        return favorites;
    }

    public void setFavorites (String[] favorites)
    {
        this.favorites = favorites;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [following = "+following+", connected = "+connected+", blocked = "+blocked+", favorites = "+favorites+"]";
    }
}
