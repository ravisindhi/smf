package com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.model.CommentsResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.common.view.IActionView;

/**
 * Created by daffodil on 20/2/17.
 */

public interface ICommentView extends IBaseView{

    void onGetAllComments(CommentsResponse[] response);
    void onError(int error_code,String message);
    void onCommentAdded(PostActionResponse response);
    void onUpdatePostAction(int position, IActionView.PostActions actionType);
}
