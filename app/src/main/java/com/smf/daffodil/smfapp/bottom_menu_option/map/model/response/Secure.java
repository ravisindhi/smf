
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Secure {

    @SerializedName("passwordResetToken")
    @Expose
    private String passwordResetToken;
    @SerializedName("numberVerify")
    @Expose
    private String numberVerify;
    @SerializedName("smsCode")
    @Expose
    private String smsCode;

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public String getNumberVerify() {
        return numberVerify;
    }

    public void setNumberVerify(String numberVerify) {
        this.numberVerify = numberVerify;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

}
