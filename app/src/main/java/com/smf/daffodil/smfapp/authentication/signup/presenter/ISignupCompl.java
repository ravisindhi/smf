package com.smf.daffodil.smfapp.authentication.signup.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.request.LoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.authentication.signup.model.SignupRequest;
import com.smf.daffodil.smfapp.authentication.signup.view.ISignupView;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.RequestCallback;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daffodil on 25/1/17.
 */

public class ISignupCompl implements ISignupPresenter {

    ISignupView _mSignupView;
    private Context _context;

    public ISignupCompl(Context _context, ISignupView _mSignupView) {
        this._context = _context;
        this._mSignupView = _mSignupView;
    }


    @Override
    public void doSignup(SignupRequest signupRequest) {
        if(validateSignupForm(signupRequest))
        {
            startSignupService(signupRequest);
        }

    }

    private boolean validateSignupForm(SignupRequest _mSignupRequest) {
        boolean _mValidModel = true;
        if (_mSignupRequest.getFirst_name() == null || _mSignupRequest.getFirst_name().trim().length() <= 0) {
            _mValidModel = false;
            _mSignupView.onErrorHandleWithView(ISignupView.FIRST_NAME_ERROR, "");
        } else if (_mSignupRequest.getLast_name() == null || _mSignupRequest.getLast_name().trim().length() <= 0) {
            _mValidModel = false;
            _mSignupView.onErrorHandleWithView(ISignupView.LAST_NAME_ERROR, "");
        } else if (_mSignupRequest.getEmail() == null || _mSignupRequest.getEmail().trim().length() <= 0) {
            _mValidModel = false;
            _mSignupView.onErrorHandleWithView(ISignupView.EMAIL_ERROR, "");
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(_mSignupRequest.getEmail()).matches()) {
            _mValidModel = false;
            _mSignupView.onErrorHandleWithSnakbar(ISignupView.EMAIL_ERROR, "");
        } else if (_mSignupRequest.getPassword() == null || _mSignupRequest.getPassword().trim().length() <= 0) {
            _mValidModel = false;
            _mSignupView.onErrorHandleWithView(ISignupView.PASSWORD_ERROR, "");
        } else if (_mSignupRequest.getConfirm_pasword() == null || _mSignupRequest.getConfirm_pasword().trim().length() <= 0) {
            _mValidModel = false;
            _mSignupView.onErrorHandleWithView(ISignupView.CONFIRM_PASSWORD_ERROR, "");
        } else if (!_mSignupRequest.getPassword().equals(_mSignupRequest.getConfirm_pasword())) {
            _mValidModel = false;
            _mSignupView.onErrorHandleWithSnakbar(ISignupView.PASSWORD_MISMATCH_ERROR, "");
        }
        return _mValidModel;
    }


    private void startSignupService(final SignupRequest loginRequest)
    {

        _mSignupView.showLoader("Please wait...");
        if (_mSignupView.onCheckNetworkConnection()) {



            String json = new Gson().toJson(loginRequest);
            final JSONObject _mRequest;
            try {
                _mRequest = new JSONObject(json);
                _mRequest.remove("confirm_pasword");
                SMFApplication.getRestClient().doSignup(new RequestCallback<LoginResponse>() {
                    @Override
                    public void onRestResponse(Exception e, LoginResponse result) {
                        if(_mSignupView.onSaveUserData(result))
                        {

                            LoginRequest request=new LoginRequest();
                            request.setEmail(loginRequest.getEmail());
                            request.setPassword(loginRequest.getPassword());
                            request.setInfo(loginRequest.getDeviceInfo());
                            startLoginService(request);

                        }
                        _mSignupView.hideLoader();
                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        _mSignupView.hideLoader();
                        e.printStackTrace();
                        _mSignupView.onErrorHandleWithSnakbar(IBaseView.SERVER_ERROR,result.getMessage());

                    }
                }, _mRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            _mSignupView.hideLoader();
            _mSignupView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR, _context.getString(R.string.internet_error_message));

        }


    }

    private void startLoginService(LoginRequest loginRequest) {

        _mSignupView.showLoader(_context.getString(R.string.please_wait_message));
        if (_mSignupView.onCheckNetworkConnection()) {

            String json = new Gson().toJson(loginRequest);
            final JSONObject _mRequest;
            try {
                _mRequest = new JSONObject(json);
                SMFApplication.getRestClient().doLogin(new RequestCallback<LoginResponse>() {
                    @Override
                    public void onRestResponse(Exception e, LoginResponse result) {
                        if (_mSignupView.onSaveUserData(result)) {
                            _mSignupView.onSuccess(result);
                        }
                        _mSignupView.hideLoader();
                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        _mSignupView.hideLoader();
                        e.printStackTrace();
                        _mSignupView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR, result.getMessage());

                    }
                }, _mRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            _mSignupView.hideLoader();
            _mSignupView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR, _context.getString(R.string.internet_error_message));

        }

    }


}
