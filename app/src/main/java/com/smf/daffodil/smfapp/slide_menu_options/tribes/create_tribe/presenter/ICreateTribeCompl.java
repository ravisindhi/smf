package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.model.ImageUploadResponse;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.FilePart;
import com.smf.daffodil.smfapp.rest_api.RequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.CreateTribeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.view.ICreateTribeView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by daffodil on 11/2/17.
 */

public class ICreateTribeCompl implements ICreateTribePresenter {

    private static final String IMAGE_TYPE = "groupsImages";
    private static final int CREATE_TYPE = 1;
    private static final int UPDATE_TYPE = 2;
    private Context context;
    private ICreateTribeView iCreateTribeView;
    private SessionManager sessionManager;

    public ICreateTribeCompl(Context context, ICreateTribeView iCreateTribeView) {
        this.context = context;
        this.iCreateTribeView = iCreateTribeView;
        sessionManager = new SessionManager(context);
    }


    @Override
    public void createTribe(CreateTribeRequest request) {

        if (validateCreateTribeForm(request)) {
            uploadImageToS3(request, CREATE_TYPE);
        }

    }

    @Override
    public void updateTribe(CreateTribeRequest request) {
        if (validateEditTribeForm(request)) {

            if (request.getImageUri() != null)
                uploadImageToS3(request, UPDATE_TYPE);
            else
                updateTribeAfterImage(request);
        }
    }

    private boolean validateCreateTribeForm(CreateTribeRequest request) {
        boolean _mValidModel = true;
        if (request.getImageUri() == null) {
            _mValidModel = false;
            iCreateTribeView.onError(ICreateTribeView.TRIBE_IMAGE_ERROR, "");
        } else if (request.getName() == null || request.getName().trim().length() <= 0) {
            _mValidModel = false;
            iCreateTribeView.onError(ICreateTribeView.TRIBE_NAME_ERROR, "");
        } else if (request.getDesc() == null || request.getDesc().trim().length() <= 0) {
            _mValidModel = false;
            iCreateTribeView.onError(ICreateTribeView.TRIBE_DESCRIPTION_ERROR, "");
        }

        return _mValidModel;
    }

    private boolean validateEditTribeForm(CreateTribeRequest request) {
        boolean _mValidModel = true;

        if (CreateTribeActivity.isProfileImageChanged) {
            if (request.getImageUri() == null) {
                _mValidModel = false;
                iCreateTribeView.onError(ICreateTribeView.TRIBE_IMAGE_ERROR, "");
            }
        }
        else{
            if (request.getImageUrl() == null || request.getImageUrl().length() <= 0) {
                _mValidModel = false;
                iCreateTribeView.onError(ICreateTribeView.TRIBE_IMAGE_ERROR, "");
            }
        }
        if (request.getName() == null || request.getName().trim().length() <= 0) {
            _mValidModel = false;
            iCreateTribeView.onError(ICreateTribeView.TRIBE_NAME_ERROR, "");
        } else if (request.getDesc() == null || request.getDesc().trim().length() <= 0) {
            _mValidModel = false;
            iCreateTribeView.onError(ICreateTribeView.TRIBE_DESCRIPTION_ERROR, "");
        }

        return _mValidModel;
    }

    private void uploadImageToS3(final CreateTribeRequest request, final int type) {
        /*Update Profile Image if changed by user*/
        iCreateTribeView.showLoader(context.getString(R.string.update_profile_image_msg));
        if (iCreateTribeView.onCheckNetworkConnection()) {


            SMFApplication.getRestClient().uploadImageRequest(new RequestCallback<ImageUploadResponse>() {
                @Override
                public void onRestResponse(Exception e, ImageUploadResponse result) {
                    if (e == null && result != null) {
                        iCreateTribeView.hideLoader();
                        request.setImageUrl(result.getFilePath());

                        if (type == CREATE_TYPE)
                            createTribeAfterImage(request);
                        else
                            updateTribeAfterImage(request);
                        //Toast.makeText(context,"image upload success",Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iCreateTribeView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    iCreateTribeView.hideLoader();

                }
            }, new FilePart(new File(request.getImageUri().getPath()), "image/jpg"), IMAGE_TYPE, sessionManager.getUserData().get_id());

        } else {
            iCreateTribeView.hideLoader();
            iCreateTribeView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    private void createTribeAfterImage(CreateTribeRequest request) {

        iCreateTribeView.showLoader(context.getString(R.string.please_wait_message));
        if (iCreateTribeView.onCheckNetworkConnection()) {

            String json = new Gson().toJson(request);
            final JSONObject _mRequest;
            try {
                _mRequest = new JSONObject(json);
                _mRequest.remove("imageUri");
                _mRequest.remove("groupId");
                SMFApplication.getRestClient().createTribe(new RequestCallback<CreateTribeResponse>() {
                    @Override
                    public void onRestResponse(Exception e, CreateTribeResponse result) {
                        iCreateTribeView.hideLoader();
                        iCreateTribeView.onCreateTribeSuccess(result);
                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        iCreateTribeView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                        iCreateTribeView.hideLoader();
                    }
                }, _mRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            iCreateTribeView.hideLoader();
            iCreateTribeView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }
    }


    private void updateTribeAfterImage(CreateTribeRequest request) {

        iCreateTribeView.showLoader(context.getString(R.string.please_wait_message));

        if (iCreateTribeView.onCheckNetworkConnection()) {

            String json = new Gson().toJson(request);
            final JSONObject _mRequest;
            try {
                _mRequest = new JSONObject(json);
                _mRequest.remove("imageUri");
                SMFApplication.getRestClient().updateTribe(new RequestCallback<CreateTribeResponse>() {
                    @Override
                    public void onRestResponse(Exception e, CreateTribeResponse result) {
                        iCreateTribeView.hideLoader();
                        iCreateTribeView.onCreateTribeSuccess(result);
                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        iCreateTribeView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                        iCreateTribeView.hideLoader();
                    }
                }, _mRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            iCreateTribeView.hideLoader();
            iCreateTribeView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }
    }
}
