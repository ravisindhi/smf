package com.smf.daffodil.smfapp.authentication.login.model.request;

/**
 * Created by daffodil on 2/2/17.
 */

public class FacebookLoginRequest {
    private String id;

    private String first_name;

    private String expiresAt;

    private String accessToken;

    private String email;

    private String locale;

    private String link;

    private String name;

    private String last_name;

    private String gender;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getExpiresAt ()
    {
        return expiresAt;
    }

    public void setExpiresAt (String expiresAt)
    {
        this.expiresAt = expiresAt;
    }

    public String getAccessToken ()
    {
        return accessToken;
    }

    public void setAccessToken (String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getLocale ()
    {
        return locale;
    }

    public void setLocale (String locale)
    {
        this.locale = locale;
    }

    public String getLink ()
    {
        return link;
    }

    public void setLink (String link)
    {
        this.link = link;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", first_name = "+first_name+", expiresAt = "+expiresAt+", accessToken = "+accessToken+", email = "+email+", locale = "+locale+", link = "+link+", name = "+name+", last_name = "+last_name+", gender = "+gender+"]";
    }

}

