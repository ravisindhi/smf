package com.smf.daffodil.smfapp.common.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.model.BlockUserResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.common.view.IActionView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 22/2/17.
 */

public class IActionCompl implements IActionPresenter{

    private Context context;
    private IActionView iActionView;

    public IActionCompl(Context context, IActionView iActionView) {
        this.context = context;
        this.iActionView = iActionView;
    }

    @Override
    public void updatePost(final UpdatePostRequest request, final int pos) {


        if (iActionView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().updatePost(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {

                        switch (request.getAction())
                        {
                            case "like":
                                iActionView.onUpdatePostAction(pos, IActionView.PostActions.like);
                                break;
                            case "dislike":
                                iActionView.onUpdatePostAction(pos, IActionView.PostActions.dislike);
                                break;
                            case "peace":
                                iActionView.onUpdatePostAction(pos, IActionView.PostActions.peace);
                                break;
                        }
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iActionView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iActionView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, request);
        } else {
            iActionView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void updateGroupPost(final UpdatePostRequest request, final int pos) {
        if (iActionView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().group_updatePost(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {

                        switch (request.getAction())
                        {
                            case "like":
                                iActionView.onUpdatePostAction(pos, IActionView.PostActions.like);
                                break;
                            case "dislike":
                                iActionView.onUpdatePostAction(pos, IActionView.PostActions.dislike);
                                break;
                            case "peace":
                                iActionView.onUpdatePostAction(pos, IActionView.PostActions.peace);
                                break;
                        }
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iActionView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iActionView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, request);
        } else {
            iActionView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void doHidePost(String postid, final int position) {
        if (iActionView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().hidePost(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {
                        iActionView.onHidePost(position);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iActionView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iActionView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, postid);
        } else {
            iActionView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void doPostReport(String postid) {
        if (iActionView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().reportPost(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {
                        iActionView.onReportPost();
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iActionView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iActionView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, postid);
        } else {
            iActionView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void doBlockUser(String userid, final String handle) {
        if (iActionView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().blockUser(new StringRequestCallback<BlockUserResponse>() {
                @Override
                public void onRestResponse(Exception e, BlockUserResponse result) {
                    if (e == null && result != null) {
                        iActionView.onBlockUser(handle);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iActionView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iActionView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, userid);
        } else {
            iActionView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void doSharePost(SharePostRequest request) {
        if (iActionView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().sharePost(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {
                        iActionView.onSharePost(result);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iActionView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iActionView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, request);
        } else {
            iActionView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void doAddComment(String postid, String comment_txt, final int position,String from) {
        if (iActionView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().addComment(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {
                        iActionView.onAddComment(position);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iActionView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iActionView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, postid,comment_txt,from);
        } else {
            iActionView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }



}
