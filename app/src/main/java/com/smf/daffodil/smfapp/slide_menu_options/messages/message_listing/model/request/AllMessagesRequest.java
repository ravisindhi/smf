package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.request;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public class AllMessagesRequest {

    private String limit;
    private String skip;

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }
}
