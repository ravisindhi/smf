package com.smf.daffodil.smfapp.authentication.signup.view;

import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;

/**
 * Created by daffodil on 25/1/17.
 */

public interface ISignupView extends IBaseView{

    public static int EMAIL_ERROR=3;
    public static int PASSWORD_ERROR=4;
    public static int FIRST_NAME_ERROR=1;
    public static int LAST_NAME_ERROR=2;
    public static int CONFIRM_PASSWORD_ERROR=5;
    public static int PASSWORD_MISMATCH_ERROR=6;


    void onErrorHandleWithView(int errorCode,String _errorMsg);
    void onErrorHandleWithSnakbar(int errorCode,String _errorMsg);
    void onSuccess(LoginResponse response);
    boolean onSaveUserData(LoginResponse response);
}
