package com.smf.daffodil.smfapp.post_and_update;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.fragments.adapters.MyCustomArrayAdapter;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.post_and_update.model.IPostAndUpdateRequest;
import com.smf.daffodil.smfapp.post_and_update.model.PostAndUpdateLocation;
import com.smf.daffodil.smfapp.post_and_update.model.PostAndUpdateResponse;
import com.smf.daffodil.smfapp.post_and_update.presenter.IPostAndPresenterCompl;
import com.smf.daffodil.smfapp.post_and_update.presenter.IPostAndUpdatePresenter;
import com.smf.daffodil.smfapp.post_and_update.view.IPostAndUpdateView;
import com.smf.daffodil.smfapp.utils.AppUtils;
import com.smf.daffodil.smfapp.utils.ImageCompression;
import com.smf.daffodil.smfapp.utils.ImageUtils;
import com.smf.daffodil.smfapp.utils.VideoUtils;
import com.smf.daffodil.smfapp.view.FullscreenVideoLayout;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;

public class PostAndUpdateActivity extends BaseActivity implements IPostAndUpdateView, View.OnTouchListener, AdapterView.OnItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.map_spinner_location)
    EditText editTextLocationSpinner;

    @BindView(R.id.location_spinner)
    Spinner spinnerLocation;

    @BindView(R.id.location_select_layout)
    LinearLayout linearLayoutLocationSelectLayout;

    @BindView(R.id.rounded_image_view)
    ImageView rounded_image_view;

    @BindView(R.id.txt_name)
    TextView textViewName;

    @BindView(R.id.whts_mind_et)
    EditText editTextWhatInUrMind;

    @BindView(R.id.btn_post_update)
    Button buttonPostAndUpdate;

    @BindView(R.id.select_image_button)
    LinearLayout buttonSelectImage;

    @BindView(R.id.select_video_button)
    LinearLayout buttonSelectVideo;

    @BindView(R.id.image_preview_layout)
    FrameLayout frameLayoutImagepreview;

    @BindView(R.id.image_preview)
    ImageView imageViewImagePreview;

    @BindView(R.id.image_preview_close)
    ImageView imageViewClose;

    @BindView(R.id.disappear_layout)
    LinearLayout disappear_layout;

    @BindView(R.id.eyes_image)
            ImageView imageViewEyes;


    // Location Spinner Drop down elements
    List<String> locationSpinnerList = new ArrayList<String>();
    ArrayAdapter<String> spinner_dataAdapter;

    SessionManager sessionManager;
    IPostAndUpdatePresenter iPostAndUpdatePresenter;
    private Uri imageUri = null;
    private Uri videoUri = null;
    private final int REQUEST_TAKE_GALLERY_VIDEO = 101;
    private IPostAndUpdateRequest request;
    private Boolean isHidden=false;
    private int selectedLocationIndex=0;
    ImageCompression imageCompression=new ImageCompression();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_and_update);
        sessionManager = new SessionManager(this);
        iPostAndUpdatePresenter = new IPostAndPresenterCompl(this, this);
        ButterKnife.bind(this);

        request=new IPostAndUpdateRequest();
        request.setHidden("false");
        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("POST AND UPDATE", true, false);
        initViews();
        addLocationSpinnerData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void initViews() {
        ImageUtils.setprofile(rounded_image_view,sessionManager.getUserData().getProfile().getProfileImage(),this);
        //Glide.with(this).load(sessionManager.getUserData().getProfile().getProfileImage()).error(R.drawable.dummy_rounded).into(rounded_image_view);
        textViewName.setText(sessionManager.getUserData().getProfile().getFirstName() + " " + sessionManager.getUserData().getProfile().getLastName());
    }

    private void addLocationSpinnerData() {
        locationSpinnerList.clear();
        locationSpinnerList.add("Local(" + sessionManager.getUserData().getProfile().getCity() + ")");
        if (sessionManager.getUserData().getProfile().getSavedLocations() != null) {
            for (int i = 0; i < sessionManager.getUserData().getProfile().getSavedLocations().size(); i++) {
                locationSpinnerList.add(sessionManager.getUserData().getProfile().getSavedLocations().get(i).getName());
            }
        }

        spinner_dataAdapter = new MyCustomArrayAdapter(this, R.layout.spinner_item, locationSpinnerList);
        spinnerLocation.setAdapter(spinner_dataAdapter);

        editTextLocationSpinner.setInputType(InputType.TYPE_NULL);
        editTextLocationSpinner.setOnTouchListener(this);
        spinnerLocation.setOnItemSelectedListener(this);

        /*Set spinner dropdown width to match parent*/
        spinnerLocation.setDropDownWidth((int) editTextLocationSpinner.getWidth());
        setupDropDownPopviewLength();

    }


    private void setupDropDownPopviewLength() {
        /*Set spinner dropdown width to match parent*/
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();
        spinnerLocation.setDropDownWidth((int) (config.screenWidthDp * dm.density));
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) editTextLocationSpinner.getLayoutParams();
        spinnerLocation.setDropDownWidth((int) (config.screenWidthDp * dm.density) - lp.leftMargin - lp.rightMargin);


    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        switch (view.getId()) {
            case R.id.map_spinner_location:
                spinnerLocation.performClick();
                break;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            editTextLocationSpinner.setText(spinnerLocation.getSelectedItem().toString());
           selectedLocationIndex=spinnerLocation.getSelectedItemPosition();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onError(int errorCode, String message) {
        switch (errorCode) {

            case IPostAndUpdateView.CONTENT_EMPTY_ERROR:
                showErrorMessage(getApplicationContext(), message, this);
                editTextWhatInUrMind.requestFocus();
                break;
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(), message, this);
                break;
        }

    }

    @Override
    public void onSuccess(PostAndUpdateResponse response) {
        Intent i=new Intent();
        setResult(101,i);
        finish();

    }

    @OnClick(R.id.btn_post_update)
    public void doPostAndUpdate() {
        request.setWhats_in_mind_txt(editTextWhatInUrMind.getText().toString().trim());
        if (imageUri != null) {
            request.setContent_uri(imageUri);
            request.setContent_type("image");
        }
        if (videoUri != null) {
            request.setContent_uri(videoUri);
            request.setContent_type("video");
        }

        if(selectedLocationIndex!=0)
        request.setCoordinates(new String[]{sessionManager.getUserData().getProfile().getSavedLocations().get(selectedLocationIndex-1).getLatLng()[0],
                sessionManager.getUserData().getProfile().getSavedLocations().get(selectedLocationIndex-1).getLatLng()[1]});

        if(getIntent().getBooleanExtra("isFromTribe",false))
            request.setGroupid(getIntent().getStringExtra("group_id"));

        iPostAndUpdatePresenter.doPostAndUpdate(request);
    }

    @OnClick(R.id.select_image_button)
    public void selectImage() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openImagePicker();
            }


            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    @OnClick(R.id.image_preview_close)
    public void removePreview()
    {
        imageUri=null;
        videoUri=null;
        request.setContent_uri(null);
        frameLayoutImagepreview.setVisibility(View.GONE);
    }

    @OnClick(R.id.select_video_button)
    public void selectVideo() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openVideoPicker();
            }


            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    private void openImagePicker() {
        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(this.getApplicationContext())
                .showCameraTile(false)
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected uri
                        videoUri=null;
                        imageUri = Uri.parse(ImageCompression.compressImage(PostAndUpdateActivity.this,uri.toString()));
                        frameLayoutImagepreview.setVisibility(View.VISIBLE);
                        imageViewImagePreview.setImageURI(imageUri);
                    }
                })
                .create();

        tedBottomPicker.show(getSupportFragmentManager());
    }


    private void openVideoPicker() {
        Intent intent = new Intent();
        intent.setType("video/mp4");

        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedImageUri = data.getData();
                imageUri=null;
                String originalPath = null;
                try {
                    originalPath = VideoUtils.getFilePath(PostAndUpdateActivity.this,selectedImageUri);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                videoUri=Uri.parse(originalPath);

                FullscreenVideoLayout fullscreenVideoLayout = new FullscreenVideoLayout(PostAndUpdateActivity.this, videoUri);

                frameLayoutImagepreview.setVisibility(View.VISIBLE);
                frameLayoutImagepreview.addView(fullscreenVideoLayout, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                fullscreenVideoLayout.setActivity(this);
                fullscreenVideoLayout.setShouldAutoplay(true);
                try {
                    fullscreenVideoLayout.setVideoURI(videoUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }



            }
        }
    }

    @OnClick(R.id.disappear_layout)
    public void toogleDisappear(){
        if(!isHidden)
        {
            imageViewEyes.setImageResource(R.drawable.ic_hide_post);
            request.setHidden("true");
            isHidden=true;

        }else
        {
            request.setHidden("false");
            imageViewEyes.setImageResource(R.drawable.ic_eyes);
            isHidden=false;

        }
    }


}
