package com.smf.daffodil.smfapp.bottom_menu_option.post.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.CommentsActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.PostData;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.SharePostActivity;
import com.smf.daffodil.smfapp.common.Constants;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.utils.DateTimeAgo;
import com.smf.daffodil.smfapp.utils.ImageUtils;
import com.smf.daffodil.smfapp.view.FullscreenVideoLayout;
import com.smf.daffodil.smfapp.view.OnLoadMoreListener;
import com.smf.daffodil.smfapp.view.TextViewExpandableAnimation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daffodil on 27/1/17.
 */

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int POST_TYPE_SHARE = 1;
    public static final int POST_TYPE_IMAGE = 2;
    public static final int POST_TYPE_TEXT = 3;
    public static final int POST_TYPE_VIDEO = 4;
    public static final int POST_TYPE_HIDDEN = 5;

    /*Load more data*/
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 8;
    private int lastVisibleItem, totalItemCount;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private PostActionsListener postActionsListener;

    private PopupWindow post_popup;

    private SessionManager sessionManager;

    List<GetPostDataResponse> postDatas;
    private Activity context;

    private int SCREEN_TYPE;

    public PostAdapter(List<GetPostDataResponse> list, Activity context, RecyclerView mRecyclerView, PostActionsListener postActionsListener, int screen_type) {
        postDatas = list;
        this.context = context;
        this.postActionsListener = postActionsListener;
        sessionManager = new SessionManager(context);
        this.SCREEN_TYPE = screen_type;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void loadMoreData(final List<GetPostDataResponse> dataResponses) {
        //Load more data for reyclerview
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("haint", "Load More 2");

                //Remove loading item
                postDatas.remove(postDatas.size() - 1);
                notifyItemRemoved(postDatas.size());

                postDatas.addAll(dataResponses);
                setLoaded();
                notifyDataSetChanged();

            }
        }, 5000);

    }

    public void loadMoreStart() {
        postDatas.add(null);
        notifyItemInserted(postDatas.size() - 1);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.home_card_main_view, parent, false);
            return new PostViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.load_more_layout, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        return postDatas.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return postDatas == null ? 0 : postDatas.size();
    }


    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostViewHolder) {
            //if(postDatas.get(position).getHidden())
            ((PostViewHolder) holder).setData(postDatas.get(position), position);


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }


    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.load_more_progressbar);
        }
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_name)
        TextView textViewName;

        @BindView(R.id.txt_time)
        TextView textViewTime;

        @BindView(R.id.txtview_post_content)
        TextViewExpandableAnimation textViewPostContent;

        @BindView(R.id.rounded_image_view)
        CircularImageView imageViewProfile;

        @BindView(R.id.post_image)
        ImageView imageViewPost;

        @BindView(R.id.video_thumbnail_image)
        ImageView imageViewVideoThmbnail;

        @BindView(R.id.txt_peace_by_count)
        TextView textViewPeaceByCount;

        @BindView(R.id.txt_comment_by_count)
        TextView textViewTxtCommentByCount;

        @BindView(R.id.emojiView)
        LinearLayout emojiLikeView;

        @BindView(R.id.hot_item)
        LinearLayout hot_view;

        @BindView(R.id.like_button)
        LinearLayout likeButton;

        @BindView(R.id.dislike_button)
        LinearLayout dislikeButton;

        @BindView(R.id.peace_button)
        LinearLayout peaceButton;

        @BindView(R.id.txt_liked)
        TextView textViewLikedText;

        @BindView(R.id.txt_disliked)
        TextView textViewDisLikedText;

        @BindView(R.id.txt_peace)
        TextView textViewPeace;

        @BindView(R.id.dot_btn)
        ImageButton imageButtonDot;

        @BindView(R.id.btn_report)
        LinearLayout ButtonReport;

        @BindView(R.id.share_post_layout)
        LinearLayout sharePostView;

        @BindView(R.id.owner_pic)
        ImageView imageViewOwnerPic;

        @BindView(R.id.txt_owner_name)
        TextView textViewOwnerName;

        @BindView(R.id.txt_owner_content)
        TextView textViewOwnerContent;

        @BindView(R.id.post_image_owner)
        ImageView imageViewOwnerPostImage;

        @BindView(R.id.txt_time_owner)
        TextView textViewOwnerPostTime;

        @BindView(R.id.et_comment)
        EditText editTextCommentBox;

        @BindView(R.id.post_comment_button)
        TextView textViewPostCommentButton;

        @BindView(R.id.txt_comments)
        TextView textViewComments;

        @BindView(R.id.txtview_post_content_normal)
        TextView textviewContentNormal;

        @BindView(R.id.post_image_view1)
        LinearLayout post_image_view;

        @BindView(R.id.post_owner_image_view)
        LinearLayout post_owner_image_view;



        @BindView(R.id.post_header_view)
        LinearLayout postHeaderView;

        @BindView(R.id.post_hidden_view)
        LinearLayout postHiddenView;

        @BindView(R.id.post_comment_view)
        LinearLayout post_comment_view;

        @BindView(R.id.post_write_comment_view)
        LinearLayout post_write_comment_view_view;

        private int position;
        GetPostDataResponse postData;


        LinearLayout post_share_view;
        //LinearLayout post_image_view;
        LinearLayout post_txt_view;
        FrameLayout post_video_view;


        public PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            post_share_view = (LinearLayout) itemView.findViewById(R.id.post_share_view);
            //post_image_view = (LinearLayout) itemView.findViewById(R.id.post_image_view);
            post_txt_view = (LinearLayout) itemView.findViewById(R.id.post_txt_view);
            post_video_view = (FrameLayout) itemView.findViewById(R.id.post_video_view);
        }

        public void setData(GetPostDataResponse data, int position) {
            this.postData = data;
            this.position = position;
            editTextCommentBox.setText("");
            if(postData.getAuthor().get_id().equals(sessionManager.getUserData().get_id()))
                ButtonReport.setVisibility(View.GONE);
            else
                ButtonReport.setVisibility(View.VISIBLE);

            if (postData.getHidden()) {
                postData.setPostType(POST_TYPE_HIDDEN);
            } else if (postData.getRealOwner() != null) {
                postData.setPostType(POST_TYPE_SHARE);
            } else if (postData.getImageUrl() != null && postData.getImageUrl().length() > 0)
                postData.setPostType(POST_TYPE_IMAGE);
            else if (postData.getVideoUrl() != null && postData.getVideoUrl().length() > 0) {
                // addThumbnail(postData);
                postData.setPostType(POST_TYPE_VIDEO);
            } else
                postData.setPostType(POST_TYPE_TEXT);

            if (postData.getContent().length() > 150) {
                textViewPostContent.setExpandLines(3);
                textViewPostContent.setVisibility(View.VISIBLE);
                textviewContentNormal.setVisibility(View.GONE);
                textViewPostContent.setText(postData.getContent());
            } else {
                textViewPostContent.setVisibility(View.GONE);
                textviewContentNormal.setVisibility(View.VISIBLE);
                textviewContentNormal.setText(postData.getContent());
            }




             /*check screen types to change cardview layout*/
            switch (SCREEN_TYPE) {
                case Constants.POST_SCREEN_TYPE_HISTORY:
                    ButtonReport.setVisibility(View.GONE);
                    post_comment_view.setVisibility(View.GONE);
                    post_write_comment_view_view.setVisibility(View.GONE);
                    break;
                case Constants.POST_SCREEN_TYPE_HOME:
                case Constants.POST_SCREEN_TYPE_SEARCH:

                    imageButtonDot.setVisibility(View.VISIBLE);
                    post_comment_view.setVisibility(View.VISIBLE);
                    post_write_comment_view_view.setVisibility(View.VISIBLE);

                    break;
                case Constants.POST_SCREEN_TYPE_TRIBE:
                    imageButtonDot.setVisibility(View.GONE);
                    post_comment_view.setVisibility(View.VISIBLE);
                    post_write_comment_view_view.setVisibility(View.VISIBLE);
                    sharePostView.setVisibility(View.GONE);

                    break;


            }



            switch (postData.getPostType()) {
                case PostData.POST_TYPE_IMAGE:
                    postHeaderView.setVisibility(View.VISIBLE);
                    post_image_view.setVisibility(View.VISIBLE);
                    post_share_view.setVisibility(View.GONE);
                    post_video_view.setVisibility(View.GONE);
                    postHiddenView.setVisibility(View.GONE);
                    post_txt_view.setVisibility(View.VISIBLE);

                    if(SCREEN_TYPE !=Constants.POST_SCREEN_TYPE_HISTORY){
                        post_comment_view.setVisibility(View.VISIBLE);
                        post_write_comment_view_view.setVisibility(View.VISIBLE);
                    }


                    Glide.with(context).load(postData.getImageUrl()).thumbnail(0.1f).placeholder(R.drawable.ic_thumbnil_image).error(R.drawable.ic_thumbnil_image).into(imageViewPost);

                    break;
                case PostData.POST_TYPE_SHARE:
                    postHeaderView.setVisibility(View.VISIBLE);
                    post_share_view.setVisibility(View.VISIBLE);
                    post_image_view.setVisibility(View.GONE);
                    post_video_view.setVisibility(View.GONE);
                    postHiddenView.setVisibility(View.GONE);

                    if(SCREEN_TYPE !=Constants.POST_SCREEN_TYPE_HISTORY){
                        post_comment_view.setVisibility(View.VISIBLE);
                        post_write_comment_view_view.setVisibility(View.VISIBLE);
                    }

                    post_txt_view.setVisibility(View.VISIBLE);
                    setSharePostData(postData);

                    break;
                case PostData.POST_TYPE_TEXT:
                    postHeaderView.setVisibility(View.VISIBLE);
                    post_share_view.setVisibility(View.GONE);
                    post_image_view.setVisibility(View.GONE);
                    post_video_view.setVisibility(View.GONE);
                    postHiddenView.setVisibility(View.GONE);
                    post_txt_view.setVisibility(View.VISIBLE);
                    if(SCREEN_TYPE !=Constants.POST_SCREEN_TYPE_HISTORY){
                        post_comment_view.setVisibility(View.VISIBLE);
                        post_write_comment_view_view.setVisibility(View.VISIBLE);
                    }
                    break;
                case PostData.POST_TYPE_VIDEO:
                    postHeaderView.setVisibility(View.VISIBLE);
                    //Glide.with(context).load(Uri.parse(postData.getVideoUrl())).asBitmap().error(R.drawable.dummy_rounded).into(holder.imageViewProfile);
                    post_share_view.setVisibility(View.GONE);
                    post_image_view.setVisibility(View.GONE);
                    postHiddenView.setVisibility(View.GONE);
                    post_video_view.setVisibility(View.VISIBLE);
                    post_txt_view.setVisibility(View.VISIBLE);
                    if(SCREEN_TYPE !=Constants.POST_SCREEN_TYPE_HISTORY){
                        post_comment_view.setVisibility(View.VISIBLE);
                        post_write_comment_view_view.setVisibility(View.VISIBLE);
                    }
                    break;
                case PostData.POST_TYPE_HIDDEN:
                    postHeaderView.setVisibility(View.GONE);
                    //Glide.with(context).load(Uri.parse(postData.getVideoUrl())).asBitmap().error(R.drawable.dummy_rounded).into(holder.imageViewProfile);
                    post_share_view.setVisibility(View.GONE);
                    post_image_view.setVisibility(View.GONE);
                    post_video_view.setVisibility(View.GONE);
                    postHiddenView.setVisibility(View.VISIBLE);
                    post_comment_view.setVisibility(View.GONE);
                    post_txt_view.setVisibility(View.GONE);
                    post_write_comment_view_view.setVisibility(View.GONE);

                    break;
            }








            ImageUtils.setprofile(imageViewProfile, postData.getAuthor().getProfile().getProfileImage(), context);
            textViewName.setText(postData.getAuthor().getProfile().getFirstName() + " " + postData.getAuthor().getProfile().getLastName());
            String date = postData.getCreatedAt().split("\\.")[0];
            textViewTime.setText(DateTimeAgo.getTimeAgo2(date + "Z"));

            textViewPeaceByCount.setText(String.valueOf(postData.getDislikedBy().size()
                    + postData.getLikedBy().size()
                    + postData.getPeaceBy().size()));

            /*set action view*/
            textViewLikedText.setText(String.valueOf(postData.getLikedBy().size()));
            textViewDisLikedText.setText(String.valueOf(postData.getDislikedBy().size()));
            textViewPeace.setText(String.valueOf(postData.getPeaceBy().size()));
            if (postData.getCommentsCount() != null)
                textViewTxtCommentByCount.setText(String.valueOf(postData.getCommentsCount()));
            else
                textViewTxtCommentByCount.setText("0");


        }





        private void setSharePostData(GetPostDataResponse postData) {

            if (postData.getCaption() != null && postData.getCaption().length() > 0) {


                if (postData.getContent().length() > 150) {
                    textViewPostContent.setExpandLines(3);
                    textViewPostContent.setVisibility(View.VISIBLE);
                    textviewContentNormal.setVisibility(View.GONE);
                    textViewPostContent.setText(postData.getCaption());
                } else {
                    textViewPostContent.setVisibility(View.GONE);
                    textviewContentNormal.setVisibility(View.VISIBLE);
                    textviewContentNormal.setText(postData.getCaption());
                }

            } else {
                textViewPostContent.setVisibility(View.GONE);
                textviewContentNormal.setVisibility(View.GONE);
            }


            ImageUtils.setprofile(imageViewOwnerPic, postData.getRealOwner().getProfile().getProfileImage(), context);
            textViewOwnerContent.setText(postData.getContent());
            textViewOwnerName.setText(postData.getRealOwner().getProfile().getFirstName() + " " +
                    postData.getRealOwner().getProfile().getLastName());


            String date = postData.getRealOwner().getCreatedAt().split("\\.")[0];
            textViewOwnerPostTime.setText(DateTimeAgo.getTimeAgo2(date + "Z"));

            if (postData.getImageUrl() != null && postData.getImageUrl().length() > 0) {
                post_owner_image_view.setVisibility(View.VISIBLE);
                Glide.with(context).load(postData.getImageUrl()).thumbnail(0.1f).placeholder(R.drawable.ic_thumbnil_image).error(R.drawable.ic_thumbnil_image).into(imageViewOwnerPostImage);
            } else {
                post_owner_image_view.setVisibility(View.GONE);
            }

        }

        @OnClick(R.id.hot_item)
        public void openEmojiView() {
            slideToTop(hot_view, postData, position);

            notifyItemChanged(position);
        }


        @OnClick({R.id.like_button, R.id.dislike_button, R.id.peace_button})
        public void performAction(View view) {
            UpdatePostRequest request = new UpdatePostRequest();
            request.setPostId(postData.get_id());
            switch (view.getId()) {
                case R.id.like_button:
                    request.setAction("like");
                    break;
                case R.id.dislike_button:
                    request.setAction("dislike");
                    break;
                case R.id.peace_button:
                    request.setAction("peace");
                    break;
            }
            post_popup.dismiss();
            postActionsListener.performAction(request, position);

        }

        @OnClick(R.id.btn_report)
        public void openPopUpView() {
            int[] location = new int[2];

            // Get the x, y location and store it in the location[] array
            // location[0] = x, location[1] = y.
            ButtonReport.getLocationOnScreen(location);

            //Initialize the Point with x, and y positions
            Point point = new Point();
            point.x = location[0];
            point.y = location[1];

            postActionsListener.openPopupview(context, point, position, postDatas.get(position));

        }


        @OnClick(R.id.share_post_layout)
        public void sharePost() {
            Intent intent = new Intent(context, SharePostActivity.class);
            intent.putExtra("post_model", postData);
            context.startActivityForResult(intent, 101);
        }

        @OnClick(R.id.post_video_view)
        public void startVideo() {
            try {
                Log.e("video url", postDatas.get(position).getVideoUrl() + "");
                Log.e("position", String.valueOf(position));
                imageViewVideoThmbnail.setVisibility(View.GONE);
                Uri videoURl = Uri.parse(postDatas.get(position).getVideoUrl());
                FullscreenVideoLayout fullscreenVideoLayout = new FullscreenVideoLayout(context, videoURl);
                post_video_view.addView(fullscreenVideoLayout, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                fullscreenVideoLayout.setActivity((Activity) context);
                fullscreenVideoLayout.setShouldAutoplay(true);
                fullscreenVideoLayout.setVideoURI(videoURl);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @OnClick(R.id.post_comment_button)
        public void postComment() {
            if (editTextCommentBox.getText().toString().trim().length() > 0) {
                postActionsListener.addComment(postData.get_id(), editTextCommentBox.getText().toString().trim(), position);
            } else {
                Toast.makeText(context, "Please enter comment !!", Toast.LENGTH_LONG).show();
            }
        }

        @OnClick(R.id.txt_comments)
        public void openCommentScreen() {
            Intent intent = new Intent(context, CommentsActivity.class);
            intent.putExtra("post_model", postData);

            if(SCREEN_TYPE==3)
            intent.putExtra("isFromPost",false);
            else
            intent.putExtra("isFromPost",true);

            context.startActivity(intent);
        }
    }

    public void slideToBottom(final View view) {

        /*TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);*/

        post_popup.dismiss();


    }

    // To animate view slide out from bottom to top
    public void slideToTop(final View view, GetPostDataResponse postData, final int position) {
       /* TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);*/

        // Inflate the popup_layout.xml


        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.emojiViewNew);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.emoji_new_layout, null);


        LinearLayout likeLayout = (LinearLayout) layout.findViewById(R.id.like_button);
        LinearLayout dislikeLayout = (LinearLayout) layout.findViewById(R.id.dislike_button);
        LinearLayout peaceLayout = (LinearLayout) layout.findViewById(R.id.peace_button);

        ImageView thumbsupImg = (ImageView) layout.findViewById(R.id.thumbs_up_count_image);
        ImageView thumbsdownImg = (ImageView) layout.findViewById(R.id.thumbs_down_count_image);
        ImageView peaceImg = (ImageView) layout.findViewById(R.id.peace_count_image);


        TextView txt_liked = (TextView) layout.findViewById(R.id.txt_liked);
        TextView txt_dislike = (TextView) layout.findViewById(R.id.txt_disliked);
        TextView txt_peace = (TextView) layout.findViewById(R.id.txt_peace);

        if (postData.getLikedBy().contains(sessionManager.getUserData().get_id()))
            thumbsupImg.setImageResource(R.drawable.ic_thumbs_up);
        else
            thumbsupImg.setImageResource(R.drawable.ic_thunbs_up_gray);

        if (postData.getDislikedBy().contains(sessionManager.getUserData().get_id()))
            thumbsdownImg.setImageResource(R.drawable.ic_thumbs_down_blue);
        else
            thumbsdownImg.setImageResource(R.drawable.ic_thumbs_down);

        if (postData.getPeaceBy().contains(sessionManager.getUserData().get_id()))
            peaceImg.setImageResource(R.drawable.ic_peace_blue);
        else
            peaceImg.setImageResource(R.drawable.ic_peace);

        txt_liked.setText(String.valueOf(postData.getLikedBy().size()));
        txt_dislike.setText(String.valueOf(postData.getDislikedBy().size()));
        txt_peace.setText(String.valueOf(postData.getPeaceBy().size()));

        YoYo.with(Techniques.RotateInUpLeft)
                .duration(700)
                .playOn(likeLayout);

        YoYo.with(Techniques.RotateInUpLeft)
                .duration(700)
                .playOn(dislikeLayout);

        YoYo.with(Techniques.RotateInUpLeft)
                .duration(700)
                .playOn(peaceLayout);


        final UpdatePostRequest request = new UpdatePostRequest();
        request.setPostId(postData.get_id());

        likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request.setAction("like");
                postActionsListener.performAction(request, position);
                post_popup.dismiss();
            }
        });

        dislikeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request.setAction("dislike");
                postActionsListener.performAction(request, position);
                post_popup.dismiss();
            }
        });

        peaceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                request.setAction("peace");
                postActionsListener.performAction(request, position);
                post_popup.dismiss();
            }
        });


        // Creating the PopupWindow
        post_popup = new PopupWindow(context);
        post_popup.setContentView(layout);
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 260, context.getResources().getDisplayMetrics());

        post_popup.setWidth(width);
        post_popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int OFFSET_X = -500;
        int OFFSET_Y = -70;

        //Clear the default translucent background
        post_popup.setBackgroundDrawable(new BitmapDrawable());

        int[] location = new int[2];

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        view.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        Point point = new Point();
        point.x = location[0];
        point.y = location[1];


        // Displaying the popup at the specified location, + offsets.
        post_popup.showAtLocation(layout, Gravity.NO_GRAVITY, point.x, point.y + OFFSET_Y);
        YoYo.with(Techniques.SlideInUp)
                .duration(700)
                .playOn(layout);

    }


    public interface PostActionsListener {
        public void performAction(UpdatePostRequest request, int position);

        public void openPopupview(Context context, Point point, int position, GetPostDataResponse getPostDataResponse);

        public void sharePost(String postid);

        public void addComment(String postid, String comment_txt, int position);
    }

}
