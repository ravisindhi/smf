package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.presenter;

import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.AllTribesPostRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;

/**
 * Created by daffodil on 13/2/17.
 */

public interface ITribeDetailPresenter {
    void getAllPosts(AllTribesPostRequest request);
    void closeTribe(String group_id);
    void joinTribe(JoinTribeRequest joinTribeRequest);
}
