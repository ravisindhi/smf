package com.smf.daffodil.smfapp.slide_menu_options.change_password.model;

/**
 * Created by daffodil on 1/2/17.
 */

public class ChangePasswordRequest {

    private String token;
    private String password;

    private String old_password;

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    private String confirm_password;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
