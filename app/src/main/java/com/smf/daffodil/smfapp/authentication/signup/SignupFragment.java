package com.smf.daffodil.smfapp.authentication.signup;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.AboutYouActivity;
import com.smf.daffodil.smfapp.authentication.login.model.request.DeviceInfo;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.authentication.signup.model.SignupRequest;
import com.smf.daffodil.smfapp.authentication.signup.presenter.ISignupCompl;
import com.smf.daffodil.smfapp.authentication.signup.presenter.ISignupPresenter;
import com.smf.daffodil.smfapp.authentication.signup.view.ISignupView;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.home.HomeActivity;
import com.smf.daffodil.smfapp.push_notification.NotificationConfig;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignupFragment extends BaseFragment implements ISignupView{

    ISignupPresenter mISignupPresenter;

    private SessionManager sessionManager;

    @BindView(R.id.et_firstname)
    EditText editTextFirstName;

    @BindView(R.id.et_last_name)
    EditText editTextLastName;

    @BindView(R.id.et_email)
    EditText editTextEmail;

    @BindView(R.id.et_password)
    EditText editTextPassword;

    @BindView(R.id.et_confirm_password)
    EditText editTextConfirmPasswrd;

    @BindView(R.id.btn_signup)
    Button buttonSignup;

    @BindView(R.id.signup_text)
    TextView textViewSignupText;





    public SignupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sessionManager=new SessionManager(getActivity());
        return inflater.inflate(R.layout.fragment_signup, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        mISignupPresenter=new ISignupCompl(getActivity(),this);

    }

    @OnClick(R.id.signup_text)
    public void redirectToLogin()
    {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_signup)
    public void openAboutYou()
    {
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences(NotificationConfig.SHARED_PREF, 0);
        DeviceInfo deviceInfo=new DeviceInfo();
        deviceInfo.setDeviceType("android");
        deviceInfo.setDeviceToken(pref.getString("regId", ""));

        SignupRequest signupRequest=new SignupRequest();
        signupRequest.setDeviceInfo(deviceInfo);
        signupRequest.setFirst_name(editTextFirstName.getText().toString().trim());
        signupRequest.setLast_name(editTextLastName.getText().toString().trim());
        signupRequest.setFullName(signupRequest.getFirst_name()+" "+signupRequest.getLast_name());
        signupRequest.setEmail(editTextEmail.getText().toString().trim());
        signupRequest.setPassword(editTextPassword.getText().toString().trim());
        signupRequest.setConfirm_pasword(editTextConfirmPasswrd.getText().toString().trim());
        mISignupPresenter.doSignup(signupRequest);
    }

    @Override
    public void onErrorHandleWithView(int errorCode, String _errorMsg) {
        switch (errorCode)
        {
            case ISignupView.FIRST_NAME_ERROR:
                replaceErrorViewWithMessage(editTextFirstName,getActivity());
                editTextFirstName.requestFocus();
                break;
            case ISignupView.LAST_NAME_ERROR:
                replaceErrorViewWithMessage(editTextLastName,getActivity());
                editTextLastName.requestFocus();
                break;
            case ISignupView.EMAIL_ERROR:
                replaceErrorViewWithMessage(editTextEmail,getActivity());
                editTextEmail.requestFocus();
                break;
            case ISignupView.PASSWORD_ERROR:
                replaceErrorViewWithMessage(editTextPassword,getActivity());
                editTextPassword.requestFocus();
                break;
            case ISignupView.CONFIRM_PASSWORD_ERROR:
                replaceErrorViewWithMessage(editTextConfirmPasswrd,getActivity());
                editTextConfirmPasswrd.requestFocus();
                break;
        }

    }

    @Override
    public void onErrorHandleWithSnakbar(int errorCode, String _errorMsg) {
        switch (errorCode)
        {
            case ISignupView.EMAIL_ERROR:
                replaceErrorView(editTextEmail,getActivity());
                showErrorMessage(getContext().getApplicationContext(),getString(R.string.email_invalid),getActivity());
                editTextEmail.requestFocus();

                break;
            case ISignupView.PASSWORD_MISMATCH_ERROR:
                replaceErrorView(editTextPassword,getActivity());
                showErrorMessage(getContext().getApplicationContext(),getString(R.string.password_mismatch_error_text),getActivity());
                editTextPassword.requestFocus();
                break;

            case IBaseView.SERVER_ERROR:
                showErrorMessage(getActivity().getApplicationContext(),_errorMsg,getActivity());
                break;
        }
    }

    @Override
    public void onSuccess(LoginResponse response) {

        if(response.getProfile().getHandle()!=null && response.getProfile().getHandle().length()>0)
        {
            launchActivity(getActivity(),HomeActivity.class);
            getActivity().finish();
        }
        else{

            launchActivity(getActivity(),AboutYouActivity.class);
            getActivity().finish();
        }
    }

    @Override
    public boolean onSaveUserData(LoginResponse response) {
        boolean isSaved=false;
        if(response!=null)
            isSaved=sessionManager.saveUserData(response);

        return isSaved;
    }


}
