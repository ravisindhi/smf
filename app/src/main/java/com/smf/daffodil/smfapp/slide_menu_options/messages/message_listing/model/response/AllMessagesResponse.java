package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by daffolap-164 on 24/2/17.
 */

public class AllMessagesResponse {

    @SerializedName("totalmessagescount")
    @Expose
    private Integer totalmessagescount;

    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Integer getTotalmessagescount() {
        return totalmessagescount;
    }

    public void setTotalmessagescount(Integer totalmessagescount) {
        this.totalmessagescount = totalmessagescount;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

}
