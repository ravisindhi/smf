package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

import com.smf.daffodil.smfapp.authentication.login.model.response.Profile;

import java.io.Serializable;

/**
 * Created by daffodil on 11/2/17.
 */

public class TribesMembers implements Serializable{

    private TribesId id;

    private String _id;

    private Profile profile;

    private String status;

    public TribesId getTribesId()
    {
        return id;
    }

    public void setTribesId(TribesId id)
    {
        this.id = id;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+ id +", _id = "+_id+", status = "+status+"]";
    }


    public TribesId getId() {
        return id;
    }

    public void setId(TribesId id) {
        this.id = id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
