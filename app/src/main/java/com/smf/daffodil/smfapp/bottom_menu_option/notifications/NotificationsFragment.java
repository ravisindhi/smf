package com.smf.daffodil.smfapp.bottom_menu_option.notifications;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.adapter.NotificationAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.AcceptRejectRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response.Notification;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.presenter.INotificationsCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.presenter.INotificationsPresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.view.INotificationView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.CommentsActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.MessagesActivity;
import com.smf.daffodil.smfapp.view.PopupDialog;
import com.smf.daffodil.smfapp.view.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends BaseFragment implements INotificationView,PopupDialog.DilogButtonHandling,NotificationAdapter.AcceptRejectListener,View.OnClickListener {

    @BindView(R.id.notification_recyclerview)
    RecyclerView mRecyclerView;

    INotificationsPresenter iNotificationsPresenter;
    private List<Notification> list=new ArrayList<>();
    private NotificationAdapter adapter;
    private PopupDialog popupDialog;




    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        iNotificationsPresenter=new INotificationsCompl(this,getActivity());
        return inflater.inflate(R.layout.fragment_notifications, container, false);




    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setHasOptionsMenu(true);
         initData();

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.action_bar_notification_menu, menu);

        LinearLayout view=(LinearLayout) menu.findItem(R.id.action_clear).getActionView();
view.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        openNotificationsPopup();
    }
});

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection


       /* view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
*/
        return false;

        /*switch (item.getItemId()) {
            case R.id.action_clear:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }*/
    }

    private void openNotificationsPopup()
    {
        popupDialog = new PopupDialog(this);
        Bundle bundle = new Bundle();
        bundle.putString("name", "Clear all Notifications");
        bundle.putString("content", "Do you want to clear all the notifications?");
        bundle.putString("accept_text", "Yes");
        bundle.putString("cancel_text", "No");
        popupDialog.setArguments(bundle);
        popupDialog.show(getActivity().getFragmentManager(),"new");
    }


    private void initData()
    {




            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), OrientationHelper.VERTICAL, false);
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            adapter = new NotificationAdapter(list, getActivity(), this);
            mRecyclerView.setAdapter(adapter);
            iNotificationsPresenter.getAllNotifications();

            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {

                    if(list.get(position).getLink()!=null)
                    {

                        if(list.get(position).getLink().contains("post") || list.get(position).getLink().contains("group"))
                        {
                            if(list.get(position).getActionItemId()!=null)
                                iNotificationsPresenter.getNotificationDetailPost(list.get(position).getLink());

                        }else if(list.get(position).getLink().contains("message") )
                        {
                            launchActivity(getActivity(), MessagesActivity.class);
                        }else{
                            Toast.makeText(getActivity(),"No detail found for this post.",Toast.LENGTH_LONG).show();
                        }
                    }

                }
            }));
        }








    @Override
    public void onGetNotifications(Notification[] result) {
        list.clear();
        if(result.length>0){

            for(int i=0;i<result.length;i++)
            {
                if(result[i].getType().equals("privateGroupRequest") && result[i].getNewRequest()){
                    list.add(result[i]);
                }else if(!result[i].getType().equals("privateGroupRequest") ){
                    list.add(result[i]);
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(int errorCode, String message) {
        switch (errorCode) {
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getContext().getApplicationContext(), message, getActivity());
                break;
        }
    }

    @Override
    public void onClearNotifications() {
        list.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAcceptRejectSuccess(int position) {
        list.remove(position);
        adapter.notifyItemRemoved(position);
    }


    @Override
    public void buttonYesClicked(String type) {
        popupDialog.dismiss();
        iNotificationsPresenter.doClearNotification();
    }

    @Override
    public void buttonNoClicked() {
        popupDialog.dismiss();
    }

    @Override
    public void acceptClicked(int position, Notification notification) {
        AcceptRejectRequest request=new AcceptRejectRequest();
        request.setGroupID(notification.getActionItemId());
        request.setMemberId(notification.getSourceUser().getId());
        request.setStatus("accepted");
        iNotificationsPresenter.onAcceptReject(request,position);
    }

    @Override
    public void rejectClicked(int position, Notification notification) {
        AcceptRejectRequest request=new AcceptRejectRequest();
        request.setGroupID(notification.getActionItemId());
        request.setMemberId(notification.getSourceUser().getId());
        request.setStatus("declined");
        iNotificationsPresenter.onAcceptReject(request,position);
    }

    public void onGetNotificationDetailPost(GetPostDataResponse postDataResponse) {

        openDetailScreen(postDataResponse);
    }

    @Override
    public void onGetNotificationDetailGroup(GetPostDataResponse postDataResponse) {
        openDetailScreen(postDataResponse);
    }

    private void openDetailScreen(GetPostDataResponse postDataResponse){
        Intent intent=new Intent(getActivity(), CommentsActivity.class);
        // Log.e("post_model",postDataResponse.toString());
        intent.putExtra("post_model", postDataResponse);
        intent.putExtra("notification", true);
        getActivity().startActivity(intent);
    }

    @Override
    public void onClick(View view) {

    }
}
