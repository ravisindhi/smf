package com.smf.daffodil.smfapp.bottom_menu_option.map;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.smf.daffodil.smfapp.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by daffolap-164 on 16/2/17.
 */

public class AddressDialogFragment extends DialogFragment {
     String strAddress;
    OpenMapDialog openMapDialog;


//    public static AddressDialogFragment newInstance(String address) {
//
//       // MapFragment.isDialogOpen=false;
//        Bundle args = new Bundle();
//        if (address!=null)
//             args.putString("Address",address);
//        AddressDialogFragment fragment = new AddressDialogFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }

    public static AddressDialogFragment newInstance(Double latt, Double longg) {

        // MapFragment.isDialogOpen=false;
        Bundle args = new Bundle();
        if (latt!=null) {
            args.putDouble("lat", latt);
            args.putDouble("long", longg);

        }
        AddressDialogFragment fragment = new AddressDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.address_dialog_view, container,
                false);
        // Do something else
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.getDialog().setCanceledOnTouchOutside(true);
        this.setCancelable(true);



        Bundle bundle=getArguments();
//        if (bundle.containsKey("Address")) {
//            strAddress= bundle.getString("Address");
//        }

        if(bundle.containsKey("lat") && bundle.containsKey("long"))
        {
            strAddress=getAddress(bundle.getDouble("lat"),bundle.getDouble("long"));
        }


       TextView textViewAddress= (TextView) rootView.findViewById(R.id.location_tv);
        textViewAddress.setText(strAddress);


        rootView.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(openMapDialog!=null)
                {
                    openMapDialog.onOpenMapDialog(strAddress);
                }
               // mapDialogFragment.show(getActivity().getFragmentManager(),"new");
            }
        });
        return rootView;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        MapFragment.isDialogOpen=false;
        super.onDismiss(dialog);
    }

    public String getAddress(Double latitude , Double longitude) {

        List<Address> addresses= new ArrayList<>();
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        StringBuilder strAddress= new StringBuilder();

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            // Address address = addresses.get(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();

            Log.e(" addresses.get(0)","->"+ addresses.get(0));

            if(address!=null)
                strAddress.append(address).append(",");

            if(city!=null)
                strAddress.append(city).append(",");

            if(state!=null)
                strAddress.append(state).append(",");

            if(country!=null)
                strAddress.append(country);

        }catch (IOException e)
        {}



        return strAddress.toString();

    }


    public interface OpenMapDialog{
        void onOpenMapDialog(String userAddress);

    }

    public void setOpenMapDialog(OpenMapDialog openMapDialog){
        this.openMapDialog=openMapDialog;

    }
}