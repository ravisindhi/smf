
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Connections {

    @SerializedName("following")
    @Expose
    private List<String> following = null;
    @SerializedName("blocked")
    @Expose
    private List<String> blocked = null;
    @SerializedName("connected")
    @Expose
    private List<String> connected = null;
    @SerializedName("favorites")
    @Expose
    private List<String> favorites = null;

    public List<String> getFollowing() {
        return following;
    }

    public void setFollowing(List<String> following) {
        this.following = following;
    }

    public List<String> getBlocked() {
        return blocked;
    }

    public void setBlocked(List<String> blocked) {
        this.blocked = blocked;
    }

    public List<String> getConnected() {
        return connected;
    }

    public void setConnected(List<String> connected) {
        this.connected = connected;
    }

    public List<String> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<String> favorites) {
        this.favorites = favorites;
    }

}
