package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;

/**
 * Created by daffodil on 11/2/17.
 */

public interface ICreateTribeView extends IBaseView{

    public final int TRIBE_IMAGE_ERROR=11;
    public static int TRIBE_NAME_ERROR=12;
    public static int TRIBE_DESCRIPTION_ERROR=13;

    void onCreateTribeSuccess(CreateTribeResponse createTribeResponse);
    void onError(int errorcode,String msg);

}
