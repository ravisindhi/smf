
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("idle")
    @Expose
    private Boolean idle;
    @SerializedName("lastLogin")
    @Expose
    private LastLogin lastLogin;
    @SerializedName("online")
    @Expose
    private Boolean online;

    public Boolean getIdle() {
        return idle;
    }

    public void setIdle(Boolean idle) {
        this.idle = idle;
    }

    public LastLogin getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LastLogin lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

}
