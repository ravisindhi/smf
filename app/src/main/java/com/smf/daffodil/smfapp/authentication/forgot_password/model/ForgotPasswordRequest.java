package com.smf.daffodil.smfapp.authentication.forgot_password.model;

/**
 * Created by daffodil on 24/1/17.
 */

public class ForgotPasswordRequest {

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String email;
}
