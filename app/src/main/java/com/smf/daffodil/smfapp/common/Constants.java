package com.smf.daffodil.smfapp.common;

/**
 * Created by daffodil on 26/7/16.
 */
public class Constants {

    public static final String NO_RESPONSE = "No response from server";
    public static final String REQUEST_TIME_OUT = "Request time out";
    public static final String STATUS = "Server Error, Please try later";
    public static final String MESSAGE_SERVER_ERROR = "Successfully Accessed";
    public static final String LOGIN_STATUS = "Successfully Accessed";

    public static String CHAT_SERVER_URL = "http://139.162.5.142:5400";
    public static String ON_MESSAGE_RECEIVED = "received-message";
    public static String ON_REPLY_MESSAGE_RECEIVED = "received-reply-message";
    public static String ON_ERROR = "errorHandler";


    public final static int POST_SCREEN_TYPE_HOME=1;
    public final static int POST_SCREEN_TYPE_HISTORY=2;
    public final static int POST_SCREEN_TYPE_TRIBE=3;
    public final static int POST_SCREEN_TYPE_SEARCH=4;


    public enum LoginValidationKeys{
        EMAIL_BLANK,
        PASSWORD_BLANK,
        EMAIL_INVALID,
        NO_INTERNET
    }

    public static interface LoginParams{
        String LOGIN_EMAIL="email";
        String LOGIN_PASSWORD="password";
        String LOGIN_DEVICEINFO="deviceInfo";
    }

    public static interface FacebookLoginParams{
        String FB_LOGIN_EMAIL="email";
        String FB_LOGIN_ACCESS_TOKEN="accessToken";
        String FB_LOGIN_ID="id";
        String FB_LOGIN_NAME="name";
        String FB_LOGIN_FIRST_NAME="first_name";
        String FB_LOGIN_LAST_NAME="last_name";
        String FB_LOGIN_LINK="link";
        String FB_LOGIN_GENDER="gender";
        String FB_LOGIN_LOCALE="locale";
    }

    public static interface ForgotPasswordParams{
        String FORGOT_EMAIL="email";
    }

    public static interface SignupParams{
        String SIGNUP_EMAIL="email";
        String SIGNUP_PASSWORD="password";
        String SIGNUP_FIRST_NAME="firstName";
        String SIGNUP_LAST_NAME="lastName";
        String SIGNUP_FULL_NAME="fullName";
        String SIGNUP_DEVICEINFO="deviceInfo";
    }

    public interface UploadImageParams{
        String UPLOAD_PROFILE_FILE="file";
        String UPLOAD_PROFILE_TYPE="type";
        String UPLOAD_PROFILE_ID="id";
    }

    public  interface UpdateProfileParams{
        String UPDATE_PROFILE_EMAIL="searchEmail";
        String UPDATE_PROFILE_FIRST_NAME="firstName";
        String UPDATE_PROFILE_LAST_NAME="lastName";
        String UPDATE_PROFILE_FULL_NAME="fullName";
        String UPDATE_PROFILE_PHONE="phoneNumber";
        String UPDATE_PROFILE_CITY="city";
        String UPDATE_PROFILE_STATE="state";
        String UPDATE_PROFILE_ZIP="zipCode";
        String UPDATE_PROFILE_PROFILE_IMAGE="profileImage";
        String UPDATE_PROFILE_HANDLE="handle";
        String UPDATE_PROFILE_DOB="dob";
        String UPDATE_PROFILE_RELATIONSHIP="relationship";
        String UPDATE_PROFILE_GENDER="gender";
        String UPDATE_PROFILE_INTRO="intro";
    }

    public interface ChangePasswordParams{
        String CHANGE_PASSWORD_OLD="oldPassword";
        String CHANGE_PASSWORD_NEW_PASSWORD="newPassword";

    }

    public interface FollowUnFollowParams{
        String FOLLOW_USER_ID="followedUserId";
        String FOLLOW_ACTION="action";
    }

    public interface GetAllPostsParams{
        String ALL_POST_SKIP="?skip=";
        String ALL_POST_LIMIT="&limit=";
        String ALL_POST_CORDINATES="&position=";
        String ALL_POST_RANGE="&range=";
        String ALL_POST_TYPE_FAVOURITE="&isFavourite=";
        String ALL_POST_TYPE_TRANDING="&TRANDING=";
    }

    public interface NewPostParams{
        String NEW_POST_CONTENT="content";
        String NEW_POST_CORDINATES_LAT="coordinates[]";
        String NEW_POST_CORDINATES_LONG="coordinates[]";
        String NEW_POST_IMAGE_URL="imageUrl";
        String NEW_POST_VIDEO_URL="videoUrl";
        String NEW_POST_HIDDEN="hidden";
    }

    public interface SearchPostsParams{
        String SEARCH_POST_VALUE="?searchValue=";
        String SEARCH_POST_SKIP="&skip=";
        String SEARCH_POST_LIMIT="&limit=";
    }

    public interface  GetHistory{
        String HISTORY_AUTHOR="?author=";
        String HISTORY_LIMIT="&limit=";
        String HISTORY_SkIP="&skip=";
    }

    public interface BlockUserParams{
        String BLOCK_USER_ID="blockUserId";
    }

    public interface  GetAllTribes{
        String ALL_TRIBES_LIMIT="?limit=";
        String ALL_TRIBES_SkIP="&skip=";
        String ALL_TRIBES_ID="&myTribes=";
    }

    public interface JoinTribeParams{
        String JOIN_TRIBE_GROUP_ID="groupId";
        String JOIN_TRIBE_USER_ID="memberId";

    }

    public interface GetAllGROUPPostsParams{
        String GROUP_ALL_POST__ID="?groupId=";
        String GROUP_ALL_POST_SKIP="&skip=";
        String GROUP_ALL_POST_LIMIT="&limit=";
        String GROUP_ALL_POST_CORDINATES="&position=";
    }

    public interface CreateGroupPost{
        String GROUP_POST_CONTENT="content";
        String GROUP_POST_GROUP_ID="group";
        String GROUP_POST_IMAGE_URL="imageUrl";
        String GROUP_POST_VIDEO_URL="videoUrl";

    }

    public interface SearchGroupPostsParams{
        String SEARCH_POST_VALUE="?searchValue=";
        String SEARCH_POST_SKIP="&skip=";
        String SEARCH_POST_LIMIT="&limit=";
        String SEARCH_POST_GROUP_ID="&groupId=";
    }

    public interface SearchTribesParams{
        String SEARCH_TRIBE_VALUE="?searchValue=";

    }

    public interface AcceptRejectParams{
        String ACCEPT_REJECT_MEMBER_ID="memberId";
        String ACCEPT_REJECT_STATUS="status";
        String ACCEPT_REJECT_GROUP_ID="groupId";

    }

    public interface UpadatePostParams{
        String UPDATE_POST_ID="postId";
        String UPDATE_POST_ACTION="action";
        String UPDATE_POST_CONTENT="content";

    }

    public interface SharePostParams{
        String SHARE_POST_ID="postId";
        String SHARE_CAPTION="caption";
        String SHARE_POST_CORDINATES_LAT="coordinates[]";
        String SHARE_POST_CORDINATES_LONG="coordinates[]";
    }

    public interface AddCommentParams{
        String COMMENT_POST_ID="postId";
        String COMMENT_CONTENT="content";
        String COMMENT_ISFROM_GROUP="groupCommentPost";
    }

    public interface GetCommentParams{
        String GET_COMMENT_POST_ID="?postId=";
    }



    public interface GetAllMessagesParams{
        String ALL_MESSAGES_LIMIT="?limit=";
        String ALL_MESSAGES_SKIP="&skip=";
    }

    public interface GetMessageCommentParams{

        String ALL_MESSAGES_ID="?messageId=";
        String ALL_MESSAGES_COMMENT_LIMIT="&limit=";
        String ALL_MESSAGES_COMMENT_SKIP="&skip=";
    }


    public interface GetNotificationDetailParams{
        String GET_COMMENT_POST_ID="?post=";
    }

    public interface UpdateCommentParams{
        String UPDATE_COMMENT_COMMENT_ID="commentId";
        String UPDATE_COMMENT_CONTENT="content";
        String UPDATE_COMMENT_ISFROM_GROUP="groupCommentPost";
        String UPDATE_COMMENT_ACTION="action";
    }



}
