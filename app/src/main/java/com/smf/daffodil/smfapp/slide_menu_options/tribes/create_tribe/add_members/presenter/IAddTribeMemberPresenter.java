package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.presenter;

/**
 * Created by daffodil on 22/2/17.
 */

public interface IAddTribeMemberPresenter {

    void getMembers();
}
