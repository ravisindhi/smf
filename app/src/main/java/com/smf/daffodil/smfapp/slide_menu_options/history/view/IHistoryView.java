package com.smf.daffodil.smfapp.slide_menu_options.history.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.post_and_update.model.PostAndUpdateResponse;

import java.util.List;

/**
 * Created by daffolap on 7/2/17.
 */

public interface IHistoryView extends IBaseView {

    final int CONTENT_EMPTY_ERROR=102;

    void onError(int errorcode,String message);
    void onSuccess(PostDataResult dataResponses);
    void onAppendData(PostDataResult dataResponses);
}
