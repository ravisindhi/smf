package com.smf.daffodil.smfapp.rest_api;

/**
 * Created by daffodil on 19/2/16.
 */
public class ErrorModel {

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public int status_code;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    String status=null;
    String message=null;
}
