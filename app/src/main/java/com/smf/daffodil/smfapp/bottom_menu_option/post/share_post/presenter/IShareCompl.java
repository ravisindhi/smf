package com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.view.IShareView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 21/2/17.
 */

public class IShareCompl implements ISharePresenter{

    private Context mContext;
    private IShareView mIShareView;

    public IShareCompl(Context mContext, IShareView mIShareView) {
        this.mContext = mContext;
        this.mIShareView = mIShareView;
    }

    @Override
    public void doSharePost(SharePostRequest request) {
        if (mIShareView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().sharePost(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {
                        mIShareView.onSharePost(result);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        mIShareView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        mIShareView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, request);
        } else {
            mIShareView.onError(IBaseView.NETWORK_ERROR, mContext.getString(R.string.internet_error_message));
        }
    }
}
