package com.smf.daffodil.smfapp.bottom_menu_option.members.model.members;

import java.io.Serializable;

/**
 * Created by daffodil on 1/2/17.
 */

public class MemberProfile implements Serializable{

    private String streetAddress;

    private String lastName;


    private String handle;

    private String profileImage;

    private String webAddress;

    private String state;

    private String facebookProfileUrl;

    private String linkedInProfileUrl;

    private String intro;

    private SavedLocations[] savedLocations;

    private String[] hiddenPosts;

    private String city;

    private String relationship;

    private String title;

    private String subscription;

    private String company;

    private String dob;

    private String zipCode;

    private String gender;

    private String twitterProfileUrl;

    private String fullName;

    private String firstName;

    public String getStreetAddress ()
    {
        return streetAddress;
    }

    public void setStreetAddress (String streetAddress)
    {
        this.streetAddress = streetAddress;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getHandle ()
    {
        return handle;
    }

    public void setHandle (String handle)
    {
        this.handle = handle;
    }

    public String getProfileImage ()
    {
        return profileImage;
    }

    public void setProfileImage (String profileImage)
    {
        this.profileImage = profileImage;
    }

    public String getWebAddress ()
    {
        return webAddress;
    }

    public void setWebAddress (String webAddress)
    {
        this.webAddress = webAddress;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getFacebookProfileUrl ()
    {
        return facebookProfileUrl;
    }

    public void setFacebookProfileUrl (String facebookProfileUrl)
    {
        this.facebookProfileUrl = facebookProfileUrl;
    }

    public String getLinkedInProfileUrl ()
    {
        return linkedInProfileUrl;
    }

    public void setLinkedInProfileUrl (String linkedInProfileUrl)
    {
        this.linkedInProfileUrl = linkedInProfileUrl;
    }

    public String getIntro ()
    {
        return intro;
    }

    public void setIntro (String intro)
    {
        this.intro = intro;
    }

    public String[] getHiddenPosts ()
    {
        return hiddenPosts;
    }

    public void setHiddenPosts (String[] hiddenPosts)
    {
        this.hiddenPosts = hiddenPosts;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getRelationship ()
    {
        return relationship;
    }

    public void setRelationship (String relationship)
    {
        this.relationship = relationship;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getSubscription ()
    {
        return subscription;
    }

    public void setSubscription (String subscription)
    {
        this.subscription = subscription;
    }

    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getZipCode ()
    {
        return zipCode;
    }

    public void setZipCode (String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getTwitterProfileUrl ()
    {
        return twitterProfileUrl;
    }

    public void setTwitterProfileUrl (String twitterProfileUrl)
    {
        this.twitterProfileUrl = twitterProfileUrl;
    }

    public String getFullName ()
    {
        return fullName;
    }

    public void setFullName (String fullName)
    {
        this.fullName = fullName;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [streetAddress = "+streetAddress+", lastName = "+lastName+", handle = "+handle+", profileImage = "+profileImage+", webAddress = "+webAddress+", state = "+state+", facebookProfileUrl = "+facebookProfileUrl+", linkedInProfileUrl = "+linkedInProfileUrl+", intro = "+intro+", hiddenPosts = "+hiddenPosts+", city = "+city+", relationship = "+relationship+", title = "+title+", subscription = "+subscription+", company = "+company+", dob = "+dob+", zipCode = "+zipCode+", gender = "+gender+", twitterProfileUrl = "+twitterProfileUrl+", fullName = "+fullName+", firstName = "+firstName+"]";
    }

    public SavedLocations[] getSavedLocations() {
        return savedLocations;
    }

    public void setSavedLocations(SavedLocations[] savedLocations) {
        this.savedLocations = savedLocations;
    }
}
