package com.smf.daffodil.smfapp.slide_menu_options.my_profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.AboutYouActivity;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.response.Profile;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.ChangePasswordActivity;
import com.smf.daffodil.smfapp.slide_menu_options.history.HistoryActivity;
import com.smf.daffodil.smfapp.slide_menu_options.my_profile.presenter.IMyPresenterCompl;
import com.smf.daffodil.smfapp.slide_menu_options.my_profile.presenter.IMyProfilePresenter;
import com.smf.daffodil.smfapp.slide_menu_options.my_profile.view.IMyProfileView;
import com.smf.daffodil.smfapp.utils.DateTimeAgo;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.BlurTransformation;

public class MyProfileActivity extends BaseActivity implements IMyProfileView{

    SessionManager sessionManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.change_password_button)
    LinearLayout linearLayoutForgotPassword;

    @BindView(R.id.history_button)
    LinearLayout linearLayoutHistory;

    @BindView(R.id.edit_profile_button)
    LinearLayout linearLayoutEditProfile;

    @BindView(R.id.blur_image_view)
    ImageView blurImageView;

    @BindView(R.id.rounded_image_view)
    ImageView rounded_image_view;

    @BindView(R.id.txt_name)
    TextView textViewName;

    @BindView(R.id.txt_handle)
    TextView textViewHandle;

    @BindView(R.id.txt_updated_time)
    TextView textViewUpdatedTime;

    @BindView(R.id.txt_dob)
    TextView textViewDOB;

    @BindView(R.id.txt_gender)
    TextView textViewGender;

    @BindView(R.id.txt_relation_status)
    TextView textViewRelationStatus;

    @BindView(R.id.txt_email)
    TextView textViewEmail;

    @BindView(R.id.txt_default_address)
    TextView textViewDefaultAddress;

    @BindView(R.id.txt_bio)
    TextView textViewBio;

    @BindView(R.id.change_account_type_switch)
    Switch aSwitchAccountChange;

    IMyProfilePresenter iMyProfilePresenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        ButterKnife.bind(this);

        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("MY PROFILE", true, false);

        sessionManager = new SessionManager(this);
        iMyProfilePresenter=new IMyPresenterCompl(MyProfileActivity.this,this);
        aSwitchAccountChange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b)
                    iMyProfilePresenter.doProfilePrivate("true");
                else
                    iMyProfilePresenter.doProfilePrivate("false");

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        Profile profile = sessionManager.getUserData().getProfile();
        textViewName.setText(profile.getFirstName() + " " + profile.getLastName());
        textViewHandle.setText("@"+profile.getHandle());

        if (profile.getDob() != null && profile.getDob().length() > 0) {

            String date = profile.getDob();
            String day = date.substring(0, 2);
            String month = date.substring(2, 4);
            String year = date.substring(4);
            textViewDOB.setText(day + "/" + month + "/" + year);
        } else {
            textViewDOB.setText("NA");
        }

        textViewEmail.setText(sessionManager.getUserData().getEmails()[0].getAddress());
        textViewDefaultAddress.setText(profile.getCity() + " " + profile.getState());
        textViewBio.setText(profile.getIntro());
        textViewRelationStatus.setText(profile.getRelationship());

        ImageUtils.setprofile(rounded_image_view, profile.getProfileImage(), this);
        //Glide.with(this).load(profile.getProfileImage()).error(R.drawable.dummy_rounded).into(rounded_image_view);
        Glide.with(this).load(profile.getProfileImage()).error(R.drawable.dummy_full).bitmapTransform(new BlurTransformation(this)).into(blurImageView);

        String date = sessionManager.getUserData().getCreatedAt().split("\\.")[0];
        textViewUpdatedTime.setText("Updated " + DateTimeAgo.getTimeAgo2(date + "Z"));


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }


    @OnClick(R.id.change_password_button)
    public void openChangePasswordScreen() {
        launchActivity(MyProfileActivity.this, ChangePasswordActivity.class);
    }

    @OnClick(R.id.edit_profile_button)
    public void openEditProfile() {

        Intent i=new Intent(MyProfileActivity.this,AboutYouActivity.class);
        i.putExtra("isFromEditProfile",true);
        startActivity(i);
    }

    @OnClick(R.id.history_button)
    public void openHistory() {

        Intent intent = new Intent(MyProfileActivity.this, HistoryActivity.class);
        intent.putExtra("user_id", sessionManager.getUserData().get_id());
        startActivity(intent);
    }
}
