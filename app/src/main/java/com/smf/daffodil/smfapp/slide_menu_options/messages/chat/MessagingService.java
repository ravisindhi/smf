package com.smf.daffodil.smfapp.slide_menu_options.messages.chat;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.common.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.Socket;

import java.net.URISyntaxException;
import java.util.Objects;

import io.socket.client.IO;
import io.socket.emitter.Emitter;


public class MessagingService extends Service {

    private Socket mSocket;

    private MessageServiceBinder mMessageServiceBinder = new MessageServiceBinder();

    private MessageCallback mMessageCallback;

    private boolean isDisconnect;

    public interface MessageCallback {
        void onNewMessageReceived(Object object);
    }

    public MessagingService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        initialiseSocket();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return mMessageServiceBinder;

    }


    private void initialiseSocket() {
        try {
            mSocket = IO.socket(Constants.CHAT_SERVER_URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_MESSAGE,onMessageReceived);
        mSocket.on(Constants.ON_MESSAGE_RECEIVED, onMessageReceived);
        mSocket.on(Constants.ON_REPLY_MESSAGE_RECEIVED, onMessageReceived);
        mSocket.on(Constants.ON_ERROR, onError);
        mSocket.connect();
    }

    public class MessageServiceBinder extends Binder {
        public MessagingService getService() {
            return MessagingService.this;
        }
    }

    public void setMessageCallback(MessageCallback callback) {
        mMessageCallback = callback;
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mSocket.emit("setUserId", SMFApplication.getSessionManager().getUserData().get_id());
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            if(!isDisconnect) {
                mSocket.connect();
            }

        }
    };

    private Emitter.Listener onMessageReceived = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("msg recieved",args[0].toString());
            if (mMessageCallback != null) {
                mMessageCallback.onNewMessageReceived(args[0]);
            }
        }
    };

    private Emitter.Listener onError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("On error",args[0].toString());

        }
    };

    public void sendMessage(String senderId, String message, String receiverId) {
        if (mSocket.connected()) {
            JSONObject messageObj = new JSONObject();
            try {
                messageObj.put("content", message);
                messageObj.put("recipient", receiverId);
                messageObj.put("author", senderId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("send-message", messageObj, new Ack() {
                @Override
                public void call(Object... args) {
                Log.e("error",args[0].toString());
                }
            });
        }
    }

    public void sendReplyMessage(String senderId, String message, String receiverId,String messageid) {
        if (mSocket.connected()) {
            JSONObject messageObj = new JSONObject();
            try {
                messageObj.put("content", message);
                messageObj.put("recipient", receiverId);
                messageObj.put("author", senderId);
                messageObj.put("messageId", messageid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("send-reply-message", messageObj, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("error",args[0].toString());

                }
            });
        }
    }


    public void disconnectSocket(boolean disconnect){
        mSocket.disconnect();
        isDisconnect=disconnect;
    }
}
