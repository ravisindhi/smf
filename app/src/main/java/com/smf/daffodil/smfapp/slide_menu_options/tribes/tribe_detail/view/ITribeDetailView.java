package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.CloseTribeResponse;

/**
 * Created by daffodil on 13/2/17.
 */

public interface ITribeDetailView extends IBaseView{

    final int NODATA=409;

    void onSuccess(PostDataResult postDataResult);
    void onAppendData(PostDataResult postDataResult);
    void onError(int errorcode,String message);
    void onTribeClose(CloseTribeResponse closeTribeResponse);
    void onSuccessFullJoinTribe();
    void onJoinError(ErrorModel errorModel);
}
