package com.smf.daffodil.smfapp.rest_api;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;



/**
 * Created by Administrator on 15-Apr-16.
 * al-oula-radio-app-android
 */
public abstract class StringRequestCallback<T> implements Response.Listener<String>, Response.ErrorListener {
    private Gson mGson = new Gson();
    private Class<T> clazz;

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void onSuccessResponse(T responseData) {
        onRestResponse(null, responseData);
    }




    @Override
    public void onErrorResponse(VolleyError error) {
        ErrorModel eModel = null;

        if (error.networkResponse != null && error.networkResponse.data != null) {
            try {
                if (error.networkResponse.headers != null) {
                    String errorString = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));
                    Log.e("Request Call back",errorString);
                    Gson gsonError = new Gson();
                    eModel = (ErrorModel) gsonError.fromJson(errorString, ErrorModel.class);
                    eModel.setStatus_code(error.networkResponse.statusCode);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        onErrorResponse(error, eModel);
    }

    @Override
    public void onResponse(String response) {
        try {
            T object;
            Log.e("Response",response);

            if (clazz != String.class) {
                object = mGson.fromJson(response, clazz);
            } else {
                object = (T) response;
            }
            onSuccessResponse(object);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            onErrorResponse(new VolleyError("Json syntax error."));
        } catch (Exception e) {
            e.printStackTrace();
            onErrorResponse(new VolleyError("Some error occurred."));
        }
    }

    public abstract void onRestResponse(Exception e, T result);
    public abstract void onErrorResponse(Exception e, ErrorModel result);
}
