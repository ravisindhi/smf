package com.smf.daffodil.smfapp.post_and_update.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.post_and_update.model.PostAndUpdateLocation;
import com.smf.daffodil.smfapp.post_and_update.model.PostAndUpdateResponse;

/**
 * Created by daffodil on 4/2/17.
 */

public interface IPostAndUpdateView extends IBaseView {

    final int CONTENT_EMPTY_ERROR=102;

    void onError(int errorcode,String message);
    void onSuccess(PostAndUpdateResponse respopnse);

}
