package com.smf.daffodil.smfapp.about_you.fragments;


import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.utils.AppUtils;
import com.smf.daffodil.smfapp.utils.ImageCompression;
import com.smf.daffodil.smfapp.utils.ImageCompressionCallback;
import com.smf.daffodil.smfapp.utils.ImageCompressionHelper;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutYouProfilePictureFragment extends BaseFragment {


    SessionManager sessionManager;
    @BindView(R.id.blur_image_view)
    ImageView imageViewBlur;

    @BindView(R.id.rounded_image_view)
    CircularImageView imageViewRounded;

    @BindView(R.id.btn_take_photo)
    Button buttonTakePhoto;

    PermissionListener permissionlistener;


    public AboutYouProfilePictureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sessionManager=new SessionManager(getActivity());
        return inflater.inflate(R.layout.fragment_about_you_profile_picture, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setImage();

    }

    @OnClick(R.id.btn_take_photo)
    public void takePicture()
    {

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openImagePicker();
            }



            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }


        };

        new TedPermission(getActivity())
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();


    }

    private void openImagePicker()
    {
        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(getActivity().getApplicationContext())
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected uri
                        showLoader("please wait...");

                        final Uri finalUri = uri;
                        compressImage(uri.toString(), uri.toString(), new ImageCompressionCallback() {
                            @Override
                            public void onCompressionComplete() {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AboutYouRequest.setProfile_uri(finalUri);
                                        Bitmap bitmap = null;
                                        try {
                                            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), finalUri);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        imageViewBlur.setImageBitmap(AppUtils.blurRenderScript(getActivity(),bitmap,20));
                                        imageViewRounded.setImageBitmap(bitmap);
                                        hideLoader();
                                    }
                                });


                            }
                        });



                    }
                })
                .create();

        tedBottomPicker.show(getFragmentManager());
    }


    private void setImage()
    {
        LoginResponse loginResponse=sessionManager.getUserData();
        if(loginResponse!=null)
        {
            ImageUtils.setprofile(imageViewRounded,loginResponse.getProfile().getProfileImage(),getActivity());
            //Glide.with(getActivity()).load(loginResponse.getProfile().getProfileImage()).error(R.drawable.dummy_rounded).into(imageViewRounded);
            Glide.with(getActivity()).load(loginResponse.getProfile().getProfileImage()).error(R.drawable.dummy_full).bitmapTransform(new BlurTransformation(getActivity())).into(imageViewBlur);
        }
    }

    public static void compressImage(final String source, final String dest, final ImageCompressionCallback callback) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ImageCompressionHelper compressionHelper = new ImageCompressionHelper();
                try {
                    String url = compressionHelper.compressImage(source, dest);
                } catch (OutOfMemoryError error) {

                } catch (Exception ignored) {
                }
                if (callback != null) {
                    callback.onCompressionComplete();
                }
            }
        });
        thread.start();
    }



}
