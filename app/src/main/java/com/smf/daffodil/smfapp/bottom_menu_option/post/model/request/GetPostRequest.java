package com.smf.daffodil.smfapp.bottom_menu_option.post.model.request;

/**
 * Created by daffodil on 3/2/17.
 */

public class GetPostRequest {

   private String skip;
   private String limit;
    private Double[] cordinates;
    private Double[] range;
    Boolean isFavorite=false;
    Boolean isTranding=false;
    Boolean isLocal=false;
    Boolean isNearBy=false;
    Boolean isLocation=false;


    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public Double[] getCordinates() {
        return cordinates;
    }

    public void setCordinates(Double[] cordinates) {
        this.cordinates = cordinates;
    }

    public Double[] getRange() {
        return range;
    }

    public void setRange(Double[] range) {
        this.range = range;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public Boolean getTranding() {
        return isTranding;
    }

    public void setTranding(Boolean tranding) {
        isTranding = tranding;
    }

    public Boolean getLocal() {
        return isLocal;
    }

    public void setLocal(Boolean local) {
        isLocal = local;
    }

    public Boolean getNearBy() {
        return isNearBy;
    }

    public void setNearBy(Boolean nearBy) {
        isNearBy = nearBy;
    }

    public Boolean getLocation() {
        return isLocation;
    }

    public void setLocation(Boolean location) {
        isLocation = location;
    }
}
