package com.smf.daffodil.smfapp.about_you.presenter;

import android.content.Context;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.fragments.view.IAboutFragmentView;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.about_you.model.ImageUploadResponse;
import com.smf.daffodil.smfapp.about_you.model.UserProfileDataRequest;
import com.smf.daffodil.smfapp.about_you.view.IAboutYouView;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.common.ApiConfig;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.FilePart;
import com.smf.daffodil.smfapp.rest_api.RequestCallback;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

import java.io.File;

/**
 * Created by daffodil on 25/1/17.
 */

public class IAboutYouCompl implements IAboutYouPresenter{

    IAboutYouView iAboutYouView;
    Context context;
    SessionManager sessionManager;
    final String IMAGE_TYPE="profilePhotos";

    public IAboutYouCompl(IAboutYouView iAboutYouView, Context context) {
        this.iAboutYouView = iAboutYouView;
        this.context=context;
        sessionManager=new SessionManager(context);
    }

    @Override
    public void checkDataValidation(AboutYouRequest aboutYouRequest) {

        if(validateAboutYouForm(aboutYouRequest))
        {
            uploadImageToS3(aboutYouRequest);
        }

    }

    private boolean validateAboutYouForm(AboutYouRequest aboutYouRequest) {
        /*boolean _mValidModel = true;

        if (aboutYouRequest.getFirstname() == null || AboutYouRequest.getFirstname().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        } else if (AboutYouRequest.getLastname() == null || AboutYouRequest.getLastname().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.LAST_NAME_ERROR, "");
        }  else if (AboutYouRequest.getHandle() == null || AboutYouRequest.getHandle().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.HANDLE_ERROR, "");
        }
        return _mValidModel;*/

        boolean _mValidModel = true;
        if (AboutYouRequest.getFirstname() == null || AboutYouRequest.getFirstname().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        } else if (AboutYouRequest.getLastname() == null || AboutYouRequest.getLastname().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        } /*else if (AboutYouRequest.getMobile() == null || AboutYouRequest.getMobile().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.MOBILE_NUMBER_ERROR, "");
        } else if (AboutYouRequest.getMobile().trim().length() < 10) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithSnakbar(IAboutFragmentView.MOBILE_INVALID_ERROR, "");
        }*/ else if (AboutYouRequest.getHandle() == null || AboutYouRequest.getHandle().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        } else if (AboutYouRequest.getDate_of_birth() == null || AboutYouRequest.getDate_of_birth().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        }else if (AboutYouRequest.getRelation_status().trim()== null || AboutYouRequest.getRelation_status().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        } else if (AboutYouRequest.getGender().trim()== null || AboutYouRequest.getGender().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        } else if (AboutYouRequest.getIntroduction() == null || AboutYouRequest.getIntroduction().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        }else if (AboutYouRequest.getCity() == null || AboutYouRequest.getCity().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        }else if (AboutYouRequest.getState() == null || AboutYouRequest.getState().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        }else if (AboutYouRequest.getZip() == null || AboutYouRequest.getZip().trim().length() <= 0) {
            _mValidModel = false;
            iAboutYouView.onError(IAboutFragmentView.MISSING_ERROR, "");
        }
        return _mValidModel;



    }


    private void uploadImageToS3(AboutYouRequest aboutYouRequest)
    {
        iAboutYouView.showLoader(context.getString(R.string.update_profile_image_msg));
        /*Update Profile Image if changed by user*/
        if(AboutYouRequest.getProfile_uri()!=null && AboutYouRequest.getProfile_uri().toString().length()>0)
        {
            if(iAboutYouView.onCheckNetworkConnection())
            {


                SMFApplication.getRestClient().uploadImageRequest(new RequestCallback<ImageUploadResponse>() {
                    @Override
                    public void onRestResponse(Exception e, ImageUploadResponse result) {
                        if(e==null && result!=null)
                        {
                            iAboutYouView.hideLoader();
                            AboutYouRequest.setProfileUrl(result.getFilePath());
                            updateUserData();
                            //Toast.makeText(context,"image upload success",Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        iAboutYouView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                        iAboutYouView.hideLoader();

                    }
                },new FilePart(new File(AboutYouRequest.getProfile_uri().getPath()),"image/jpg"),IMAGE_TYPE,sessionManager.getUserData().get_id());

            }else
            {
                iAboutYouView.hideLoader();
                iAboutYouView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
            }
        }else
        {

            updateUserData();
        }

    }

    /*Update profile data*/
    private void updateUserData()
    {
        iAboutYouView.showLoader(context.getString(R.string.update_profile_data_msg));
        if(iAboutYouView.onCheckNetworkConnection())
        {
            iAboutYouView.showLoader(context.getString(R.string.update_profile_data_msg));
            UserProfileDataRequest request=setRequestData();

                SMFApplication.getRestClient().doUpdateProfile(new StringRequestCallback<UserProfileDataRequest>() {
                    @Override
                    public void onRestResponse(Exception e, UserProfileDataRequest result) {

                        if(e==null && result!=null)
                        {
                            iAboutYouView.hideLoader();
                            saveNewUpdatedData();
                            //Toast.makeText(context,"new data upload successful",Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onErrorResponse(Exception e, ErrorModel result) {
                        iAboutYouView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                        iAboutYouView.hideLoader();
                    }
                },request);
            }else
            {
                iAboutYouView.hideLoader();
                iAboutYouView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));

            }


    }

    private UserProfileDataRequest setRequestData()
    {
        UserProfileDataRequest request=new UserProfileDataRequest();
        request.setFirstname(AboutYouRequest.getFirstname());
        request.setLastName(AboutYouRequest.getLastname());
        request.setPhoneNumber(AboutYouRequest.getMobile());
        request.setCity(AboutYouRequest.getCity());
        request.setState(AboutYouRequest.getState());
        request.setZip(AboutYouRequest.getZip());
        request.setProfileImage(AboutYouRequest.getProfileUrl());
        request.setHandle(AboutYouRequest.getHandle());
        request.setIntro(AboutYouRequest.getIntroduction());
        request.setGender(AboutYouRequest.getGender());
        request.setRelationship(AboutYouRequest.getRelation_status());
        request.setDob(AboutYouRequest.getDate_of_birth());
        return request;


    }

    /*save new data locally*/
    private void saveNewUpdatedData()
    {
        iAboutYouView.showLoader(context.getString(R.string.saving_data_msg));
        if(iAboutYouView.onCheckNetworkConnection())
        {



            SMFApplication.getRestClient().getUserProfileData(new StringRequestCallback<LoginResponse>() {
                @Override
                public void onRestResponse(Exception e, LoginResponse result) {

                    if(e==null && result!=null)
                    {
                        iAboutYouView.hideLoader();
                        sessionManager.saveUserData(result);
                        iAboutYouView.onSuccess(null);
                        //Toast.makeText(context,"new data savededdddddddddddddddddd successful",Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iAboutYouView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                    iAboutYouView.hideLoader();
                }
            });
        }else
        {
            iAboutYouView.hideLoader();
            iAboutYouView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));

        }

    }

}
