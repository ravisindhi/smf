package com.smf.daffodil.smfapp.splash.view;

/**
 * Created by daffodil on 23/1/17.
 */

public interface iSplashView {
     void onTimeComplete();
}
