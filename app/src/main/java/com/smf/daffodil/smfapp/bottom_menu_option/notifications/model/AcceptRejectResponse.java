package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model;

import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.OwnerId;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;

/**
 * Created by daffodil on 24/2/17.
 */

public class AcceptRejectResponse {



    private String _id;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
