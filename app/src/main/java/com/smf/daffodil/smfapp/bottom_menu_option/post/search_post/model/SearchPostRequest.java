package com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.model;

/**
 * Created by daffodil on 10/2/17.
 */

public class SearchPostRequest {

    private String limit;
    private String skip;
    private String search_value="";
    private String groupId;

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getSearch_value() {
        return search_value;
    }

    public void setSearch_value(String search_value) {
        this.search_value = search_value;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
