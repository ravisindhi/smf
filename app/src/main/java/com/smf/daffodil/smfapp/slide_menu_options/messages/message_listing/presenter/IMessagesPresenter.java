package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.presenter;


import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.request.AllMessagesRequest;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public interface IMessagesPresenter {

    void getAllMessages(AllMessagesRequest allMessagesRequest, boolean isLoadMore);
}
