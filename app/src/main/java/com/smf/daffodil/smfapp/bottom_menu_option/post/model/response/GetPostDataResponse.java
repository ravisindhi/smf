package com.smf.daffodil.smfapp.bottom_menu_option.post.model.response;

import android.net.Uri;
import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by daffodil on 3/2/17.
 */

public class GetPostDataResponse implements Serializable{

    private String updatedAt;

    private String visible;

    private Integer commentsCount=0;

    private int postType;

    private String content;

    @Nullable
    private PostAuthor author;

    private List<String> likedBy;

    private List<String> dislikedBy;

    private String _id;

    private PostLocation location;

    private Boolean hidden;

    private String __v;

    private String removed;
    private String imageUrl;

    private String videoUrl;

    private List<String> peaceBy;

    private String createdAt;

    private String permissions;

    private Uri video_thumbnail_uri;

    private PostAuthor realOwner;

    private String caption;

    public int getPostType() {
        return postType;
    }

    public void setPostType(int postType) {
        this.postType = postType;
    }

    public Uri getVideo_thumbnail_uri() {
        return video_thumbnail_uri;
    }

    public void setVideo_thumbnail_uri(Uri video_thumbnail_uri) {
        this.video_thumbnail_uri = video_thumbnail_uri;
    }



    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }



    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public PostAuthor getAuthor ()
    {
        return author;
    }

    public void setAuthor (PostAuthor author)
    {
        this.author = author;
    }

    public List<String> getLikedBy ()
    {
        return likedBy;
    }

    public void setLikedBy (List<String> likedBy)
    {
        this.likedBy = likedBy;
    }

    public List<String> getDislikedBy ()
    {
        return dislikedBy;
    }

    public void setDislikedBy (List<String> dislikedBy)
    {
        this.dislikedBy = dislikedBy;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public PostLocation getLocation ()
    {
        return location;
    }

    public void setLocation (PostLocation location)
    {
        this.location = location;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public Boolean getHidden ()
    {
        return hidden;
    }

    public void setHidden (Boolean hidden)
    {
        this.hidden = hidden;
    }

    public String get__v ()
    {
        return __v;
    }

    public void set__v (String __v)
    {
        this.__v = __v;
    }

    public String getRemoved ()
    {
        return removed;
    }

    public void setRemoved (String removed)
    {
        this.removed = removed;
    }

    public List<String> getPeaceBy ()
    {
        return peaceBy;
    }

    public void setPeaceBy (List<String> peaceBy)
    {
        this.peaceBy = peaceBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updatedAt = "+updatedAt+", content = "+content+", author = "+author+", likedBy = "+likedBy+", dislikedBy = "+dislikedBy+", _id = "+_id+", location = "+location+", createdAt = "+createdAt+", hidden = "+hidden+", __v = "+__v+", removed = "+removed+", peaceBy = "+peaceBy+"]";
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public PostAuthor getRealOwner() {
        return realOwner;
    }

    public void setRealOwner(PostAuthor realOwner) {
        this.realOwner = realOwner;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }
}
