package com.smf.daffodil.smfapp.bottom_menu_option.post.share_post;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.CommentsActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.adapter.CommentsAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.model.CommentsResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.presenter.ICommentCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.presenter.ICommentsPresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.PostData;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.presenter.IShareCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.presenter.ISharePresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.view.IShareView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.utils.DateTimeAgo;
import com.smf.daffodil.smfapp.utils.ImageUtils;
import com.smf.daffodil.smfapp.view.TextViewExpandableAnimation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SharePostActivity extends BaseActivity implements IShareView {

    private ISharePresenter iSharePresenter;

    public static final int POST_TYPE_SHARE = 1;
    public static final int POST_TYPE_IMAGE = 2;
    public static final int POST_TYPE_TEXT = 3;
    public static final int POST_TYPE_VIDEO = 4;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.txt_name)
    TextView textViewName;

    @BindView(R.id.txt_time)
    TextView textViewTime;

    @BindView(R.id.txtview_post_content)
    TextViewExpandableAnimation textViewPostContent;

    @BindView(R.id.rounded_image_view)
    CircularImageView imageViewProfile;

    @BindView(R.id.post_image)
    ImageView imageViewPost;

    @BindView(R.id.video_thumbnail_image)
    ImageView imageViewVideoThmbnail;

    @BindView(R.id.txt_peace_by_count)
    TextView textViewPeaceByCount;

    @BindView(R.id.txt_comment_by_count)
    TextView textViewTxtCommentByCount;

    @BindView(R.id.emojiView)
    LinearLayout emojiLikeView;

    @BindView(R.id.hot_item)
    LinearLayout hot_view;

    @BindView(R.id.like_button)
    LinearLayout likeButton;

    @BindView(R.id.dislike_button)
    LinearLayout dislikeButton;

    @BindView(R.id.peace_button)
    LinearLayout peaceButton;

    @BindView(R.id.txt_liked)
    TextView textViewLikedText;

    @BindView(R.id.txt_disliked)
    TextView textViewDisLikedText;

    @BindView(R.id.txt_peace)
    TextView textViewPeace;

    @BindView(R.id.dot_btn)
    ImageButton imageButtonDot;

    @BindView(R.id.btn_report)
    LinearLayout ButtonReport;

    @BindView(R.id.share_post_layout)
    LinearLayout sharePostView;

    @BindView(R.id.owner_pic)
    ImageView imageViewOwnerPic;

    @BindView(R.id.txt_owner_name)
    TextView textViewOwnerName;

    @BindView(R.id.txt_owner_content)
    TextView textViewOwnerContent;

    @BindView(R.id.post_image_owner)
    ImageView imageViewOwnerPostImage;

    @BindView(R.id.txt_time_owner)
    TextView textViewOwnerPostTime;

    @BindView(R.id.et_comment)
    EditText editTextCommentBox;

    @BindView(R.id.post_comment_button)
    TextView textViewPostCommentButton;

    @BindView(R.id.txt_comments)
    TextView textViewComments;

    @BindView(R.id.txtview_post_content_normal)
    TextView textviewContentNormal;

    private GetPostDataResponse postData;
    private LinearLayout post_share_view;
    private LinearLayout post_image_view;
    private LinearLayout post_txt_view;
    private FrameLayout post_video_view;
    List<CommentsResponse> list=new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_post);

        iSharePresenter=new IShareCompl(SharePostActivity.this,this);

        ButterKnife.bind(this);
        setupToolbar(toolbar,R.layout.toolbar_home_layout);
        setupToolBarName("Share",true,false);


        post_share_view = (LinearLayout) findViewById(R.id.post_share_view);
        post_image_view = (LinearLayout) findViewById(R.id.post_image_view3);
        post_txt_view = (LinearLayout) findViewById(R.id.post_txt_view);
        post_video_view = (FrameLayout) findViewById(R.id.post_video_view);
        initdata();

        post_share_view.setVisibility(View.GONE);
        imageButtonDot.setVisibility(View.GONE);

        textViewPostCommentButton.setText("Share");
        editTextCommentBox.setHint("Enter Caption");

    }

    private void initdata()
    {
        postData= (GetPostDataResponse) getIntent().getSerializableExtra("post_model");
        if (postData.getRealOwner() !=null ){
            postData.setPostType(POST_TYPE_SHARE);
        }
        else if (postData.getImageUrl() != null && postData.getImageUrl().length() > 0)
            postData.setPostType(POST_TYPE_IMAGE);
        else if (postData.getVideoUrl() != null && postData.getVideoUrl().length() > 0) {
            // addThumbnail(postData);
            postData.setPostType(POST_TYPE_VIDEO);
        }
        else
            postData.setPostType(POST_TYPE_TEXT);


        if(postData.getContent().length()>150)
        {
            textViewPostContent.setExpandLines(3);
            textViewPostContent.setVisibility(View.VISIBLE);
            textviewContentNormal.setVisibility(View.GONE);
            textViewPostContent.setText(postData.getContent());
        }
        else{
            textViewPostContent.setVisibility(View.GONE);
            textviewContentNormal.setVisibility(View.VISIBLE);
            textviewContentNormal.setText(postData.getContent());
        }


        switch (postData.getPostType()) {
            case PostData.POST_TYPE_IMAGE:
                post_image_view.setVisibility(View.VISIBLE);
                post_share_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.GONE);
                textViewPostContent.setText(postData.getContent());
                Glide.with(this).load(postData.getImageUrl()).placeholder(R.drawable.ic_thumbnil_image).thumbnail(0.1f).error(R.drawable.dummy_rounded).into(imageViewPost);

                break;
            case PostData.POST_TYPE_SHARE:
                post_share_view.setVisibility(View.VISIBLE);
                post_image_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.GONE);
                setSharePostData(postData);

                break;
            case PostData.POST_TYPE_TEXT:
                post_share_view.setVisibility(View.GONE);
                post_image_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.GONE);
                textViewPostContent.setText(postData.getContent());
                break;
            case PostData.POST_TYPE_VIDEO:
                //Glide.with(context).load(Uri.parse(postData.getVideoUrl())).asBitmap().error(R.drawable.dummy_rounded).into(holder.imageViewProfile);
                post_share_view.setVisibility(View.GONE);
                post_image_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.VISIBLE);
                textViewPostContent.setText(postData.getContent());
                break;
        }


        ImageUtils.setprofile(imageViewProfile, postData.getAuthor().getProfile().getProfileImage(), this);
        textViewName.setText(postData.getAuthor().getProfile().getFirstName() + " " + postData.getAuthor().getProfile().getLastName());
        String date = postData.getCreatedAt().split("\\.")[0];
        textViewTime.setText(DateTimeAgo.getTimeAgo2(date + "Z"));

        textViewPeaceByCount.setText(String.valueOf(postData.getDislikedBy().size()
                + postData.getLikedBy().size()
                + postData.getPeaceBy().size()));

            /*set action view*/
        textViewLikedText.setText(String.valueOf(postData.getLikedBy().size()));
        textViewDisLikedText.setText(String.valueOf(postData.getDislikedBy().size()));
        textViewPeace.setText(String.valueOf(postData.getPeaceBy().size()));
        if(postData.getCommentsCount() !=null)
            textViewTxtCommentByCount.setText(String.valueOf(postData.getCommentsCount()));
        else
            textViewTxtCommentByCount.setText("0");


    }

    @OnClick(R.id.post_comment_button)
    public void sharePost()
    {
        SharePostRequest request=new SharePostRequest();
        request.setPost_id(postData.get_id());
        request.setCaption(editTextCommentBox.getText().toString().trim());
        if(postData.getLocation()!=null)
        request.setCordinates(new String[]{postData.getLocation().getCoordinates().get(0),
        postData.getLocation().getCoordinates().get(1)});
        else
        request.setCordinates(new String[]{"0.0","0.0"});
        iSharePresenter.doSharePost(request);
    }

    private void setSharePostData(GetPostDataResponse postData){

        if(postData.getCaption()!=null && postData.getCaption().length()>0){


            if(postData.getContent().length()>150)
            {
                textViewPostContent.setExpandLines(3);
                textViewPostContent.setVisibility(View.VISIBLE);
                textviewContentNormal.setVisibility(View.GONE);
                textViewPostContent.setText(postData.getCaption());
            }

            else{
                textViewPostContent.setVisibility(View.GONE);
                textviewContentNormal.setVisibility(View.VISIBLE);
                textviewContentNormal.setText(postData.getCaption());
            }

        }else{
            textViewPostContent.setVisibility(View.GONE);
            textviewContentNormal.setVisibility(View.GONE);
        }


        ImageUtils.setprofile(imageViewOwnerPic,postData.getRealOwner().getProfile().getProfileImage(),this);
        textViewOwnerContent.setText(postData.getContent());
        textViewOwnerName.setText(postData.getRealOwner().getProfile().getFirstName()+" "+
                postData.getRealOwner().getProfile().getLastName());


        String date = postData.getRealOwner().getCreatedAt().split("\\.")[0];
        textViewOwnerPostTime.setText(DateTimeAgo.getTimeAgo2(date + "Z"));

        if(postData.getImageUrl()!=null && postData.getImageUrl().length()>0){
            imageViewOwnerPostImage.setVisibility(View.VISIBLE);
            ImageUtils.setprofile(imageViewOwnerPostImage,postData.getImageUrl(),this);
        }else {
            imageViewOwnerPostImage.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }


    @Override
    public void onSharePost(PostActionResponse response) {
        Intent i=new Intent();
        setResult(101,i);
        finish();
        Toast.makeText(this,"Post Shared successfully",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(int errorcode, String message) {
        switch (errorcode)
        {
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(),message,this);
                break;
        }
    }
}
