package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

import java.io.Serializable;

/**
 * Created by daffodil on 11/2/17.
 */

public class AllTribesResponse implements Serializable{

    private Result[] result;

    private String totalTribescount="";

    public Result[] getResult ()
    {
        return result;
    }

    public void setResult (Result[] result)
    {
        this.result = result;
    }

    public String getTotalTribescount ()
    {
        return totalTribescount;
    }

    public void setTotalTribescount (String totalTribescount)
    {
        this.totalTribescount = totalTribescount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", totalTribescount = "+totalTribescount+"]";
    }
}
