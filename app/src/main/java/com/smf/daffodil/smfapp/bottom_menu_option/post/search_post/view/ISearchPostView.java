package com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;

/**
 * Created by daffodil on 10/2/17.
 */

public interface ISearchPostView extends IBaseView {

    static final int NO_DATA=401;

    void onSuccess(PostDataResult postDataResult);
    void onAppendData(PostDataResult postDataResult);
    void onError(int errorcode,String message);
}
