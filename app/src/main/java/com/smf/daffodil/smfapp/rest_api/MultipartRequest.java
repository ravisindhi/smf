package com.smf.daffodil.smfapp.rest_api;


import com.android.volley.VolleyLog;


import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daffolap-164 on 25/4/16.
 */
class MultipartRequest<T,K> extends GsonRequest<T, K> {

    private MultipartEntity entity = new MultipartEntity();
    private Map<String, FilePart> mFilePartMap = new HashMap<>();
    private Map<String, String> mStringPartMap = new HashMap<>();


    public MultipartRequest(int method, String url, RequestCallback<K> listener, Class<K> responseType) {
        super(method, url, responseType, null, listener);
    }


    private void buildMultiPartEntity() {
        try {
            for (Map.Entry<String, FilePart> entry : mFilePartMap.entrySet()) {
                entity.addPart(entry.getKey(), new FileBody(entry.getValue().getFile(), entry.getValue().getMimeType()));
            }
            for (Map.Entry<String, String> entry : mStringPartMap.entrySet()) {
                entity.addPart(entry.getKey(), new StringBody(entry.getValue()));
            }

        } catch (UnsupportedEncodingException e) {
            VolleyLog.e("UnsupportedEncodingException");
        }
    }

    public void putFilePart(String key, FilePart value) {
        mFilePartMap.put(key, value);
    }

    public void putStringPart(String key, String value) {
        mStringPartMap.put(key, value);
    }

    @Override
    public String getBodyContentType() {
        return entity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() {
        buildMultiPartEntity();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            entity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

//    @Override
//    public Response<K> parseNetworkResponse(NetworkResponse response) {
//        try {
//            String json = new String(response.data,
//                    HttpHeaderParser.parseCharset(response.headers));
//            //--return success response with a cache entry for this response--
//            Cache.Entry entry = HttpHeaderParser.parseCacheHeaders(response, false);
//            return Response.success(json, entry);
//        } catch (UnsupportedEncodingException e) {
//            ParseError parseError = new ParseError(response);
//            parseError.initCause(e);
//            VolleyLog.e("Error Unsupported encoding", e);
//            return Response.error(parseError);
//        }
//    }
}
