package com.smf.daffodil.smfapp.common.presenter;

import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;

/**
 * Created by daffodil on 22/2/17.
 */

public interface IActionPresenter {

    void updatePost(UpdatePostRequest request, int pos);
    void updateGroupPost(UpdatePostRequest request, int pos);
    void doHidePost(String postid,int position);
    void doPostReport(String postid);
    void doBlockUser(String userid,String handle);
    void doSharePost(SharePostRequest request);
    void doAddComment(String postid,String comment_txt,int position,String from);
}
