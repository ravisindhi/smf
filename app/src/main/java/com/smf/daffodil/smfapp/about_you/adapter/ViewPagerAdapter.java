package com.smf.daffodil.smfapp.about_you.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.smf.daffodil.smfapp.about_you.fragments.AboutYouFragmentDetail;
import com.smf.daffodil.smfapp.about_you.fragments.AboutYouProfilePictureFragment;
import com.smf.daffodil.smfapp.about_you.fragments.AboutYouReviewProfileFragment;

/**
 * Created by daffodil on 24/1/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 3;
    private Context mContext;
    private int[] mResources;

    public ViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return new AboutYouFragmentDetail();
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return new AboutYouProfilePictureFragment();
            case 2: // Fragment # 0 - This will show FirstFragment different title
                return new AboutYouReviewProfileFragment();
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
