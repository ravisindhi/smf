package com.smf.daffodil.smfapp.bottom_menu_option.members.model.members;

import java.io.Serializable;

/**
 * Created by daffodil on 13/2/17.
 */

public class SavedLocations implements Serializable{

    private String[] latLng;

    private String location;

    private String name;

    public String[] getLatLng ()
    {
        return latLng;
    }

    public void setLatLng (String[] latLng)
    {
        this.latLng = latLng;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [latLng = "+latLng+", location = "+location+", name = "+name+"]";
    }
}
