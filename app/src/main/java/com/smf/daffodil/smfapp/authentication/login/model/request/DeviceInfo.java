package com.smf.daffodil.smfapp.authentication.login.model.request;

/**
 * Created by daffodil on 17/2/17.
 */

public class DeviceInfo {

    private String deviceToken;
    private String deviceType;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceType() {
        return "android";
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
