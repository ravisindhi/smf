package com.smf.daffodil.smfapp.bottom_menu_option.post.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daffodil on 27/1/17.
 */

public final class PostDataGet {



    public static List<PostData> getPostData()
    {
        List<PostData> list=new ArrayList<>();
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_SHARE));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_IMAGE));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_TEXT));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_SHARE));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_IMAGE));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_SHARE));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_TEXT));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_IMAGE));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_TEXT));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_IMAGE));
        list.add(new PostData("Ravi lalwani","June 24, 2016 at 1:40pm",null,null,null,null,PostData.POST_TYPE_SHARE));
        return list;
    }
}
