package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.presenter;

import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.AddMemberRequest;

/**
 * Created by daffodil on 15/2/17.
 */

public interface ITribeMemberPresneter {

    void removeTribeMember(JoinTribeRequest joinTribeRequest);
    void searchMember(String name);
    void addTribeMember(AddMemberRequest request);
}
