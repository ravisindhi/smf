package com.smf.daffodil.smfapp.bottom_menu_option.post.share_post.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;

/**
 * Created by daffodil on 21/2/17.
 */

public interface IShareView extends IBaseView{
    void onSharePost(PostActionResponse response);
    void onError(int errorcode,String message);
}
