package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

/**
 * Created by daffodil on 11/2/17.
 */

public class TribePassword {

    private String bcrypt;

    public String getBcrypt ()
    {
        return bcrypt;
    }

    public void setBcrypt (String bcrypt)
    {
        this.bcrypt = bcrypt;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [bcrypt = "+bcrypt+"]";
    }
}
