package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

import java.io.Serializable;

/**
 * Created by daffodil on 11/2/17.
 */

public class Result implements Serializable{

    private String updatedAt;

    private String desc;

    private String _id;

    private String imageUrl;

    private String createdAt;

    private OwnerId ownerId;

    private String name;

    private String __v;

    private String privateGroup;

    private TribesMembers[] members;

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getDesc ()
    {
        return desc;
    }

    public void setDesc (String desc)
    {
        this.desc = desc;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getImageUrl ()
    {
        return imageUrl;
    }

    public void setImageUrl (String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public OwnerId getOwnerId ()
    {
        return ownerId;
    }

    public void setOwnerId (OwnerId ownerId)
    {
        this.ownerId = ownerId;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String get__v ()
    {
        return __v;
    }

    public void set__v (String __v)
    {
        this.__v = __v;
    }

    public String getPrivateGroup ()
    {
        return privateGroup;
    }

    public void setPrivateGroup (String privateGroup)
    {
        this.privateGroup = privateGroup;
    }

    public TribesMembers[] getMembers ()
    {
        return members;
    }

    public void setMembers (TribesMembers[] members)
    {
        this.members = members;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updatedAt = "+updatedAt+", desc = "+desc+", _id = "+_id+", imageUrl = "+imageUrl+", createdAt = "+createdAt+", ownerId = "+ownerId+", name = "+name+", __v = "+__v+", privateGroup = "+privateGroup+", members = "+members+"]";
    }
}
