
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginToken {

    @SerializedName("hashedToken")
    @Expose
    private String hashedToken;
    @SerializedName("when")
    @Expose
    private String when;

    public String getHashedToken() {
        return hashedToken;
    }

    public void setHashedToken(String hashedToken) {
        this.hashedToken = hashedToken;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

}
