package com.smf.daffodil.smfapp.slide_menu_options.history.presenter;

import android.content.Context;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.history.model.GetHistoryRequest;
import com.smf.daffodil.smfapp.slide_menu_options.history.view.IHistoryView;

import java.util.Arrays;

/**
 * Created by daffolap on 7/2/17.
 */

public class IHistoryPresenterCompl implements IHistoryPresenter {

    IHistoryView iHistoryView;
    Context context;

    public IHistoryPresenterCompl(IHistoryView iHistoryView, Context context){
        this.iHistoryView=iHistoryView;
        this.context=context;

    }
    @Override
    public void getHistory(GetHistoryRequest request) {
        startGetHistoryService(request);
    }

    private void startGetHistoryService(final GetHistoryRequest request) {

        if(request.getSkip().equals("0"))
            iHistoryView.showLoader(context.getString(R.string.please_wait_message));

        if (iHistoryView.onCheckNetworkConnection()) {


            SMFApplication.getRestClient().getHistory(new StringRequestCallback<PostDataResult>() {
                @Override
                public void onRestResponse(Exception e, PostDataResult result) {
                    if (e == null && result != null) {

                        if(request.getLimit().equals("10")){
                            if (result.getResult().size() > 0)
                                iHistoryView.onSuccess(result);
                            else
                                iHistoryView.onError(IAllPostView.NODATA, "No data found !!");


                        }else {
                            iHistoryView.onAppendData(result);
                        }
                        iHistoryView.hideLoader();
                    }
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    try {
                        iHistoryView.hideLoader();
                        e.printStackTrace();
                        iHistoryView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    }catch (Exception ex)
                    {
                        Toast.makeText(context,"No data found !!",Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                    // iChangePasswordView.onErrorWithMessage(IBaseView.NETWORK_ERROR,result.getMessage());

                }
            },request);

        }else {
            iHistoryView.hideLoader();
            iHistoryView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }
}
