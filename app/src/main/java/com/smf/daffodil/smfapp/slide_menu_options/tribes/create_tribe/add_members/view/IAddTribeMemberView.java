package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.TribesMemberSearchResponse;

/**
 * Created by daffodil on 22/2/17.
 */

public interface IAddTribeMemberView extends IBaseView{

    void onError(int errorcode,String msg);
    void onGetMember(TribesMemberSearchResponse result);
}
