package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.adapter.TribesAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.search_tribe.SearchTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.presenter.ITribesHomeCompl;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.presenter.ITribesHomePresenter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.view.ITribesHomeView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.CreateTribeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.TribeDetailAdminActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;
import com.smf.daffodil.smfapp.utils.AppUtils;
import com.smf.daffodil.smfapp.view.PopupDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TribesHomeActivity extends BaseActivity implements ITribesHomeView, CompoundButton.OnCheckedChangeListener ,AbsListView.OnScrollListener,SwipeRefreshLayout.OnRefreshListener,PopupDialog.DilogButtonHandling {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.all_tribes_btn)
    RadioButton radioButtonAllTribes;

    @BindView(R.id.my_tribes_btn)
    RadioButton radioButtonMyTribes;

    @BindView(R.id.create_tribe_btn)
    RadioButton radioButtonCreateTribe;

    @BindView(R.id.et_search)
    EditText editTextSearch;

    @BindView(R.id.search_progress_bar)
    ProgressBar progressBarSearch;

    @BindView(R.id.grid_view)
    StaggeredGridView staggeredGridView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;


    public static final int ADMIN_TYPE=1;
    public static final int USER_TYPE=2;

    private PopupDialog popupDialog;
    private int currentTribePosition;





    AllTribesRequest allTribesRequest = new AllTribesRequest();
    ITribesHomePresenter iTribesHomePresenter;
    TribesAdapter tribesAdapter;
    List<Result> list = new ArrayList<>();
    SessionManager sessionManager;
    private int total_count;

    public static final int TRIBE_REQUEST_CODE = 120;
    /*load more*/
    private View footerview;

    private static final int SEARCH_TEXT_CHANGED = 101;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tribes_home);

        ButterKnife.bind(this);
        iTribesHomePresenter = new ITribesHomeCompl(this, TribesHomeActivity.this);
        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("TRIBES", true, false);

        sessionManager = new SessionManager(this);
        radioButtonAllTribes.setOnCheckedChangeListener(this);
        radioButtonMyTribes.setOnCheckedChangeListener(this);
        radioButtonCreateTribe.setOnCheckedChangeListener(this);

        footerview=getLayoutInflater().inflate(R.layout.load_more_layout, null);

        tribesAdapter = new TribesAdapter(this, list);


        staggeredGridView.addFooterView(footerview);
        staggeredGridView.setAdapter(tribesAdapter);
        radioButtonAllTribes.setChecked(true);
        swipeRefreshLayout.setOnRefreshListener(this);
        editTextSearch.addTextChangedListener(searchWatcher);

        staggeredGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                if(radioButtonAllTribes.isChecked() && list.get(i).getPrivateGroup().equals("true"))
                {
                    currentTribePosition=i;
                     popupDialog =new PopupDialog(TribesHomeActivity.this);
                    Bundle bundle = new Bundle();
                    bundle.putString("name", "Private Tribe");
                    bundle.putString("content", getString(R.string.join_private_tribe_msg));
                    bundle.putString("accept_text", "Yes");
                    bundle.putString("cancel_text", "No");
                    popupDialog.setArguments(bundle);
                    popupDialog.show(getFragmentManager(),"new");



                }else {
                    Intent intent=new Intent(TribesHomeActivity.this, TribeDetailAdminActivity.class);
                    intent.putExtra("tribe_name",list.get(i).getName());
                    intent.putExtra("group_id",list.get(i).get_id());
                    intent.putExtra("tribe_detail_model",list.get(i));

                    if(sessionManager.getUserData().get_id().equals(list.get(i).getOwnerId().get_id()))
                        intent.putExtra("user_type",ADMIN_TYPE);
                    else
                        intent.putExtra("user_type",USER_TYPE);
                    startActivity(intent);
                }


            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void getAllTribes() {
        allTribesRequest.setLimit("10");
        allTribesRequest.setSkip("0");
        allTribesRequest.setMyTribes("");
        iTribesHomePresenter.getAllTribes(allTribesRequest,false);
        staggeredGridView.setOnScrollListener(null);

    }

    private void getMyTribes() {
        allTribesRequest.setLimit("10");
        allTribesRequest.setSkip("0");
        allTribesRequest.setMyTribes(sessionManager.getUserData().get_id());
        iTribesHomePresenter.getAllTribes(allTribesRequest,false);

    }

    private void loadMoreAllTribes(String limit,String skip)
    {
        allTribesRequest.setLimit(limit);
        allTribesRequest.setSkip(skip);
        allTribesRequest.setMyTribes("");
        iTribesHomePresenter.getAllTribes(allTribesRequest,true);
    }

    private void loadMoreMyTribes(String limit,String skip)
    {
        allTribesRequest.setLimit(limit);
        allTribesRequest.setSkip(skip);
        allTribesRequest.setMyTribes(sessionManager.getUserData().get_id());
        iTribesHomePresenter.getAllTribes(allTribesRequest,true);
    }

    @Override
    public void onGetTribes(AllTribesResponse membersDataList) {
        list.clear();
        staggeredGridView.setOnScrollListener(this);
        footerview.setVisibility(View.GONE);
        progressBarSearch.setVisibility(View.GONE);
       total_count=Integer.parseInt(membersDataList.getTotalTribescount());
        swipeRefreshLayout.setRefreshing(false);
        list.addAll(Arrays.asList(membersDataList.getResult()));
        tribesAdapter.notifyDataSetChanged();
        if (membersDataList.getTotalTribescount().equalsIgnoreCase("0"))
            showErrorMessage(getApplicationContext(), "You didn't created any tribe yet !!", this);




    }

    @Override
    public void onGetSearchTribes(SearchTribeResponse response) {
        list.clear();
        staggeredGridView.setOnScrollListener(this);
        footerview.setVisibility(View.GONE);
        progressBarSearch.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        list.addAll(Arrays.asList(response.getResult()));
        tribesAdapter.notifyDataSetChanged();
        if (response.getResult().length==0)
            showErrorMessage(getApplicationContext(), "No Tribe Found !!", this);

    }

    @Override
    public void onAppendMoreData(AllTribesResponse membersDataList) {
        footerview.setVisibility(View.GONE);
        list.addAll(Arrays.asList(membersDataList.getResult()));
        tribesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onError(int errorCode, String message) {
        switch (errorCode) {
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(), message, this);
                break;
        }
    }

    @Override
    public void onSuccessFullJoinTribe() {
        popupDialog.dismiss();
        showErrorMessage(getApplicationContext(),"Request sent to Tribe.",this);
    }

    @Override
    public void onGetSearchResult(AllTribesResponse membersDataList) {
        progressBarSearch.setVisibility(View.GONE);
        footerview.setVisibility(View.GONE);
        if(membersDataList.getResult().length!=0){
            list.clear();
            staggeredGridView.setOnScrollListener(this);
           // total_count=Integer.parseInt(membersDataList.getTotalTribescount());
            swipeRefreshLayout.setRefreshing(false);
            list.addAll(Arrays.asList(membersDataList.getResult()));
            tribesAdapter.notifyDataSetChanged();
        }else
        {
            Toast.makeText(getApplicationContext(),"No tribes found with "+editTextSearch.getText().toString().trim()+"",Toast.LENGTH_LONG).show();

        }



    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        if (b) {
            switch (compoundButton.getId()) {
                case R.id.all_tribes_btn:
                    getAllTribes();
                    break;
                case R.id.my_tribes_btn:
                    getMyTribes();
                    break;
                case R.id.create_tribe_btn:
                    Intent i = new Intent(this, CreateTribeActivity.class);
                    overridePendingTransition(R.anim.trans_left_in, R.anim.trans_right_out);
                    startActivityForResult(i, TRIBE_REQUEST_CODE);
                    break;
            }
            compoundButton.setTag(null);
        }


    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TRIBE_REQUEST_CODE)
            //radioButtonAllTribes.setChecked(true);
    }
*/
    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                && (staggeredGridView.getLastVisiblePosition() - staggeredGridView.getHeaderViewsCount() -
                staggeredGridView.getFooterViewsCount()) >= (tribesAdapter.getCount() - 1)) {

            footerview.setVisibility(View.VISIBLE);
            String oldlimit=allTribesRequest.getLimit();

            String newSkip=String.valueOf(Integer.parseInt(oldlimit));
            String newLimit=String.valueOf(Integer.parseInt(oldlimit)+10);

            if(tribesAdapter.getCount()-1>=total_count)
            {
                footerview.setVisibility(View.GONE);
            }
            else {
                if(allTribesRequest.getMyTribes()!=null && allTribesRequest.getMyTribes().length()>0)
                {
                    loadMoreMyTribes(newLimit,newSkip);
                }else
                {
                    loadMoreAllTribes(newLimit,newSkip);
                }
            }



        }
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    @Override
    public void onRefresh() {
        if(allTribesRequest.getMyTribes()!=null && allTribesRequest.getMyTribes().length()>0)
        {
            getMyTribes();
            //radioButtonMyTribes.setChecked(true);

        }else
        {
            getAllTribes();
            //radioButtonAllTribes.setChecked(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(allTribesRequest.getMyTribes()!=null && allTribesRequest.getMyTribes().length()>0)
        {
            //getMyTribes();
            radioButtonMyTribes.setChecked(true);

        }else
        {
           // getAllTribes();
            radioButtonAllTribes.setChecked(true);
        }
    }



    private void searchTribes(String txt) {
        allTribesRequest.setLimit("10");
        allTribesRequest.setSkip("0");
        allTribesRequest.setMyTribes("");
        allTribesRequest.setSearchValue(txt);
        iTribesHomePresenter.searchTribes(allTribesRequest,false);
        staggeredGridView.setOnScrollListener(null);

    }

    /*Handle text watcher*/
    private final TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String handle_text = charSequence.toString().trim();

            progressBarSearch.setVisibility(View.VISIBLE);
            handler.removeMessages(SEARCH_TEXT_CHANGED);
            handler.sendMessageDelayed(handler.obtainMessage(SEARCH_TEXT_CHANGED, handle_text), 500);


        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String name = (String) msg.obj;

            if (name.length() > 0)
                searchTribes((String) msg.obj);
            else
            {
                if(radioButtonAllTribes.isChecked())
                    getAllTribes();
                else
                    getMyTribes();
            }
        }
    };

    @Override
    public void buttonYesClicked(String type) {

        iTribesHomePresenter.joinTribe(new JoinTribeRequest(list.get(currentTribePosition).get_id(),
                sessionManager.getUserData().get_id()));
    }

    @Override
    public void buttonNoClicked() {
        popupDialog.dismiss();
    }
}
