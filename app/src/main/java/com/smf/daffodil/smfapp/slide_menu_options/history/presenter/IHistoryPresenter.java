package com.smf.daffodil.smfapp.slide_menu_options.history.presenter;

import com.smf.daffodil.smfapp.slide_menu_options.history.model.GetHistoryRequest;

/**
 * Created by daffolap on 7/2/17.
 */

public interface IHistoryPresenter {

    void getHistory(GetHistoryRequest request);
}
