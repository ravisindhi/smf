package com.smf.daffodil.smfapp.application;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.rest_api.RestClient;
import com.smf.daffodil.smfapp.utils.SiCookieStore2;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by daffodil on 30/1/17.
 */

public class SMFApplication extends MultiDexApplication {

    public static RestClient sRestClient;
    public static SessionManager sSessionManager;
    public static SiCookieStore2 siCookieStore;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
        //HOW TO USE
        printHashKey();

         siCookieStore = new SiCookieStore2(this);



        CookieManager cookieManager = new CookieManager((CookieStore) siCookieStore, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);


    }

    public static void removeAllCookies()
    {
        siCookieStore.removeAll();
    }


    private void init() {
        sRestClient = new RestClient(this);
        sSessionManager=new SessionManager(getApplicationContext());

    }

    public static RestClient getRestClient() {
        return sRestClient;
    }
    public static SessionManager getSessionManager() {
        return sSessionManager;
    }

    public void printHashKey(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.smf.daffodil.smfapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
