package com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;

/**
 * Created by daffodil on 9/2/17.
 */

public interface IUserProfileView extends IBaseView {

    void onError(int errorcode,String mesg);
    void onSuccess();
}
