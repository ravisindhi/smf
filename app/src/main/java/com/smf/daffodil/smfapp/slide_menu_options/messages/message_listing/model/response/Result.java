package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by daffolap-164 on 24/2/17.
 */

public class Result implements Serializable{
    private String messageId;

    private String content;

    private String author;

    private String _id;

    private String createdAt;

    private String __v;

    private String read;

    private SenderDetail[] senderDetail;

    private ReceiverDetail[] receiverDetail;

    private String recipient;



    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getAuthor ()
    {
        return author;
    }

    public void setAuthor (String author)
    {
        this.author = author;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String get__v ()
    {
        return __v;
    }

    public void set__v (String __v)
    {
        this.__v = __v;
    }

    public String getRead ()
    {
        return read;
    }

    public void setRead (String read)
    {
        this.read = read;
    }

    public SenderDetail[] getSenderDetail ()
    {
        return senderDetail;
    }

    public void setSenderDetail (SenderDetail[] senderDetail)
    {
        this.senderDetail = senderDetail;
    }

    public ReceiverDetail[] getReceiverDetail ()
    {
        return receiverDetail;
    }

    public void setReceiverDetail (ReceiverDetail[] receiverDetail)
    {
        this.receiverDetail = receiverDetail;
    }

    public String getRecipient ()
    {
        return recipient;
    }

    public void setRecipient (String recipient)
    {
        this.recipient = recipient;
    }


    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
