package com.smf.daffodil.smfapp.bottom_menu_option.members.presenter;

/**
 * Created by daffodil on 26/1/17.
 */

public interface IMemberPresenter {

    void getAllMembers();
    void followUnfollow(String action,String memberId);
    void searchMemberByName(String name);
}
