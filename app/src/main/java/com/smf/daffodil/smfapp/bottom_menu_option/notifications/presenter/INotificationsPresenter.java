package com.smf.daffodil.smfapp.bottom_menu_option.notifications.presenter;

import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.AcceptRejectRequest;

/**
 * Created by daffodil on 17/2/17.
 */

public interface INotificationsPresenter {

    void getAllNotifications();
    void doClearNotification();
    void onAcceptReject(AcceptRejectRequest request,int position);
    void getNotificationDetailPost(String postId);
    void getNotificationDetailGroup(String postId);
}
