package com.smf.daffodil.smfapp.bottom_menu_option.map;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.smf.daffodil.smfapp.R;

/**
 * Created by daffodil on 27/1/17.
 */

public class MapDialogFragment extends DialogFragment {
    SendLocation sendLocation;

    public MapDialogFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.location_dialog_view, container,
                false);
        // Do something else
        getDialog().setCanceledOnTouchOutside(true);
       final EditText editTextLocation=(EditText) rootView.findViewById(R.id.location_edt);
       final Button btnCancel=(Button)rootView.findViewById(R.id.cancel_btn);
       final Button btnSave=(Button) rootView.findViewById(R.id.save_btn);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(sendLocation!=null)
                    sendLocation.sendLocationToServer(editTextLocation.getText().toString().trim());

                dismiss();
            }


        });


        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
       // getDialog().setCanceledOnTouchOutside(true);

       this.getDialog().setCanceledOnTouchOutside(true);
        this.setCancelable(true);
        return rootView;
    }

    public interface SendLocation{
        void sendLocationToServer(String userAddress);

    }

    public void setSendLocation(SendLocation sendLocation){
        this.sendLocation=sendLocation;

    }







}
