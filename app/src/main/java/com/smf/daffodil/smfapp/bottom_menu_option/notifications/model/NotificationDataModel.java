package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model;

/**
 * Created by daffodil on 26/1/17.
 */

public class NotificationDataModel {

    public static final int HEADER_TYPE = 0;
    public static final int NOTIFICATION_TYPE = 1;

    private String mName;
    private String mDescription;
    private String mDate;
    private String mTime;
    private String mImageUrl;
    private int mType;

    public NotificationDataModel(String mName,String mDescription,String mDate,String mImageUrl,String mTime,int mType){
        this.mName=mName;
        this.mDescription=mDescription;
        this.mDate=mDate;
        this.mTime=mTime;
        this.mImageUrl=mImageUrl;
        this.mType=mType;
    }

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }


}
