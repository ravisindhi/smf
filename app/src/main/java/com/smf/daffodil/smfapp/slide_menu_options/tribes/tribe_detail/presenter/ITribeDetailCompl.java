package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.AllTribesPostRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.CloseTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.view.ITribeDetailView;

/**
 * Created by daffodil on 13/2/17.
 */

public class ITribeDetailCompl implements ITribeDetailPresenter{

    private Context context;
    private ITribeDetailView iTribeDetailView;

    public ITribeDetailCompl(Context context, ITribeDetailView iTribeDetailView) {
        this.context = context;
        this.iTribeDetailView = iTribeDetailView;
    }

    @Override
    public void getAllPosts(AllTribesPostRequest request) {

        startGetAllPostsService(request);

    }

    @Override
    public void closeTribe(String group_id) {
        iTribeDetailView.showLoader(context.getString(R.string.please_wait_message));
        if (iTribeDetailView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().closeTribe(new StringRequestCallback<CloseTribeResponse>() {
                @Override
                public void onRestResponse(Exception e, CloseTribeResponse result) {
                    iTribeDetailView.hideLoader();
                    iTribeDetailView.onTribeClose(result);

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    iTribeDetailView.hideLoader();
                    iTribeDetailView.onError(IBaseView.SERVER_ERROR, result.getMessage());

                }
            },group_id);

        }else
        {
            iTribeDetailView.hideLoader();
            iTribeDetailView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }

    }

    @Override
    public void joinTribe(JoinTribeRequest joinTribeRequest) {
        iTribeDetailView.showLoader(context.getString(R.string.please_wait_message));

        if (iTribeDetailView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().joinTribe(new StringRequestCallback<CreateTribeResponse>() {
                @Override
                public void onRestResponse(Exception e, CreateTribeResponse result) {
                    iTribeDetailView.hideLoader();
                    iTribeDetailView.onSuccessFullJoinTribe();
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    iTribeDetailView.hideLoader();
                    if(result.getMessage().contains("exists"))
                    {
                        result.setMessage("You already sent join request to this tribe.");
                        iTribeDetailView.onJoinError(result);
                    }else
                    {
                       iTribeDetailView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    }


                }
            },joinTribeRequest);

        }else
        {
            iTribeDetailView.hideLoader();
            iTribeDetailView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }

    }

    private void startGetAllPostsService(final AllTribesPostRequest request) {



        if (iTribeDetailView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().getAllGroupPost(new StringRequestCallback<PostDataResult>() {


                @Override
                public void onRestResponse(Exception e, PostDataResult result) {
                    if (e == null && result != null) {


                        if(request.getLimit().equals("10")){

                            if(result.getResult().size()>0)
                                iTribeDetailView.onSuccess(result);
                            else
                                iTribeDetailView.onError(IAllPostView.NODATA, "No data found !!");

                        }else {
                            iTribeDetailView.onAppendData(result);
                        }

                        iTribeDetailView.hideLoader();

                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    iTribeDetailView.hideLoader();
                    iTribeDetailView.onError(IBaseView.SERVER_ERROR, result.getMessage());

                }


            }, request);
        } else {
            iTribeDetailView.hideLoader();
            iTribeDetailView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }
}
