package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

/**
 * Created by daffodil on 11/2/17.
 */

public class AllTribesRequest {
    private String limit;
    private String skip;
    private String myTribes;
    private String searchValue;

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getMyTribes() {
        return myTribes;
    }

    public void setMyTribes(String myTribes) {
        this.myTribes = myTribes;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }
}
