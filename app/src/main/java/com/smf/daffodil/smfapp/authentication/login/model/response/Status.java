package com.smf.daffodil.smfapp.authentication.login.model.response;

import java.io.Serializable;

/**
 * Created by daffodil on 30/1/17.
 */

public class Status implements Serializable{

    private String idle;

    private String online;

    public String getIdle ()
    {
        return idle;
    }

    public void setIdle (String idle)
    {
        this.idle = idle;
    }

    public String getOnline ()
    {
        return online;
    }

    public void setOnline (String online)
    {
        this.online = online;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [idle = "+idle+", online = "+online+"]";
    }
}
