package com.smf.daffodil.smfapp.authentication.login;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.AboutYouActivity;
import com.smf.daffodil.smfapp.authentication.forgot_password.ForgotPasswordFragment;
import com.smf.daffodil.smfapp.authentication.login.model.request.DeviceInfo;
import com.smf.daffodil.smfapp.authentication.login.model.request.FacebookLoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.request.LoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.authentication.login.presenter.ILoginPresenter;
import com.smf.daffodil.smfapp.authentication.login.presenter.ILoginPresenterCompl;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.authentication.signup.SignupFragment;
import com.smf.daffodil.smfapp.authentication.terms_condition.TermsAndCondtionsFragment;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.home.HomeActivity;
import com.smf.daffodil.smfapp.push_notification.NotificationConfig;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment implements ILoginView{

    ILoginPresenter iLoginPresenter;
    CallbackManager callbackManager;

    @BindView(R.id.et_email_address)
    EditText editTextEmail;

    @BindView(R.id.et_password)
    EditText editTextPassword;

    @BindView(R.id.btn_login)
    Button buttonLogin;

    @BindView(R.id.btn_facebook_login)
    LinearLayout buttonFacebookLogin;

    @BindView(R.id.signup_text)
    TextView textViewSignup;

    @BindView(R.id.txt_forgot_password)
    TextView textViewForgotPassword;

    @BindView(R.id.txt_terms_conditions)
    TextView textViewTermsConditions;

    @BindView(R.id.login_button)
    LoginButton loginButtonFacebook;

    SessionManager sessionManager;
    private Context _mContext;
    private FacebookLoginRequest facebookLoginRequest;


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        callbackManager = CallbackManager.Factory.create();
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        _mContext=getActivity();
        iLoginPresenter=new ILoginPresenterCompl(getActivity(),this);
        sessionManager=new SessionManager(_mContext);


        /*Facebook login setup*/
        loginButtonFacebook.setReadPermissions("email");
        loginButtonFacebook.setFragment(this);
        fbCallbackHandle();
    }

    private void fbCallbackHandle()
    {
        loginButtonFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                facebookLoginRequest=new FacebookLoginRequest();
                facebookLoginRequest.setAccessToken(loginResult.getAccessToken().getToken());
                facebookLoginRequest.setExpiresAt(loginResult.getAccessToken().getExpires().toString());



                GraphRequest request=GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (object != null) {
                            facebookLoginRequest.setId(object.optString("id"));
                            facebookLoginRequest.setEmail(object.optString("email"));
                            facebookLoginRequest.setName(object.optString("name"));
                            facebookLoginRequest.setFirst_name(object.optString("first_name"));
                            facebookLoginRequest.setLast_name(object.optString("last_name"));
                            facebookLoginRequest.setGender(object.optString("gender").substring(0,1).toLowerCase()+object.optString("gender").substring(1)); //make first letter capital
                            facebookLoginRequest.setLink(object.optString("link"));
                            facebookLoginRequest.setLocale(object.optString("locale"));
                            iLoginPresenter.doFacebookLogin(facebookLoginRequest);
                        }


                    }
                });
                Bundle parameters=new Bundle();
                parameters.putString("fields","id,name,email,gender, birthday,link,first_name,last_name,locale");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(),"",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.txt_forgot_password,R.id.txt_terms_conditions,R.id.signup_text,R.id.btn_login,R.id.btn_facebook_login})
    public void redirectToFragment(View view)
    {
        switch (view.getId())
        {
            case R.id.signup_text:
                    replaceFragment(R.id.activity_auth_container,new SignupFragment(),"SIGNUP");
                break;

            case R.id.txt_forgot_password:
                replaceFragment(R.id.activity_auth_container,new ForgotPasswordFragment(),"ForgotPasswordFragment");
                break;

            case R.id.txt_terms_conditions:
                replaceFragment(R.id.activity_auth_container,new TermsAndCondtionsFragment(),"TERMS");

                break;

            case R.id.btn_login:
                doLogin();
                break;

            case R.id.btn_facebook_login:
                loginButtonFacebook.performClick();
                break;
        }


    }



    private void doLogin()
    {

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences(NotificationConfig.SHARED_PREF, 0);
        DeviceInfo deviceInfo=new DeviceInfo();
        deviceInfo.setDeviceType("android");
        deviceInfo.setDeviceToken(pref.getString("regId", ""));

        LoginRequest loginRequest=new LoginRequest();
        loginRequest.setInfo(deviceInfo);
        loginRequest.setEmail(editTextEmail.getText().toString().trim());
        loginRequest.setPassword(editTextPassword.getText().toString().trim());
        iLoginPresenter.doLogin(loginRequest);
    }

    @Override
    public void onErrorHandleWithView(int errorCode, String _errorMsg) {

        switch (errorCode)
        {

            case 1:
                replaceErrorViewWithMessage(editTextEmail,getActivity());
                editTextEmail.requestFocus();
                break;
            case 2:
                replaceErrorViewWithMessage(editTextPassword,getActivity());
                editTextPassword.requestFocus();
                break;
        }

    }


    @Override
    public void onErrorHandleWithSnakbar(int errorCode, String _errorMsg) {
        Log.e("error code",String.valueOf(errorCode));
        switch (errorCode)
        {

            case 1:
                replaceErrorView(editTextEmail,getActivity());
                showErrorMessage(getContext().getApplicationContext(),"Email address is invalid",getActivity());
                editTextEmail.requestFocus();

                break;
            case 2:
                replaceErrorView(editTextPassword,getActivity());
                editTextPassword.requestFocus();
                break;

            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getContext().getApplicationContext(),_errorMsg,getActivity());
                break;
        }

    }

    @Override
    public void onSuccess(LoginResponse loginResponse) {



        if(loginResponse.getProfile().getHandle()!=null && loginResponse.getProfile().getHandle().length()>0)
        {
            launchActivity(getActivity(),HomeActivity.class);
            getActivity().finish();
        }
        else{
            launchActivity(getActivity(),AboutYouActivity.class);
            getActivity().finish();
        }

    }

    @Override
    public boolean onSaveUserData(LoginResponse response) {
        boolean isSaved=false;
        if(response!=null)
             isSaved=sessionManager.saveUserData(response);
        return isSaved;
    }




}
