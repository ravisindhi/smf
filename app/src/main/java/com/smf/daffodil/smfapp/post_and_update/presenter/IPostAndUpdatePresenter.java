package com.smf.daffodil.smfapp.post_and_update.presenter;

import com.smf.daffodil.smfapp.post_and_update.model.IPostAndUpdateRequest;

/**
 * Created by daffodil on 4/2/17.
 */

public interface IPostAndUpdatePresenter {

    void doPostAndUpdate(IPostAndUpdateRequest request);

}
