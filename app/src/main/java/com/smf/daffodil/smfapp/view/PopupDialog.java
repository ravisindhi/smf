package com.smf.daffodil.smfapp.view;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 15/2/17.
 */

public class PopupDialog extends DialogFragment {

    DilogButtonHandling dilogButtonHandling;

    @BindView(R.id.txt_dilog_header)
    TextView textViewHeader;

    @BindView(R.id.txt_dilog_message)
    TextView textViewMessage;

    @BindView(R.id.diloag_btn_cancel)
    Button buttonCancel;

    @BindView(R.id.diloag_btn_accept)
    Button buttonAccpept;

    public PopupDialog(){

    }
    public PopupDialog(DilogButtonHandling dilogButtonHandling){
            this.dilogButtonHandling=dilogButtonHandling;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_popup_layout, container,
                false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        // Do something else
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String header = bundle.getString("name", "");
            String content = bundle.getString("content", "");
            String btn_accept_txt = bundle.getString("accept_text", null);
            String btn_cancel_txt = bundle.getString("cancel_text", null);

            if(btn_accept_txt==null)
                buttonAccpept.setVisibility(View.GONE);
            if (btn_cancel_txt==null)
                buttonCancel.setVisibility(View.GONE);

            final String type=bundle.getString("action_type", "");
            textViewHeader.setText(header);
            textViewMessage.setText(content);
            buttonAccpept.setText(btn_accept_txt);
            buttonCancel.setText(btn_cancel_txt);

            buttonAccpept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dilogButtonHandling.buttonYesClicked(type);
                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dilogButtonHandling.buttonNoClicked();
                }
            });


        }
    }

    public interface DilogButtonHandling{
        void buttonYesClicked(String type);
        void buttonNoClicked();
    }
}
