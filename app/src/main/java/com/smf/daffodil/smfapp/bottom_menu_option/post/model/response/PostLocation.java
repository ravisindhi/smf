package com.smf.daffodil.smfapp.bottom_menu_option.post.model.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by daffodil on 3/2/17.
 */

public class PostLocation implements Serializable{

    private String type;

    private List<String> coordinates=null;

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public List<String> getCoordinates ()
    {
        return coordinates;
    }

    public void setCoordinates (List<String> coordinates)
    {
        this.coordinates = coordinates;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [type = "+type+", coordinates = "+coordinates+"]";
    }
}
