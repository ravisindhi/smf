package com.smf.daffodil.smfapp.bottom_menu_option.post.model;

/**
 * Created by daffodil on 27/1/17.
 */

public class PostData {


    public static final int POST_TYPE_SHARE=1;
    public static final int POST_TYPE_IMAGE=2;
    public static final int POST_TYPE_TEXT=3;
    public static final int POST_TYPE_VIDEO=4;
    public static final int POST_TYPE_HIDDEN=5;

    private String name;
    private String time;
    private String image;

    private String shareText;
    private String shareUrl;
    private String shareimage;
    private String shareVedio;
    private int type;

    public PostData(String name,String time,String image,String shareText,String shareUrl,String shareVedio,int type)
    {
        this.name=name;
        this.time=time;
        this.image=image;
        this.shareText=shareText;
        this.shareUrl=shareUrl;
        this.shareVedio=shareVedio;
        this.type=type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getShareVedio() {
        return shareVedio;
    }

    public void setShareVedio(String shareVedio) {
        this.shareVedio = shareVedio;
    }

    public String getShareimage() {
        return shareimage;
    }

    public void setShareimage(String shareimage) {
        this.shareimage = shareimage;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareText() {
        return shareText;
    }

    public void setShareText(String shareText) {
        this.shareText = shareText;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
