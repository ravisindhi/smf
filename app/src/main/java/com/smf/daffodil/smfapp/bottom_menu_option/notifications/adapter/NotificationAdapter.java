package com.smf.daffodil.smfapp.bottom_menu_option.notifications.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.NotificationDataModel;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response.Notification;
import com.smf.daffodil.smfapp.utils.DateTimeAgo;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 26/1/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Notification> mList;
    private Context context;
    private final int HEADER_TYPE = 1;
    private final int NOTIFICATION_TYPE = 2;
    private final int ACCEPT_REJECT_TYPE = 3;
    private double time_miliseconds;
    private AcceptRejectListener acceptRejectListenerl;

    public NotificationAdapter(List<Notification> mList, Context context,AcceptRejectListener acceptRejectListenerl) {
        this.mList = mList;
        this.context = context;
        this.acceptRejectListenerl=acceptRejectListenerl;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case HEADER_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_header_item_view, parent, false);
                return new NotificationHeaderViewHolder(view, parent.getContext(),HEADER_TYPE);

            case NOTIFICATION_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item_view, parent, false);
                return new NotificationDataViewHolder(view, parent.getContext(),NOTIFICATION_TYPE);

            case ACCEPT_REJECT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notifiaction_accept_reject_view, parent, false);
                return new NotificationAcceptRejectViewHolder(view, parent.getContext(),ACCEPT_REJECT_TYPE);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final Notification dataModel = mList.get(position);
        if (dataModel != null) {
            switch (holder.getItemViewType()) {
                case HEADER_TYPE:
                    ((NotificationHeaderViewHolder) holder).textViewDate.setText(convertDate(String.valueOf(dataModel.getCreatedAt()),"dd/MM/yyyy hh:mm:ss"));
                    ImageUtils.setprofile(((NotificationHeaderViewHolder) holder).profile_image,dataModel.getSourceUser().getProfile().getProfileImage(),context);
                    ((NotificationHeaderViewHolder) holder).textViewName.setText(changepost(dataModel));
                    ((NotificationHeaderViewHolder) holder).textViewTime.setText(getDate(dataModel.getCreatedAt()));
                    break;
                case NOTIFICATION_TYPE:
                    ImageUtils.setprofile(((NotificationDataViewHolder) holder).profile_image,dataModel.getSourceUser().getProfile().getProfileImage(),context);
                    ((NotificationDataViewHolder) holder).textViewName.setText(changepost(dataModel));
                    ((NotificationDataViewHolder) holder).textViewTime.setText(getDate(dataModel.getCreatedAt()));
                    break;
                case ACCEPT_REJECT_TYPE:



                        ImageUtils.setprofile(((NotificationAcceptRejectViewHolder) holder).profile_image,dataModel.getSourceUser().getProfile().getProfileImage(),context);
                        ((NotificationAcceptRejectViewHolder) holder).textViewName.setText(changepost(dataModel));
                        ((NotificationAcceptRejectViewHolder) holder).buttonAccpet.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                acceptRejectListenerl.acceptClicked(position,dataModel);
                            }
                        });

                        ((NotificationAcceptRejectViewHolder) holder).buttonReject.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                acceptRejectListenerl.rejectClicked(position,dataModel);
                            }
                        });




                    break;

            }
        }

    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();

    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null) {
            Notification object = mList.get(position);
            if (object != null) {

                if (mList.get(position).getType().equals("privateGroupRequest")) {
                    return ACCEPT_REJECT_TYPE;
                } else {

                    return NOTIFICATION_TYPE;
                }
            }
        }
        return 0;
    }

    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }


    public static class NotificationDataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_name)
        TextView textViewName;

        @BindView(R.id.txt_time)
        TextView textViewTime;

        @BindView(R.id.rounded_image_view)
        ImageView profile_image;

        private int type;

        public NotificationDataViewHolder(View itemView, Context context,int type) {
            super(itemView);
            this.type=type;
            ButterKnife.bind(this, itemView);
        }
    }

    public static class NotificationHeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_date)
        TextView textViewDate;

        @BindView(R.id.txt_name)
        TextView textViewName;

        @BindView(R.id.txt_time)
        TextView textViewTime;

        @BindView(R.id.rounded_image_view)
        ImageView profile_image;

        private int type;

        public NotificationHeaderViewHolder(View itemView, Context context,int type) {
            super(itemView);
            this.type=type;
            ButterKnife.bind(this, itemView);
        }
    }

    public static class NotificationAcceptRejectViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_name)
        TextView textViewName;

        @BindView(R.id.rounded_image_view)
        ImageView profile_image;

        @BindView(R.id.diloag_btn_accept)
        Button buttonAccpet;

        @BindView(R.id.diloag_btn_cancel)
        Button buttonReject;

        private int type;

        public NotificationAcceptRejectViewHolder(View itemView, Context context,int type) {
            super(itemView);
            this.type=type;
            ButterKnife.bind(this, itemView);
        }
    }



    private Spanned changepost(Notification notification) {

        String name="@"+notification.getSourceUser().getProfile().getFirstName()+" "+
                notification.getSourceUser().getProfile().getLastName();

        String message= notification.getText().replaceFirst(notification.getSourceUser().getProfile().getFirstName(),"")
                .replaceFirst(notification.getSourceUser().getProfile().getLastName(),"").trim();
        String text = "<font color=#61BDD6>" + name + " </font> " + " "+message;
        return Html.fromHtml(text);
    }

    private String getDate(Long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String result = df.format(time);

        return DateTimeAgo.getTimeAgo2(result);
    }

    public interface AcceptRejectListener{
        void acceptClicked(int position,Notification notification);
        void rejectClicked(int position,Notification notification);
    }

}
