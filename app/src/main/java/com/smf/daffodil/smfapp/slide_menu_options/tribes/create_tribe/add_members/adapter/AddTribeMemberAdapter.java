package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.TribesHomeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.adapter.TribeMemberAdapter;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 22/2/17.
 */

public class AddTribeMemberAdapter extends BaseAdapter {

    private Context context;
    private List<TribesMembers> list = new ArrayList<>();
    private Result result;
    private int userType;
    private OnClickListen onClickListen;

    public AddTribeMemberAdapter(Context context, List<TribesMembers> result,OnClickListen onClickListen) {
        this.context = context;
        this.list=result;
        this.onClickListen=onClickListen;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public TribesMembers getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final AddTribeMemberAdapter.TribeMemberItemHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.add_member_item, viewGroup, false);
            viewHolder = new AddTribeMemberAdapter.TribeMemberItemHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (AddTribeMemberAdapter.TribeMemberItemHolder) view.getTag();
        }


        try {
          // get tribes members data
                if (list.get(i).get_id() != null && list.get(i).get_id().length() > 0) {
                    ImageUtils.setprofile(viewHolder.imageViewTribeMemberImage, list.get(i).getProfile().getProfileImage(), context);
                    viewHolder.textViewMemberName.setText(list.get(i).getProfile().getFullName());
                    viewHolder.textViewTribeMemberHandle.setText("@" + list.get(i).getProfile().getHandle());
                    viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(viewHolder.tickMark.getVisibility()==View.VISIBLE){
                                onClickListen.onRemove(i);
                                viewHolder.tickMark.setVisibility(View.GONE);
                            }
                            else{
                                onClickListen.onAdd(i);
                                viewHolder.tickMark.setVisibility(View.VISIBLE);

                            }

                        }
                    });

                }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return view;
    }


    class TribeMemberItemHolder {

        @BindView(R.id.tribe_member_image)
        ImageView imageViewTribeMemberImage;


        @BindView(R.id.member_name)
        TextView textViewMemberName;

        @BindView(R.id.tribe_member_handle)
        TextView textViewTribeMemberHandle;

        @BindView(R.id.tick_mark)
        ImageView tickMark;

        @BindView(R.id.main_card)
        CardView cardView;




        public TribeMemberItemHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public interface OnClickListen
    {
        void onAdd(int position);
        void onRemove(int position);
    }

}