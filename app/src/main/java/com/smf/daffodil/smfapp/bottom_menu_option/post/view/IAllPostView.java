package com.smf.daffodil.smfapp.bottom_menu_option.post.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;

import java.util.List;

/**
 * Created by daffodil on 3/2/17.
 */

public interface IAllPostView extends IBaseView {

    final int NODATA=404;


    void onSuccess(PostDataResult postDataResult);
    void onAppendData(PostDataResult postDataResult);
    void onError(int errorcode,String message);


}
