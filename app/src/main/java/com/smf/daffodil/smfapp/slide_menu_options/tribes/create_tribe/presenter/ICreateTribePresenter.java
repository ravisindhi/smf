package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.presenter;

import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.UpdateTribeRequest;

/**
 * Created by daffodil on 11/2/17.
 */

public interface ICreateTribePresenter {

    void createTribe(CreateTribeRequest request);
    void updateTribe(CreateTribeRequest request);
}
