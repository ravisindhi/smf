package com.smf.daffodil.smfapp.bottom_menu_option.map.presenter;

import com.smf.daffodil.smfapp.bottom_menu_option.map.model.request.AddLocationRequest;

/**
 * Created by daffolap-164 on 16/2/17.
 */

public interface IAddLocationPresenter {
    void addLocation(AddLocationRequest request);
}
