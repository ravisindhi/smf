package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.presenter;

import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;

/**
 * Created by daffodil on 11/2/17.
 */

public interface ITribesHomePresenter {

    void getAllTribes(AllTribesRequest allTribesRequest,boolean isLoadMore);
    void searchTribesMember(String name);
    void joinTribe(JoinTribeRequest joinTribeRequest);
    void searchTribes(AllTribesRequest allTribesRequest,boolean isLoadMore);
}
