package com.smf.daffodil.smfapp.about_you.presenter;

import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;

/**
 * Created by daffodil on 25/1/17.
 */

public interface IAboutYouPresenter {

    void checkDataValidation(AboutYouRequest aboutYouRequest);
}
