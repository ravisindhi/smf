package com.smf.daffodil.smfapp.authentication.login.model.response;

import java.io.Serializable;

/**
 * Created by daffodil on 30/1/17.
 */

public class Metadata implements Serializable{

    private String smsVerified;

    public String getSmsVerified ()
    {
        return smsVerified;
    }

    public void setSmsVerified (String smsVerified)
    {
        this.smsVerified = smsVerified;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [smsVerified = "+smsVerified+"]";
    }
}
