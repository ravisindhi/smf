package com.smf.daffodil.smfapp.authentication.login.model.request;

/**
 * Created by daffodil on 24/1/17.
 */

public class LoginRequest {

    String email;

    DeviceInfo deviceInfo;
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String password;

    public DeviceInfo getInfo() {
        return deviceInfo;
    }

    public void setInfo(DeviceInfo info) {
        this.deviceInfo = info;
    }
}
