package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.TribesHomeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.adapter.TribesAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.presenter.ITribeMemberPresneter;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 14/2/17.
 */

public class TribeMemberAdapter extends BaseAdapter {

    private Context context;
    private List<TribesMembers> list = new ArrayList<>();
    private Result result;
    private int userType;
    private OnRemoveMemberListener onRemoveMemberListener;

    public TribeMemberAdapter(Context context, Result result, int userType,OnRemoveMemberListener onRemoveMemberListener) {
        this.context = context;

        if (result != null)
            this.list.addAll(Arrays.asList(result.getMembers()));
        this.result = result;
        this.list.add(0, new TribesMembers());
        this.userType = userType;
        this.onRemoveMemberListener=onRemoveMemberListener;
    }

    public void removemember(int postion)
    {
        list.remove(postion);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public TribesMembers getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        TribeMemberAdapter.TribeMemberItemHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.tribe_member_item, viewGroup, false);
            viewHolder = new TribeMemberAdapter.TribeMemberItemHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (TribeMemberAdapter.TribeMemberItemHolder) view.getTag();
        }


        try {
            if (i == 0)//get tribe owner data
            {

                viewHolder.imageViewRemoveMember.setVisibility(View.GONE);
                viewHolder.textViewAdminText.setVisibility(View.VISIBLE);

                ImageUtils.setprofile(viewHolder.imageViewTribeMemberImage, result.getOwnerId().getProfile().getProfileImage(), context);
                viewHolder.textViewMemberName.setText(result.getOwnerId().getProfile().getFirstName() + " " +
                        result.getOwnerId().getProfile().getLastName());
                viewHolder.textViewTribeMemberHandle.setText("@" + result.getOwnerId().getProfile().getHandle());


            } else { // get tribes members data

                if (userType == TribesHomeActivity.USER_TYPE)
                    viewHolder.imageViewRemoveMember.setVisibility(View.GONE);
                else
                    viewHolder.imageViewRemoveMember.setVisibility(View.VISIBLE);

                if(list.get(i).get_id()!=null && list.get(i).get_id().length()>0)
                {
                    viewHolder.textViewAdminText.setVisibility(View.GONE);
                    ImageUtils.setprofile(viewHolder.imageViewTribeMemberImage, list.get(i).getTribesId().getProfile().getProfileImage(), context);
                    viewHolder.textViewMemberName.setText(list.get(i).getTribesId().getProfile().getFirstName() + " " +
                            list.get(i).getTribesId().getProfile().getLastName());
                    viewHolder.textViewTribeMemberHandle.setText("@" + list.get(i).getTribesId().getProfile().getHandle());
                    viewHolder.imageViewRemoveMember.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onRemoveMemberListener.onRemoveMember(i);
                        }
                    });
                }



            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return view;
    }


    class TribeMemberItemHolder {

        @BindView(R.id.tribe_member_image)
        ImageView imageViewTribeMemberImage;

        @BindView(R.id.tribe_remove_member_icon)
        ImageView imageViewRemoveMember;

        @BindView(R.id.member_name)
        TextView textViewMemberName;

        @BindView(R.id.tribe_member_handle)
        TextView textViewTribeMemberHandle;

        @BindView(R.id.tribe_admin_txt)
        TextView textViewAdminText;


        public TribeMemberItemHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public interface OnRemoveMemberListener{
        void onRemoveMember(int position);
    }
}
