package com.smf.daffodil.smfapp.about_you.fragments;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.utils.AppUtils;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutYouReviewProfileFragment extends BaseFragment {

    SessionManager sessionManager;

    @BindView(R.id.blur_image_view)
    ImageView imageViewBlurImage;

    @BindView(R.id.rounded_image_view)
    ImageView imageViewROunded;

    @BindView(R.id.txt_address)
    TextView textViewAddress;

    @BindView(R.id.txt_email)
    TextView textViewEmail;

    @BindView(R.id.txt_handle)
    TextView textViewHandle;


    public AboutYouReviewProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sessionManager = new SessionManager(getActivity());
        return inflater.inflate(R.layout.fragment_about_you_review_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        showLoader("please wait...");
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        initViews();
    }

    private void initViews() {
        try {



            LoginResponse loginResponse = sessionManager.getUserData();
            if (AboutYouRequest.getProfile_uri() != null) {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), AboutYouRequest.getProfile_uri());
                imageViewBlurImage.setImageBitmap(AppUtils.blurRenderScript(getActivity(), bitmap, 20));
                imageViewROunded.setImageBitmap(bitmap);
            } else {
                if (loginResponse != null) {
                    ImageUtils.setprofile(imageViewROunded,loginResponse.getProfile().getProfileImage(),getActivity());
                    //Glide.with(getActivity()).load(loginResponse.getProfile().getProfileImage()).error(R.drawable.dummy_rounded).into(imageViewROunded);
                    Glide.with(getActivity()).load(loginResponse.getProfile().getProfileImage()).bitmapTransform(new BlurTransformation(getActivity())).error(R.drawable.dummy_full).into(imageViewBlurImage);
                }
            }

            if (AboutYouRequest.getCity().length() > 0 || AboutYouRequest.getState().length() > 0)
                textViewAddress.setText(AboutYouRequest.getCity() + " " + AboutYouRequest.getState());
            else
                textViewAddress.setText("Not Mentioned");

            textViewEmail.setText(loginResponse.getEmails()[0].getAddress());

            if (AboutYouRequest.getHandle().length() > 0)
                textViewHandle.setText("@"+AboutYouRequest.getHandle());
            else
                textViewHandle.setText("Not Mentioned");



            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    hideLoader();
                }
            }, 3000);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
