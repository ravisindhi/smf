package com.smf.daffodil.smfapp.about_you.model;

/**
 * Created by daffodil on 30/1/17.
 */

public class HandleResponse {
    private boolean availability;

    public HandleResponse(boolean availability) {
        this.availability = availability;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}
