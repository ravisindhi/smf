package com.smf.daffodil.smfapp.bottom_menu_option.map.view;

import com.smf.daffodil.smfapp.base.presenter.IBasePresenter;
import com.smf.daffodil.smfapp.base.view.IBaseView;

/**
 * Created by daffolap-164 on 16/2/17.
 */

public interface IMapView extends IBaseView {

    void onError(int errorcode, String msg);
    void onSuccess();



}
