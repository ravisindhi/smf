package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daffodil on 26/1/17.
 */

public final class NotificationData {

    public static List<NotificationDataModel> getNotificationData() {
        List<NotificationDataModel> list = new ArrayList<>();

        list.add(new NotificationDataModel("@ Ravi Lalwani", "liked", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.HEADER_TYPE));
        list.add(new NotificationDataModel("@ Mohit Rakhra", "shared", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Vikas Verma", "commented on", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Shubham Sahgal", "share", "reacted to, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Ravi Lalwani", "commented on", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Ravi Arora", "share", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.HEADER_TYPE));
        list.add(new NotificationDataModel("@ Ravi ", "shared", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Ravi Lalwani", "liked", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Ravi Lalwani", "shared", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Ravi Lalwani", "commented on", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.HEADER_TYPE));
        list.add(new NotificationDataModel("@ Ravi Lalwani", "shared", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Ravi Lalwani", "liked", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        list.add(new NotificationDataModel("@ Ravi Lalwani", "shared", "Monday, January 2nd", null, "9.30 AM", NotificationDataModel.NOTIFICATION_TYPE));
        return list;
    }


}
