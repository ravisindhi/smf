package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response.AllMessagesResponse;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public interface IMessagesView extends IBaseView {

    void onGetMessages(AllMessagesResponse allMessagesResponse);
    void onAppendMoreData(AllMessagesResponse allMessagesResponse);
    void onError(int errorcode, String message);


}
