package com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.model.BlockUserResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.view.IUserProfileView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 9/2/17.
 */

public class IUserProfileCompl implements IUserProfilePresenter{

    IUserProfileView iUserProfileView;
    Context context;

    public IUserProfileCompl(IUserProfileView iUserProfileView, Context context) {
        this.iUserProfileView = iUserProfileView;
        this.context = context;
    }


    @Override
    public void doBlockMember(String id) {
        if(iUserProfileView.onCheckNetworkConnection())
        {


            //iMemberView.showLoader(_context.getString(R.string.please_wait_message));
            SMFApplication.getRestClient().blockUser(new StringRequestCallback<BlockUserResponse>() {



                @Override
                public void onRestResponse(Exception e, BlockUserResponse result) {
                    if(e==null && result !=null)
                    {

                        //iMemberView.hideLoader();
                        iUserProfileView.onSuccess();
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iUserProfileView.hideLoader();
                    e.printStackTrace();
                    iUserProfileView.onError(IBaseView.NETWORK_ERROR,result.getMessage());

                }


            },id);
        }else
        {
            iUserProfileView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }
}
