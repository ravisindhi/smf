
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Metadata {

    @SerializedName("isActivated")
    @Expose
    private Boolean isActivated;
    @SerializedName("smsVerified")
    @Expose
    private String smsVerified;

    public Boolean getIsActivated() {
        return isActivated;
    }

    public void setIsActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public String getSmsVerified() {
        return smsVerified;
    }

    public void setSmsVerified(String smsVerified) {
        this.smsVerified = smsVerified;
    }

}
