package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model;

import android.net.Uri;

import java.util.List;

/**
 * Created by daffodil on 11/2/17.
 */

public class CreateTribeRequest {

    private String name;
    private String desc;
    private String groupId;



    private String privateGroup;
    private List<CreateTribeMembersList> members;
    private String imageUrl;
    private Uri imageUri;

    public String getPrivateGroup() {
        return privateGroup;
    }

    public void setPrivateGroup(String privateGroup) {
        this.privateGroup = privateGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }





    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }



    public List<CreateTribeMembersList> getMembers() {
        return members;
    }

    public void setMembers(List<CreateTribeMembersList> members) {
        this.members = members;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
