package com.smf.daffodil.smfapp.base;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.nispok.snackbar.Snackbar;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.FullScreenPlayerActivity;

import dmax.dialog.SpotsDialog;

/**
 * Created by daffodil on 23/1/17.
 */

public abstract class BaseFragment extends Fragment implements IBaseView{

    private ProgressDialog mProgressDialog;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private AlertDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }



    protected void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    protected void replaceFragment(int container_id,Fragment fragment,String tag)
    {
        if(fragment !=null)
        {
            fragmentManager=getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.trans_left_in,R.anim.trans_right_out);
            fragmentTransaction.replace(container_id, fragment,tag);
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    protected <T> void launchActivity(Activity _context, Class<T> cls) {
        if (_context != null) {
            Intent intent = new Intent(_context, cls);
            getActivity().overridePendingTransition(R.anim.trans_left_in,R.anim.trans_right_out);
            startActivity(intent);

        }
    }



    protected void replaceErrorViewWithMessage(View view,Context _context)
    {
        if(view instanceof EditText)
        {   final int version = Build.VERSION.SDK_INT;
            if(!((EditText) view).getHint().toString().contains("Enter"))
            {
                ((EditText) view).setHint("Enter "+((EditText) view).getHint().toString());
                ((EditText) view).setHintTextColor(getActivity().getResources().getColor(R.color.colorErrorTextColor));

            }

           if (version >= 21) {
                ((EditText) view).setBackground(ContextCompat.getDrawable(_context, R.drawable.rounded_corner_red));
            } else {
                ((EditText) view).setBackground(_context.getResources().getDrawable(R.drawable.rounded_corner_red));
            }
        }
    }

    protected void replaceErrorView(View view,Context _context)
    {
        if(view instanceof EditText)
        {   final int version = Build.VERSION.SDK_INT;
             if (version >= 21) {
                 ((EditText) view).setTextColor(getActivity().getResources().getColor(R.color.colorErrorTextColor));

                 ((EditText) view).setBackground(ContextCompat.getDrawable(_context, R.drawable.rounded_corner_red));
            } else {
                ((EditText) view).setBackground(_context.getResources().getDrawable(R.drawable.rounded_corner_red));
                 ((EditText) view).setTextColor(getActivity().getResources().getColor(R.color.colorErrorTextColor));

             }
        }
    }



    protected void replaceOriginalView(View view,Context _context)
    {
        if(view instanceof EditText)
        {   final int version = Build.VERSION.SDK_INT;
            ((EditText) view).setHint(((EditText) view).getHint().toString());
            ((EditText) view).setHintTextColor(getActivity().getResources().getColor(R.color.colorBlackHint));
            if (version >= 21) {
                ((EditText) view).setBackground(ContextCompat.getDrawable(_context, R.drawable.rounded_corner_black));
            } else {
                ((EditText) view).setBackground(_context.getResources().getDrawable(R.drawable.rounded_corner_black));
            }
        }
    }

    protected void showErrorMessage(Context app_context,String text,Activity _context)
    {
        Snackbar.with(app_context)
                .text(text)
                .show(_context);


    }

    @Override
    public void showLoader(String message) {

        if(dialog==null)
        {
            dialog = new SpotsDialog(getActivity(), R.style.Custom);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
        }
        if(!dialog.isShowing()){
            dialog.setMessage(message);
            dialog.show();
        }


    }

    @Override
    public void hideLoader() {
        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
        }
    }

    @Override
    public boolean onCheckNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
