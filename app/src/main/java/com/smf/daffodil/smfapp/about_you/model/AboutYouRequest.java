package com.smf.daffodil.smfapp.about_you.model;

import android.net.Uri;

/**
 * Created by daffodil on 25/1/17.
 */

public class AboutYouRequest {

    static  String firstname="";
    static String lastname="";
    static String mobile="";
    static String handle="";
    static String date_of_birth="";
    static String relation_status="";
    static String gender="";
    static String introduction="";
    static String city="";
    static String state="";
    static String profileUrl="";
    static Uri profile_uri;

    public static String getProfileUrl() {
        return profileUrl;
    }

    public static void setProfileUrl(String profileUrl) {
        AboutYouRequest.profileUrl = profileUrl;
    }




    public static Uri getProfile_uri() {
        return profile_uri;
    }

    public static void setProfile_uri(Uri profile_uri) {
        AboutYouRequest.profile_uri = profile_uri;
    }



    public static String getZip() {
        return zip;
    }

    public static void setZip(String zip) {
        AboutYouRequest.zip = zip;
    }

    public static String getState() {
        return state;
    }

    public static void setState(String state) {
        AboutYouRequest.state = state;
    }

    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        AboutYouRequest.city = city;
    }

    public static String getIntroduction() {
        return introduction;
    }

    public static void setIntroduction(String introduction) {
        AboutYouRequest.introduction = introduction;
    }

    public static String getGender() {
        return gender;
    }

    public static void setGender(String gender) {
        AboutYouRequest.gender = gender;
    }

    public static String getRelation_status() {
        return relation_status;
    }

    public static void setRelation_status(String relation_status) {
        AboutYouRequest.relation_status = relation_status;
    }

    public static String getDate_of_birth() {
        return date_of_birth;
    }

    public static void setDate_of_birth(String date_of_birth) {
        AboutYouRequest.date_of_birth = date_of_birth;
    }

    public static String getHandle() {
        return handle;
    }

    public static void setHandle(String handle) {
        AboutYouRequest.handle = handle;
    }

    public static String getMobile() {
        return mobile;
    }

    public static void setMobile(String mobile) {
        AboutYouRequest.mobile = mobile;
    }

    public static String getLastname() {
        return lastname;
    }

    public static void setLastname(String lastname) {
        AboutYouRequest.lastname = lastname;
    }

    public static String getFirstname() {
        return firstname;
    }

    public static void setFirstname(String firstname) {
        AboutYouRequest.firstname = firstname;
    }

    static String zip;


}
