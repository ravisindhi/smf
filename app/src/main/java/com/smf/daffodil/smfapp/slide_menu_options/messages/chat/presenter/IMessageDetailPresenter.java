package com.smf.daffodil.smfapp.slide_menu_options.messages.chat.presenter;


import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.request.MessageDetailRequest;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public interface IMessageDetailPresenter {
    void getMessageComment(MessageDetailRequest messageDetailRequest, boolean isLoadMore);
}
