package com.smf.daffodil.smfapp.bottom_menu_option.map;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.barcode.Barcode;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.fragments.adapters.MyCustomArrayAdapter;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.bottom_menu_option.map.model.request.AddLocationRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.map.presenter.IAddLocationCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.map.presenter.IAddLocationPresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.map.view.IMapView;
import com.smf.daffodil.smfapp.common.SessionManager;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

/**
 * A simple {@link Fragment} subclass.
 */
@RuntimePermissions
public class MapFragment extends BaseFragment implements IMapView, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, RadioGroup.OnCheckedChangeListener,
        View.OnClickListener, MapDialogFragment.SendLocation, AddressDialogFragment.OpenMapDialog,
        AdapterView.OnItemSelectedListener, android.location.LocationListener{

    @BindView(R.id.map_spinner_location)
    EditText editText_map_spinner_location;

    @BindView(R.id.satallite_btn)
    RadioButton radioButtonSatelliteView;

    @BindView(R.id.map_btn)
    RadioButton radioButtonMap;

    @BindView(R.id.map_view_btn)
    RadioGroup radioGroupMapView;

    @BindView(R.id.marker_iv)
    ImageView imageViewMarker;

    @BindView(R.id.location_spinner)
    Spinner mspinnerLocation;

    AddressDialogFragment addressDialogFragment;
    MapDialogFragment mapDialogFragment;
    GoogleMap map;
    SupportMapFragment mapFragment;
    String strAddress, strUserAddress;
    IAddLocationPresenter iAddLocationPresenter;
    double latitude;
    double longitude;
    private LatLng  latLng;

    List<String> locationSpinnerList = new ArrayList<String>();
    ArrayAdapter<String> spinner_dataAdapter;
    private SessionManager sessionManager;
    private GPSTracker gpsTracker;


    static public boolean isDialogOpen = false;


    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sessionManager = new SessionManager(getActivity());
        gpsTracker=new GPSTracker(getActivity());
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        editText_map_spinner_location.setInputType(InputType.TYPE_NULL);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        iAddLocationPresenter = new IAddLocationCompl(getActivity(), this);
        radioGroupMapView.setOnCheckedChangeListener(this);
        imageViewMarker.setOnClickListener(this);
        addLocationSpinnerData();

    }


    @OnTouch(R.id.map_spinner_location)
    public boolean onLocationTouch(View view, MotionEvent motionEvent){
        mspinnerLocation.performClick();
        return true;
    }


    private void addLocationSpinnerData() {
        /*Set spinner dropdown width to match parent*/
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();
        mspinnerLocation.setDropDownWidth((int) (config.screenWidthDp * dm.density));

        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(mspinnerLocation);

            // Set popupWindow height to 500px
            popupWindow.setHeight(config.screenHeightDp/2);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }


        locationSpinnerList.clear();
        locationSpinnerList.add("Local(" + sessionManager.getUserData().getProfile().getCity() + ")");
        if (sessionManager.getUserData().getProfile().getSavedLocations() != null) {
            for (int i = 0; i < sessionManager.getUserData().getProfile().getSavedLocations().size(); i++) {
                locationSpinnerList.add(sessionManager.getUserData().getProfile().getSavedLocations().get(i).getName());
            }
        }


        spinner_dataAdapter = new MyCustomArrayAdapter(getActivity(), R.layout.spinner_item, locationSpinnerList);
        mspinnerLocation.setAdapter(spinner_dataAdapter);
        mspinnerLocation.setOnItemSelectedListener(this);
        //mspinnerLocation.setSelection(0, false);
    }


    private void prepareMap() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        map = googleMap;
//        MarkerOptions k = new MarkerOptions()
//                .position(new LatLng(28.573208, 77.206383))
////                .icon(BitmapDescriptorFactory
////                        .fromResource(R.drawable.ic_map_marker))
//                .draggable(true).snippet("Near INA metro station");
//        map.addMarker(k);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        setMyLocationEnabled();
        map.getUiSettings().setZoomControlsEnabled(false);
        map.getUiSettings().setZoomGesturesEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(true);
        map.getUiSettings().setTiltGesturesEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setOnMarkerClickListener(this);

        /*LocationManager locationManager=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria=new Criteria();
        String bestProvider=locationManager.getBestProvider(criteria,true);
        Location location=locationManager.getLastKnownLocation(bestProvider);

        if(location!=null){
            onLocationChanged(location);
        }
        locationManager.requestLocationUpdates(bestProvider,20000,0.0f, this);*/

        double latitude = gpsTracker.getLatitude();
        double longitude = gpsTracker.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(12).build();

        map.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
    }



    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    private void setMyLocationEnabled() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                map.setMyLocationEnabled(true);
            }


            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }


        };

        new TedPermission(getActivity())
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .check();

    }


    @Override
    public boolean onMarkerClick(Marker marker) {


        return true;
    }


    @Override
    public void onClick(View view) {

        if (map != null) {
            LatLng latLng = map.getCameraPosition().target;
            if (latLng != null && !isDialogOpen) {
                // addressDialogFragment = AddressDialogFragment.newInstance(getAddress(latLng));
                addressDialogFragment = AddressDialogFragment.newInstance(latLng.latitude, latLng.longitude);
                addressDialogFragment.show(getActivity().getFragmentManager(), "new");
                addressDialogFragment.setOpenMapDialog(this);
                isDialogOpen = true;
            }

        }



    }


    @Override
    public void sendLocationToServer(String location) {
        isDialogOpen = false;

        //hide keyboard
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        strUserAddress = location;
        AddLocationRequest addLocationRequest = new AddLocationRequest();
        List<Double> latLonList = new ArrayList<>();
        latLonList.add(latitude);
        latLonList.add(longitude);
        addLocationRequest.setLatLng(latLonList);
        addLocationRequest.setLocation(strAddress);
        addLocationRequest.setName(strUserAddress);
        iAddLocationPresenter.addLocation(addLocationRequest);


    }

    @Override
    public void onOpenMapDialog(String address) {
        isDialogOpen = false;
        addressDialogFragment.dismiss();
        strAddress = address;
        mapDialogFragment = new MapDialogFragment();
        mapDialogFragment.setSendLocation(this);
        mapDialogFragment.show(getActivity().getFragmentManager(), "new");
    }

    @Override
    public void onError(int errorcode, String msg) {

    }

    @Override
    public void onSuccess() {
        locationSpinnerList.clear();
        locationSpinnerList.add("Local(" + sessionManager.getUserData().getProfile().getCity() + ")");
        if (sessionManager.getUserData().getProfile().getSavedLocations() != null) {
            for (int i = 0; i < sessionManager.getUserData().getProfile().getSavedLocations().size(); i++) {
                locationSpinnerList.add(sessionManager.getUserData().getProfile().getSavedLocations().get(i).getName());
            }
        }
        spinner_dataAdapter = new MyCustomArrayAdapter(getActivity(), R.layout.spinner_item, locationSpinnerList);
        mspinnerLocation.setAdapter(spinner_dataAdapter);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.map_btn:
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.satallite_btn:
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        editText_map_spinner_location.setText(mspinnerLocation.getSelectedItem().toString());



    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(12).build();

        map.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


}
