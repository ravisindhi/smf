package com.smf.daffodil.smfapp.about_you.fragments.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.vikasverma.customfontviews.UI.UITextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daffodil on 25/1/17.
 */

public class MyCustomArrayAdapter extends ArrayAdapter<String> {


    Context context;
    List<String> list=new ArrayList<>();

    public MyCustomArrayAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context=context;
        list.clear();
        list.addAll(objects);
    }



    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row;
        if(position==0)
            //row=inflater.inflate(R.layout.spinner_item_second, parent, false);
            row=inflater.inflate(R.layout.spinner_item, parent, false);
        else
            row=inflater.inflate(R.layout.spinner_item, parent, false);

        UITextView label=(UITextView)row.findViewById(R.id.spinner_item_text);
        label.setText(list.get(position));



        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
