package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.search_tribe;

import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;

/**
 * Created by daffodil on 14/2/17.
 */

public class SearchTribeResponse {

    private String searchFor;

    private Result[] result;

    public String getSearchFor() {
        return searchFor;
    }

    public void setSearchFor(String searchFor) {
        this.searchFor = searchFor;
    }

    public Result[] getResult() {
        return result;
    }

    public void setResult(Result[] result) {
        this.result = result;
    }
}
