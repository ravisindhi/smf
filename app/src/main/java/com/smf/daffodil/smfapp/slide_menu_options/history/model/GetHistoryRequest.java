package com.smf.daffodil.smfapp.slide_menu_options.history.model;

/**
 * Created by daffodil on 10/2/17.
 */

public class GetHistoryRequest {

    private String author;
    private String limit;
    private String skip;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }
}
