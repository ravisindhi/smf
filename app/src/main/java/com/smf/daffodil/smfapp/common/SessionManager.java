package com.smf.daffodil.smfapp.common;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.AuthActivity;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.home.HomeActivity;

/**
 * Created by daffodil on 23/1/17.
 */

public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    Gson gson = new Gson();

    // Sharedpref file name
    private static final String PREF_NAME = "SMF";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USER_DATA = "user_data";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Home Activity
            Intent i = new Intent(_context, AuthActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }else
        {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, HomeActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
    }


    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();




        SMFApplication.removeAllCookies();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, AuthActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }


    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void setIsLoggedIn() {

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }



    /**
     * Save user data in prefrences
     *
     * @param mData
     **/
    public boolean saveUserData(LoginResponse mData) {
        String userdata = gson.toJson(mData).toString();
        editor.putString(KEY_USER_DATA, userdata);
        return editor.commit();
    }

    public LoginResponse getUserData() {
        String user_data = pref.getString(KEY_USER_DATA, null);
        if (user_data != null)
            return gson.fromJson(user_data, LoginResponse.class);
        else
            return null;
    }
}
