package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model;

import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeMembersList;

import java.util.List;

/**
 * Created by daffodil on 16/2/17.
 */

public class AddMemberRequest {
    private String groupId;
    private CreateTribeMembersList member;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }


    public CreateTribeMembersList getMembers() {
        return member;
    }

    public void setMembers(CreateTribeMembersList members) {
        this.member = members;
    }
}
