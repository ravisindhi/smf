package com.smf.daffodil.smfapp.slide_menu_options.messages.chat.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.request.MessageDetailRequest;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.response.MessageDetailResponse;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.view.IMessageDetailView;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public class IMessageDetailPresenterCompl implements IMessageDetailPresenter {

    IMessageDetailView iMessageDetailView;
    Context context;


    public IMessageDetailPresenterCompl(IMessageDetailView iMessagesView , Context context)
    {
        this.iMessageDetailView=iMessagesView;
        this.context=context;
    }


    @Override
    public void getMessageComment(MessageDetailRequest messageDetailRequest, boolean isLoadMore) {

        if(iMessageDetailView.onCheckNetworkConnection())
        {
            iMessageDetailView.showLoader(context.getString(R.string.please_wait_message));

            SMFApplication.getRestClient().getMessageComment(new StringRequestCallback<MessageDetailResponse>() {
                @Override
                public void onRestResponse(Exception e, MessageDetailResponse result) {
                    iMessageDetailView.hideLoader();
                    iMessageDetailView.onGetMessageComment(result);
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    iMessageDetailView.hideLoader();

                    try{
                        iMessageDetailView.hideLoader();
                        e.printStackTrace();
                        iMessageDetailView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                    }catch (Exception ex)
                    {
                        e.printStackTrace();
                        iMessageDetailView.onError(IBaseView.NETWORK_ERROR,"Internal Error please try again !!");
                    }

                }
            },messageDetailRequest);
        }else {
            iMessageDetailView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }
}
