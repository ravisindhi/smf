package com.smf.daffodil.smfapp.bottom_menu_option.post.model.response;

/**
 * Created by daffodil on 3/2/17.
 */

public class PostProfile {

    private String streetAddress;

    private String lastName;

    private String handle;

    private String searchEmail;

    private String profileImage;

    private String webAddress;

    private String state;

    private String facebookProfileUrl;

    private String linkedInProfileUrl;

    private String intro;

    private String[] hiddenPosts;

    private String city;

    private String title;

    private String phoneNumber;

    private String subscription;

    private String company;

    private String dob;

    private String fullName;

    private String twitterProfileUrl;

    private String firstName;

    public String getStreetAddress ()
    {
        return streetAddress;
    }

    public void setStreetAddress (String streetAddress)
    {
        this.streetAddress = streetAddress;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getHandle ()
    {
        return handle;
    }

    public void setHandle (String handle)
    {
        this.handle = handle;
    }

    public String getSearchEmail ()
    {
        return searchEmail;
    }

    public void setSearchEmail (String searchEmail)
    {
        this.searchEmail = searchEmail;
    }

    public String getProfileImage ()
    {
        return profileImage;
    }

    public void setProfileImage (String profileImage)
    {
        this.profileImage = profileImage;
    }

    public String getWebAddress ()
    {
        return webAddress;
    }

    public void setWebAddress (String webAddress)
    {
        this.webAddress = webAddress;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getFacebookProfileUrl ()
    {
        return facebookProfileUrl;
    }

    public void setFacebookProfileUrl (String facebookProfileUrl)
    {
        this.facebookProfileUrl = facebookProfileUrl;
    }

    public String getLinkedInProfileUrl ()
    {
        return linkedInProfileUrl;
    }

    public void setLinkedInProfileUrl (String linkedInProfileUrl)
    {
        this.linkedInProfileUrl = linkedInProfileUrl;
    }

    public String getIntro ()
    {
        return intro;
    }

    public void setIntro (String intro)
    {
        this.intro = intro;
    }

    public String[] getHiddenPosts ()
    {
        return hiddenPosts;
    }

    public void setHiddenPosts (String[] hiddenPosts)
    {
        this.hiddenPosts = hiddenPosts;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getPhoneNumber ()
    {
        return phoneNumber;
    }

    public void setPhoneNumber (String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getSubscription ()
    {
        return subscription;
    }

    public void setSubscription (String subscription)
    {
        this.subscription = subscription;
    }

    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getFullName ()
    {
        return fullName;
    }

    public void setFullName (String fullName)
    {
        this.fullName = fullName;
    }

    public String getTwitterProfileUrl ()
    {
        return twitterProfileUrl;
    }

    public void setTwitterProfileUrl (String twitterProfileUrl)
    {
        this.twitterProfileUrl = twitterProfileUrl;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [streetAddress = "+streetAddress+", lastName = "+lastName+", handle = "+handle+", searchEmail = "+searchEmail+", profileImage = "+profileImage+", webAddress = "+webAddress+", state = "+state+", facebookProfileUrl = "+facebookProfileUrl+", linkedInProfileUrl = "+linkedInProfileUrl+", intro = "+intro+", hiddenPosts = "+hiddenPosts+", city = "+city+", title = "+title+", phoneNumber = "+phoneNumber+", subscription = "+subscription+", company = "+company+", dob = "+dob+", fullName = "+fullName+", twitterProfileUrl = "+twitterProfileUrl+", firstName = "+firstName+"]";
    }
}
