package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response;

/**
 * Created by daffodil on 27/2/17.
 */

public class MessageProfile {

    private String profileImage;

    private String fullName;

    public String getProfileImage ()
    {
        return profileImage;
    }

    public void setProfileImage (String profileImage)
    {
        this.profileImage = profileImage;
    }

    public String getFullName ()
    {
        return fullName;
    }

    public void setFullName (String fullName)
    {
        this.fullName = fullName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [profileImage = "+profileImage+", fullName = "+fullName+"]";
    }
}
