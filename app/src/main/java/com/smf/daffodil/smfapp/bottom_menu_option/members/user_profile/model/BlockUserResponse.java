package com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.model;

/**
 * Created by daffodil on 9/2/17.
 */

public class BlockUserResponse {

    private String[] blockedUser;

    public String[] getBlockedUser ()
    {
        return blockedUser;
    }

    public void setBlockedUser (String[] blockedUser)
    {
        this.blockedUser = blockedUser;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [blockedUser = "+blockedUser+"]";
    }
}
