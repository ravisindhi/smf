package com.smf.daffodil.smfapp.bottom_menu_option.members;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.bottom_menu_option.members.adapter.MemberAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.presenter.IMemberCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.members.presenter.IMemberPresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.UserProfileActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.members.view.IMemberView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.home.HomeActivity;
import com.smf.daffodil.smfapp.utils.AppUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MembersFragment extends BaseFragment implements IMemberView, MemberAdapter.FollowUnfollowButtonClickListener {

    IMemberPresenter iMemberPresenter;
    private static final int SEARCH_TEXT_CHANGED = 101;

    @BindView(R.id.grid_view)
    StaggeredGridView staggeredGridView;

    @BindView(R.id.et_search)
    EditText editTextSearch;

    @BindView(R.id.search_progress_bar)
    ProgressBar progressBarSearch;

    MemberAdapter memberAdapter;
    List<MemberResponse> memberlist = new ArrayList<>();
    SessionManager sessionManager;


    public MembersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        iMemberPresenter = new IMemberCompl(this, getActivity());
        sessionManager=new SessionManager(getActivity());
        return inflater.inflate(R.layout.fragment_members, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        memberAdapter = new MemberAdapter(getActivity(), memberlist, this);
        staggeredGridView.setAdapter(memberAdapter);
        iMemberPresenter.getAllMembers();
        editTextSearch.addTextChangedListener(searchWatcher);


        staggeredGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(getActivity(), UserProfileActivity.class);
                intent.putExtra("member_data",memberlist.get(i).getProfile());
                if(Arrays.asList(sessionManager.getUserData().getConnections().getFavorites()).contains(memberlist.get(i).get_id()))
                    intent.putExtra("follow_text","Following");
                else
                    intent.putExtra("follow_text","Follow");

                intent.putExtra("user_id",memberlist.get(i).get_id());
                intent.putExtra("email",memberlist.get(i).getEmails()[0].getAddress());

               startActivity(intent);
            }
        });

    }
/*
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.members_menu, menu);
    }*/

    @Override
    public void onGetMember(MemberResponse[] list) {

        progressBarSearch.setVisibility(View.GONE);
        if (list.length > 0) {
            memberlist.clear();
            memberlist.addAll(Arrays.asList(list));
            memberAdapter.notifyDataSetChanged();

        } else {

            Toast.makeText(getActivity(), "No Member found !!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onError(int errorCode, String message) {
        switch (errorCode) {
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getContext().getApplicationContext(), message, getActivity());
                break;
        }
    }

    @Override
    public void onFollowUnfollowSuccess(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void followUnfollowClicked(String action, String member_id) {
        iMemberPresenter.followUnfollow(action, member_id);
    }

    /*Handle text watcher*/
    private final TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String handle_text = charSequence.toString().trim();

            progressBarSearch.setVisibility(View.VISIBLE);
            handler.removeMessages(SEARCH_TEXT_CHANGED);
            handler.sendMessageDelayed(handler.obtainMessage(SEARCH_TEXT_CHANGED, handle_text), 500);


        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String name = (String) msg.obj;

            if (name.length() > 0)
                iMemberPresenter.searchMemberByName((String) msg.obj);
            else
                iMemberPresenter.getAllMembers();
        }
    };


}
