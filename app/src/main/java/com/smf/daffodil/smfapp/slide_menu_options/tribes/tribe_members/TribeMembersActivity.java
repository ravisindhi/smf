package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberProfile;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.UserProfileActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.home.HomeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.TribesHomeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.adapter.TribesAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeMembersList;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.adapter.TribeMemberAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.adapter.TribeMemberSearchAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.AddMemberRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.TribesMemberSearchResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.presenter.ITribeMemberCompl;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.presenter.ITribeMemberPresneter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.view.ITribeMemberView;
import com.smf.daffodil.smfapp.utils.AppUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TribeMembersActivity extends BaseActivity implements
        ITribeMemberView, TribeMemberAdapter.OnRemoveMemberListener
        ,TribeMemberSearchAdapter.AddMemberLisetner {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_search)
    EditText editTextSearch;

    @BindView(R.id.search_progress_bar)
    ProgressBar progressBarSearch;

    @BindView(R.id.grid_view)
    StaggeredGridView staggeredGridView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.card_view_search)
    CardView cardViewSearch;

    @BindView(R.id.member_spinner)
    Spinner spinnerMemberList;

    private Result resultTribeDetail;
    private TribeMemberAdapter adapter;
    private int userType;
    private ITribeMemberPresneter iTribeMemberPresneter;
    private final int SEARCH_TEXT_CHANGED=111;
    private List<TribesMembers> list=new ArrayList<>();
    private TribeMemberSearchAdapter search_adapter;
    private SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tribe_members);

        ButterKnife.bind(this);
        iTribeMemberPresneter = new ITribeMemberCompl(TribeMembersActivity.this, this);
        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("MEMBERS", true, false);
        userType = getIntent().getIntExtra("user_type", 2);
        sessionManager=new SessionManager(this);

        if (userType == TribesHomeActivity.ADMIN_TYPE)
            cardViewSearch.setVisibility(View.VISIBLE);
        else
            cardViewSearch.setVisibility(View.GONE);

        resultTribeDetail = (Result) getIntent().getSerializableExtra("tribe_model");
        adapter = new TribeMemberAdapter(TribeMembersActivity.this, resultTribeDetail, userType, this);
        staggeredGridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        editTextSearch.addTextChangedListener(searchWatcher);

        staggeredGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(i>0)
                {
                    Intent intent=new Intent(TribeMembersActivity.this, UserProfileActivity.class);
                    intent.putExtra("follow_text","Following");
                    intent.putExtra("user_id",resultTribeDetail.getMembers()[i-1].get_id());

                    String json=new Gson().toJson(resultTribeDetail.getMembers()[i-1].getTribesId().getProfile());
                    MemberProfile profile=new Gson().fromJson(json,MemberProfile.class);
                    intent.putExtra("member_data",profile);
                    startActivity(intent);
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onSuccessRemoveMember(int position) {

        adapter.removemember(position);
    }

    @Override
    public void onError(int errorcode, String msg) {
        switch (errorcode) {
            case IBaseView.NETWORK_ERROR:
            case IBaseView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(), msg, this);
                break;
        }
    }

    @Override
    public void onGetMember(TribesMemberSearchResponse result) {
        progressBarSearch.setVisibility(View.GONE);
        if (result.getResult().length > 0) {
            list.clear();
            list.addAll(Arrays.asList(result.getResult()));

            search_adapter=new TribeMemberSearchAdapter(TribeMembersActivity.this,R.layout.search_tribe_member_item,list,TribeMembersActivity.this);
            spinnerMemberList.setAdapter(search_adapter);
            search_adapter.notifyDataSetChanged();
            spinnerMemberList.performClick();

        } else {


            Toast.makeText(this, "No Member found !!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAddMember() {
        editTextSearch.setText("");
        spinnerMemberList.clearFocus();
        showErrorMessage(getApplicationContext(), "Member added successfully .", this);
    }

    @Override
    public void onRemoveMember(int position) {
        JoinTribeRequest request = new JoinTribeRequest(resultTribeDetail.get_id(),
                resultTribeDetail.getMembers()[position - 1].get_id());
        request.setPosition(position);
        iTribeMemberPresneter.removeTribeMember(request);
    }


    /*Handle text watcher*/
    private final TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String handle_text = charSequence.toString().trim();

            progressBarSearch.setVisibility(View.VISIBLE);
            handler.removeMessages(SEARCH_TEXT_CHANGED);
            handler.sendMessageDelayed(handler.obtainMessage(SEARCH_TEXT_CHANGED, handle_text), 500);


        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String name = (String) msg.obj;

            if (name.length() > 0)
                iTribeMemberPresneter.searchMember((String) msg.obj);
            else
                progressBarSearch.setVisibility(View.GONE);

        }
    };

    @Override
    public void addMember(String userid) {
        AddMemberRequest request=new AddMemberRequest();
        request.setGroupId(resultTribeDetail.get_id());
        request.setMembers(new CreateTribeMembersList(userid,"pending"));
        View root = spinnerMemberList.getRootView();
        root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.ACTION_DOWN));
        iTribeMemberPresneter.addTribeMember(request);
    }


}
