package com.smf.daffodil.smfapp.slide_menu_options.change_password.model;

/**
 * Created by daffodil on 1/2/17.
 */

public class ChangePasswordResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
