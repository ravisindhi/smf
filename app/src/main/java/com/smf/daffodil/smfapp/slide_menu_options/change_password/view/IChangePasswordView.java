package com.smf.daffodil.smfapp.slide_menu_options.change_password.view;


import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordResponse;

/**
 * Created by daffodil on 1/2/17.
 */

public interface IChangePasswordView extends IBaseView{


    public static int OLD_PASSWORD_ERROR=1;
    public static int NEW_PASSWORD_ERROR=2;
    public static int CONFIRM_PASSWORD_ERROR=3;
    public static int PASSWORD_MISMATCH=4;

    void onError(int errorCode);
    void onErrorWithMessage(int errorCode,String message);
    void onSuccess(ChangePasswordResponse response);

}
