package com.smf.daffodil.smfapp.view;

/**
 * Created by daffodil on 4/2/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
