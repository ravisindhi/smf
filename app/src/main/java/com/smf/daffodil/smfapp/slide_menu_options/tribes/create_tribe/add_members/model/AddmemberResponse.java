package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.model;

import java.io.Serializable;

/**
 * Created by daffodil on 22/2/17.
 */

public class AddmemberResponse implements Serializable{

    private AddmemberResult[] result;

    private String searchFor;

    public AddmemberResult[] getResult ()
    {
        return result;
    }

    public void setResult (AddmemberResult[] result)
    {
        this.result = result;
    }

    public String getSearchFor ()
    {
        return searchFor;
    }

    public void setSearchFor (String searchFor)
    {
        this.searchFor = searchFor;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", searchFor = "+searchFor+"]";
    }
}
