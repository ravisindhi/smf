
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @SerializedName("createdAt")
    @Expose
    private Long createdAt;
    @SerializedName("sourceUser")
    @Expose
    private String sourceUser;
    @SerializedName("read")
    @Expose
    private Boolean read;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("actionItemId")
    @Expose
    private String actionItemId;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("direct")
    @Expose
    private Boolean direct;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("isNewRequest")
    @Expose
    private Boolean isNewRequest;

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getSourceUser() {
        return sourceUser;
    }

    public void setSourceUser(String sourceUser) {
        this.sourceUser = sourceUser;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getActionItemId() {
        return actionItemId;
    }

    public void setActionItemId(String actionItemId) {
        this.actionItemId = actionItemId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getDirect() {
        return direct;
    }

    public void setDirect(Boolean direct) {
        this.direct = direct;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsNewRequest() {
        return isNewRequest;
    }

    public void setIsNewRequest(Boolean isNewRequest) {
        this.isNewRequest = isNewRequest;
    }

}
