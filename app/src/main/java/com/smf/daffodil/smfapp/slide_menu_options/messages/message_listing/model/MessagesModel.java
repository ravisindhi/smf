package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model;

/**
 * Created by daffodil on 30/1/17.
 */

public class MessagesModel {

    public static final int MESSAGE_TYPE_NEW=1;
    public static final int MESSAGE_TYPE_OLD=2;

    private String profileImage;
    private String name;
    private String message;
    private int messageType;

    public MessagesModel(String profileImage, String name, String message, int messageType) {
        this.profileImage = profileImage;
        this.name = name;
        this.message = message;
        this.messageType = messageType;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }



    public static int getMessageTypeNew() {
        return MESSAGE_TYPE_NEW;
    }

    public static int getMessageTypeOld() {
        return MESSAGE_TYPE_OLD;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
