package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.MessageDetailActivity;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.adapter.MessagesAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.request.AllMessagesRequest;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response.AllMessagesResponse;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response.Result;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.presenter.IMessagesPresenter;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.presenter.IMessagesPresenterCompl;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.view.IMessagesView;
import com.smf.daffodil.smfapp.view.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesActivity extends BaseActivity implements AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener, IMessagesView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.message_list_recyclerview)
    RecyclerView recyclerViewMessages;



    private MessagesAdapter adapter;
    List<Result> results=new ArrayList<>();
    AllMessagesRequest allMessagesRequest = new AllMessagesRequest();
    IMessagesPresenter iMessagesPresenter;
    private boolean isFromMember=false;
    private SessionManager sessionManager;
    private int flag=0;





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);

        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("MESSAGES", true, false);

        iMessagesPresenter = new IMessagesPresenterCompl(this, MessagesActivity.this);

        adapter = new MessagesAdapter(this,results);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewMessages.setLayoutManager(mLayoutManager);
        recyclerViewMessages.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMessages.setAdapter(adapter);
        getAllMessages();

        recyclerViewMessages.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent i=new Intent(MessagesActivity.this, MessageDetailActivity.class);
                String recepientid;
                if(results.get(position).getAuthor().equals(sessionManager.getUserData().get_id())){
                    i.putExtra("recepient_id",results.get(position).getRecipient());
                    i.putExtra("name",results.get(position).getReceiverDetail()[0].getProfile().getFullName());
                    i.putExtra("profile_url",results.get(position).getReceiverDetail()[0].getProfile().getProfileImage());

                }else{
                    i.putExtra("recepient_id",results.get(position).getAuthor());
                    i.putExtra("name",results.get(position).getSenderDetail()[0].getProfile().getFullName());
                    i.putExtra("profile_url",results.get(position).getSenderDetail()[0].getProfile().getProfileImage());

                }
                i.putExtra("isReply",true);
                i.putExtra("msg_id",results.get(position).get_id());
                startActivity(i);
            }
        }));


        //   insertDummyData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }


    @Override
    public void onRefresh() {

    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {

    }

    private void getAllMessages() {
        //allMessagesRequest.setLimit("10");
        // allMessagesRequest.setSkip("0");
        iMessagesPresenter.getAllMessages(allMessagesRequest, false);

    }

    private void loadMoreAllMessages(String limit, String skip) {
        allMessagesRequest.setLimit(limit);
        allMessagesRequest.setSkip(skip);
        iMessagesPresenter.getAllMessages(allMessagesRequest, true);
    }

    @Override
    public void onGetMessages(AllMessagesResponse allMessagesResponse) {
        results.clear();
        results.addAll(allMessagesResponse.getResult());
        adapter.notifyDataSetChanged();
        Intent intent=new Intent(MessagesActivity.this, MessageDetailActivity.class);
        if(getIntent().getBooleanExtra("isFromMember",false)){


            for(int i=0;i<results.size();i++){
                if(getIntent().getStringExtra("userid").equals(results.get(i).getRecipient())||
                        getIntent().getStringExtra("userid").equals(results.get(i).getAuthor())){
                    flag=1;
                    break;
                }
            }
            if(flag==0){

                intent.putExtra("isReply",false);
                intent.putExtra("recepient_id",getIntent().getStringExtra("userid"));
                startActivity(intent);
            }
        }
        Log.e("done","dona done doneeeeeeeeee");
    }


    @Override
    public void onAppendMoreData(AllMessagesResponse allMessagesResponse) {

    }

    @Override
    public void onError(int errorcode, String message) {

    }
}
