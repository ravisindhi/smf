package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.adapter.PostAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.SearchPostActivity;
import com.smf.daffodil.smfapp.common.Constants;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.common.presenter.IActionCompl;
import com.smf.daffodil.smfapp.common.presenter.IActionPresenter;
import com.smf.daffodil.smfapp.common.view.IActionView;
import com.smf.daffodil.smfapp.post_and_update.PostAndUpdateActivity;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.TribesHomeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.CreateTribeActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.AllTribesPostRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.CloseTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.presenter.ITribeDetailCompl;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.presenter.ITribeDetailPresenter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.view.ITribeDetailView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.TribeMembersActivity;
import com.smf.daffodil.smfapp.view.OnLoadMoreListener;
import com.smf.daffodil.smfapp.view.PopupDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TribeDetailAdminActivity extends BaseActivity implements ITribeDetailView,PopupDialog.DilogButtonHandling
        ,PostAdapter.PostActionsListener,IActionView{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

   /* @BindView(R.id.et_search)
    EditText et_search;*/

    @BindView(R.id.post_recyclerview)
    RecyclerView recyclerViewPosts;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

   /* @BindView(R.id.card_view_search)
    CardView cardViewSearch;*/

    @BindView(R.id.card_view_share)
    CardView cardViewShare;

    private PopupWindow settings_popup;
    private Point point;
    private int totalDisplayFeed;
    private int totalFeeds;
    private int userType;

    private PostAdapter adapter;
    //all posts datas
    List<GetPostDataResponse> postDatas = new ArrayList<>();
    AllTribesPostRequest getPostRequest = new AllTribesPostRequest();
    ITribeDetailPresenter iTribeDetailPresenter;
    IActionPresenter iActionPresenter;
    private SessionManager sessionManager;

    private final int ADMIN_TYPE = 1;
    private final int USER_TYPE = 2;
    private Result result;
    private PopupDialog popupDialog;
    private PopupWindow post_popup;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tribe_detail);

        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        iTribeDetailPresenter = new ITribeDetailCompl(TribeDetailAdminActivity.this, this);
        iActionPresenter = new IActionCompl(TribeDetailAdminActivity.this, this);

        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName(getIntent().getStringExtra("tribe_name"), true, false);
        userType = getIntent().getIntExtra("user_type", TribesHomeActivity.USER_TYPE);
        //et_search.setInputType(InputType.TYPE_NULL);
        result= (Result) getIntent().getSerializableExtra("tribe_detail_model");

        initViews();
        refreshLayout();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_members:
                Intent intent = new Intent(TribeDetailAdminActivity.this, TribeMembersActivity.class);
                intent.putExtra("user_type", ADMIN_TYPE);
                intent.putExtra("tribe_model", getIntent().getSerializableExtra("tribe_detail_model"));
                startActivity(intent);
                break;
            case R.id.action_settings:
                View menuItemView = findViewById(R.id.action_settings);
                showPopup(menuItemView, ADMIN_TYPE);
                break;
            case R.id.action_dots:
                View menuItemView2 = findViewById(R.id.action_dots);
                showPopup(menuItemView2, USER_TYPE);

                break;
            case R.id.action_search:
                Intent intent1 = new Intent(TribeDetailAdminActivity.this, SearchPostActivity.class);
                intent1.putExtra("isFromTribe", true);
                intent1.putExtra("group_id",result.get_id());
                startActivity(intent1);
                break;
        }
        return true;
    }


    private void showPopup(View v, int type) {
        int[] location = new int[2];

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        v.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        point = new Point();
        point.x = location[0];
        point.y = location[1];
        showStatusPopup(TribeDetailAdminActivity.this, point, type);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (userType == TribesHomeActivity.ADMIN_TYPE)
            inflater.inflate(R.menu.action_bar_tribe_admin_menu, menu);
        else
            inflater.inflate(R.menu.action_bar_tribe_menu, menu);
        return true;
    }



    private void showStatusPopup(final Activity context, Point p, final int type) {

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.tribes_settings_popup_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.tribes_settings_popupview, null);

        /*action button*/
        LinearLayout actionoption1 = (LinearLayout) layout.findViewById(R.id.action_option1);
        LinearLayout actionoption2 = (LinearLayout) layout.findViewById(R.id.action_option2);

        /*inner items*/
        ImageView icon1 = (ImageView) layout.findViewById(R.id.popup_icon1);
        ImageView icon2 = (ImageView) layout.findViewById(R.id.popup_icon2);

        TextView textView1 = (TextView) layout.findViewById(R.id.popup_text1);
        TextView textView2 = (TextView) layout.findViewById(R.id.popup_text2);

        if (type == ADMIN_TYPE) {
            icon1.setImageResource(R.drawable.ic_edit_tribe);
            icon2.setImageResource(R.drawable.ic_close_tribe);
            textView1.setText("Edit Tribe");
            textView2.setText("Permanently Close Tribe");
        } else {
            icon1.setImageResource(R.drawable.ic_action_members_small);
            icon2.setImageResource(R.drawable.ic_action_join_tribe);
            textView1.setText("Members");
            textView2.setText("Join Tribe");
        }

        actionoption1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == ADMIN_TYPE) {
                    Intent i = new Intent(TribeDetailAdminActivity.this, CreateTribeActivity.class);
                    i.putExtra("isFromEdit", true);
                    i.putExtra("tribe_model", getIntent().getSerializableExtra("tribe_detail_model"));
                    startActivity(i);
                } else {

                    Intent intent = new Intent(TribeDetailAdminActivity.this, TribeMembersActivity.class);
                    intent.putExtra("tribe_model", getIntent().getSerializableExtra("tribe_detail_model"));
                    intent.putExtra("user_type", USER_TYPE);
                    startActivity(intent);
                }

                settings_popup.dismiss();
            }
        });

        actionoption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == ADMIN_TYPE) {
                    iTribeDetailPresenter.closeTribe(result.get_id());
                } else {

                    JoinTribeRequest joinTribeRequest = new JoinTribeRequest(result.get_id(), sessionManager.getUserData().get_id());
                    iTribeDetailPresenter.joinTribe(joinTribeRequest);
                }
                settings_popup.dismiss();
            }
        });


        // Creating the PopupWindow
        settings_popup = new PopupWindow(context);
        settings_popup.setContentView(layout);

        settings_popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        settings_popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        settings_popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int OFFSET_X = -20;
        int OFFSET_Y = 100;

        //Clear the default translucent background
        settings_popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        settings_popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);
    }

    private void refreshLayout() {
        //swipeRefreshLayout.setColorSchemeResources(getActivity().getResources().getColor(colorButton));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                getPosts();
            }
        });
    }

    private void getPosts() {
        showLoader("Please wait...");
        getPostRequest.setSkip("0");
        getPostRequest.setLimit("10");
        getPostRequest.setGroupid(getIntent().getStringExtra("group_id"));
        iTribeDetailPresenter.getAllPosts(getPostRequest);
    }

    private void initViews() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewPosts.setLayoutManager(mLayoutManager);
        recyclerViewPosts.setItemAnimator(new DefaultItemAnimator());

        adapter = new PostAdapter(postDatas, this, recyclerViewPosts,this, Constants.POST_SCREEN_TYPE_TRIBE);
        recyclerViewPosts.setAdapter(adapter);

    }

    @Override
    public void onSuccess(PostDataResult dataResponses) {
        postDatas.clear();
        Log.e("data size", String.valueOf(dataResponses.getResult().size()));
        Log.e("total size", String.valueOf(dataResponses.getTotalfeedscount()));

        totalDisplayFeed = dataResponses.getResult().size();
        totalFeeds = dataResponses.getTotalfeedscount();

        swipeRefreshLayout.setRefreshing(false);
        postDatas.addAll(dataResponses.getResult());
        adapter.notifyDataSetChanged();


        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                Log.e("couunt>>DisplayFeed", String.valueOf(totalFeeds) + ">>>>" + String.valueOf(totalDisplayFeed));

                doLoadMore();

            }
        });
    }

    private void doLoadMore() {
        String oldskip = getPostRequest.getSkip();
        String oldlimit = getPostRequest.getLimit();

        String newSkip = String.valueOf(Integer.parseInt(oldlimit));
        String newLimit = String.valueOf(Integer.parseInt(oldlimit) + 10);
        adapter.loadMoreStart();
        getPostRequest.setSkip(newSkip);
        getPostRequest.setLimit(newLimit);
        iTribeDetailPresenter.getAllPosts(getPostRequest);
    }

    @Override
    public void onAppendData(PostDataResult dataResponses) {
        swipeRefreshLayout.setRefreshing(false);
        Log.e("data size load more", String.valueOf(dataResponses.getResult().size()));
        totalDisplayFeed = totalDisplayFeed + dataResponses.getResult().size();
        adapter.loadMoreData(dataResponses.getResult());
    }


    @Override
    public void onError(int errorcode, String message) {
        switch (errorcode) {

            case ILoginView.NETWORK_ERROR:
            case ITribeDetailView.NODATA:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(this.getApplicationContext(), message, this);
                break;
        }
    }

    @Override
    public void onTribeClose(CloseTribeResponse closeTribeResponse) {
        showErrorMessage(this.getApplicationContext(), closeTribeResponse.getMessage(), this);
        finish();
    }

    @Override
    public void onSuccessFullJoinTribe() {

        popupDialog = new PopupDialog(this);
        Bundle bundle = new Bundle();
        bundle.putString("name", "Success");
        bundle.putString("content", "Your request to join Tribe "+getIntent().getStringExtra("tribe_name")+" has been sent successfully");
        bundle.putString("accept_text", "Ok");
        bundle.putString("cancel_text", null);
        bundle.putString("action_type","join_tribe_success");
        popupDialog.setArguments(bundle);
        popupDialog.show(getFragmentManager(),"new");

    }

    @Override
    public void onJoinError(ErrorModel errorModel) {
        popupDialog = new PopupDialog(this);
        Bundle bundle = new Bundle();
        bundle.putString("name", "Alert");
        bundle.putString("content", "You already sent request to join Tribe "+getIntent().getStringExtra("tribe_name")+"");
        bundle.putString("accept_text", "Ok");
        bundle.putString("cancel_text", null);
        bundle.putString("action_type","join_tribe_success");
        popupDialog.setArguments(bundle);
        popupDialog.show(getFragmentManager(),"new");
    }


    @OnClick(R.id.card_view_share)
    public void addPost() {
        if (userType == ADMIN_TYPE || isMemberPresentInTribe(Arrays.asList(result.getMembers()))) {
            Intent intent = new Intent(TribeDetailAdminActivity.this, PostAndUpdateActivity.class);
            intent.putExtra("isFromTribe", true);
            intent.putExtra("group_id",result.get_id());
            startActivity(intent);
        }else{

            popupDialog = new PopupDialog(this);
            Bundle bundle = new Bundle();
            bundle.putString("name", "No Access");
            bundle.putString("content", "You are not a member ,Do you want to join this Tribe ?");
            bundle.putString("accept_text", "Yes");
            bundle.putString("cancel_text", "No");
            bundle.putString("action_type","no_access");
            popupDialog.setArguments(bundle);
            popupDialog.show(getFragmentManager(),"new");

        }

    }


    private boolean isMemberPresentInTribe(List<TribesMembers> list)
    {
        boolean ispresent=false;
        String user_id=sessionManager.getUserData().get_id();
        for (int i=0;i<list.size();i++){
            if(list.get(i).get_id().equals(user_id))
                ispresent=true;
        }
        return ispresent;

    }

    @Override
    public void buttonYesClicked(String type) {
        popupDialog.dismiss();
        if(type.equals("no_access")){
            JoinTribeRequest joinTribeRequest = new JoinTribeRequest(result.get_id(), sessionManager.getUserData().get_id());
            iTribeDetailPresenter.joinTribe(joinTribeRequest);
        }else if(type.equals("join_tribe_success")){
            popupDialog.dismiss();
        }

    }

    @Override
    public void buttonNoClicked() {
        popupDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPosts();
    }

    @Override
    public void onUpdatePostAction(int position, PostActions actiontype) {
        switch (actiontype) {
            case like:
                if(!postDatas.get(position).getLikedBy().contains(sessionManager.getUserData().get_id()))
                    postDatas.get(position).getLikedBy().add(sessionManager.getUserData().get_id());
                else
                    postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());

                /*postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case dislike:
                if(!postDatas.get(position).getDislikedBy().contains(sessionManager.getUserData().get_id()))
                    postDatas.get(position).getDislikedBy().add(sessionManager.getUserData().get_id());
                else
                    postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());

                /* postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case peace:
                if(!postDatas.get(position).getPeaceBy().contains(sessionManager.getUserData().get_id()))
                    postDatas.get(position).getPeaceBy().add(sessionManager.getUserData().get_id());
                else
                    postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());

                /*postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                */break;
        }
        adapter.notifyItemChanged(position);
    }

    @Override
    public void onHidePost(int position) {
        hidePost(position);
        Toast.makeText(this, "Post hide successfully", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onReportPost() {
        Toast.makeText(this, "Post has been reported.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBlockUser(String handle) {
        Toast.makeText(this, handle + " blocked successfully.", Toast.LENGTH_LONG).show();
        getPosts();

    }

    @Override
    public void onSharePost(PostActionResponse response) {
        Toast.makeText(this, "Post shared successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAddComment(int position) {
        postDatas.get(position).setCommentsCount(postDatas.get(position).getCommentsCount() + 1);
        adapter.notifyItemChanged(position);
        Toast.makeText(this, "Comment added successfully", Toast.LENGTH_LONG).show();
    }


    @Override
    public void performAction(UpdatePostRequest request, int position) {
        iActionPresenter.updateGroupPost(request, position);
    }

    @Override
    public void openPopupview(Context context, Point p, final int position, final GetPostDataResponse response) {
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) findViewById(R.id.post_popup_view_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.post_pop_view_layout, null);

        /*action button*/
        LinearLayout actionReportPost = (LinearLayout) layout.findViewById(R.id.action_report_post);
        LinearLayout actionHidePost = (LinearLayout) layout.findViewById(R.id.action_hide_post);
        LinearLayout actionBlockUser = (LinearLayout) layout.findViewById(R.id.action_block_user);
        TextView userName = (TextView) actionBlockUser.findViewById(R.id.popup_user_handle);

        userName.setText("Block @" + response.getAuthor().getProfile().getHandle());

        // Creating the PopupWindow
        post_popup = new PopupWindow(context);
        post_popup.setContentView(layout);

        post_popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int OFFSET_X = -500;
        int OFFSET_Y = 70;

        //Clear the default translucent background
        post_popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        post_popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);


        actionHidePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doHidePost(response.get_id(), position);
                hidePost(position);
                post_popup.dismiss();
            }
        });

        actionReportPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doPostReport(response.get_id());
                post_popup.dismiss();
            }
        });

        actionBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doBlockUser(response.getAuthor().get_id(),
                        "@" + response.getAuthor().getProfile().getHandle());
                post_popup.dismiss();
            }
        });


    }

    @Override
    public void sharePost(String postid) {

        SharePostRequest request = new SharePostRequest();
        request.setCaption("test sharing post");
        request.setPost_id(postid);
        request.setCordinates(new String[]{"0", "0"});
        iActionPresenter.doSharePost(request);
    }

    @Override
    public void addComment(String postid, String comment_txt, int position) {
        iActionPresenter.doAddComment(postid, comment_txt, position,"group");
    }


    private void hidePost(int position) {
        postDatas.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, postDatas.size());
    }
}
