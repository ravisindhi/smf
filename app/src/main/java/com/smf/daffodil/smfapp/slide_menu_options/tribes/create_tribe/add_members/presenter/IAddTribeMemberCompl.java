package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.view.IAddTribeMemberView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.TribesMemberSearchResponse;

/**
 * Created by daffodil on 22/2/17.
 */

public class IAddTribeMemberCompl implements IAddTribeMemberPresenter{

    private Context context;
    private IAddTribeMemberView iAddTribeMemberView;

    public IAddTribeMemberCompl(Context context, IAddTribeMemberView iAddTribeMemberView) {
        this.context = context;
        this.iAddTribeMemberView = iAddTribeMemberView;
    }

    @Override
    public void getMembers() {
        if(iAddTribeMemberView.onCheckNetworkConnection())
        {
            iAddTribeMemberView.showLoader("Please wait...");
            SMFApplication.getRestClient().groupMemberSearchByName(new StringRequestCallback<TribesMemberSearchResponse>() {

                @Override
                public void onRestResponse(Exception e, TribesMemberSearchResponse result) {
                    if(e==null && result !=null)
                    {
                        iAddTribeMemberView.onGetMember(result);
                        iAddTribeMemberView.hideLoader();
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    e.printStackTrace();
                    iAddTribeMemberView.hideLoader();
                    iAddTribeMemberView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                }

            },"");
        }else
        {
            iAddTribeMemberView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }
}
