package com.smf.daffodil.smfapp.splash.presenter;

import android.os.Handler;

import com.smf.daffodil.smfapp.splash.view.iSplashView;

/**
 * Created by daffodil on 23/1/17.
 */

public class ISplashCompl implements ISplashPresenter {

    iSplashView splashView;
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    public ISplashCompl(iSplashView iSplashView)
    {
        this.splashView=iSplashView;
    }

    @Override
    public void starTimer() {

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                splashView.onTimeComplete();
            }
        }, SPLASH_TIME_OUT);
    }
}
