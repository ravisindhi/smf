package com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 20/2/17.
 */

public class CommentsViewHolder2 extends RecyclerView.ViewHolder {

    @BindView(R.id.rounded_image_view_comment)
    ImageView commentuserImage;

    @BindView(R.id.txt_name_comment)
    TextView commentUserName;

    @BindView(R.id.txt_content_comment)
    TextView textViewCommentContent;

    @BindView(R.id.hot_comment_txt)
    TextView textViewCommentLikes;

    @BindView(R.id.txt_comment_time)
    TextView textViewCommentTime;


    public CommentsViewHolder2(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
