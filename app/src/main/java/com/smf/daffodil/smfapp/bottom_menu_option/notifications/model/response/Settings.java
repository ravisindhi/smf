
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Settings {

    @SerializedName("deactivated")
    @Expose
    private Boolean deactivated;
    @SerializedName("profilePrivate")
    @Expose
    private Boolean profilePrivate;
    @SerializedName("addressPublic")
    @Expose
    private Boolean addressPublic;
    @SerializedName("emailPublic")
    @Expose
    private Boolean emailPublic;
    @SerializedName("phonePublic")
    @Expose
    private Boolean phonePublic;

    public Boolean getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(Boolean deactivated) {
        this.deactivated = deactivated;
    }

    public Boolean getProfilePrivate() {
        return profilePrivate;
    }

    public void setProfilePrivate(Boolean profilePrivate) {
        this.profilePrivate = profilePrivate;
    }

    public Boolean getAddressPublic() {
        return addressPublic;
    }

    public void setAddressPublic(Boolean addressPublic) {
        this.addressPublic = addressPublic;
    }

    public Boolean getEmailPublic() {
        return emailPublic;
    }

    public void setEmailPublic(Boolean emailPublic) {
        this.emailPublic = emailPublic;
    }

    public Boolean getPhonePublic() {
        return phonePublic;
    }

    public void setPhonePublic(Boolean phonePublic) {
        this.phonePublic = phonePublic;
    }

}
