package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model;

/**
 * Created by daffodil on 17/2/17.
 */

public class NotificationClearResponse {

    private String ok;
    private String nModified;
    private String n;

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getnModified() {
        return nModified;
    }

    public void setnModified(String nModified) {
        this.nModified = nModified;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }
}
