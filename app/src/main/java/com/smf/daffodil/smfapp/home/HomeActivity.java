package com.smf.daffodil.smfapp.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.model.response.Profile;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.map.MapFragment;
import com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.SearchPostActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.MessagesActivity;
import com.smf.daffodil.smfapp.slide_menu_options.my_profile.MyProfileActivity;
import com.smf.daffodil.smfapp.slide_menu_options.slide_menu_lib.SlidingMenu;
import com.smf.daffodil.smfapp.bottom_menu_option.post.PostFragment;
import com.smf.daffodil.smfapp.bottom_menu_option.members.MembersFragment;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.NotificationsFragment;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.TribesHomeActivity;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener,View.OnClickListener{

    private SlidingMenu menu;
    private SessionManager sessionManager;

    @BindView(R.id.bottom_navigation_bar)
    BottomNavigationViewEx navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.home_container)
    LinearLayout linearLayoutHomeContainer;


    /*Slide Menu controllers*/
    private View menuView ;
    private ImageView btnCross;
    private LinearLayout linearLayoutMyProfile;
    private LinearLayout linearLayoutMessages;
    private LinearLayout linearLayoutMyTribes;
    private LinearLayout linearLayoutSearch;
    private LinearLayout linearLayoutLogout;
    private CircularImageView slide_profile_image;
    private TextView slide_name_txt;
    private TextView slide_email_txt;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        sessionManager.setIsLoggedIn();
        setupToolbar(toolbar,R.layout.toolbar_home_layout);
        setupToolBarName("Location",false,true);
        addSlideMenu();
        setupHome();
        setUpBottomNavigationView();
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        navigationView.setOnNavigationItemSelectedListener(this);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile profile=sessionManager.getUserData().getProfile();
        String email=sessionManager.getUserData().getEmails()[0].getAddress();
        ImageUtils.setprofile(slide_profile_image,profile.getProfileImage(),this);

       // Glide.with(this).load(profile.getProfileImage()).error(R.drawable.dummy_rounded).into(slide_profile_image);
        slide_name_txt.setText(profile.getFirstName()+" "+profile.getLastName());
        slide_email_txt.setText(email);


    }

    private void setupHome()
    {
        fragmentTransaction(R.id.home_container,new PostFragment());
        setupToolBarName("Location",false,true);
    }



    private void setUpBottomNavigationView()
    {
        navigationView.enableAnimation(false);
        navigationView.enableShiftingMode(false);
        navigationView.enableItemShiftingMode(false);
        navigationView.setTextVisibility(false);
    }

    private void addSlideMenu()
    {
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setSelectorEnabled(false);
        menu.setFadeDegree(0.35f);
        menu.setSlidingEnabled(false);
        menu.attachToActivity(this, SlidingMenu.TOUCHMODE_FULLSCREEN);

        /*Slide Menu views*/
        menuView = getLayoutInflater().inflate(R.layout.slide_menu_layout, null);
        btnCross=(ImageView)menuView.findViewById(R.id.btn_cross);
        slide_profile_image=(CircularImageView) menuView.findViewById(R.id.slide_menu_profile_image);
        slide_name_txt=(TextView) menuView.findViewById(R.id.slide_menu_name);
        slide_email_txt=(TextView) menuView.findViewById(R.id.slide_menu_email);
        linearLayoutMyProfile=(LinearLayout)menuView.findViewById(R.id.slide_my_profile_option) ;
        linearLayoutMessages=(LinearLayout)menuView.findViewById(R.id.slide_messages_option) ;
        linearLayoutMyTribes=(LinearLayout)menuView.findViewById(R.id.slide_tribes_option) ;
        linearLayoutSearch=(LinearLayout)menuView.findViewById(R.id.slide_search_option) ;
        linearLayoutLogout=(LinearLayout)menuView.findViewById(R.id.slide_logout_option) ;


        /*Set click listeners*/
        btnCross.setOnClickListener(this);
        linearLayoutMyProfile.setOnClickListener(this);
        linearLayoutMessages.setOnClickListener(this);
        linearLayoutMyTribes.setOnClickListener(this);
        linearLayoutSearch.setOnClickListener(this);
        linearLayoutLogout.setOnClickListener(this);
        menu.setMenu(menuView);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()!=R.id.action_menu)
        {
            if(item.isChecked())
                item.setChecked(false);
            else
                item.setChecked(true);
        }
        switch (item.getItemId())
        {
            case R.id.action_info:
                fragmentTransaction(R.id.home_container,new PostFragment());
                setupToolBarName("Location",false,true);
                break;
            case R.id.action_map:
                fragmentTransaction(R.id.home_container,new MapFragment());
                setupToolBarName("ADD A LOCATION",false,false);
                break;
            case R.id.action_users:
                fragmentTransaction(R.id.home_container,new MembersFragment());
                setupToolBarName("MEMBERS",false,false);
                break;
            case R.id.action_bell:
                fragmentTransaction(R.id.home_container,new NotificationsFragment());
                setupToolBarName("NOTIFICATIONS",false,false);
                break;
            case R.id.action_menu:
                    menu.toggle();
                break;

        }
        return false;
    }



    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_cross:
                menu.toggle();
                break;
            case R.id.slide_my_profile_option:
                launchActivity(this, MyProfileActivity.class);
                menu.toggle();
                break;
            case R.id.slide_messages_option:
                launchActivity(this, MessagesActivity.class);
                menu.toggle();
                break;
            case R.id.slide_tribes_option:
                launchActivity(this, TribesHomeActivity.class);
                menu.toggle();
                break;
            case R.id.slide_search_option:
                launchActivity(this, SearchPostActivity.class);
                menu.toggle();
                break;
            case R.id.slide_logout_option:
                menu.toggle();
                sessionManager.logoutUser();
                finish();
                break;
        }
    }
}
