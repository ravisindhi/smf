package com.smf.daffodil.smfapp.bottom_menu_option.notifications.presenter;

import android.content.Context;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.AcceptRejectRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.AcceptRejectResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.NotificationClearResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response.Notification;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response.NotificationResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.view.INotificationView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;

/**
 * Created by daffodil on 17/2/17.
 */

public class INotificationsCompl implements INotificationsPresenter{

    private INotificationView iNotificationView;
    private Context context;

    public INotificationsCompl(INotificationView iNotificationView, Context context) {
        this.iNotificationView = iNotificationView;
        this.context = context;
    }


    @Override
    public void getAllNotifications() {

        iNotificationView.showLoader(context.getString(R.string.please_wait_message));

        if(iNotificationView.onCheckNetworkConnection())
        {



            SMFApplication.getRestClient().getNotifications(new StringRequestCallback<Notification[]>() {
                @Override
                public void onRestResponse(Exception e, Notification[] result) {

                    if(e==null && result!=null)
                    {
                        iNotificationView.onGetNotifications(result);
                    }
                    iNotificationView.hideLoader();
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iNotificationView.hideLoader();
                    if(e==null && result!=null) {
                        iNotificationView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    }else{
                        iNotificationView.onError(IBaseView.SERVER_ERROR, "No data found !!");


                    }
                }
            });
        }else
        {
            iNotificationView.hideLoader();
            iNotificationView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));

        }
    }

    @Override
    public void doClearNotification() {
        iNotificationView.showLoader(context.getString(R.string.please_wait_message));

        if(iNotificationView.onCheckNetworkConnection())
        {

            SMFApplication.getRestClient().clearNotifications(new StringRequestCallback<NotificationClearResponse>() {
                @Override
                public void onRestResponse(Exception e, NotificationClearResponse result) {

                    if(e==null && result!=null)
                    {
                        iNotificationView.onClearNotifications();
                    }
                    iNotificationView.hideLoader();
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iNotificationView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                    iNotificationView.hideLoader();
                }
            });
        }else
        {
            iNotificationView.hideLoader();
            iNotificationView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));

        }
    }

    @Override
    public void onAcceptReject(AcceptRejectRequest request, final int position) {
        iNotificationView.showLoader(context.getString(R.string.please_wait_message));

        if(iNotificationView.onCheckNetworkConnection())
        {

            SMFApplication.getRestClient().doAcceptReject(new StringRequestCallback<AcceptRejectResponse>() {
                @Override
                public void onRestResponse(Exception e, AcceptRejectResponse result) {

                    if(e==null && result!=null)
                    {
                        iNotificationView.onAcceptRejectSuccess(position);
                    }
                    iNotificationView.hideLoader();
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iNotificationView.hideLoader();
                    Toast.makeText(context,"Error please try after some time",Toast.LENGTH_LONG).show();
                }
            },request);
        }else
        {
            iNotificationView.hideLoader();
            iNotificationView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));

        }
    }

    /*Swati Code*/
    @Override
    public void getNotificationDetailPost(String postId) {

        if(iNotificationView.onCheckNetworkConnection())
        {

            iNotificationView.showLoader(context.getString(R.string.please_wait_message));

            SMFApplication.getRestClient().getNotificationDetailPost(new StringRequestCallback<GetPostDataResponse>() {
                @Override
                public void onRestResponse(Exception e, GetPostDataResponse result) {
                    iNotificationView.hideLoader();
                    if (e == null && result != null) {
                        iNotificationView.onGetNotificationDetailPost(result);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    iNotificationView.hideLoader();

                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iNotificationView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iNotificationView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, postId);
        } else {
            iNotificationView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }

    }

    @Override
    public void getNotificationDetailGroup(String postId) {




    }


}
