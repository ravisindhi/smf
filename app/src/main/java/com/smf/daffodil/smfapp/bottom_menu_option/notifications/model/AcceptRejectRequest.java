package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model;

/**
 * Created by daffodil on 17/2/17.
 */

public class AcceptRejectRequest {

    private String groupID;
    private String memberId;
    private String status;

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
