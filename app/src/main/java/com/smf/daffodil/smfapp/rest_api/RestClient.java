package com.smf.daffodil.smfapp.rest_api;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.smf.daffodil.smfapp.about_you.model.HandleResponse;
import com.smf.daffodil.smfapp.about_you.model.ImageUploadResponse;
import com.smf.daffodil.smfapp.about_you.model.UserProfileDataRequest;
import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordRequest;
import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordResponse;
import com.smf.daffodil.smfapp.authentication.login.model.request.FacebookLoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.map.model.response.AddLocationResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.model.BlockUserResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.follow_unfollow.FollowUnfollowRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.follow_unfollow.FollowUnfollowResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.AcceptRejectRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.AcceptRejectResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.NotificationClearResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response.Notification;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response.NotificationResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.model.CommentsResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.model.SearchPostRequest;
import com.smf.daffodil.smfapp.common.ApiConfig;
import com.smf.daffodil.smfapp.common.Constants;
import com.smf.daffodil.smfapp.post_and_update.model.IPostAndUpdateRequest;
import com.smf.daffodil.smfapp.post_and_update.model.PostAndUpdateResponse;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordRequest;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordResponse;
import com.smf.daffodil.smfapp.slide_menu_options.history.model.GetHistoryRequest;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.request.MessageDetailRequest;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.response.MessageDetailResponse;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.request.AllMessagesRequest;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response.AllMessagesResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.search_tribe.SearchTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.AllTribesPostRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.CloseTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.TribesMemberSearchResponse;

import org.json.JSONObject;


/**
 * Created by user on 12/8/2015.
 */
public class RestClient {

    public VolleyManager getmVolleyManager() {
        return mVolleyManager;
    }

    protected static final String PROTOCOL_CHARSET = "utf-8";
    protected static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    VolleyManager mVolleyManager;

    public RestClient(Context appContext) {
        mVolleyManager = new VolleyManager(appContext);
    }



    public RequestHandler doLogin(final RequestCallback<LoginResponse> listener, JSONObject request) {

        GsonRequest gsonRequest = new GsonRequest(Request.Method.POST, ApiConfig.ApiBaseMethods.LOGIN, LoginResponse.class, request, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler doFacebookLogin(final StringRequestCallback<LoginResponse> listener, FacebookLoginRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.FACEBOOK_LOGIN, listener);

        listener.setClazz(LoginResponse.class);
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_EMAIL,request.getEmail());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_ACCESS_TOKEN,request.getAccessToken());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_ID,request.getId());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_FIRST_NAME,request.getFirst_name());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_LAST_NAME,request.getLast_name());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_NAME,request.getName());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_GENDER,request.getGender());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_LINK,request.getLink());
        gsonRequest.putParams(Constants.FacebookLoginParams.FB_LOGIN_LOCALE,request.getLocale());
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler doForgotPassword(final StringRequestCallback<ForgotPasswordResponse> listener, ForgotPasswordRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.FORGOT_PASSWORD, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(ForgotPasswordResponse.class);
        gsonRequest.putParams(Constants.ForgotPasswordParams.FORGOT_EMAIL,request.getEmail());
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }



    public RequestHandler doSignup(final RequestCallback<LoginResponse> listener, JSONObject request) {

        GsonRequest gsonRequest = new GsonRequest(Request.Method.POST, ApiConfig.ApiBaseMethods.REGISTER, LoginResponse.class, request, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler checkHandle(final StringRequestCallback<HandleResponse> listener, String handle_txt)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.CHECK_HANDLER+handle_txt, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(HandleResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler uploadImageRequest(RequestCallback<ImageUploadResponse> listener, FilePart image, String imageType, String _UserID) {

        Log.e("image type",imageType);
        Log.e("_UserID",_UserID);
        Log.e("file",image.getFile().toString());

        StringBuilder imageUploadUrl = new StringBuilder(ApiConfig.ApiBaseMethods.UPLOAD_IMAGE);
        MultipartRequest<String,ImageUploadResponse> multipartRequest =new MultipartRequest<>(Request.Method.POST,imageUploadUrl.toString(),listener,ImageUploadResponse.class);
        multipartRequest.setShouldCache(false);
        multipartRequest.putFilePart(Constants.UploadImageParams.UPLOAD_PROFILE_FILE,image);
        multipartRequest.putStringPart(Constants.UploadImageParams.UPLOAD_PROFILE_TYPE,imageType);
        multipartRequest.putStringPart(Constants.UploadImageParams.UPLOAD_PROFILE_ID,_UserID);

        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 40, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return new RequestHandler(mVolleyManager.addToRequestQueue(multipartRequest));
    }


    public RequestHandler doUpdateProfile( StringRequestCallback<UserProfileDataRequest> listener, UserProfileDataRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.UPDATE_PROFILE, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(UserProfileDataRequest.class);
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_EMAIL,request.getSearchEmail());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_FIRST_NAME,request.getFirstname());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_LAST_NAME,request.getLastName());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_FULL_NAME,request.getFirstname()+" "+request.getLastName());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_CITY,request.getCity());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_STATE,request.getState());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_ZIP,request.getZip());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_PROFILE_IMAGE,request.getProfileImage());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_HANDLE,request.getHandle());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_DOB,request.getDob());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_RELATIONSHIP,request.getRelationship());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_GENDER,request.getGender());
        gsonRequest.putParams(Constants.UpdateProfileParams.UPDATE_PROFILE_INTRO,request.getIntro());

        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }



    public RequestHandler getUserProfileData(final StringRequestCallback<LoginResponse> listener)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.USER_PROFILE_DATA, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(LoginResponse.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler doChangePassword(final StringRequestCallback<ChangePasswordResponse> listener, ChangePasswordRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.CHANGE_PASSWORD, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        gsonRequest.putParams(Constants.ChangePasswordParams.CHANGE_PASSWORD_OLD,request.getOld_password());
        gsonRequest.putParams(Constants.ChangePasswordParams.CHANGE_PASSWORD_NEW_PASSWORD,request.getPassword());
        listener.setClazz(ChangePasswordResponse.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler getAllMembers(final StringRequestCallback<MemberResponse[]> listener)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.MEMBERS, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(MemberResponse[].class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler doFollowUnfollow(final StringRequestCallback<FollowUnfollowResponse> listener, FollowUnfollowRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.FOLOW_UNFOLLOW_USER, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(FollowUnfollowResponse.class);

        Log.e("userid",request.getMember_id());
        Log.e("action",request.getAction());

        gsonRequest.putParams(Constants.FollowUnFollowParams.FOLLOW_USER_ID,request.getMember_id());
        gsonRequest.putParams(Constants.FollowUnFollowParams.FOLLOW_ACTION,request.getAction());
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler searchMemberByName(final StringRequestCallback<MemberResponse[]> listener,String name)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.SEARCH_MEMBERS+"?searchValue="+name, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(MemberResponse[].class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler getAllPost(final StringRequestCallback<PostDataResult> listener, GetPostRequest request)
    {

        String get_post_url=ApiConfig.ApiBaseMethods.GET_ALL_POSTS+Constants.GetAllPostsParams.ALL_POST_SKIP+
               request.getSkip()+Constants.GetAllPostsParams.ALL_POST_LIMIT+request.getLimit();


        if(request.getNearBy())
            get_post_url=get_post_url+Constants.GetAllPostsParams.ALL_POST_RANGE+"[0,250]";
        else if(request.getFavorite())
            get_post_url=get_post_url+Constants.GetAllPostsParams.ALL_POST_TYPE_FAVOURITE+request.getFavorite().toString();
        else if(request.getTranding())
            get_post_url=get_post_url+Constants.GetAllPostsParams.ALL_POST_TYPE_TRANDING+request.getTranding().toString();
        else if(request.getLocation())
            get_post_url=get_post_url+Constants.GetAllPostsParams.ALL_POST_CORDINATES+"["+request.getCordinates()[0]+","+request.getCordinates()[1]+"]";



        Log.e("all post url",get_post_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_post_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(PostDataResult.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler doUpdatePost(final StringRequestCallback<PostAndUpdateResponse> listener, IPostAndUpdateRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.CREATE_NEW_POST, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(PostAndUpdateResponse.class);
        gsonRequest.putParams(Constants.NewPostParams.NEW_POST_CONTENT,request.getWhats_in_mind_txt());

        if(request.getCoordinates()!=null)
        {
            gsonRequest.putParams(Constants.NewPostParams.NEW_POST_CORDINATES_LAT,request.getCoordinates()[0]);
            gsonRequest.putParams(Constants.NewPostParams.NEW_POST_CORDINATES_LONG,request.getCoordinates()[1]);

        }
        gsonRequest.putParams(Constants.NewPostParams.NEW_POST_HIDDEN,request.getHidden());

        if(request.getContent_type()!=null && request.getContent_type().length()>0 && request.getContent_type().equals("image"))
            gsonRequest.putParams(Constants.NewPostParams.NEW_POST_IMAGE_URL,request.getConten_server_url());
        else if(request.getContent_type()!=null && request.getContent_type().length()>0 && request.getContent_type().equals("video"))
            gsonRequest.putParams(Constants.NewPostParams.NEW_POST_VIDEO_URL,request.getConten_server_url());


        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

     // Get history of user
    public RequestHandler getHistory(final StringRequestCallback<PostDataResult> listener, GetHistoryRequest request)
    {
        String get_history_url=ApiConfig.ApiBaseMethods.HISTORY+Constants.GetHistory.HISTORY_AUTHOR+
                request.getAuthor()+
                Constants.GetHistory.HISTORY_LIMIT+request.getLimit()+Constants.GetHistory.HISTORY_SkIP+request.getSkip();

        Log.e("history url",get_history_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_history_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(PostDataResult.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler blockUser(final StringRequestCallback<BlockUserResponse> listener, String id)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.BLOCK_USER, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.BlockUserParams.BLOCK_USER_ID,id);
        listener.setClazz(BlockUserResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler searchPost(final StringRequestCallback<PostDataResult> listener, SearchPostRequest request)
    {

        String search_post_url=ApiConfig.ApiBaseMethods.SEARCH_POST+Constants.SearchPostsParams.SEARCH_POST_VALUE+
                request.getSearch_value()+Constants.SearchPostsParams.SEARCH_POST_SKIP+request.getSkip()+
                Constants.SearchPostsParams.SEARCH_POST_LIMIT+request.getLimit();

        Log.e("search_post_url",search_post_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,search_post_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(PostDataResult.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    // Get tribes of user
    public RequestHandler getAllTribes(final StringRequestCallback<AllTribesResponse> listener, AllTribesRequest request)
    {
        String get_all_tribes_url=ApiConfig.ApiBaseMethods.ALL_TRIBES+
                Constants.GetAllTribes.ALL_TRIBES_LIMIT+request.getLimit()+Constants.GetAllTribes.ALL_TRIBES_SkIP+
                request.getSkip();

        /*to get my tribes modify url with appending userid */
        if(request.getMyTribes()!=null && request.getMyTribes().length()>0)
            get_all_tribes_url=get_all_tribes_url+Constants.GetAllTribes.ALL_TRIBES_ID+request.getMyTribes();


        Log.e("get_all_tribes_url url",get_all_tribes_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_all_tribes_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(AllTribesResponse.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler searchTribeMemberByName(final StringRequestCallback<SearchTribeResponse> listener, String name)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.TRIBE_MEMBER_SEARCH+"?searchValue="+name, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(SearchTribeResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }




    public RequestHandler createTribe(final RequestCallback<CreateTribeResponse> listener, JSONObject request) {

        GsonRequest gsonRequest = new GsonRequest(Request.Method.POST, ApiConfig.ApiBaseMethods.CREATE_TRIBE, CreateTribeResponse.class, request, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler updateTribe(final RequestCallback<CreateTribeResponse> listener, JSONObject request) {

        GsonRequest gsonRequest = new GsonRequest(Request.Method.PUT, ApiConfig.ApiBaseMethods.UPDATE_TRIBE, CreateTribeResponse.class, request, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler getAllGroupPost(final StringRequestCallback<PostDataResult> listener, AllTribesPostRequest request)
    {

        String get_post_url=ApiConfig.ApiBaseMethods.GROUP_ALL_POST+Constants.GetAllGROUPPostsParams.GROUP_ALL_POST__ID+
                request.getGroupid()+Constants.GetAllGROUPPostsParams.GROUP_ALL_POST_LIMIT+request.getLimit()+
                Constants.GetAllGROUPPostsParams.GROUP_ALL_POST_CORDINATES+request.getCordinates()+
                Constants.GetAllGROUPPostsParams.GROUP_ALL_POST_SKIP+request.getSkip();

        Log.e("all post url",get_post_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_post_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(PostDataResult.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler closeTribe(final StringRequestCallback<CloseTribeResponse> listener, String group_id)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.CLOSE_TRIBE, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams("groupId",group_id);
        listener.setClazz(CloseTribeResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler joinTribe(final StringRequestCallback<CreateTribeResponse> listener, JoinTribeRequest request) {

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.JOIN_TRIBE, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.JoinTribeParams.JOIN_TRIBE_GROUP_ID,request.getGroupId());
        gsonRequest.putParams(Constants.JoinTribeParams.JOIN_TRIBE_USER_ID,request.getMemberId());
        listener.setClazz(CreateTribeResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler removeTribeMember(final StringRequestCallback<CreateTribeResponse> listener, JoinTribeRequest request) {

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.REMOVE_TRIBE_MEMBER, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.JoinTribeParams.JOIN_TRIBE_GROUP_ID,request.getGroupId());
        gsonRequest.putParams(Constants.JoinTribeParams.JOIN_TRIBE_USER_ID,request.getMemberId());
        listener.setClazz(CreateTribeResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler addMember(final RequestCallback<CreateTribeResponse> listener, JSONObject request) {

        GsonRequest gsonRequest = new GsonRequest(Request.Method.PUT, ApiConfig.ApiBaseMethods.ADD_TRIBE_MEMBER, CreateTribeResponse.class, request, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler createGroupPost(final StringRequestCallback<PostAndUpdateResponse> listener, IPostAndUpdateRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.CREATE_GROUP_POST, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(PostAndUpdateResponse.class);
        gsonRequest.putParams(Constants.CreateGroupPost.GROUP_POST_CONTENT,request.getWhats_in_mind_txt());
        gsonRequest.putParams(Constants.CreateGroupPost.GROUP_POST_GROUP_ID,request.getGroupid());
        if(request.getContent_type()!=null && request.getContent_type().length()>0 && request.getContent_type().equals("image"))
            gsonRequest.putParams(Constants.CreateGroupPost.GROUP_POST_IMAGE_URL,request.getConten_server_url());
        else if(request.getContent_type()!=null && request.getContent_type().length()>0 && request.getContent_type().equals("video"))
            gsonRequest.putParams(Constants.CreateGroupPost.GROUP_POST_VIDEO_URL,request.getConten_server_url());


        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler searchPostGroup(final StringRequestCallback<PostDataResult> listener, SearchPostRequest request)
    {

        String search_post_url=ApiConfig.ApiBaseMethods.SEARCH_GROUP_POST+Constants.SearchGroupPostsParams.SEARCH_POST_VALUE+
                request.getSearch_value()+Constants.SearchGroupPostsParams.SEARCH_POST_SKIP+request.getSkip()+
                Constants.SearchGroupPostsParams.SEARCH_POST_LIMIT+request.getLimit();

        if(request.getGroupId()!=null && request.getGroupId().length()>0)
            search_post_url=search_post_url+Constants.SearchGroupPostsParams.SEARCH_POST_GROUP_ID+request.getGroupId();

        Log.e("search_group_post_url",search_post_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,search_post_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(PostDataResult.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler searchTribes(final StringRequestCallback<AllTribesResponse> listener, AllTribesRequest request)
    {
        String get_all_tribes_url=ApiConfig.ApiBaseMethods.SEARCH_TRIBES+
                Constants.SearchTribesParams.SEARCH_TRIBE_VALUE+request.getSearchValue();

        Log.e("get_all_tribes_url url",get_all_tribes_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_all_tribes_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(AllTribesResponse.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler groupMemberSearchByName(final StringRequestCallback<TribesMemberSearchResponse> listener, String name)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.SEARCH_GROUP_MEMBER+"?searchValue="+name, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(TribesMemberSearchResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler getNotifications(final StringRequestCallback<Notification[]> listener)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.GET_NOTIFICATIONS, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(Notification[].class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }



    public RequestHandler clearNotifications(final StringRequestCallback<NotificationClearResponse> listener)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.CLEAR_NOTIFICATIONS, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(NotificationClearResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler doAcceptReject(final StringRequestCallback<AcceptRejectResponse> listener, AcceptRejectRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.ACCEPT_REJECT_API, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        gsonRequest.putParams(Constants.AcceptRejectParams.ACCEPT_REJECT_GROUP_ID,request.getGroupID());
        gsonRequest.putParams(Constants.AcceptRejectParams.ACCEPT_REJECT_MEMBER_ID,request.getMemberId());
        gsonRequest.putParams(Constants.AcceptRejectParams.ACCEPT_REJECT_STATUS,request.getStatus());

        listener.setClazz(AcceptRejectResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler addLocation(final RequestCallback<AddLocationResponse> listener, JSONObject request) {

        GsonRequest gsonRequest = new GsonRequest(Request.Method.POST, ApiConfig.ApiBaseMethods.ADD_LOCATION, AddLocationResponse.class, request, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler updatePost(final StringRequestCallback<PostActionResponse> listener, UpdatePostRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.UPDATE_POST_API, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.UpadatePostParams.UPDATE_POST_ID,request.getPostId());
        gsonRequest.putParams(Constants.UpadatePostParams.UPDATE_POST_ACTION,request.getAction());
        listener.setClazz(PostActionResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler group_updatePost(final StringRequestCallback<PostActionResponse> listener, UpdatePostRequest request)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.GROUP_UPDATE_POST_API, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.UpadatePostParams.UPDATE_POST_ID,request.getPostId());
        gsonRequest.putParams(Constants.UpadatePostParams.UPDATE_POST_ACTION,request.getAction());
        listener.setClazz(PostActionResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler hidePost(final StringRequestCallback<PostActionResponse> listener, String postid)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.HIDE_POST_API, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams("postId",postid);
        listener.setClazz(PostActionResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler reportPost(final StringRequestCallback<PostActionResponse> listener, String postid)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.REPORT_POST_API, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams("postId",postid);
        listener.setClazz(PostActionResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler sharePost(final StringRequestCallback<PostActionResponse> listener, SharePostRequest sharerequest)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.SHARE_POST_API, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.SharePostParams.SHARE_POST_ID,sharerequest.getPost_id());
        gsonRequest.putParams(Constants.SharePostParams.SHARE_CAPTION,sharerequest.getCaption());
        gsonRequest.putParams(Constants.SharePostParams.SHARE_POST_CORDINATES_LAT,sharerequest.getCordinates()[0]);
        gsonRequest.putParams(Constants.SharePostParams.SHARE_POST_CORDINATES_LONG,sharerequest.getCordinates()[1]);
        listener.setClazz(PostActionResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler addComment(final StringRequestCallback<PostActionResponse> listener, String postid,String content,String from)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.ADD_COMMENT_API, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.AddCommentParams.COMMENT_POST_ID,postid);
        gsonRequest.putParams(Constants.AddCommentParams.COMMENT_CONTENT,content);
        if(from.equals("post"))
            gsonRequest.putParams(Constants.AddCommentParams.COMMENT_ISFROM_GROUP,"false");
        else
            gsonRequest.putParams(Constants.AddCommentParams.COMMENT_ISFROM_GROUP,"true");

        listener.setClazz(PostActionResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler getComment(final StringRequestCallback<CommentsResponse[]> listener, String postid)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,ApiConfig.ApiBaseMethods.GET_COMMENT_API+
                Constants.GetCommentParams.GET_COMMENT_POST_ID+postid, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(CommentsResponse[].class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler getNotificationDetailPost(final StringRequestCallback<GetPostDataResponse> listener, String postid)
    {

        String get_notification_detail_url=ApiConfig.ApiBaseMethods.GET_NOTIFICATION_DETAIL_POST+ postid;

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_notification_detail_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Log.e("get_notification_url",get_notification_detail_url);
        listener.setClazz(GetPostDataResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    // Get All messages comment
    public RequestHandler getMessageComment(final StringRequestCallback<MessageDetailResponse> listener, MessageDetailRequest request)
    {
        String get_messages_comment_url=ApiConfig.ApiBaseMethods.ALL_MESSAGES_COMMENT+
                Constants.GetMessageCommentParams.ALL_MESSAGES_ID+request.getMessageId()+
                Constants.GetMessageCommentParams.ALL_MESSAGES_COMMENT_LIMIT+request.getLimit()+Constants.GetMessageCommentParams.ALL_MESSAGES_COMMENT_SKIP+
                request.getSkip();


        Log.e("get_all_message_comment url",get_messages_comment_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_messages_comment_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(MessageDetailResponse.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    // Get All messages
    public RequestHandler getAllMessages(final StringRequestCallback<AllMessagesResponse> listener, AllMessagesRequest request)
    {
        String get_all_messages_url=ApiConfig.ApiBaseMethods.ALL_MESSAGES;


        Log.e("get_all_message_url url",get_all_messages_url);

        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.GET,get_all_messages_url, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        listener.setClazz(AllMessagesResponse.class);
        gsonRequest.setShouldCache(false);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }

    public RequestHandler doPrivateProfile(final StringRequestCallback<ErrorModel> listener, String isPrivate)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.POST,ApiConfig.ApiBaseMethods.PRIVATE_PROFILE, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams("privateProfile",isPrivate);
        listener.setClazz(ErrorModel.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


    public RequestHandler updateComment(final StringRequestCallback<PostActionResponse> listener, String commentId,String content,String action,String from)
    {
        GsonStringRequest gsonRequest=new GsonStringRequest(Request.Method.PUT,ApiConfig.ApiBaseMethods.UPDATE_COMMENT_URL, listener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        gsonRequest.putParams(Constants.UpdateCommentParams.UPDATE_COMMENT_COMMENT_ID,commentId);
        gsonRequest.putParams(Constants.UpdateCommentParams.UPDATE_COMMENT_ACTION,action);
        if(from.equals("post"))
            gsonRequest.putParams(Constants.UpdateCommentParams.UPDATE_COMMENT_ISFROM_GROUP,"false");
        else
            gsonRequest.putParams(Constants.UpdateCommentParams.UPDATE_COMMENT_ISFROM_GROUP,"true");


        listener.setClazz(PostActionResponse.class);
        return new RequestHandler(mVolleyManager.addToRequestQueue(gsonRequest));
    }


}
