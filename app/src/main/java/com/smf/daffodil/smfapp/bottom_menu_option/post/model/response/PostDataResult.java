package com.smf.daffodil.smfapp.bottom_menu_option.post.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by daffolap on 7/2/17.
 */

public class PostDataResult implements Serializable{
    @SerializedName(value="name",alternate = {"totalfeedscount","totalfeedcount"})
    private Integer totalfeedscount;
    private List<GetPostDataResponse> result;

    public Integer getTotalfeedscount() {
        return totalfeedscount;
    }

    public void setTotalfeedscount(Integer totalfeedscount) {
        this.totalfeedscount = totalfeedscount;
    }

    public List<GetPostDataResponse> getResult() {
        return result;
    }

    public void setResult(List<GetPostDataResponse> result) {
        this.result = result;
    }
}
