package com.smf.daffodil.smfapp.bottom_menu_option.members.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.follow_unfollow.FollowUnfollowRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.follow_unfollow.FollowUnfollowResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.view.IMemberView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 26/1/17.
 */

public class IMemberCompl implements IMemberPresenter{

   private IMemberView iMemberView;
    private Context _context;
    SessionManager sessionManager;

    public IMemberCompl(IMemberView iMemberView, Context _context) {
        this.iMemberView = iMemberView;
        this._context = _context;
        sessionManager=new SessionManager(_context);
    }

    @Override
    public void getAllMembers() {
        getFollowUnfollowData();
    }


    private void getFollowUnfollowData()
    {
        iMemberView.showLoader(_context.getString(R.string.please_wait_message));
        if(iMemberView.onCheckNetworkConnection())
        {



            SMFApplication.getRestClient().getUserProfileData(new StringRequestCallback<LoginResponse>() {
                @Override
                public void onRestResponse(Exception e, LoginResponse result) {

                    if(e==null && result!=null)
                    {
                        sessionManager.saveUserData(result);
                        iMemberView.hideLoader();
                        //Toast.makeText(context,"new data savededdddddddddddddddddd successful",Toast.LENGTH_SHORT).show();
                        getMemberData();
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iMemberView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                    iMemberView.hideLoader();
                }
            });
        }else
        {
            iMemberView.hideLoader();
            iMemberView.onError(IBaseView.NETWORK_ERROR,_context.getString(R.string.internet_error_message));

        }
    }

    private void getMemberData()
    {
        iMemberView.showLoader(_context.getString(R.string.please_wait_message));
        if(iMemberView.onCheckNetworkConnection())
        {


            SMFApplication.getRestClient().getAllMembers(new StringRequestCallback<MemberResponse[]>() {



                @Override
                public void onRestResponse(Exception e, MemberResponse[] result) {
                    if(e==null && result !=null)
                    {

                        iMemberView.hideLoader();
                        iMemberView.onGetMember(result);
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iMemberView.hideLoader();
                    e.printStackTrace();
                    iMemberView.onError(IBaseView.NETWORK_ERROR,result.getMessage());

                }


            });
        }else
        {
            iMemberView.hideLoader();
            iMemberView.onError(IBaseView.NETWORK_ERROR,_context.getString(R.string.internet_error_message));
        }
    }



    @Override
    public void followUnfollow(String action, String memberId) {

        iMemberView.showLoader(_context.getString(R.string.please_wait_message));
        if(iMemberView.onCheckNetworkConnection())
        {

            FollowUnfollowRequest followUnfollowRequest=new FollowUnfollowRequest();
            followUnfollowRequest.setMember_id(memberId);
            followUnfollowRequest.setAction(action);


            SMFApplication.getRestClient().doFollowUnfollow(new StringRequestCallback<FollowUnfollowResponse>() {



                @Override
                public void onRestResponse(Exception e, FollowUnfollowResponse result) {
                    if(e==null && result !=null)
                    {

                        iMemberView.hideLoader();
                        iMemberView.onFollowUnfollowSuccess(result.getMessage());
                        //iMemberView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iMemberView.hideLoader();
                    if(e==null && result !=null) {
                        e.printStackTrace();
                        iMemberView.onError(IBaseView.NETWORK_ERROR,result.getMessage());

                    }


                }


            },followUnfollowRequest);
        }else
        {
            iMemberView.hideLoader();
            iMemberView.onError(IBaseView.NETWORK_ERROR,_context.getString(R.string.internet_error_message));
        }

    }

    @Override
    public void searchMemberByName(String name) {
        if(iMemberView.onCheckNetworkConnection())
        {

            SMFApplication.getRestClient().searchMemberByName(new StringRequestCallback<MemberResponse[]>() {



                @Override
                public void onRestResponse(Exception e, MemberResponse[] result) {
                    if(e==null && result !=null)
                    {


                        iMemberView.onGetMember(result);
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    e.printStackTrace();
                    iMemberView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                }

            },name);
        }else
        {
            iMemberView.onError(IBaseView.NETWORK_ERROR,_context.getString(R.string.internet_error_message));
        }
    }
}
