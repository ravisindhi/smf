package com.smf.daffodil.smfapp.base.view;

/**
 * Created by daffodil on 30/1/17.
 */

public interface IBaseView {

    public static final int NETWORK_ERROR=100;
    public static final int SERVER_ERROR=400;

    void showLoader(String message);
    void hideLoader();
    boolean onCheckNetworkConnection();
}
