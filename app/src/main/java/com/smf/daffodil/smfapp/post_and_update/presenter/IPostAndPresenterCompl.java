package com.smf.daffodil.smfapp.post_and_update.presenter;

import android.content.Context;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.about_you.model.ImageUploadResponse;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.request.LoginRequest;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.common.ApiConfig;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.post_and_update.model.IPostAndUpdateRequest;
import com.smf.daffodil.smfapp.post_and_update.model.PostAndUpdateResponse;
import com.smf.daffodil.smfapp.post_and_update.view.IPostAndUpdateView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.FilePart;
import com.smf.daffodil.smfapp.rest_api.RequestCallback;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

import java.io.File;

/**
 * Created by daffodil on 4/2/17.
 */

public class IPostAndPresenterCompl implements IPostAndUpdatePresenter{

    private Context context;
    private IPostAndUpdateView iPostAndUpdateView;
    final String IMAGE_TYPE="newsFeedImages";
    final String VIDEO_TYPE="newsFeedVideos";
    SessionManager sessionManager;

    public IPostAndPresenterCompl(Context context, IPostAndUpdateView iPostAndUpdateView) {
        this.context = context;
        this.iPostAndUpdateView = iPostAndUpdateView;
        sessionManager=new SessionManager(context);
    }

    @Override
    public void doPostAndUpdate(IPostAndUpdateRequest request) {

        if(validatePostAndUpdateForm(request))
        {
            if(request.getContent_uri() !=null )
                startPostImageVideoUpload(request);
            else{
                if(request.getGroupid()!=null && request.getGroupid().length()>0)
                    startGroupPostAndUpdateService(request);
                else
                    startPostAndUpdateService(request);
            }

        }

    }


    private boolean validatePostAndUpdateForm(IPostAndUpdateRequest request)
    {
        boolean _mValidModel = true;
        if (request.getWhats_in_mind_txt() == null || request.getWhats_in_mind_txt().trim().length() <= 0) {
            _mValidModel = false;
            iPostAndUpdateView.onError(IPostAndUpdateView.CONTENT_EMPTY_ERROR, "Please enter whats in your mind.");
        }
        return _mValidModel;
    }

    private void startPostImageVideoUpload(final IPostAndUpdateRequest request)
    { iPostAndUpdateView.showLoader(context.getString(R.string.uploading_contet_msg));
        if(iPostAndUpdateView.onCheckNetworkConnection())
        {


            SMFApplication.getRestClient().uploadImageRequest(new RequestCallback<ImageUploadResponse>() {
                @Override
                public void onRestResponse(Exception e, ImageUploadResponse result) {
                    if(e==null && result!=null)
                    {
                        iPostAndUpdateView.hideLoader();

                        if(result.getFilePath() !=null && result.getFilePath().length()>0)
                        request.setConten_server_url(result.getFilePath());
                        else if(result.getVideoUrl() !=null && result.getVideoUrl().length()>0)
                            request.setConten_server_url(result.getVideoUrl());
                        else
                            Toast.makeText(context,"Error in uploading please try after some time.",Toast.LENGTH_LONG).show();


                        if(request.getGroupid()!=null && request.getGroupid().length()>0)
                            startGroupPostAndUpdateService(request);
                        else
                            startPostAndUpdateService(request);


                        //Toast.makeText(context,"image upload success",Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iPostAndUpdateView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                    iPostAndUpdateView.hideLoader();

                }
            },getFileType(request),contentType(request),sessionManager.getUserData().get_id());

        }else
        {
            iPostAndUpdateView.hideLoader();
            iPostAndUpdateView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }

    private void startPostAndUpdateService(final IPostAndUpdateRequest request)
    {
        iPostAndUpdateView.showLoader(context.getString(R.string.createing_post_msg));
        if(iPostAndUpdateView.onCheckNetworkConnection())
        {


            SMFApplication.getRestClient().doUpdatePost(new StringRequestCallback<PostAndUpdateResponse>() {
                @Override
                public void onRestResponse(Exception e, PostAndUpdateResponse result) {
                    iPostAndUpdateView.hideLoader();
                    iPostAndUpdateView.onSuccess(result);
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iPostAndUpdateView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                    iPostAndUpdateView.hideLoader();
                }
            },request);

        }else
        {
            iPostAndUpdateView.hideLoader();
            iPostAndUpdateView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }


    private void startGroupPostAndUpdateService(final IPostAndUpdateRequest request)
    {
        iPostAndUpdateView.showLoader(context.getString(R.string.createing_post_msg));

        if(iPostAndUpdateView.onCheckNetworkConnection())
        {


            SMFApplication.getRestClient().createGroupPost(new StringRequestCallback<PostAndUpdateResponse>() {
                @Override
                public void onRestResponse(Exception e, PostAndUpdateResponse result) {
                    iPostAndUpdateView.hideLoader();
                    iPostAndUpdateView.onSuccess(result);
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    iPostAndUpdateView.onError(IBaseView.SERVER_ERROR,result.getMessage());
                    iPostAndUpdateView.hideLoader();
                }
            },request);

        }else
        {
            iPostAndUpdateView.hideLoader();
            iPostAndUpdateView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }

    private FilePart getFileType(IPostAndUpdateRequest request)
    {
        if(request.getContent_type().equals("image"))
            return new FilePart(new File(request.getContent_uri().getPath()),"image/jpg");
        else
            return new FilePart(new File(request.getContent_uri().getPath()),"video/mp4");
    }

    private String contentType(IPostAndUpdateRequest request)
    {
        if(request.getContent_type().equals("image"))
            return IMAGE_TYPE;
        else
            return VIDEO_TYPE;
    }

}
