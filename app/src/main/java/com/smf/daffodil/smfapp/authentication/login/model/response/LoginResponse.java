package com.smf.daffodil.smfapp.authentication.login.model.response;

import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.SavedLocations;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Notifications;

import java.util.List;

/**
 * Created by daffodil on 30/1/17.
 */

public class LoginResponse {

    private Status status;

    private Settings settings;


    private Secure secure;

    private Emails[] emails;

    private String password;

    private String _id;

    private Notifications[] notifications;

    private String createdAt;

    private String[] pinned;

    private String[] workspaces;

    private Connections connections;

    private Profile profile;

    private Metadata metadata;



    public Status getStatus ()
    {
        return status;
    }

    public void setStatus (Status status)
    {
        this.status = status;
    }

    public Settings getSettings ()
    {
        return settings;
    }

    public void setSettings (Settings settings)
    {
        this.settings = settings;
    }


    public Secure getSecure ()
    {
        return secure;
    }

    public void setSecure (Secure secure)
    {
        this.secure = secure;
    }

    public Emails[] getEmails ()
    {
        return emails;
    }

    public void setEmails (Emails[] emails)
    {
        this.emails = emails;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public Notifications[] getNotifications ()
    {
        return notifications;
    }

    public void setNotifications (Notifications[] notifications)
    {
        this.notifications = notifications;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String[] getPinned ()
    {
        return pinned;
    }

    public void setPinned (String[] pinned)
    {
        this.pinned = pinned;
    }

    public String[] getWorkspaces ()
    {
        return workspaces;
    }

    public void setWorkspaces (String[] workspaces)
    {
        this.workspaces = workspaces;
    }

    public Connections getConnections ()
    {
        return connections;
    }

    public void setConnections (Connections connections)
    {
        this.connections = connections;
    }

    public Profile getProfile ()
    {
        return profile;
    }

    public void setProfile (Profile profile)
    {
        this.profile = profile;
    }

    public Metadata getMetadata ()
    {
        return metadata;
    }

    public void setMetadata (Metadata metadata)
    {
        this.metadata = metadata;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", settings = "+settings+", secure = "+secure+", emails = "+emails+", password = "+password+", _id = "+_id+", notifications = "+notifications+", createdAt = "+createdAt+", pinned = "+pinned+", workspaces = "+workspaces+", connections = "+connections+", profile = "+profile+", metadata = "+metadata+"]";
    }


}
