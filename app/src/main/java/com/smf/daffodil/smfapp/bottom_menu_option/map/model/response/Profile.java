
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.SavedLocations;

public class Profile {

    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("handle")
    @Expose
    private String handle;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("relationship")
    @Expose
    private String relationship;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("intro")
    @Expose
    private String intro;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;
    @SerializedName("savedLocations")
    @Expose
    private List<SavedLocations> savedLocations = null;
    @SerializedName("hiddenPosts")
    @Expose
    private List<String> hiddenPosts = null;
    @SerializedName("subscription")
    @Expose
    private String subscription;
    @SerializedName("twitterProfileUrl")
    @Expose
    private String twitterProfileUrl;
    @SerializedName("facebookProfileUrl")
    @Expose
    private String facebookProfileUrl;
    @SerializedName("linkedInProfileUrl")
    @Expose
    private String linkedInProfileUrl;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("webAddress")
    @Expose
    private String webAddress;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("streetAddress")
    @Expose
    private String streetAddress;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("company")
    @Expose
    private String company;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<SavedLocations> getSavedLocations() {
        return savedLocations;
    }

    public void setSavedLocations(List<SavedLocations> savedLocations) {
        this.savedLocations = savedLocations;
    }

    public List<String> getHiddenPosts() {
        return hiddenPosts;
    }

    public void setHiddenPosts(List<String> hiddenPosts) {
        this.hiddenPosts = hiddenPosts;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    public String getTwitterProfileUrl() {
        return twitterProfileUrl;
    }

    public void setTwitterProfileUrl(String twitterProfileUrl) {
        this.twitterProfileUrl = twitterProfileUrl;
    }

    public String getFacebookProfileUrl() {
        return facebookProfileUrl;
    }

    public void setFacebookProfileUrl(String facebookProfileUrl) {
        this.facebookProfileUrl = facebookProfileUrl;
    }

    public String getLinkedInProfileUrl() {
        return linkedInProfileUrl;
    }

    public void setLinkedInProfileUrl(String linkedInProfileUrl) {
        this.linkedInProfileUrl = linkedInProfileUrl;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

}
