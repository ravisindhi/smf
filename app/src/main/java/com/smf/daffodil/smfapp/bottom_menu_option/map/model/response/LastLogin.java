
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastLogin {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("userAgent")
    @Expose
    private String userAgent;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

}
