package com.smf.daffodil.smfapp.authentication.forgot_password;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordRequest;
import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordResponse;
import com.smf.daffodil.smfapp.authentication.forgot_password.presenter.IForgotPasswordCompl;
import com.smf.daffodil.smfapp.authentication.forgot_password.presenter.IForgotPasswordPresenter;
import com.smf.daffodil.smfapp.authentication.forgot_password.view.IForgotPasswordView;
import com.smf.daffodil.smfapp.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends BaseFragment implements IForgotPasswordView{

    IForgotPasswordPresenter iForgotPasswordPresenter;

    @BindView(R.id.header_text)
    TextView textViewHeader;

    @BindView(R.id.btn_forgot_password)
    Button buttonForgotPassword;

    @BindView(R.id.et_email_address)
    EditText editTextEmail;



    public ForgotPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        textViewHeader.setText(R.string.forgot_password);
        iForgotPasswordPresenter=new IForgotPasswordCompl(getActivity(),this);
    }

    @OnClick(R.id.btn_forgot_password)
    public void doForgotPassword()
    {
        ForgotPasswordRequest forgotPasswordRequest=new ForgotPasswordRequest();
        forgotPasswordRequest.setEmail(editTextEmail.getText().toString().trim());
        iForgotPasswordPresenter.doForgotPassword(forgotPasswordRequest);
    }

    @Override
    public void onErrorHandleWithView(int errorCode, String _errorMsg) {
        replaceErrorViewWithMessage(editTextEmail,getActivity());
        editTextEmail.requestFocus();
    }

    @Override
    public void onErrorHandleWithSnakbar(int errorCode, String _errorMsg) {
        replaceErrorView(editTextEmail,getActivity());
        showErrorMessage(getContext().getApplicationContext(),"Email address is invalid",getActivity());
        editTextEmail.requestFocus();
    }

    @Override
    public void onSuccess(ForgotPasswordResponse forgotPasswordResponse) {
        replaceOriginalView(editTextEmail,getActivity());
        editTextEmail.setText("");
        editTextEmail.requestFocus();
        showErrorMessage(getContext().getApplicationContext(),forgotPasswordResponse.getMessage(),getActivity());


    }
}
