
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceInfo {

    @SerializedName("android")
    @Expose
    private List<String> android = null;
    @SerializedName("ios")
    @Expose
    private List<String> ios = null;

    public List<String> getAndroid() {
        return android;
    }

    public void setAndroid(List<String> android) {
        this.android = android;
    }

    public List<String> getIos() {
        return ios;
    }

    public void setIos(List<String> ios) {
        this.ios = ios;
    }

}
