package com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.model.CommentsResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.view.ICommentView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.common.view.IActionView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 20/2/17.
 */

public class ICommentCompl implements ICommentsPresenter{

    private Context context;
    private ICommentView iCommentView;

    public ICommentCompl(Context context, ICommentView iCommentView) {
        this.context = context;
        this.iCommentView = iCommentView;
    }


    @Override
    public void getAllComments(String postid) {

        if (iCommentView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().getComment(new StringRequestCallback<CommentsResponse[]>() {
                @Override
                public void onRestResponse(Exception e, CommentsResponse[] result) {
                    if (e == null && result != null) {
                        iCommentView.onGetAllComments(result);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iCommentView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iCommentView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, postid);
        } else {
            iCommentView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }

    }

    @Override
    public void addComment(String postid,String text_comment) {
        if (iCommentView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().addComment(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {
                        iCommentView.onCommentAdded(result);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iCommentView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iCommentView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, postid,text_comment,"post");
        } else {
            iCommentView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void updatePost(final UpdatePostRequest request, final int pos,String from) {
        if (iCommentView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().updateComment(new StringRequestCallback<PostActionResponse>() {
                @Override
                public void onRestResponse(Exception e, PostActionResponse result) {
                    if (e == null && result != null) {

                        switch (request.getAction())
                        {
                            case "like":
                                iCommentView.onUpdatePostAction(pos, IActionView.PostActions.like);
                                break;
                            case "dislike":
                                iCommentView.onUpdatePostAction(pos, IActionView.PostActions.dislike);
                                break;
                            case "peace":
                                iCommentView.onUpdatePostAction(pos, IActionView.PostActions.peace);
                                break;
                        }
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iCommentView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iCommentView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }

            }, request.getPostId(),request.getContent(),request.getAction(),from);
        } else {
            iCommentView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }
}
