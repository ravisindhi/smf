package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.greenfrvr.hashtagview.HashtagView;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.AddTribeMemberActivity;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeMembersList;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.presenter.ICreateTribeCompl;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.presenter.ICreateTribePresenter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.view.ICreateTribeView;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;

public class CreateTribeActivity extends BaseActivity implements ICreateTribeView{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tribe_image)
    CircularImageView imageViewTribe;

    @BindView(R.id.et_tribe_name)
    EditText editTextTribeName;

    @BindView(R.id.et_tribe_introduction)
    EditText editTextTribeIntro;

    @BindView(R.id.btn_create_tribe)
    Button buttonCreateTribe;

    @BindView(R.id.checkbox_layout)
    LinearLayout linearLayoutCheckboxLayout;

    @BindView(R.id.members_hashtag)
    HashtagView hashtagView;

    @BindView(R.id.add_memmber_layout)
    LinearLayout et_add_member;



    @BindView(R.id.tick_mark)
    ImageView imageViewtick;

    @BindView(R.id.image_view_remove_pic)
    ImageView imageViewRemovePic;

    @BindView(R.id.add_text_member)
    TextView addclick;

    @BindView(R.id.addMemberText)
    TextView textViewHineAddMember;

    private final int ADD_MEMBER=420;

    public static boolean isProfileImageChanged=false;

    ICreateTribePresenter iCreateTribePresenter;
    CreateTribeRequest request = new CreateTribeRequest();
    List<CreateTribeMembersList> tribeMembersLists = new ArrayList<>();
    Result result;
    List<String> membershandle=new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tribe);

        ButterKnife.bind(this);
        setupToolbar(toolbar, R.layout.toolbar_home_layout);

      /*  arrayAdapter=new ArrayAdapter<CreateTribeMembersList>(this,android.R.layout.simple_list_item_1,tribeMembersLists);
        editTextAddMember.setAdapter(arrayAdapter);*/

        iCreateTribePresenter = new ICreateTribeCompl(CreateTribeActivity.this, this);
        if (getIntent().getBooleanExtra("isFromEdit", false)) {
            setupToolBarName("EDIT TRIBE", true, false);
            setInitialData();
        } else {
            setupToolBarName("CREATE TRIBE", true, false);
        }
    }


    public static final HashtagView.DataTransform<String> HASH = new HashtagView.DataTransform<String>() {
        @Override
        public CharSequence prepare(String item) {
            SpannableString spannableString = new SpannableString(item);
            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#61BDD6")), 0, 0, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return spannableString;
        }
    };

    private void setInitialData() {
         result = (Result) getIntent().getSerializableExtra("tribe_model");
        ImageUtils.setprofile(imageViewTribe, result.getImageUrl(), this);
        editTextTribeName.setText(result.getName());
        editTextTribeIntro.setText(result.getDesc());
        request.setImageUrl(result.getImageUrl());
        imageViewRemovePic.setVisibility(View.VISIBLE);
        linearLayoutCheckboxLayout.setVisibility(View.GONE);
        et_add_member.setVisibility(View.GONE);
        addclick.setVisibility(View.GONE);
        request.setPrivateGroup(result.getPrivateGroup());
    }

    @OnClick(R.id.image_view_remove_pic)
    public void removePic()
    {
        imageViewTribe.setImageResource(R.drawable.ic_tribe_cam);
        request.setImageUrl("");
        isProfileImageChanged=true;
        request.setImageUri(null);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(120);
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onCreateTribeSuccess(CreateTribeResponse createTribeResponse) {
        setResult(120);
        finish();
    }

    @OnClick(R.id.add_text_member)
    public void addMember()
    {
        Intent i=new Intent(CreateTribeActivity.this,AddTribeMemberActivity.class);
        startActivityForResult(i,ADD_MEMBER);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ADD_MEMBER && data!=null){
            tribeMembersLists.clear();
            hashtagView.removeAllViews();
            membershandle.clear();
            tribeMembersLists= (List<CreateTribeMembersList>) data.getSerializableExtra("members_list");
            for(int i=0;i<tribeMembersLists.size();i++){
                membershandle.add("@"+tribeMembersLists.get(i).getHandle());
            }

            hashtagView.setData(membershandle,HASH);
            textViewHineAddMember.setVisibility(View.GONE);
            hashtagView.setVisibility(View.VISIBLE);

        }
        else{
            textViewHineAddMember.setVisibility(View.VISIBLE);
            hashtagView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onError(int errorCode, String msg) {
        switch (errorCode) {

            case ICreateTribeView.TRIBE_IMAGE_ERROR:
                showErrorMessage(getApplicationContext(), "Please select an image for your tribe !!", this);
                break;
            case ICreateTribeView.TRIBE_NAME_ERROR:
                replaceErrorViewWithMessage(editTextTribeName, this);
                break;
            case ICreateTribeView.TRIBE_DESCRIPTION_ERROR:
                replaceErrorViewWithMessage(editTextTribeIntro, this);
                break;
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(), msg, this);
                break;
        }
    }

    @OnClick(R.id.btn_create_tribe)
    public void createTribe() {
        request.setName(editTextTribeName.getText().toString().trim());
        request.setDesc(editTextTribeIntro.getText().toString().trim());
        if (imageViewtick.getVisibility() == View.VISIBLE)
            request.setPrivateGroup("1");
        else
            request.setPrivateGroup("0");

       /* for (int i = 0; i < 1; i++) {
            tribeMembersLists.add(new CreateTribeMembersList("58899fa3d5507c2742281243", "pending"));
        }*/
        request.setMembers(tribeMembersLists);

        if (getIntent().getBooleanExtra("isFromEdit", false))
        {
            request.setGroupId(result.get_id());
            iCreateTribePresenter.updateTribe(request);
        } else
            iCreateTribePresenter.createTribe(request);

    }

    @OnClick(R.id.tribe_image)
    public void takePicture() {

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openImagePicker();
            }


            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();


    }

    private void openImagePicker() {
        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(getApplicationContext())
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected uri

                        try {
                            request.setImageUri(uri);
                            isProfileImageChanged=true;
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            imageViewTribe.setImageBitmap(bitmap);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                })
                .create();

        tedBottomPicker.show(getSupportFragmentManager());
    }

    @OnClick(R.id.checkbox_layout)
    public void toggleTick() {
        if (imageViewtick.getVisibility() == View.VISIBLE)
            imageViewtick.setVisibility(View.GONE);
        else
            imageViewtick.setVisibility(View.VISIBLE);
    }






}

