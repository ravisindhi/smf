package com.smf.daffodil.smfapp.common.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;

/**
 * Created by daffodil on 22/2/17.
 */

public interface IActionView extends IBaseView{

    enum PostActions{
        like,dislike,peace
    }

    void onUpdatePostAction(int position, IActionView.PostActions actionType);
    void onHidePost(int position);
    void onReportPost();
    void onBlockUser(String handle);
    void onSharePost(PostActionResponse response);
    void onAddComment(int position);
    void onError(int errorcode,String message);
}
