
package com.smf.daffodil.smfapp.bottom_menu_option.map.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LatLng {

    @SerializedName("s")
    @Expose
    private Integer s;
    @SerializedName("e")
    @Expose
    private Integer e;
    @SerializedName("c")
    @Expose
    private List<Integer> c = null;

    public Integer getS() {
        return s;
    }

    public void setS(Integer s) {
        this.s = s;
    }

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public List<Integer> getC() {
        return c;
    }

    public void setC(List<Integer> c) {
        this.c = c;
    }

}
