package com.smf.daffodil.smfapp.authentication.login.model.response;

import java.io.Serializable;

/**
 * Created by daffodil on 30/1/17.
 */

public class Secure implements Serializable{

    private String smsCode;

    private String passwordResetToken;

    private String numberVerify;

    public String getSmsCode ()
    {
        return smsCode;
    }

    public void setSmsCode (String smsCode)
    {
        this.smsCode = smsCode;
    }

    public String getPasswordResetToken ()
    {
        return passwordResetToken;
    }

    public void setPasswordResetToken (String passwordResetToken)
    {
        this.passwordResetToken = passwordResetToken;
    }

    public String getNumberVerify ()
    {
        return numberVerify;
    }

    public void setNumberVerify (String numberVerify)
    {
        this.numberVerify = numberVerify;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [smsCode = "+smsCode+", passwordResetToken = "+passwordResetToken+", numberVerify = "+numberVerify+"]";
    }
}
