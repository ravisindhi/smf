package com.smf.daffodil.smfapp.authentication;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.LoginFragment;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthActivity extends BaseActivity {

    SessionManager sessionManager;

    @BindView(R.id.activity_auth_container)
    RelativeLayout container;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        fragmentTransaction(new LoginFragment());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sessionManager.getUserData()!=null && sessionManager.getUserData().getProfile().getHandle() !=null &&
                sessionManager.getUserData().getProfile().getHandle().length()>0)
        {
            launchActivity(AuthActivity.this, HomeActivity.class);
            finish();
        }
    }

    private void fragmentTransaction(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.activity_auth_container, fragment)
                    .commit();
        }
    }

}
