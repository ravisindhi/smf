package com.smf.daffodil.smfapp.bottom_menu_option.members.model.members;

import com.smf.daffodil.smfapp.authentication.login.model.response.Emails;
import com.smf.daffodil.smfapp.bottom_menu_option.map.model.response.Email;

import java.io.Serializable;

/**
 * Created by daffodil on 1/2/17.
 */

public class MemberResponse implements Serializable{

    private String _id;

    private Email[] emails;

    private MemberProfile profile;

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public MemberProfile getProfile ()
    {
        return profile;
    }

    public void setProfile (MemberProfile profile)
    {
        this.profile = profile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [_id = "+_id+", profile = "+profile+"]";
    }


    public Email[] getEmails() {
        return emails;
    }

    public void setEmails(Email[] emails) {
        this.emails = emails;
    }
}
