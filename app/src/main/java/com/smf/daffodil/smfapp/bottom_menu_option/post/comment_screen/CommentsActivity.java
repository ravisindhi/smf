package com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.adapter.CommentsAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.model.CommentsResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.presenter.ICommentCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.presenter.ICommentsPresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.view.ICommentView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.PostData;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostAuthor;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.common.view.IActionView;
import com.smf.daffodil.smfapp.utils.AppUtils;
import com.smf.daffodil.smfapp.utils.DateTimeAgo;
import com.smf.daffodil.smfapp.utils.ImageUtils;
import com.smf.daffodil.smfapp.view.TextViewExpandableAnimation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentsActivity extends BaseActivity implements ICommentView,CommentsAdapter.PostActionsListener{

    public static final int POST_TYPE_SHARE = 1;
    public static final int POST_TYPE_IMAGE = 2;
    public static final int POST_TYPE_TEXT = 3;
    public static final int POST_TYPE_VIDEO = 4;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.txt_name)
    TextView textViewName;

    @BindView(R.id.txt_time)
    TextView textViewTime;

    @BindView(R.id.txtview_post_content)
    TextViewExpandableAnimation textViewPostContent;

    @BindView(R.id.rounded_image_view)
    CircularImageView imageViewProfile;

    @BindView(R.id.post_image)
    ImageView imageViewPost;

    @BindView(R.id.video_thumbnail_image)
    ImageView imageViewVideoThmbnail;

    @BindView(R.id.txt_peace_by_count)
    TextView textViewPeaceByCount;

    @BindView(R.id.txt_comment_by_count)
    TextView textViewTxtCommentByCount;

    @BindView(R.id.emojiView)
    LinearLayout emojiLikeView;

    @BindView(R.id.hot_item)
    LinearLayout hot_view;

    @BindView(R.id.like_button)
    LinearLayout likeButton;

    @BindView(R.id.dislike_button)
    LinearLayout dislikeButton;

    @BindView(R.id.peace_button)
    LinearLayout peaceButton;

    @BindView(R.id.txt_liked)
    TextView textViewLikedText;

    @BindView(R.id.txt_disliked)
    TextView textViewDisLikedText;

    @BindView(R.id.txt_peace)
    TextView textViewPeace;

    @BindView(R.id.dot_btn)
    ImageButton imageButtonDot;

    @BindView(R.id.btn_report)
    LinearLayout ButtonReport;

    @BindView(R.id.share_post_layout)
    LinearLayout sharePostView;

    @BindView(R.id.owner_pic)
    ImageView imageViewOwnerPic;

    @BindView(R.id.txt_owner_name)
    TextView textViewOwnerName;

    @BindView(R.id.txt_owner_content)
    TextView textViewOwnerContent;

    @BindView(R.id.post_image_owner)
    ImageView imageViewOwnerPostImage;

    @BindView(R.id.txt_time_owner)
    TextView textViewOwnerPostTime;

    @BindView(R.id.et_comment)
    EditText editTextCommentBox;

    @BindView(R.id.post_comment_button)
    TextView textViewPostCommentButton;

    @BindView(R.id.txt_comments)
    TextView textViewComments;


    @BindView(R.id.comments_box_view)
    LinearLayout post_comment_view;



    @BindView(R.id.comment_view_layout)
    LinearLayout comment_view_layout;

    @BindView(R.id.comments_recyclerview)
    RecyclerView recyclerViewComments;

    @BindView(R.id.txtview_post_content_normal)
    TextView textviewContentNormal;




    private GetPostDataResponse postData;
    private LinearLayout post_share_view;
    private LinearLayout post_image_view;
    private LinearLayout post_txt_view;
    private FrameLayout post_video_view;
    List<CommentsResponse> list=new ArrayList<>();

    private ICommentsPresenter iCommentsPresenter;
    private CommentsAdapter adapter;
    private SessionManager sessionManager;
    private String typeComment;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);
        setupToolbar(toolbar,R.layout.toolbar_home_layout);
        setupToolBarName("Comments",true,false);
        sessionManager=new SessionManager(this);
        iCommentsPresenter=new ICommentCompl(CommentsActivity.this,this);

        post_share_view = (LinearLayout) findViewById(R.id.post_share_view);
        post_image_view = (LinearLayout) findViewById(R.id.post_image_view3);
        post_txt_view = (LinearLayout) findViewById(R.id.post_txt_view);
        post_video_view = (FrameLayout) findViewById(R.id.post_video_view);


        post_share_view.setVisibility(View.GONE);
        imageButtonDot.setVisibility(View.GONE);

        initdata();
        initRecyclerview();

    }

    private void initdata()
    {
        postData= (GetPostDataResponse) getIntent().getSerializableExtra("post_model");


        if(getIntent().getBooleanExtra("isFromPost",false)){
            typeComment="post";
        }else{
            typeComment="group";
        }

        if(getIntent().getBooleanExtra("isHiddenPost",false)){
            post_comment_view.setVisibility(View.GONE);
            comment_view_layout.setVisibility(View.GONE);
            setupToolBarName("Hidden Post",true,false);
        }else if(getIntent().getBooleanExtra("notification",false)) {
            setupToolBarName("Post Detail",true,false);
        }

        if (postData.getRealOwner() !=null ){
            postData.setPostType(POST_TYPE_SHARE);
        }
        else if (postData.getImageUrl() != null && postData.getImageUrl().length() > 0)
            postData.setPostType(POST_TYPE_IMAGE);
        else if (postData.getVideoUrl() != null && postData.getVideoUrl().length() > 0) {
            // addThumbnail(postData);
            postData.setPostType(POST_TYPE_VIDEO);
        }
        else
            postData.setPostType(POST_TYPE_TEXT);



        if(postData.getContent().length()>150)
        {
            textViewPostContent.setExpandLines(3);
            textViewPostContent.setVisibility(View.VISIBLE);
            textviewContentNormal.setVisibility(View.GONE);
            textViewPostContent.setText(postData.getContent());
        }
        else{
            textViewPostContent.setVisibility(View.GONE);
            textviewContentNormal.setVisibility(View.VISIBLE);
            textviewContentNormal.setText(postData.getContent());
        }


        switch (postData.getPostType()) {
            case PostData.POST_TYPE_IMAGE:
                post_image_view.setVisibility(View.VISIBLE);
                post_share_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.GONE);
                textViewPostContent.setText(postData.getContent());
                Glide.with(this).load(postData.getImageUrl()).thumbnail(0.1f).placeholder(R.drawable.ic_thumbnil_image).error(R.drawable.dummy_rounded).into(imageViewPost);

                break;
            case PostData.POST_TYPE_SHARE:
                post_share_view.setVisibility(View.VISIBLE);
                post_image_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.GONE);
                setSharePostData(postData);

                break;
            case PostData.POST_TYPE_TEXT:
                post_share_view.setVisibility(View.GONE);
                post_image_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.GONE);
                textViewPostContent.setText(postData.getContent());
                break;
            case PostData.POST_TYPE_VIDEO:
                //Glide.with(context).load(Uri.parse(postData.getVideoUrl())).asBitmap().error(R.drawable.dummy_rounded).into(holder.imageViewProfile);
                post_share_view.setVisibility(View.GONE);
                post_image_view.setVisibility(View.GONE);
                post_video_view.setVisibility(View.VISIBLE);
                textViewPostContent.setText(postData.getContent());
                break;
        }


        ImageUtils.setprofile(imageViewProfile, postData.getAuthor().getProfile().getProfileImage(), this);
        textViewName.setText(postData.getAuthor().getProfile().getFirstName() + " " + postData.getAuthor().getProfile().getLastName());
        String date = postData.getCreatedAt().split("\\.")[0];
        textViewTime.setText(DateTimeAgo.getTimeAgo2(date + "Z"));

        textViewPeaceByCount.setText(String.valueOf(postData.getDislikedBy().size()
                + postData.getLikedBy().size()
                + postData.getPeaceBy().size()));

            /*set action view*/
        textViewLikedText.setText(String.valueOf(postData.getLikedBy().size()));
        textViewDisLikedText.setText(String.valueOf(postData.getDislikedBy().size()));
        textViewPeace.setText(String.valueOf(postData.getPeaceBy().size()));
        if(postData.getCommentsCount() !=null)
            textViewTxtCommentByCount.setText(String.valueOf(postData.getCommentsCount()));
        else
            textViewTxtCommentByCount.setText("0");

        iCommentsPresenter.getAllComments(postData.get_id());

    }

    @OnClick(R.id.post_comment_button)
    public void addComment()
    {
        if(editTextCommentBox.getText().toString().trim().length()>0)
        iCommentsPresenter.addComment(postData.get_id(),editTextCommentBox.getText().toString().trim());
        else
            Toast.makeText(this,"Please enter a comment",Toast.LENGTH_LONG).show();
    }

    private void setSharePostData(GetPostDataResponse postData){

        if(postData.getCaption()!=null && postData.getCaption().length()>0){


            if(postData.getContent().length()>150)
            {
                textViewPostContent.setExpandLines(3);
                textViewPostContent.setVisibility(View.VISIBLE);
                textviewContentNormal.setVisibility(View.GONE);
                textViewPostContent.setText(postData.getCaption());
            }

            else{
                textViewPostContent.setVisibility(View.GONE);
                textviewContentNormal.setVisibility(View.VISIBLE);
                textviewContentNormal.setText(postData.getCaption());
            }

        }else{
            textViewPostContent.setVisibility(View.GONE);
            textviewContentNormal.setVisibility(View.GONE);
        }


        ImageUtils.setprofile(imageViewOwnerPic,postData.getRealOwner().getProfile().getProfileImage(),this);
        textViewOwnerContent.setText(postData.getContent());
        textViewOwnerName.setText(postData.getRealOwner().getProfile().getFirstName()+" "+
                postData.getRealOwner().getProfile().getLastName());


        String date = postData.getRealOwner().getCreatedAt().split("\\.")[0];
        textViewOwnerPostTime.setText(DateTimeAgo.getTimeAgo2(date + "Z"));

        if(postData.getImageUrl()!=null && postData.getImageUrl().length()>0){
            imageViewOwnerPostImage.setVisibility(View.VISIBLE);
            ImageUtils.setprofile(imageViewOwnerPostImage,postData.getImageUrl(),this);
        }else {
            imageViewOwnerPostImage.setVisibility(View.GONE);
        }

    }

    private void initRecyclerview()
    {

         adapter = new CommentsAdapter(list, this,this);
        recyclerViewComments.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this){
            @Override
            public boolean canScrollVertically() {
               return false;
            }
        };
        recyclerViewComments.setLayoutManager(linearLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerViewComments.setItemAnimator(itemAnimator);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getIntent().getBooleanExtra("isHiddenPost",false))
                {
                    Intent i=new Intent();
                    i.putExtra("pos",getIntent().getIntExtra("pos",-1));
                    setResult(102,i);
                }
                onBackPressed();
                break;
        }
        return true;
    }


    @Override
    public void onGetAllComments(CommentsResponse[] response) {
        list.clear();
        list.addAll(Arrays.asList(response));
        if(list!=null)
            adapter.notifyDataSetChanged();

       //( AppUtils.setListViewHeightBasedOnChildren(recyclerViewComments);
    }

    @Override
    public void onError(int error_code, String message) {
        switch (error_code)
        {
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(),message,this);
                break;
        }
    }

    @Override
    public void onCommentAdded(PostActionResponse response) {
        /*in response author profile is missing*/


        iCommentsPresenter.getAllComments(postData.get_id());
        editTextCommentBox.setText("");
        Toast.makeText(this,"Comment added successfully",Toast.LENGTH_LONG).show();
        textViewTxtCommentByCount.setText(String.valueOf(Integer.parseInt(textViewTxtCommentByCount.getText().toString())+1));
    }

    @Override
    public void onUpdatePostAction(int position, IActionView.PostActions actiontype) {

        switch (actiontype) {
            case like:
                if(!list.get(position).getLikedBy().contains(sessionManager.getUserData().get_id()))
                    list.get(position).getLikedBy().add(sessionManager.getUserData().get_id());
                else
                    list.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());

                /*postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case dislike:
                if(!list.get(position).getDislikedBy().contains(sessionManager.getUserData().get_id()))
                    list.get(position).getDislikedBy().add(sessionManager.getUserData().get_id());
                else
                    list.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());

                /* postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case peace:
                if(!list.get(position).getPeaceBy().contains(sessionManager.getUserData().get_id()))
                    list.get(position).getPeaceBy().add(sessionManager.getUserData().get_id());
                else
                    list.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());

                /*postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                */break;
        }
        adapter.notifyItemChanged(position);

    }

    @Override
    public void performAction(UpdatePostRequest request, int position) {
        iCommentsPresenter.updatePost(request,position,typeComment);
    }
}
