package com.smf.daffodil.smfapp.authentication.login.view;

import android.view.View;

import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;

/**
 * Created by daffodil on 24/1/17.
 */

public interface ILoginView extends IBaseView{
    public static int EMAIL_ERROR=1;
    public static int PASSWORD_ERROR=2;

    void onErrorHandleWithView(int errorCode,String _errorMsg);
    void onErrorHandleWithSnakbar(int errorCode,String _errorMsg);
    void onSuccess(LoginResponse response);
    boolean onSaveUserData(LoginResponse response);

}
