package com.smf.daffodil.smfapp.about_you.fragments.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.fragments.view.IAboutFragmentView;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.about_you.model.HandleResponse;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.authentication.login.model.request.LoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.authentication.signup.model.SignupRequest;
import com.smf.daffodil.smfapp.authentication.signup.view.ISignupView;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 25/1/17.
 */

public class IAboutPresenterCompl implements IAboutFragmentPresenter{

    IAboutFragmentView iAboutFragmentView;
    Context context;
    private final int STATUS_CODE=402;


    public IAboutPresenterCompl(Context context,IAboutFragmentView iAboutFragmentView)
    {
        this.iAboutFragmentView=iAboutFragmentView;
        this.context=context;
    }

    @Override
    public void getAboutYouData(AboutYouRequest aboutYouRequest) {

        if (validateAboutYouForm(aboutYouRequest))
        {
            iAboutFragmentView.onSuccess(aboutYouRequest);
        }

    }

    @Override
    public void checkHandleName(String handle_txt) {
        startGetHandleService(handle_txt);
    }


    private void startGetHandleService(String handle_txt)
    {
        if(iAboutFragmentView.onCheckNetworkConnection())
        {

            SMFApplication.getRestClient().checkHandle(new StringRequestCallback<HandleResponse>() {



                @Override
                public void onRestResponse(Exception e, HandleResponse result) {
                    if(e==null && result !=null)
                    {
                        iAboutFragmentView.onHandleMessage(result);
                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();

                    if(result.getStatus_code()!=STATUS_CODE)
                    {
                        iAboutFragmentView.onHandleError();
                        iAboutFragmentView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR,result.getMessage());
                    }else
                    {
                        iAboutFragmentView.onHandleMessage(new HandleResponse(false));
                    }

                }


            },handle_txt);
        }else
        {
            iAboutFragmentView.onHandleError();
            iAboutFragmentView.onErrorHandleWithSnakbar(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }



    private boolean validateAboutYouForm(AboutYouRequest aboutYouRequest) {
        boolean _mValidModel = true;
        if (AboutYouRequest.getFirstname() == null || AboutYouRequest.getFirstname().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.FIRST_NAME_ERROR, "");
        } else if (AboutYouRequest.getLastname() == null || AboutYouRequest.getLastname().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.LAST_NAME_ERROR, "");
        } /*else if (AboutYouRequest.getMobile() == null || AboutYouRequest.getMobile().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.MOBILE_NUMBER_ERROR, "");
        } else if (AboutYouRequest.getMobile().trim().length() < 10) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithSnakbar(IAboutFragmentView.MOBILE_INVALID_ERROR, "");
        }*/ else if (AboutYouRequest.getHandle() == null || AboutYouRequest.getHandle().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.HANDLE_ERROR, "");
        } else if (AboutYouRequest.getDate_of_birth() == null || AboutYouRequest.getDate_of_birth().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.DOB_ERROR, "");
        }else if (AboutYouRequest.getRelation_status().trim()== null || AboutYouRequest.getRelation_status().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithSnakbar(IAboutFragmentView.RELATION_STATUS_ERROR, "");
        } else if (AboutYouRequest.getGender().trim()== null || AboutYouRequest.getGender().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithSnakbar(IAboutFragmentView.GENDER_ERROR, "");
        } else if (AboutYouRequest.getIntroduction() == null || AboutYouRequest.getIntroduction().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.INTRO_ERROR, "");
        }else if (AboutYouRequest.getCity() == null || AboutYouRequest.getCity().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.CITY_ERROR, "");
        }else if (AboutYouRequest.getState() == null || AboutYouRequest.getState().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.STATE_ERROR, "");
        }else if (AboutYouRequest.getZip() == null || AboutYouRequest.getZip().trim().length() <= 0) {
            _mValidModel = false;
            iAboutFragmentView.onErrorHandleWithView(IAboutFragmentView.ZIP_ERROR, "");
        }
        return _mValidModel;
    }


}
