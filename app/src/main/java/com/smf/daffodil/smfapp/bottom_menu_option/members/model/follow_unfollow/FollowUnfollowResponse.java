package com.smf.daffodil.smfapp.bottom_menu_option.members.model.follow_unfollow;

/**
 * Created by daffodil on 2/2/17.
 */

public class FollowUnfollowResponse {

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;


}
