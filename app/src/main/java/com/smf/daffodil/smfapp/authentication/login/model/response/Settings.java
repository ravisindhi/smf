package com.smf.daffodil.smfapp.authentication.login.model.response;

import java.io.Serializable;

/**
 * Created by daffodil on 30/1/17.
 */

public class Settings implements Serializable{

    private String profilePrivate;

    private String addressPublic;

    private String phonePublic;

    private String emailPublic;

    private String deactivated;

    public String getProfilePrivate ()
    {
        return profilePrivate;
    }

    public void setProfilePrivate (String profilePrivate)
    {
        this.profilePrivate = profilePrivate;
    }

    public String getAddressPublic ()
    {
        return addressPublic;
    }

    public void setAddressPublic (String addressPublic)
    {
        this.addressPublic = addressPublic;
    }

    public String getPhonePublic ()
    {
        return phonePublic;
    }

    public void setPhonePublic (String phonePublic)
    {
        this.phonePublic = phonePublic;
    }

    public String getEmailPublic ()
    {
        return emailPublic;
    }

    public void setEmailPublic (String emailPublic)
    {
        this.emailPublic = emailPublic;
    }

    public String getDeactivated ()
    {
        return deactivated;
    }

    public void setDeactivated (String deactivated)
    {
        this.deactivated = deactivated;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [profilePrivate = "+profilePrivate+", addressPublic = "+addressPublic+", phonePublic = "+phonePublic+", emailPublic = "+emailPublic+", deactivated = "+deactivated+"]";
    }
}
