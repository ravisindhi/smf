package com.smf.daffodil.smfapp.bottom_menu_option.members.model.follow_unfollow;

/**
 * Created by daffodil on 2/2/17.
 */

public class FollowUnfollowRequest {

    private String member_id;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    private String action;
}
