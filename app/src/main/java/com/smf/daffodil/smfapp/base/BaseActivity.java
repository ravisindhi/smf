package com.smf.daffodil.smfapp.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nispok.snackbar.Snackbar;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.base.view.IBaseView;

import dmax.dialog.SpotsDialog;

/**
 * Created by daffodil on 23/1/17.
 */

public abstract class BaseActivity extends AppCompatActivity implements IBaseView{

    private android.app.AlertDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Method to launch another activity and finish current one
     *
     * @param cls
     * @param <T>
     */
    protected <T> void launchActivity(Activity _context, Class<T> cls) {
        if (_context != null) {
            Intent intent = new Intent(_context, cls);
            overridePendingTransition(R.anim.trans_left_in, R.anim.trans_right_out);
            startActivity(intent);

        }
    }

    /**
     * Method to setup custom action bar
     *
     * @param toolbar
     */
    protected void setupToolbar(Toolbar toolbar, int toolBarLayout) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(toolBarLayout, null), new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
    }


    /**
     * Method to setup custom action bar title
     *
     * @param txt
     */
    protected void setupToolBarName(String txt, boolean isBackEnable, boolean isArrowShow) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackEnable);
        View v = getSupportActionBar().getCustomView();
        TextView titleTxtView = (TextView) v.findViewById(R.id.header_text);
        ImageView arrow_icon = (ImageView) v.findViewById(R.id.down_arrow);

        if(arrow_icon!=null)
        {
            if (isArrowShow)
                arrow_icon.setVisibility(View.VISIBLE);
            else
                arrow_icon.setVisibility(View.GONE);

        }

        titleTxtView.setText(txt);

    }

    protected void fragmentTransaction(int container, Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(container, fragment)
                    .commit();
        }
    }

    protected void showErrorMessage(Context app_context,String text,Activity _context)
    {
        Snackbar.with(app_context)
                .text(text)
                .show(_context);


    }

    @Override
    public void showLoader(String message) {

        if(dialog==null)
        {
            dialog = new SpotsDialog(BaseActivity.this, R.style.Custom);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

        }
        if(!dialog.isShowing()){
            dialog.setMessage(message);
            dialog.show();
        }

    }

    @Override
    public void hideLoader() {
        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
        }
    }

    @Override
    public boolean onCheckNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    protected void replaceErrorViewWithMessage(View view,Context _context)
    {
        if(view instanceof EditText)
        {   final int version = Build.VERSION.SDK_INT;
            if(!((EditText) view).getHint().toString().contains("Enter"))
            {
                ((EditText) view).setHint("Enter "+((EditText) view).getHint().toString());
                ((EditText) view).setHintTextColor(getResources().getColor(R.color.colorErrorTextColor));

            }

            if (version >= 21) {
                ((EditText) view).setBackground(ContextCompat.getDrawable(_context, R.drawable.rounded_corner_red));
            } else {
                ((EditText) view).setBackground(_context.getResources().getDrawable(R.drawable.rounded_corner_red));
            }
        }
    }

    protected void replaceErrorView(View view,Context _context)
    {
        if(view instanceof EditText)
        {   final int version = Build.VERSION.SDK_INT;
            if (version >= 21) {
                ((EditText) view).setTextColor(getResources().getColor(R.color.colorErrorTextColor));

                ((EditText) view).setBackground(ContextCompat.getDrawable(_context, R.drawable.rounded_corner_red));
            } else {
                ((EditText) view).setBackground(_context.getResources().getDrawable(R.drawable.rounded_corner_red));
                ((EditText) view).setTextColor(getResources().getColor(R.color.colorErrorTextColor));

            }
        }
    }



    protected void replaceOriginalView(View view,Context _context)
    {

        if(view instanceof EditText)
        {   final int version = Build.VERSION.SDK_INT;
            ((EditText) view).setHint(((EditText) view).getHint().toString());
            ((EditText) view).setHintTextColor(getResources().getColor(R.color.colorHint));
            if (version >= 21) {
                ((EditText) view).setBackground(ContextCompat.getDrawable(_context, R.drawable.rounded_corner_black));
            } else {
                ((EditText) view).setBackground(_context.getResources().getDrawable(R.drawable.rounded_corner_black));
            }
        }
    }

    protected void onBackClicked()
    {
        onBackPressed();
    }


}
