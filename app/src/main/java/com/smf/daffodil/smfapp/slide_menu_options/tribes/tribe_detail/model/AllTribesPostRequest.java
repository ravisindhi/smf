package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model;

/**
 * Created by daffodil on 13/2/17.
 */

public class AllTribesPostRequest {

    private String skip;
    private String limit;
    private String[] cordinates;
    private String groupid;


    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String[] getCordinates() {
        return cordinates;
    }

    public void setCordinates(String[] cordinates) {
        this.cordinates = cordinates;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }
}
