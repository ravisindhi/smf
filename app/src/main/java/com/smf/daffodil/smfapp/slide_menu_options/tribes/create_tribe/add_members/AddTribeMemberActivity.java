package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.TribesMembers;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.adapter.AddTribeMemberAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.presenter.IAddTribeMemberCompl;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.presenter.IAddTribeMemberPresenter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.view.IAddTribeMemberView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeMembersList;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_members.model.TribesMemberSearchResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddTribeMemberActivity extends BaseActivity implements IAddTribeMemberView,AddTribeMemberAdapter.OnClickListen{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.grid_view)
    StaggeredGridView staggeredGridView;

    @BindView(R.id.btn_done_member_add)
    Button buttonDoneMemberAdd;

    List<CreateTribeMembersList> tribeMembersLists=new ArrayList<>();

    List<TribesMembers> members=new ArrayList<>();


    private AddTribeMemberAdapter adapter;
    IAddTribeMemberPresenter iAddTribeMemberPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tribe_member);
        iAddTribeMemberPresenter=new IAddTribeMemberCompl(AddTribeMemberActivity.this,this);
        ButterKnife.bind(this);


        adapter=new AddTribeMemberAdapter(this,members,this);
        staggeredGridView.setAdapter(adapter);
        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("ADD MEMBERS", true, false);
        iAddTribeMemberPresenter.getMembers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onError(int errorcode, String msg) {
        switch (errorcode) {
            case IBaseView.NETWORK_ERROR:
            case IBaseView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(), msg, this);
                break;
        }
    }

    @Override
    public void onGetMember(TribesMemberSearchResponse result) {
        if(result.getResult().length>0){
            members.clear();
            members.addAll(Arrays.asList(result.getResult()));
            adapter.notifyDataSetChanged();
        }else{
            Toast.makeText(this,"No Members found ",Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_done_member_add)
    public void addMember()
    {
        Intent i=new Intent();

        i.putExtra("members_list", (Serializable) tribeMembersLists);
        setResult(420,i);
        finish();
    }

    @Override
    public void onAdd(int position) {
        CreateTribeMembersList  member=new CreateTribeMembersList();
        member.setHandle(members.get(position).getProfile().getHandle());
        member.setId(members.get(position).get_id());
        member.setStatus("pending");
        tribeMembersLists.add(member);
    }



    @Override
    public void onRemove(int position) {

        CreateTribeMembersList  member=new CreateTribeMembersList();
        member.setHandle(members.get(position).getProfile().getHandle());
        member.setId(members.get(position).get_id());
        member.setStatus("pending");
        tribeMembersLists.remove(member);

    }
}
