package com.smf.daffodil.smfapp.about_you.fragments.view;

import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.about_you.model.HandleResponse;
import com.smf.daffodil.smfapp.base.view.IBaseView;

/**
 * Created by daffodil on 25/1/17.
 */

public interface IAboutFragmentView extends IBaseView{


    public static int FIRST_NAME_ERROR=1;
    public static int LAST_NAME_ERROR=2;
    public static int MOBILE_NUMBER_ERROR=3;
    public static int HANDLE_ERROR=4;
    public static int DOB_ERROR=5;
    public static int RELATION_STATUS_ERROR=6;
    public static int GENDER_ERROR=7;
    public static int INTRO_ERROR=8;
    public static int CITY_ERROR=9;
    public static int STATE_ERROR=10;
    public static int ZIP_ERROR=11;
    public static int MOBILE_INVALID_ERROR=12;
    public static int MISSING_ERROR=21;




    void onErrorHandleWithView(int errorCode,String _errorMsg);
    void onErrorHandleWithSnakbar(int errorCode,String _errorMsg);
    void onSuccess(AboutYouRequest aboutYouRequest);
    void onHandleMessage(HandleResponse handleResponse);
    void onHandleError();
}
