package com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post;

/**
 * Created by daffodil on 19/2/17.
 */

public class UpdatePostRequest {

    private String postId;
    private String action;
    private String content;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
