package com.smf.daffodil.smfapp.bottom_menu_option.post.model.response;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by daffodil on 19/2/17.
 */

public class PostActionResponse {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("content")
    @Expose
    private String content=null;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("visible")
    @Expose
    private Boolean visible;
    @SerializedName("peaceBy")
    @Expose
    private List<String> peaceBy = null;
    @SerializedName("hidden")
    @Expose
    private Boolean hidden;
    @SerializedName("removed")
    @Expose
    private Boolean removed;
    @SerializedName("dislikedBy")
    @Expose
    private List<String> dislikedBy = null;
    @SerializedName("likedBy")
    @Expose
    private List<String> likedBy = null;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("location")
    @Expose
    private PostLocation location;
    @SerializedName("permissions")
    @Expose
    private String permissions;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public List<String> getPeaceBy() {
        return peaceBy;
    }

    public void setPeaceBy(List<String> peaceBy) {
        this.peaceBy = peaceBy;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    public List<String> getDislikedBy() {
        return dislikedBy;
    }

    public void setDislikedBy(List<String> dislikedBy) {
        this.dislikedBy = dislikedBy;
    }

    public List<String> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(List<String> likedBy) {
        this.likedBy = likedBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public PostLocation getLocation() {
        return location;
    }

    public void setLocation(PostLocation location) {
        this.location = location;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

}
