package com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response.Result;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public class MessageDetailResponse {

    @SerializedName("totalmessagescommentscount")
    @Expose
    private Integer totalmessagescommentscount;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Integer getTotalmessagescommentscount() {
        return totalmessagescommentscount;
    }

    public void setTotalmessagescommentscount(Integer totalmessagescommentscount) {
        this.totalmessagescommentscount = totalmessagescommentscount;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

}
