package com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.presenter;

import android.content.Context;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.model.SearchPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.view.ISearchPostView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

/**
 * Created by daffodil on 10/2/17.
 */

public class ISearchPostCompl implements ISearchPostPresenter{

    private Context context;
    private ISearchPostView iSearchPostView;

    public ISearchPostCompl(Context context, ISearchPostView iSearchPostView) {
        this.context = context;
        this.iSearchPostView = iSearchPostView;
    }

    @Override
    public void searchPost(SearchPostRequest request) {
        startSearchPostsService(request);
    }

    @Override
    public void searchGroupPost(SearchPostRequest request) {
        startGroupPostSearch(request);
    }

    private void startSearchPostsService(final SearchPostRequest request) {
        if (iSearchPostView.onCheckNetworkConnection()) {




            SMFApplication.getRestClient().searchPost(new StringRequestCallback<PostDataResult>() {


                @Override
                public void onRestResponse(Exception e, PostDataResult result) {
                    if (e == null && result != null) {


                        if (request.getLimit().equals("10")) {

                            if (result.getResult().size() > 0)
                                iSearchPostView.onSuccess(result);
                            else
                                iSearchPostView.onError(IAllPostView.NODATA, "No data found !!");

                        } else {
                            iSearchPostView.onAppendData(result);
                        }





                    }

                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    try {
                        e.printStackTrace();

                        iSearchPostView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    }catch (Exception ex)
                    {

                        iSearchPostView.onError(ISearchPostView.NO_DATA,"No data found !!");
                    }
                }


            }, request);
        } else {

            iSearchPostView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }


    private void startGroupPostSearch(final SearchPostRequest request) {
        if (iSearchPostView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().searchPostGroup(new StringRequestCallback<PostDataResult>() {

                @Override
                public void onRestResponse(Exception e, PostDataResult result) {
                    if (e == null && result != null) {

                        if (request.getLimit().equals("10")) {
                            if (result.getResult().size() > 0)
                                iSearchPostView.onSuccess(result);
                            else
                                iSearchPostView.onError(IAllPostView.NODATA, "No data found !!");
                        } else {
                            iSearchPostView.onAppendData(result);
                        }
                    }
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    try {
                        e.printStackTrace();

                        iSearchPostView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    }catch (Exception ex)
                    {

                        iSearchPostView.onError(ISearchPostView.NO_DATA,"No data found !!");
                    }
                }


            }, request);
        } else {

            iSearchPostView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }
}
