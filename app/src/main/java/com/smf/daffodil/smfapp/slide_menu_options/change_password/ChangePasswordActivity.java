package com.smf.daffodil.smfapp.slide_menu_options.change_password;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordRequest;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.model.ChangePasswordResponse;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.presenter.IChangePasswordCompl;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.presenter.IChangePasswordPresenter;
import com.smf.daffodil.smfapp.slide_menu_options.change_password.view.IChangePasswordView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity implements IChangePasswordView{

    IChangePasswordPresenter iChangePasswordPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_old_password)
    EditText editTextOldPassword;

    @BindView(R.id.et_new_password)
    EditText editTextNewPassword;


    @BindView(R.id.et_confirm_password)
    EditText editTextConfirmPassword;

    @BindView(R.id.btn_change_password)
    Button buttonChangePassword;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);
        iChangePasswordPresenter=new IChangePasswordCompl(this,this);
        setupToolbar(toolbar,R.layout.toolbar_home_layout);
        setupToolBarName("Change Password",true,false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @OnClick(R.id.btn_change_password)
    public void doChangePassword()
    {
        ChangePasswordRequest request=new ChangePasswordRequest();
        request.setOld_password(editTextOldPassword.getText().toString().trim());
        request.setPassword(editTextNewPassword.getText().toString().trim());
        request.setConfirm_password(editTextConfirmPassword.getText().toString().trim());

        iChangePasswordPresenter.doChangePassword(request);
    }

    @Override
    public void onError(int errorCode) {

        switch (errorCode)
        {

            case IChangePasswordView.OLD_PASSWORD_ERROR:
                replaceErrorViewWithMessage(editTextOldPassword,this);
                editTextOldPassword.requestFocus();
                break;
            case IChangePasswordView.NEW_PASSWORD_ERROR:
                replaceErrorViewWithMessage(editTextNewPassword,this);
                editTextNewPassword.requestFocus();
                break;
            case IChangePasswordView.CONFIRM_PASSWORD_ERROR:
                replaceErrorViewWithMessage(editTextConfirmPassword,this);
                editTextConfirmPassword.requestFocus();
                break;
            case IChangePasswordView.PASSWORD_MISMATCH:
                replaceErrorView(editTextNewPassword,this);
                replaceErrorView(editTextConfirmPassword,this);
                showErrorMessage(getApplicationContext(),"Password and confirm password not matched",this);
                editTextNewPassword.requestFocus();
                break;
        }

    }

    @Override
    public void onErrorWithMessage(int errorCode, String message) {
        switch (errorCode){
            case IChangePasswordView.NETWORK_ERROR:
            case IChangePasswordView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(),message,this);

                break;
        }
    }

    @Override
    public void onSuccess(ChangePasswordResponse response) {

        showErrorMessage(getApplicationContext(),response.getMessage(),this);
        editTextConfirmPassword.setText("");
        editTextNewPassword.setText("");
        editTextOldPassword.setText("");

        replaceOriginalView(editTextConfirmPassword,this);
        replaceOriginalView(editTextNewPassword,this);
        replaceOriginalView(editTextOldPassword,this);
    }
}
