package com.smf.daffodil.smfapp.common;

/**
 * Created by daffodil on 3/6/16.
 */
public class ApiConfig {
    public static final String BASE_URL = "http://139.162.5.142:5400/";

    public interface ApiBaseMethods
    {
        String LOGIN=BASE_URL+"login";
        String FACEBOOK_LOGIN=BASE_URL+"facebook/login";
        String FORGOT_PASSWORD=BASE_URL+"forgotPassword";
        String REGISTER=BASE_URL+"register";
        String CHECK_HANDLER=BASE_URL+"checkHandle/";
        String UPLOAD_IMAGE=BASE_URL+"uploadImage";
        String UPDATE_PROFILE=BASE_URL+"updateProfile";
        String USER_PROFILE_DATA=BASE_URL+"whoAmI";
        String CHANGE_PASSWORD=BASE_URL+"changePassword";
        String MEMBERS=BASE_URL+"members";
        String FOLOW_UNFOLLOW_USER=BASE_URL+"followUser";
        String SEARCH_MEMBERS=BASE_URL+"searchMember";
        String GET_ALL_POSTS=BASE_URL+"allPosts";
        String CREATE_NEW_POST=BASE_URL+"newPost";
        String HISTORY=BASE_URL+"userHistory";
        String BLOCK_USER=BASE_URL+"blockUser";
        String SEARCH_POST=BASE_URL+"searchPost";
        String ALL_TRIBES=BASE_URL+"tribes";
        String CREATE_TRIBE=BASE_URL+"createGroup";
        String UPDATE_TRIBE=BASE_URL+"updateTribe";
        String GROUP_ALL_POST=BASE_URL+"group/allPosts";
        String CLOSE_TRIBE=BASE_URL+"closeTribe";
        String TRIBE_MEMBER_SEARCH=BASE_URL+"memberSearch";
        String JOIN_TRIBE=BASE_URL+"joinRequest";
        String REMOVE_TRIBE_MEMBER=BASE_URL+"removeMember";
        String ADD_TRIBE_MEMBER=BASE_URL+"addMember";
        String CREATE_GROUP_POST=BASE_URL+"group/newPost";
        String SEARCH_GROUP_POST=BASE_URL+"group/searchPost";
        String SEARCH_TRIBES=BASE_URL+"searchTribe";
        String SEARCH_GROUP_MEMBER=BASE_URL+"memberSearch";
        String GET_NOTIFICATIONS=BASE_URL+"getNotification";
        String CLEAR_NOTIFICATIONS=BASE_URL+"clearNotifications";
        String ACCEPT_REJECT_API=BASE_URL+"changeStatus";
        String UPDATE_POST_API=BASE_URL+"updatePost";
        String GROUP_UPDATE_POST_API=BASE_URL+"group/updatePost";

        String HIDE_POST_API=BASE_URL+"hidePost";
        String REPORT_POST_API=BASE_URL+"reportPost";
        String SHARE_POST_API=BASE_URL+"sharePost";
        String ADD_COMMENT_API=BASE_URL+"addComment";
        String GET_COMMENT_API=BASE_URL+"comments";
        String ADD_LOCATION=BASE_URL+"addLocation";
        String GET_NOTIFICATION_DETAIL_POST="http://139.162.5.142:5400";
        String ALL_MESSAGES=BASE_URL+"latestMessages";
        String ALL_MESSAGES_COMMENT=BASE_URL+"messagesComments";
        String PRIVATE_PROFILE=BASE_URL+"profilePrivate";
        String UPDATE_COMMENT_URL=BASE_URL+"updateComment";


    }


    public interface ErrorCodes {
        int SERVER_ERROR = 500;
        int UNAUTHRIZED_ERROR=401;
    }
}
