package com.smf.daffodil.smfapp.slide_menu_options.messages.chat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.MessageDetailActivity;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.ChatBodyModel;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.response.Result;
import com.smf.daffodil.smfapp.utils.ImageUtils;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public class MessageDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    List<ChatBodyModel> mMessageList;
    Context context;
    SessionManager sessionManager;
    public static final int MESSAGE_TYPE_AUTHOR = 1;
    public static final int MESSAGE_TYPE_RECIPIENT = 2;


    public  MessageDetailAdapter(Context context,List<ChatBodyModel> mMessageList)
    {
        this.context=context;
        this.mMessageList=mMessageList;
        sessionManager=new SessionManager(context);
    }


    @Override
    public int getItemViewType(int position) {
        return  mMessageList.get(position).getAuthor().equals(sessionManager.getUserData().get_id()) ? MESSAGE_TYPE_AUTHOR : MESSAGE_TYPE_RECIPIENT;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == MESSAGE_TYPE_AUTHOR) {
            View view = LayoutInflater.from(context).inflate(R.layout.author_message_list_item, parent, false);
            return new AuthorViewHolder(view);
        } else if (viewType == MESSAGE_TYPE_RECIPIENT) {
            View view = LayoutInflater.from(context).inflate(R.layout.recipient_message_list_item, parent, false);
            return new RecipientViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AuthorViewHolder) {

            if(mMessageList.get(position)!=null)
            ((AuthorViewHolder) holder).setData(mMessageList.get(position));

        } else if (holder instanceof RecipientViewHolder) {

            if(mMessageList.get(position)!=null)
                ((RecipientViewHolder) holder).setData(mMessageList.get(position));

        }

    }


    @Override
    public int getItemCount() {
        return mMessageList.size();
    }



    public class AuthorViewHolder extends RecyclerView.ViewHolder
    {

        @BindView(R.id.rounded_image_view)
        ImageView imageViewAuthor;

        @BindView(R.id.txt_msg_content)
        TextView textViewContent;


        public AuthorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            sessionManager=new SessionManager(context);
        }


        public void setData(ChatBodyModel result)
        {
            if(result.getContent()!=null && result.getContent().length()>0)
                textViewContent.setText(result.getContent());

            ImageUtils.setprofile(imageViewAuthor,sessionManager.getUserData().getProfile().getProfileImage(),context);

        }
    }

    public class RecipientViewHolder extends RecyclerView.ViewHolder
    {


        @BindView(R.id.rounded_image_view)
        ImageView imageViewRecipient;

        @BindView(R.id.txt_msg_content)
        TextView textViewContent;


        public RecipientViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void setData(ChatBodyModel result)
        {
            if(result.getContent()!=null && result.getContent().length()>0){
                textViewContent.setText(result.getContent());
            }

            if(MessageDetailActivity.recepientImageUrl!=null && MessageDetailActivity.recepientImageUrl.length()>0)
                ImageUtils.setprofile(imageViewRecipient,MessageDetailActivity.recepientImageUrl,context);
        }
    }
}
