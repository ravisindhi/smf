package com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model;

/**
 * Created by daffodil on 15/2/17.
 */

public class JoinTribeRequest {
    private String groupId;
    private String memberId;
    private int position;

    public JoinTribeRequest(String groupId, String memberId) {
        this.groupId = groupId;
        this.memberId = memberId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
