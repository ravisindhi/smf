package com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.request.AllMessagesRequest;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.model.response.AllMessagesResponse;
import com.smf.daffodil.smfapp.slide_menu_options.messages.message_listing.view.IMessagesView;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public class IMessagesPresenterCompl implements IMessagesPresenter {

    IMessagesView iMessagesView;
    Context context;

    public IMessagesPresenterCompl(IMessagesView iMessagesView , Context context)
    {
        this.iMessagesView=iMessagesView;
        this.context=context;
    }



    @Override
    public void getAllMessages(AllMessagesRequest allMessagesRequest , boolean isLoadMore) {

        if(iMessagesView.onCheckNetworkConnection())
        {
            iMessagesView.showLoader(context.getString(R.string.please_wait_message));

            SMFApplication.getRestClient().getAllMessages(new StringRequestCallback<AllMessagesResponse>() {
                @Override
                public void onRestResponse(Exception e, AllMessagesResponse result) {
                    iMessagesView.hideLoader();
                    iMessagesView.onGetMessages(result);
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    iMessagesView.hideLoader();

                    try{
                        iMessagesView.hideLoader();
                        e.printStackTrace();
                        iMessagesView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                    }catch (Exception ex)
                    {
                        e.printStackTrace();
                        iMessagesView.onError(IBaseView.NETWORK_ERROR,"Internal Error please try again !!");
                    }

                }
            },allMessagesRequest);

        }else
        {
            iMessagesView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }

    }
}
