package com.smf.daffodil.smfapp.bottom_menu_option.map.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by daffolap-164 on 16/2/17.
 */

public class AddLocationRequest {

    @SerializedName("latLng")
    private List<Double> latLng = null;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private String location;

    public List<Double> getLatLng() {
        return latLng;
    }

    public void setLatLng(List<Double> latLng) {
        this.latLng = latLng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
