package com.smf.daffodil.smfapp.slide_menu_options.my_profile.presenter;

import android.content.Context;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.my_profile.view.IMyProfileView;

/**
 * Created by daffodil on 25/2/17.
 */

public class IMyPresenterCompl implements IMyProfilePresenter{

    private Context context;
    private IMyProfileView iMyProfileView;

    public IMyPresenterCompl(Context context, IMyProfileView iMyProfileView) {
        this.context = context;
        this.iMyProfileView = iMyProfileView;
    }

    @Override
    public void doProfilePrivate(String type) {

        if (iMyProfileView.onCheckNetworkConnection()) {
            iMyProfileView.showLoader("Please wait");

            SMFApplication.getRestClient().doPrivateProfile(new StringRequestCallback<ErrorModel>() {
                @Override
                public void onRestResponse(Exception e, ErrorModel result) {
                    if (e == null && result != null) {

                        Toast.makeText(context,result.getMessage(),Toast.LENGTH_LONG).show();
                        iMyProfileView.hideLoader();
                    }
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {


                        Toast.makeText(context,"Server Error !!",Toast.LENGTH_SHORT).show();

                }
            },type);

        }else {
            iMyProfileView.hideLoader();
            Toast.makeText(context,"No Internet",Toast.LENGTH_LONG).show();
        }
    }
}
