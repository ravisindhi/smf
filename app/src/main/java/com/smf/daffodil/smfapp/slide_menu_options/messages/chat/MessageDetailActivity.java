package com.smf.daffodil.smfapp.slide_menu_options.messages.chat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.adapter.MessageDetailAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.ChatBodyModel;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.request.MessageDetailRequest;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.response.MessageDetailResponse;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.model.response.Result;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.presenter.IMessageDetailPresenter;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.presenter.IMessageDetailPresenterCompl;
import com.smf.daffodil.smfapp.slide_menu_options.messages.chat.view.IMessageDetailView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffolap-164 on 20/2/17.
 */

public class MessageDetailActivity extends BaseActivity implements IMessageDetailView, MessagingService.MessageCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.message_list_recyclerview)
    RecyclerView recyclerViewMessages;

    @BindView(R.id.send_message)
    TextView btnSendMessage;

    @BindView(R.id.message_edt)
    EditText editTextMessage;

    MessageDetailRequest messageDetailRequest = new MessageDetailRequest();
    IMessageDetailPresenter iMessageDetailPresenter;
    ArrayList<ChatBodyModel> list = new ArrayList<>();
    int total_count;
    private MessageDetailAdapter adapter;


    private MessagingService mMessagingService;
    private boolean isBound;
    private String recipientId = "589d5b32ab09f47b82c1018a";
    private Boolean isReply=false;
    private Integer firstTime=0;
    private String recepientName;
    private String recepientImage;
    ChatBodyModel chatModel;
    private SessionManager sessionManager;
    public static String recepientImageUrl;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_detail);

        ButterKnife.bind(this);

        setupToolbar(toolbar, R.layout.toolbar_home_layout);


        iMessageDetailPresenter = new IMessageDetailPresenterCompl(this, MessageDetailActivity.this);

        adapter = new MessageDetailAdapter(MessageDetailActivity.this, list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setStackFromEnd(true);
        recyclerViewMessages.setLayoutManager(mLayoutManager);
        recyclerViewMessages.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMessages.setAdapter(adapter);

        sessionManager=new SessionManager(this);

        recipientId=getIntent().getStringExtra("recepient_id");
        recepientName=getIntent().getStringExtra("name");
        recepientImageUrl=getIntent().getStringExtra("profile_url");
        setupToolBarName(recepientName, true, false);

        if(getIntent().getBooleanExtra("isReply",false)){
            firstTime++;
        }

        getAllMessages();

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if( editTextMessage.getText()!=null && editTextMessage.getText().toString().trim().length()>0){

                   if(getIntent().getBooleanExtra("isReply",false) && firstTime>0){
                       mMessagingService.sendReplyMessage(SMFApplication.getSessionManager().getUserData().get_id(),
                               editTextMessage.getText().toString().trim(),recipientId,getIntent().getStringExtra("msg_id"));
                   }else{
                       mMessagingService.sendMessage(SMFApplication.getSessionManager().getUserData().get_id(),
                               editTextMessage.getText().toString().trim(),recipientId);
                       firstTime++;
                   }

                   chatModel=new ChatBodyModel();
                   chatModel.setAuthor(sessionManager.getUserData().get_id());
                   chatModel.setContent(editTextMessage.getText().toString().trim());
                   list.add(chatModel);
                   adapter.notifyDataSetChanged();
                   scrollToBottom();

               }

                editTextMessage.setText("");
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MessagingService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void scrollToBottom() {
        recyclerViewMessages.scrollToPosition(adapter.getItemCount()-1);

    }
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mMessagingService = ((MessagingService.MessageServiceBinder) iBinder).getService();
            isBound = true;
            mMessagingService.setMessageCallback(MessageDetailActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mMessagingService = null;
            isBound = false;
        }
    };

    @Override
    public void onGetMessageComment(MessageDetailResponse messageDetailResponse) {

        total_count =messageDetailResponse.getTotalmessagescommentscount();
        ChatBodyModel model;
        for(int i=0;i<messageDetailResponse.getResult().size();i++){
            model=new ChatBodyModel();
            model.set_id(messageDetailResponse.getResult().get(i).getMessageId());
            model.setContent(messageDetailResponse.getResult().get(i).getContent());
            model.setAuthor(messageDetailResponse.getResult().get(i).getAuthor());
            model.setRecipient(messageDetailResponse.getResult().get(i).getRecipient());
            list.add(model);
        }

       // list.addAll(messageDetailResponse.getResult());
        adapter.notifyDataSetChanged();
        if (messageDetailResponse.getTotalmessagescommentscount()==0)
            showErrorMessage(getApplicationContext(), "There is no message yet !!", this);

        scrollToBottom();

    }

    @Override
    public void onError(int errorcode, String message) {

    }

    private void getAllMessages() {
        messageDetailRequest.setLimit("30");
        messageDetailRequest.setSkip("0");
        messageDetailRequest.setMessageId(getIntent().getStringExtra("msg_id"));
        iMessageDetailPresenter.getMessageComment(messageDetailRequest, false);

    }

    @Override
    public void onNewMessageReceived(final Object object) {
        final ChatBodyModel model=new Gson().fromJson(object.toString(),ChatBodyModel.class);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.add(model);

                adapter.notifyItemInserted(list.size());
                Log.e("onNewMessageReceived",""+object);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMessagingService.disconnectSocket(true);
        unbindService(mServiceConnection);
    }
}
