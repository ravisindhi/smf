package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

import java.io.Serializable;

/**
 * Created by daffodil on 13/2/17.
 */

public class Notifications implements Serializable{

    private String direct;

    private String sourceUser;

    private String icon;

    private String text;

    private String actionItemId;

    private String createdAt;

    private String link;

    private String read;

    private String type;

    public String getDirect ()
    {
        return direct;
    }

    public void setDirect (String direct)
    {
        this.direct = direct;
    }

    public String getSourceUser ()
    {
        return sourceUser;
    }

    public void setSourceUser (String sourceUser)
    {
        this.sourceUser = sourceUser;
    }

    public String getIcon ()
    {
        return icon;
    }

    public void setIcon (String icon)
    {
        this.icon = icon;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getActionItemId ()
    {
        return actionItemId;
    }

    public void setActionItemId (String actionItemId)
    {
        this.actionItemId = actionItemId;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getLink ()
    {
        return link;
    }

    public void setLink (String link)
    {
        this.link = link;
    }

    public String getRead ()
    {
        return read;
    }

    public void setRead (String read)
    {
        this.read = read;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [direct = "+direct+", sourceUser = "+sourceUser+", icon = "+icon+", text = "+text+", actionItemId = "+actionItemId+", createdAt = "+createdAt+", link = "+link+", read = "+read+", type = "+type+"]";
    }
}
