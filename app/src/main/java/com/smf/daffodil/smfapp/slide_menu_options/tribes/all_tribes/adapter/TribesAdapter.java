package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.bottom_menu_option.members.adapter.MemberAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.adapter.PostAdapter;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.Result;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daffodil on 11/2/17.
 */

public class TribesAdapter extends BaseAdapter {

    private Context context;
    private List<Result> list = new ArrayList<>();

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public TribesAdapter(Context context, List<Result> list) {
        this.context = context;
        this.list = list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        AllTribesItemHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.tribes_item_view, viewGroup, false);
            viewHolder = new AllTribesItemHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (AllTribesItemHolder) view.getTag();
        }


        ImageUtils.setprofile(viewHolder.imageViewTribeImage, list.get(i).getImageUrl(), context);
        viewHolder.textViewTribeName.setText(list.get(i).getName());
        viewHolder.textViewTribeMemberCount.setText(String.valueOf(list.get(i).getMembers().length+1));
        viewHolder.textViewTribeDesc.setText(list.get(i).getDesc());
        if (list.get(i).getPrivateGroup().equalsIgnoreCase("false"))
            viewHolder.imageViewTribeLock.setVisibility(View.GONE);
        else
            viewHolder.imageViewTribeLock.setVisibility(View.VISIBLE);


        return view;
    }

    class AllTribesItemHolder {

        @BindView(R.id.tribe_image)
        ImageView imageViewTribeImage;

        @BindView(R.id.tribe_lock)
        ImageView imageViewTribeLock;

        @BindView(R.id.tribe_name)
        TextView textViewTribeName;

        @BindView(R.id.tribe_member_count)
        TextView textViewTribeMemberCount;

        @BindView(R.id.tribe_desc)
        TextView textViewTribeDesc;


        public AllTribesItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
