package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.search_tribe.SearchTribeResponse;

/**
 * Created by daffodil on 11/2/17.
 */

public interface ITribesHomeView extends IBaseView{

    void onGetTribes(AllTribesResponse membersDataList);
    void onGetSearchTribes(SearchTribeResponse respone);
    void onAppendMoreData(AllTribesResponse membersDataList);
    void onError(int errorcode,String message);
    void onSuccessFullJoinTribe();
    void onGetSearchResult(AllTribesResponse membersDataList);

}
