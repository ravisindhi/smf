package com.smf.daffodil.smfapp.authentication.forgot_password.presenter;

import com.smf.daffodil.smfapp.authentication.forgot_password.model.ForgotPasswordRequest;

/**
 * Created by daffodil on 24/1/17.
 */

public interface IForgotPasswordPresenter {

    void doForgotPassword(ForgotPasswordRequest request);
}
