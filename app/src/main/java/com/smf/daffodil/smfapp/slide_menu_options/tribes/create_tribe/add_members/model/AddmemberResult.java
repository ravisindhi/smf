package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.model;

/**
 * Created by daffodil on 22/2/17.
 */

public class AddmemberResult {

    private String _id;

    private AddMemberProfile profile;

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public AddMemberProfile getProfile ()
    {
        return profile;
    }

    public void setProfile (AddMemberProfile profile)
    {
        this.profile = profile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [_id = "+_id+", profile = "+profile+"]";
    }
}
