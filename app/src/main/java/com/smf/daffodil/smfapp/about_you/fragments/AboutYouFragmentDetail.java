package com.smf.daffodil.smfapp.about_you.fragments;


import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.AboutYouActivity;
import com.smf.daffodil.smfapp.about_you.communication.ActivityCommunicator;
import com.smf.daffodil.smfapp.about_you.communication.FragmentCommunicator;
import com.smf.daffodil.smfapp.about_you.fragments.adapters.MyCustomArrayAdapter;
import com.smf.daffodil.smfapp.about_you.fragments.presenter.IAboutFragmentPresenter;
import com.smf.daffodil.smfapp.about_you.fragments.presenter.IAboutPresenterCompl;
import com.smf.daffodil.smfapp.about_you.fragments.view.IAboutFragmentView;
import com.smf.daffodil.smfapp.about_you.model.AboutYouRequest;
import com.smf.daffodil.smfapp.about_you.model.HandleResponse;
import com.smf.daffodil.smfapp.authentication.login.model.response.LoginResponse;
import com.smf.daffodil.smfapp.authentication.signup.view.ISignupView;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.utils.AppUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTouch;
import io.blackbox_vision.datetimepickeredittext.view.DatePickerEditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutYouFragmentDetail extends BaseFragment implements FragmentCommunicator, IAboutFragmentView, AdapterView.OnItemSelectedListener {


    ActivityCommunicator activityCommunicator;
    private SessionManager sessionManager;
    IAboutFragmentPresenter iAboutFragmentPresenter;
    private static final int HANDLE_TEXT_CHANGED = 101;

    @BindView(R.id.et_first_name)
    EditText editTextFirstName;

    @BindView(R.id.et_last_name)
    EditText editTextLastName;

    @BindView(R.id.et_handle)
    EditText editTextHandle;

    @BindView(R.id.et_date_of_birth)
    DatePickerEditText editTextDOB;

    @BindView(R.id.et_introduction)
    EditText editTextIntroduction;

    @BindView(R.id.et_city)
    EditText editTextCity;

    @BindView(R.id.et_state)
    EditText editTextState;

    @BindView(R.id.et_zip_code)
    EditText editTextZip;

    @BindView(R.id.et_relationship)
    EditText editTextRelationship;

    @BindView(R.id.et_gender)
    EditText editTextGender;

    @BindView(R.id.relation_spinner)
    Spinner spinnerRelation;

    @BindView(R.id.gender_spinner)
    Spinner spinnerGender;

    @BindView(R.id.handle_progress_bar)
    ProgressBar progressBarHandle;

    @BindView(R.id.txt_handle_error)
    TextView textViewHandleError;

    private int check = 0;


    ArrayAdapter<String> dataAdapter;
    ArrayAdapter<String> dataAdapter2;


    // Relationship Spinner Drop down elements
    List<String> relations = new ArrayList<String>();

    // Gender Spinner Drop down elements
    List<String> gender = new ArrayList<String>();


    public AboutYouFragmentDetail() {
        // Required empty public constructor

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((AboutYouActivity) context).fragmentCommunicator = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        activityCommunicator = (ActivityCommunicator) getActivity();
        iAboutFragmentPresenter = new IAboutPresenterCompl(getActivity(), this);
        sessionManager = new SessionManager(getActivity());
        return inflater.inflate(R.layout.fragment_about_you_fragment_detail, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        /*custom edit text set typeface*/
        editTextDOB.setManager(getFragmentManager());
        setTypeface();

        /*hide relationship edit text click keyboard*/
        editTextRelationship.setInputType(InputType.TYPE_NULL);
        editTextGender.setInputType(InputType.TYPE_NULL);




         /*Relationship Spinner Drop down elements*/
        relations.clear();
        relations.add("Select Relationship Status");
        relations.add("Single");
        relations.add("Committed");
        relations.add("Married");
        relations.add("Divorced");

         /*Gender Spinner Drop down elements*/
        gender.clear();
        gender.add("Select Gender");
        gender.add("Male");
        gender.add("Female");

        // Creating adapter for spinner

        dataAdapter = new MyCustomArrayAdapter(getActivity(), R.layout.spinner_item, relations);
        dataAdapter2 = new MyCustomArrayAdapter(getActivity(), R.layout.spinner_item, gender);


        // attaching data adapter to spinner
        spinnerRelation.setAdapter(dataAdapter);
        spinnerGender.setAdapter(dataAdapter2);

        /*prevent spinner to load data first time*/
        spinnerRelation.setSelection(0, false);
        spinnerGender.setSelection(0, false);
        spinnerRelation.setOnItemSelectedListener(this);
        spinnerGender.setOnItemSelectedListener(this);
        setupDropDownPopviewLength();//setup dropdown width custom
        editTextHandle.addTextChangedListener(handleWatcher);

        setInitialDataIfPresent();//set Initial Data if present in prefrences

        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, -18); // to get previous year add -1
        Date nextYear = cal.getTime();


        String newDate= (String) DateFormat.format("MM/dd/yyyy",nextYear);
        Log.e("18year back>>>",newDate);
          editTextDOB.setMaxDate(newDate);

        editTextDOB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                editTextDOB.invalidate();
                editTextDOB.requestFocus();
                return true;
            }
        });

    }



    @OnClick(R.id.et_date_of_birth)
    public void openDatePicker2(){
        editTextDOB.invalidate();
        editTextDOB.requestFocus();
    }

    private void setupDropDownPopviewLength()
    {
        /*Set spinner dropdown width to match parent*/
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();
        spinnerRelation.setDropDownWidth((int) (config.screenWidthDp * dm.density));
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) editTextRelationship.getLayoutParams();
        spinnerGender.setDropDownWidth((int) (config.screenWidthDp * dm.density)-lp.leftMargin-lp.rightMargin);
        spinnerRelation.setDropDownWidth((int) (config.screenWidthDp * dm.density)-lp.leftMargin-lp.rightMargin);


    }

    @Override
    public void passDataToFragment() {

        setData();
        iAboutFragmentPresenter.getAboutYouData(null);
    }

    @Override
    public void setMandetoryData() {
        setData();
    }

    private void setData() {
        AboutYouRequest.setFirstname(editTextFirstName.getText().toString().trim());
        AboutYouRequest.setLastname(editTextLastName.getText().toString().trim());
        AboutYouRequest.setDate_of_birth(editTextDOB.getText().toString().trim().replaceAll("/",""));

        if (textViewHandleError.getVisibility() == View.GONE)
            AboutYouRequest.setHandle(editTextHandle.getText().toString().trim());


        AboutYouRequest.setIntroduction(editTextIntroduction.getText().toString().trim());
        AboutYouRequest.setCity(editTextCity.getText().toString().trim());
        AboutYouRequest.setState(editTextState.getText().toString().trim());
        AboutYouRequest.setZip(editTextZip.getText().toString().trim());
        AboutYouRequest.setGender(editTextGender.getText().toString().trim());
        AboutYouRequest.setRelation_status(editTextRelationship.getText().toString().trim());
    }


    @Override
    public void onErrorHandleWithView(int errorCode, String _errorMsg) {
        switch (errorCode) {
            case IAboutFragmentView.FIRST_NAME_ERROR:
                replaceErrorViewWithMessage(editTextFirstName, getActivity());
                editTextFirstName.requestFocus();
                break;
            case IAboutFragmentView.LAST_NAME_ERROR:
                replaceErrorViewWithMessage(editTextLastName, getActivity());
                editTextLastName.requestFocus();
                break;
            case IAboutFragmentView.HANDLE_ERROR:
                replaceErrorViewWithMessage(editTextHandle, getActivity());
                editTextHandle.requestFocus();
                break;
            case IAboutFragmentView.DOB_ERROR:
                replaceErrorViewWithMessage(editTextDOB, getActivity());
                editTextDOB.requestFocus();
                break;
            case IAboutFragmentView.INTRO_ERROR:
                replaceErrorViewWithMessage(editTextIntroduction, getActivity());
                editTextIntroduction.requestFocus();
                break;
            case IAboutFragmentView.CITY_ERROR:
                replaceErrorViewWithMessage(editTextCity, getActivity());
                editTextCity.requestFocus();
                break;
            case IAboutFragmentView.STATE_ERROR:
                replaceErrorViewWithMessage(editTextState, getActivity());
                editTextState.requestFocus();
                break;
            case IAboutFragmentView.ZIP_ERROR:
                replaceErrorViewWithMessage(editTextZip, getActivity());
                editTextZip.requestFocus();
                break;


        }
    }

    private void setInitialDataIfPresent() {
        if (sessionManager.getUserData() != null) {
            LoginResponse data = sessionManager.getUserData();
            if (data.getProfile().getFirstName() != null && data.getProfile().getFirstName().length() > 0) {
                editTextFirstName.setText(data.getProfile().getFirstName());
            }
            if (data.getProfile().getLastName() != null && data.getProfile().getLastName().length() > 0) {
                editTextLastName.setText(data.getProfile().getLastName());
            }

            if (data.getProfile().getHandle() != null && data.getProfile().getHandle().length() > 0) {
                editTextHandle.setText(data.getProfile().getHandle());
            }
            if (data.getProfile().getDob() != null && data.getProfile().getDob().length() > 0) {

                String date=data.getProfile().getDob();
                String day=date.substring(0,2);
                String month=date.substring(2,4);
                String year=date.substring(4);
                editTextDOB.setText(day+"/"+month+"/"+year);
            }
            if (data.getProfile().getRelationship() != null && data.getProfile().getRelationship().length() > 0) {
                editTextRelationship.setText(data.getProfile().getRelationship());
            }
            if (data.getProfile().getGender() != null && data.getProfile().getGender().length() > 0) {
                editTextGender.setText(data.getProfile().getGender());
            }
            if (data.getProfile().getIntro() != null && data.getProfile().getIntro().length() > 0) {
                editTextIntroduction.setText(data.getProfile().getIntro());
            }
            if (data.getProfile().getCity() != null && data.getProfile().getCity().length() > 0) {
                editTextCity.setText(data.getProfile().getCity());
            }
            if (data.getProfile().getState() != null && data.getProfile().getState().length() > 0) {
                editTextState.setText(data.getProfile().getState());
            }
            if (data.getProfile().getZipCode() != null && data.getProfile().getZipCode().length() > 0) {
                editTextZip.setText(data.getProfile().getZipCode());
            }
        }

    }

    @Override
    public void onErrorHandleWithSnakbar(int errorCode, String _errorMsg) {
        switch (errorCode) {


            case IAboutFragmentView.GENDER_ERROR:
                replaceErrorView(editTextGender, getActivity());
                showErrorMessage(getContext().getApplicationContext(), getString(R.string.gender_error_text), getActivity());
                editTextGender.requestFocus();

                break;

            case IAboutFragmentView.RELATION_STATUS_ERROR:
                replaceErrorView(editTextRelationship, getActivity());
                showErrorMessage(getContext().getApplicationContext(), getString(R.string.relationship_error), getActivity());
                editTextRelationship.requestFocus();

                break;
            case IBaseView.NETWORK_ERROR:
                showErrorMessage(getContext().getApplicationContext(), _errorMsg, getActivity());

                break;

        }
    }


    @Override
    public void onSuccess(AboutYouRequest aboutYouRequest) {
        activityCommunicator.passDataToActivity(aboutYouRequest);
    }

    @Override
    public void onHandleMessage(HandleResponse handleResponse) {
        progressBarHandle.setVisibility(View.GONE);
        if (handleResponse.isAvailability())
            textViewHandleError.setVisibility(View.GONE);
        else
            textViewHandleError.setVisibility(View.VISIBLE);


    }

    @Override
    public void onHandleError() {
        progressBarHandle.setVisibility(View.GONE);
        textViewHandleError.setVisibility(View.GONE);
    }


    @OnTouch(R.id.et_relationship)
    public boolean selectRelationship() {
        spinnerRelation.performClick();
        return true;
    }

    @OnTouch(R.id.et_gender)
    public boolean selectGender() {
        spinnerGender.performClick();
        return true;
    }


    private void setTypeface() {
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/QUICKSAND-MEDIUM.TTF");
        editTextDOB.setTypeface(font);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        Spinner spinner = (Spinner) adapterView;
        if (spinner.getId() == R.id.relation_spinner) {
            if (spinner.getSelectedItemPosition() != 0)
                editTextRelationship.setText(spinner.getSelectedItem().toString());


        } else if (spinner.getId() == R.id.gender_spinner) {
            if (spinner.getSelectedItemPosition() != 0)
                editTextGender.setText(spinner.getSelectedItem().toString());
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //Toast.makeText(getActivity(), "no", Toast.LENGTH_SHORT).show();
    }


    /*Handle text watcher*/
    private final TextWatcher handleWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String handle_text = charSequence.toString().trim();
            if (handle_text != null && handle_text.length() > 0) {

                if(!handle_text.contains("@")){
                    progressBarHandle.setVisibility(View.VISIBLE);
                    handler.removeMessages(HANDLE_TEXT_CHANGED);
                    handler.sendMessageDelayed(handler.obtainMessage(HANDLE_TEXT_CHANGED, handle_text), 500);
                }else{
                    Toast.makeText(getActivity(),"Please remove '@' from the handle.",Toast.LENGTH_LONG).show();
                    progressBarHandle.setVisibility(View.GONE);
                }

            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            iAboutFragmentPresenter.checkHandleName((String) msg.obj);
        }
    };



}
