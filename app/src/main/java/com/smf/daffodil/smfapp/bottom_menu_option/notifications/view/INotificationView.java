package com.smf.daffodil.smfapp.bottom_menu_option.notifications.view;

import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response.Notification;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;

import java.util.List;

/**
 * Created by daffodil on 17/2/17.
 */

public interface INotificationView extends IBaseView{

    void onGetNotifications(Notification[] result);
    void onError(int errorcode,String message);
    void onClearNotifications();
    void onAcceptRejectSuccess(int position);
    void onGetNotificationDetailPost(GetPostDataResponse postDataResponse);
    void onGetNotificationDetailGroup(GetPostDataResponse postDataResponse);
}
