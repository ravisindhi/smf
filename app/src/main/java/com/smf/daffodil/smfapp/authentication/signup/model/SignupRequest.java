package com.smf.daffodil.smfapp.authentication.signup.model;

import com.google.gson.annotations.SerializedName;
import com.smf.daffodil.smfapp.authentication.login.model.request.DeviceInfo;

/**
 * Created by daffodil on 25/1/17.
 */

public class SignupRequest {

    DeviceInfo deviceInfo;


    @SerializedName("firstName")
    String first_name;

    @SerializedName("lastName")
    String last_name;

    @SerializedName("email")
    String email;

    @SerializedName("password")
    String password;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @SerializedName("fullName")
    String fullName;

    public String getConfirm_pasword() {
        return confirm_pasword;
    }

    public void setConfirm_pasword(String confirm_pasword) {
        this.confirm_pasword = confirm_pasword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    String confirm_pasword;

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}
