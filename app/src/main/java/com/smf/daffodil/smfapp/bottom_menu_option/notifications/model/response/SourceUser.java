
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SourceUser {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("favorites")
    @Expose
    private List<String> favorites = null;
    @SerializedName("deviceInfo")
    @Expose
    private DeviceInfo deviceInfo;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("settings")
    @Expose
    private Settings settings;
    @SerializedName("pinned")
    @Expose
    private List<Object> pinned = null;
    @SerializedName("notifications")
    @Expose
    private List<Notification_> notifications = null;
    @SerializedName("workspaces")
    @Expose
    private List<Object> workspaces = null;
    @SerializedName("connections")
    @Expose
    private Connections connections;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("secure")
    @Expose
    private Secure secure;
    @SerializedName("services")
    @Expose
    private Services services;
    @SerializedName("emails")
    @Expose
    private List<Email> emails = null;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<String> favorites) {
        this.favorites = favorites;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public List<Object> getPinned() {
        return pinned;
    }

    public void setPinned(List<Object> pinned) {
        this.pinned = pinned;
    }

    public List<Notification_> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification_> notifications) {
        this.notifications = notifications;
    }

    public List<Object> getWorkspaces() {
        return workspaces;
    }

    public void setWorkspaces(List<Object> workspaces) {
        this.workspaces = workspaces;
    }

    public Connections getConnections() {
        return connections;
    }

    public void setConnections(Connections connections) {
        this.connections = connections;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Secure getSecure() {
        return secure;
    }

    public void setSecure(Secure secure) {
        this.secure = secure;
    }

    public Services getServices() {
        return services;
    }

    public void setServices(Services services) {
        this.services = services;
    }

    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
