package com.smf.daffodil.smfapp.bottom_menu_option.post;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.about_you.fragments.adapters.MyCustomArrayAdapter;
import com.smf.daffodil.smfapp.authentication.login.view.ILoginView;
import com.smf.daffodil.smfapp.base.BaseFragment;
import com.smf.daffodil.smfapp.bottom_menu_option.post.adapter.PostAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.CommentsActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.presenter.IAllPostCompl;
import com.smf.daffodil.smfapp.bottom_menu_option.post.presenter.IAllPostPresenter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.search_post.SearchPostActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.common.Constants;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.common.presenter.IActionCompl;
import com.smf.daffodil.smfapp.common.presenter.IActionPresenter;
import com.smf.daffodil.smfapp.common.view.IActionView;
import com.smf.daffodil.smfapp.post_and_update.PostAndUpdateActivity;
import com.smf.daffodil.smfapp.view.OnLoadMoreListener;
import com.smf.daffodil.smfapp.view.PopupDialog;
import com.smf.daffodil.smfapp.view.RecyclerItemClickListener;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends BaseFragment implements IAllPostView, View.OnTouchListener
        , AdapterView.OnItemSelectedListener, OnLoadMoreListener, PostAdapter.PostActionsListener,IActionView,PopupDialog.DilogButtonHandling{

    @BindView(R.id.post_recyclerview)
    RecyclerView recyclerViewPosts;

    @BindView(R.id.location_spinner)
    Spinner mspinnerLocation;

    @BindView(R.id.card_view_whts_mind_txt)
    CardView cardViewWhatsMind;

    @BindView(R.id.main_layout)
    LinearLayout linearLayoutMain;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;


    LinearLayout linearLayoutHeaderMain;

    private int totalDisplayFeed;
    private int totalFeeds;
    private Toolbar toolbar;
    private TextView mHeaderTextView;

    private ImageView down_arrow;

    private PostAdapter adapter;
    private final int DATA_RESULT = 101;
    private final int DATA_RESULT2 = 102;
    private PopupWindow post_popup;

    private int dropdown_selected_pos=0;
    private String reportid;
    private PopupDialog popupDialog;

    //all posts datas
    List<GetPostDataResponse> postDatas = new ArrayList<>();

    IAllPostPresenter iAllPostPresenter;
    IActionPresenter iActionPresenter;

    // Location Spinner Drop down elements
    List<String> locationSpinnerList = new ArrayList<String>();
    ArrayAdapter<String> spinner_dataAdapter;
    GetPostRequest getPostRequest = new GetPostRequest();
    private SessionManager sessionManager;

    @Override
    public void buttonYesClicked(String type) {
        iActionPresenter.doPostReport(reportid);
        reportid=null;
        popupDialog.dismiss();
    }

    @Override
    public void buttonNoClicked() {
        popupDialog.dismiss();
    }


    public enum PostLocationType{
        local,nearby,tranding,favorite,location
    }


    public PostFragment() {

        // Required empty public constructor
    }

    private void openReportPopup()
    {
        popupDialog = new PopupDialog(this);
        Bundle bundle = new Bundle();
        bundle.putString("name", "Report Post");
        bundle.putString("content", "You are reporting that this post is not compliant with our community standards. Please confirm that you have reviewed our standards.");
        bundle.putString("accept_text", "Report");
        bundle.putString("cancel_text", "Cancel");
        popupDialog.setArguments(bundle);
        popupDialog.show(getActivity().getFragmentManager(),"new");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        iAllPostPresenter = new IAllPostCompl(getActivity(), this);
        iActionPresenter=new IActionCompl(getActivity(),this);
        sessionManager = new SessionManager(getActivity());
        return inflater.inflate(R.layout.fragment_post, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mHeaderTextView = (TextView) toolbar.findViewById(R.id.header_text);
        down_arrow = (ImageView) toolbar.findViewById(R.id.down_arrow);
        linearLayoutHeaderMain=(LinearLayout)toolbar.findViewById(R.id.header_main);
        mHeaderTextView.setOnTouchListener(this);

        mspinnerLocation.setOnItemSelectedListener(this);

        setHasOptionsMenu(true);
        addLocationSpinnerData();
        initViews();
        refreshLayout();
    }


    private void refreshLayout() {
        //swipeRefreshLayout.setColorSchemeResources(getActivity().getResources().getColor(colorButton));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                getPosts(PostLocationType.local,dropdown_selected_pos);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.action_bar_home_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                launchActivity(getActivity(), SearchPostActivity.class);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initViews() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext().getApplicationContext());
        recyclerViewPosts.setLayoutManager(mLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerViewPosts.setItemAnimator(itemAnimator);
        adapter = new PostAdapter(postDatas, getActivity(), recyclerViewPosts, this, Constants.POST_SCREEN_TYPE_HOME);
        recyclerViewPosts.setAdapter(adapter);
        recyclerViewPosts.setHasFixedSize(true);

        recyclerViewPosts.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (postDatas.get(position).getHidden()) {
                    Intent i = new Intent(getActivity(), CommentsActivity.class);
                    i.putExtra("isHiddenPost", true);
                    i.putExtra("pos", position);
                    i.putExtra("post_model", postDatas.get(position));
                    startActivityForResult(i, 102);


                }
            }
        }));

    }

    private void getPosts(PostLocationType type,int position) {
        showLoader("Please wait...");
        getPostRequest.setSkip("0");
        getPostRequest.setLimit("10");

        switch (position){
            case 0:
                getPostRequest.setLocal(true);
                getPostRequest.setNearBy(false);
                getPostRequest.setTranding(false);
                getPostRequest.setFavorite(false);
                getPostRequest.setLocation(false);
                break;
            case 1:
                getPostRequest.setLocal(false);
                getPostRequest.setNearBy(true);
                getPostRequest.setTranding(false);
                getPostRequest.setFavorite(false);
                getPostRequest.setLocation(false);
                getPostRequest.setRange(new Double[]{0d,250d});
                break;
            case 3:
                getPostRequest.setLocal(false);
                getPostRequest.setNearBy(false);
                getPostRequest.setTranding(true);
                getPostRequest.setFavorite(false);
                getPostRequest.setLocation(false);
                break;
            case 2:
                getPostRequest.setLocal(false);
                getPostRequest.setNearBy(false);
                getPostRequest.setTranding(false);
                getPostRequest.setFavorite(true);
                getPostRequest.setLocation(false);
                break;
            default:
                getPostRequest.setCordinates(new Double[]{Double.parseDouble(sessionManager.getUserData().getProfile().getSavedLocations().get(position-4).getLatLng()[0]),
                        Double.parseDouble(sessionManager.getUserData().getProfile().getSavedLocations().get(position-4).getLatLng()[1])});
                getPostRequest.setLocal(false);
                getPostRequest.setNearBy(false);
                getPostRequest.setTranding(false);
                getPostRequest.setFavorite(false);
                getPostRequest.setLocation(true);
                break;


        }

        iAllPostPresenter.getAllPosts(getPostRequest);
    }


    private void addLocationSpinnerData() {
        /*Set spinner dropdown width to match parent*/
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();
        mspinnerLocation.setDropDownWidth((int) (config.screenWidthDp * dm.density));

        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(mspinnerLocation);

            // Set popupWindow height to 500px
            popupWindow.setHeight(500);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }


        locationSpinnerList.clear();
        locationSpinnerList.add("Local(" + sessionManager.getUserData().getProfile().getCity() + ")");
        locationSpinnerList.add("Nearby");
        locationSpinnerList.add("Favorites");
        locationSpinnerList.add("Trending");



        if (sessionManager.getUserData().getProfile().getSavedLocations() != null) {
            for (int i = 0; i < sessionManager.getUserData().getProfile().getSavedLocations().size(); i++) {
                locationSpinnerList.add(sessionManager.getUserData().getProfile().getSavedLocations().get(i).getName());
            }
        }


        spinner_dataAdapter = new MyCustomArrayAdapter(getActivity(), R.layout.spinner_item, locationSpinnerList);
        mspinnerLocation.setAdapter(spinner_dataAdapter);
        //mspinnerLocation.setSelection(0, false);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.header_text:
              case R.id.down_arrow:
            case R.id.header_main:
                mspinnerLocation.performClick();
                break;


        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            mHeaderTextView.setText(mspinnerLocation.getSelectedItem().toString());
            if(mspinnerLocation.getSelectedItemPosition()==0){
                dropdown_selected_pos=0;
                getPosts(PostLocationType.local,dropdown_selected_pos);
            }else if(mspinnerLocation.getSelectedItemPosition()==1){
                dropdown_selected_pos=1;
                getPosts(PostLocationType.nearby,dropdown_selected_pos);
            }else if(mspinnerLocation.getSelectedItemPosition()==2){
            dropdown_selected_pos=2;
            getPosts(PostLocationType.favorite,dropdown_selected_pos);
            }else if(mspinnerLocation.getSelectedItemPosition()==3){
                dropdown_selected_pos=3;
                getPosts(PostLocationType.tranding,dropdown_selected_pos);
            }else {
                dropdown_selected_pos=mspinnerLocation.getSelectedItemPosition();
                getPosts(PostLocationType.location,dropdown_selected_pos);
            }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.card_view_whts_mind_txt)
    public void openPostAndUpdate() {
        Intent i = new Intent(getActivity(), PostAndUpdateActivity.class);
        startActivityForResult(i, DATA_RESULT);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DATA_RESULT) {
            swipeRefreshLayout.setRefreshing(true);
            getPosts(PostLocationType.local,dropdown_selected_pos);
        } else if (requestCode == DATA_RESULT2) {
            try {
                if (data.getIntExtra("pos", -1) != -1) {
                    iActionPresenter.doHidePost(postDatas.get(data.getIntExtra("pos", -1)).get_id(), (data.getIntExtra("pos", -1)));
                }
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public void onSuccess(PostDataResult dataResponses) {
        postDatas.clear();
        Log.e("data size", String.valueOf(dataResponses.getResult().size()));
        Log.e("total size", String.valueOf(dataResponses.getTotalfeedscount()));

        totalDisplayFeed = dataResponses.getResult().size();
        totalFeeds = dataResponses.getTotalfeedscount();

        swipeRefreshLayout.setRefreshing(false);
        postDatas.addAll(dataResponses.getResult());
        adapter.notifyDataSetChanged();
        hideLoader();

        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                Log.e("couunt>>DisplayFeed", String.valueOf(totalFeeds) + ">>>>" + String.valueOf(totalDisplayFeed));

                doLoadMore();

            }
        });

    }


    private void doLoadMore() {
        String oldskip = getPostRequest.getSkip();
        String oldlimit = getPostRequest.getLimit();

        String newSkip = String.valueOf(Integer.parseInt(oldlimit));
        String newLimit = String.valueOf(Integer.parseInt(oldlimit) + 10);
        adapter.loadMoreStart();
        getPostRequest.setSkip(newSkip);
        getPostRequest.setLimit(newLimit);
        iAllPostPresenter.getAllPosts(getPostRequest);
    }

    @Override
    public void onAppendData(PostDataResult dataResponses) {
        swipeRefreshLayout.setRefreshing(false);
        Log.e("data size load more", String.valueOf(dataResponses.getResult().size()));
        totalDisplayFeed = totalDisplayFeed + dataResponses.getResult().size();
        adapter.loadMoreData(dataResponses.getResult());

    }

    @Override
    public void onError(int errorcode, String message) {
        switch (errorcode) {
            case ILoginView.NETWORK_ERROR:
            case ILoginView.SERVER_ERROR:
                showErrorMessage(getContext().getApplicationContext(), message, getActivity());
                break;
            case IAllPostView.NODATA:
                showErrorMessage(getContext().getApplicationContext(), message, getActivity());
                postDatas.clear();
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onUpdatePostAction(int position, PostActions actiontype) {
        switch (actiontype) {
            case like:
                if(!postDatas.get(position).getLikedBy().contains(sessionManager.getUserData().get_id()))
                postDatas.get(position).getLikedBy().add(sessionManager.getUserData().get_id());
                else
                    postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());

                /*postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case dislike:
                if(!postDatas.get(position).getDislikedBy().contains(sessionManager.getUserData().get_id()))
                    postDatas.get(position).getDislikedBy().add(sessionManager.getUserData().get_id());
               else
                    postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());

                /* postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case peace:
                if(!postDatas.get(position).getPeaceBy().contains(sessionManager.getUserData().get_id()))
                    postDatas.get(position).getPeaceBy().add(sessionManager.getUserData().get_id());
                else
                    postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());

                /*postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                */break;
        }
        adapter.notifyItemChanged(position);
    }

    @Override
    public void onHidePost(int position) {
        hidePost(position);
        Toast.makeText(getActivity(), "Post hide successfully", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onReportPost() {
        Toast.makeText(getActivity(), "Post has been reported.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBlockUser(String handle) {
        Toast.makeText(getActivity(), handle + " blocked successfully.", Toast.LENGTH_LONG).show();
        getPosts(PostLocationType.local,dropdown_selected_pos);

    }

    @Override
    public void onSharePost(PostActionResponse response) {
        Toast.makeText(getActivity(), "Post shared successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAddComment(int position) {
        postDatas.get(position).setCommentsCount(postDatas.get(position).getCommentsCount() + 1);
        adapter.notifyItemChanged(position);
        Toast.makeText(getActivity(), "Comment added successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void performAction(UpdatePostRequest request, int position) {
        iActionPresenter.updatePost(request, position);
    }

    @Override
    public void openPopupview(Context context, Point p, final int position, final GetPostDataResponse response) {
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.post_popup_view_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.post_pop_view_layout, null);

        /*action button*/
        LinearLayout actionReportPost = (LinearLayout) layout.findViewById(R.id.action_report_post);
        LinearLayout actionHidePost = (LinearLayout) layout.findViewById(R.id.action_hide_post);
        LinearLayout actionBlockUser = (LinearLayout) layout.findViewById(R.id.action_block_user);
        TextView userName = (TextView) actionBlockUser.findViewById(R.id.popup_user_handle);

        userName.setText("Block @" + response.getAuthor().getProfile().getHandle());

        // Creating the PopupWindow
        post_popup = new PopupWindow(context);
        post_popup.setContentView(layout);
         post_popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int OFFSET_X = -500;
        int OFFSET_Y = 70;

        //Clear the default translucent background
        post_popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        post_popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);


        actionHidePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doHidePost(response.get_id(), position);
                hidePost(position);
                post_popup.dismiss();
            }
        });

        actionReportPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openReportPopup();
                reportid=response.get_id();

                post_popup.dismiss();
            }
        });

        actionBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doBlockUser(response.getAuthor().get_id(),
                        "@" + response.getAuthor().getProfile().getHandle());
                post_popup.dismiss();
            }
        });


    }

    @Override
    public void sharePost(String postid) {

        SharePostRequest request = new SharePostRequest();
        request.setCaption("test sharing post");
        request.setPost_id(postid);
        request.setCordinates(new String[]{"0", "0"});
        iActionPresenter.doSharePost(request);
    }

    @Override
    public void addComment(String postid, String comment_txt, int position) {
        iActionPresenter.doAddComment(postid, comment_txt, position,"post");
    }


    private void hidePost(int position) {
        postDatas.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, postDatas.size());
    }


}
