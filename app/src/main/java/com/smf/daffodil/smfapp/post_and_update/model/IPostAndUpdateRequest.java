package com.smf.daffodil.smfapp.post_and_update.model;

import android.net.Uri;

/**
 * Created by daffodil on 4/2/17.
 */

public class IPostAndUpdateRequest {

    private String whats_in_mind_txt;

    private String conten_server_url;

    private Uri content_uri;

    private String content_type;

    private String[] coordinates;

    private String groupid;

    private String hidden;


    public String[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String[] coordinates) {
        this.coordinates = coordinates;
    }



    public String getWhats_in_mind_txt() {
        return whats_in_mind_txt;
    }

    public void setWhats_in_mind_txt(String whats_in_mind_txt) {
        this.whats_in_mind_txt = whats_in_mind_txt;
    }

    public String getConten_server_url() {
        return conten_server_url;
    }

    public void setConten_server_url(String conten_server_url) {
        this.conten_server_url = conten_server_url;
    }

    public Uri getContent_uri() {
        return content_uri;
    }

    public void setContent_uri(Uri content_uri) {
        this.content_uri = content_uri;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }


    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }
}
