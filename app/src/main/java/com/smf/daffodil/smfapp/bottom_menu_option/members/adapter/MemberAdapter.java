package com.smf.daffodil.smfapp.bottom_menu_option.members.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.bottom_menu_option.members.model.members.MemberResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.members.view.IMemberView;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.utils.ImageUtils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daffodil on 26/1/17.
 */

public class MemberAdapter extends BaseAdapter {

    Context context;
    List<MemberResponse> membersList;
    FollowUnfollowButtonClickListener followUnfollowButtonClickListener;
    SessionManager sessionManager;
    private String[] followingMembers;

    public MemberAdapter(Context context, List<MemberResponse> membersList,FollowUnfollowButtonClickListener followUnfollowButtonClickListener) {

        this.context = context;
        this.membersList = membersList;
        this.followUnfollowButtonClickListener=followUnfollowButtonClickListener;
        sessionManager=new SessionManager(context);

    }

    @Override
    public int getCount() {
        return membersList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final MemberViewHolder viewHolder;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.member_item_view, viewGroup, false);
            viewHolder = new MemberViewHolder(view,context);
            followingMembers=sessionManager.getUserData().getConnections().getFavorites();
            view.setTag(viewHolder);
        } else {
            viewHolder = (MemberViewHolder) view.getTag();
        }

        //check if user following member then change layout
        if(followingMembers.length>0)
        {
            if(Arrays.asList(followingMembers).contains(membersList.get(i).get_id()))
                viewHolder.setFollowLayout();
            else
                viewHolder.setunFollowLayout();
        }

        viewHolder.textViewMemberName.setText(membersList.get(i).getProfile().getFirstName() + " " + membersList.get(i).getProfile().getLastName());
        viewHolder.textViewMemberHandle.setText("@"+membersList.get(i).getProfile().getHandle());
        viewHolder.textViewMemberAddress.setText(membersList.get(i).getProfile().getCity()+", "+membersList.get(i).getProfile().getState());
        ImageUtils.setprofile(viewHolder.imageViewMemberImage,membersList.get(i).getProfile().getProfileImage(),context);
        //Glide.with(context).load(membersList.get(i).getProfile().getProfileImage()).error(R.drawable.dummy_rounded).into(viewHolder.imageViewMemberImage);

        viewHolder.linearLayoutMemberFollowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder.textViewFollwTxt.getText().toString().equalsIgnoreCase("Follow"))
                {
                    followUnfollowButtonClickListener.followUnfollowClicked(IMemberView.Action.Follow.toString(),membersList.get(i).get_id());
                    viewHolder.setFollowLayout();
                }else
                {
                    followUnfollowButtonClickListener.followUnfollowClicked(IMemberView.Action.Unfollow.toString(),membersList.get(i).get_id());

                    viewHolder.setunFollowLayout();
                }
            }
        });

        return view;

    }

    public interface FollowUnfollowButtonClickListener{
        void followUnfollowClicked(String action,String member_id);
    }



    public class MemberViewHolder {

        Context context;
        @BindView(R.id.member_image)
        ImageView imageViewMemberImage;

        @BindView(R.id.star_image)
        ImageView imageViewStar;

        @BindView(R.id.follw_txt)
        TextView textViewFollwTxt;

        @BindView(R.id.member_name)
        TextView textViewMemberName;

        @BindView(R.id.member_handle)
        TextView textViewMemberHandle;

        @BindView(R.id.member_address)
        TextView textViewMemberAddress;


        @BindView(R.id.member_follow)
        LinearLayout linearLayoutMemberFollowBtn;


        public MemberViewHolder(View view,Context context) {
            this.context=context;
            ButterKnife.bind(this,view);
        }


        private void setFollowLayout()
        {
            linearLayoutMemberFollowBtn.setBackground(context.getResources().getDrawable(R.drawable.ic_blue_fill));
            textViewFollwTxt.setTextColor(Color.parseColor("#FFFFFF"));
            textViewFollwTxt.setText("Following");
            imageViewStar.setImageResource(R.drawable.ic_white_border_star);

        }

        private void setunFollowLayout()
        {
            linearLayoutMemberFollowBtn.setBackground(context.getResources().getDrawable(R.drawable.ic_blue_border));
            textViewFollwTxt.setTextColor(context.getResources().getColor(R.color.colorButton));
            textViewFollwTxt.setText("Follow");
            imageViewStar.setImageResource(R.drawable.ic_blue_border_star);

        }


    }
}
