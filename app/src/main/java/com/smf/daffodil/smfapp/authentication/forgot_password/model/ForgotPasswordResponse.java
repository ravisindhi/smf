package com.smf.daffodil.smfapp.authentication.forgot_password.model;

/**
 * Created by daffodil on 30/1/17.
 */

public class ForgotPasswordResponse {

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
}
