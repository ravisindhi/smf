
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Connections {

    @SerializedName("following")
    @Expose
    private List<Object> following = null;
    @SerializedName("blocked")
    @Expose
    private List<Object> blocked = null;
    @SerializedName("connected")
    @Expose
    private List<Object> connected = null;
    @SerializedName("favorites")
    @Expose
    private List<String> favorites = null;

    public List<Object> getFollowing() {
        return following;
    }

    public void setFollowing(List<Object> following) {
        this.following = following;
    }

    public List<Object> getBlocked() {
        return blocked;
    }

    public void setBlocked(List<Object> blocked) {
        this.blocked = blocked;
    }

    public List<Object> getConnected() {
        return connected;
    }

    public void setConnected(List<Object> connected) {
        this.connected = connected;
    }

    public List<String> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<String> favorites) {
        this.favorites = favorites;
    }

}
