package com.smf.daffodil.smfapp.authentication.login.presenter;

import com.smf.daffodil.smfapp.authentication.login.model.request.FacebookLoginRequest;
import com.smf.daffodil.smfapp.authentication.login.model.request.LoginRequest;

/**
 * Created by daffodil on 24/1/17.
 */

public interface ILoginPresenter {

    void doLogin(LoginRequest loginRequest);
    void doFacebookLogin(FacebookLoginRequest facebookLoginRequest);
}
