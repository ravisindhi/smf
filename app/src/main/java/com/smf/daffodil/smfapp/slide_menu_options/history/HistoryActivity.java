package com.smf.daffodil.smfapp.slide_menu_options.history;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.base.BaseActivity;
import com.smf.daffodil.smfapp.bottom_menu_option.post.adapter.PostAdapter;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.common.Constants;
import com.smf.daffodil.smfapp.common.SessionManager;
import com.smf.daffodil.smfapp.common.presenter.IActionCompl;
import com.smf.daffodil.smfapp.common.presenter.IActionPresenter;
import com.smf.daffodil.smfapp.common.view.IActionView;
import com.smf.daffodil.smfapp.slide_menu_options.history.model.GetHistoryRequest;
import com.smf.daffodil.smfapp.slide_menu_options.history.presenter.IHistoryPresenter;
import com.smf.daffodil.smfapp.slide_menu_options.history.presenter.IHistoryPresenterCompl;
import com.smf.daffodil.smfapp.slide_menu_options.history.view.IHistoryView;
import com.smf.daffodil.smfapp.view.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends BaseActivity implements IHistoryView,PostAdapter.PostActionsListener,IActionView{

    @BindView(R.id.post_recyclerview)
    RecyclerView recyclerViewPosts;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private PostAdapter adapter;
    IHistoryPresenter iHistoryPresenter;
    IActionPresenter iActionPresenter;
    //history data
    List<GetPostDataResponse> postDatas=new ArrayList<>();
    GetHistoryRequest getHistoryRequest=new GetHistoryRequest();

    private int totalDisplayFeed;
    private int totalFeeds;
    private PopupWindow post_popup;
    private SessionManager sessionManager;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        iHistoryPresenter=new IHistoryPresenterCompl(this,HistoryActivity.this);
        iActionPresenter=new IActionCompl(this,HistoryActivity.this);
        sessionManager=new SessionManager(this);
        ButterKnife.bind(this);


        getHistoryRequest.setAuthor(getIntent().getStringExtra("user_id"));
        initViews();
        getHistory();



        setupToolbar(toolbar, R.layout.toolbar_home_layout);
        setupToolBarName("HISTORY", true, false);
    }

    private void getHistory() {

        getHistoryRequest.setSkip("0");
        getHistoryRequest.setLimit("10");
        iHistoryPresenter.getHistory(getHistoryRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void initViews()
    {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewPosts.setLayoutManager(mLayoutManager);
        recyclerViewPosts.setItemAnimator(new DefaultItemAnimator());
        adapter=new PostAdapter(postDatas,HistoryActivity.this,recyclerViewPosts,this, Constants.POST_SCREEN_TYPE_HISTORY);
        recyclerViewPosts.setAdapter(adapter);

    }



    @Override
    public void onError(int errorcode, String message) {
        switch (errorcode) {
            case IHistoryView.NETWORK_ERROR:
            case IHistoryView.SERVER_ERROR:
                showErrorMessage(getApplicationContext(), message, HistoryActivity.this);
                break;
        }

    }

    @Override
    public void onSuccess(PostDataResult dataResponses) {

        postDatas.clear();
        Log.e("data size", String.valueOf(dataResponses.getResult().size()));
        Log.e("total size", String.valueOf(dataResponses.getTotalfeedscount()));

        totalDisplayFeed = dataResponses.getResult().size();
        totalFeeds = dataResponses.getTotalfeedscount();

        swipeRefreshLayout.setRefreshing(false);
        postDatas.addAll(dataResponses.getResult());
        adapter.notifyDataSetChanged();


        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                Log.e("couunt>>DisplayFeed", String.valueOf(totalFeeds) + ">>>>" + String.valueOf(totalDisplayFeed));

                doLoadMore();

            }
        });

    }


    private void doLoadMore() {
        String oldskip = getHistoryRequest.getSkip();
        String oldlimit = getHistoryRequest.getLimit();

        String newSkip = String.valueOf(Integer.parseInt(oldlimit));
        String newLimit = String.valueOf(Integer.parseInt(oldlimit) + 10);
        adapter.loadMoreStart();
        getHistoryRequest.setSkip(newSkip);
        getHistoryRequest.setLimit(newLimit);
        iHistoryPresenter.getHistory(getHistoryRequest);
    }

    @Override
    public void onAppendData(PostDataResult dataResponses) {
        swipeRefreshLayout.setRefreshing(false);
        Log.e("data size load more", String.valueOf(dataResponses.getResult().size()));
        //totalDisplayFeed = totalDisplayFeed + dataResponses.getResult().size();
        totalDisplayFeed = totalDisplayFeed + dataResponses.getResult().size();
        adapter.loadMoreData(dataResponses.getResult());
    }

    @Override
    public void onUpdatePostAction(int position, PostActions actiontype) {
        switch (actiontype) {
            case like:
                postDatas.get(position).getLikedBy().add(sessionManager.getUserData().get_id());
                /*postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case dislike:
                postDatas.get(position).getDislikedBy().add(sessionManager.getUserData().get_id());
               /* postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getPeaceBy().remove(sessionManager.getUserData().get_id());
                */break;
            case peace:
                postDatas.get(position).getPeaceBy().add(sessionManager.getUserData().get_id());
                /*postDatas.get(position).getLikedBy().remove(sessionManager.getUserData().get_id());
                postDatas.get(position).getDislikedBy().remove(sessionManager.getUserData().get_id());
                */break;
        }
        adapter.notifyItemChanged(position);
    }

    @Override
    public void onHidePost(int position) {
        hidePost(position);
        Toast.makeText(this, "Post hide successfully", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onReportPost() {
        Toast.makeText(this, "Post has been reported.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBlockUser(String handle) {
        Toast.makeText(this, handle + " blocked successfully.", Toast.LENGTH_LONG).show();
        getHistory();

    }

    @Override
    public void onSharePost(PostActionResponse response) {
        Toast.makeText(this, "Post shared successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAddComment(int position) {
        postDatas.get(position).setCommentsCount(postDatas.get(position).getCommentsCount() + 1);
        adapter.notifyItemChanged(position);
        Toast.makeText(this, "Comment added successfully", Toast.LENGTH_LONG).show();
    }


    @Override
    public void performAction(UpdatePostRequest request, int position) {
        iActionPresenter.updatePost(request, position);
    }

    @Override
    public void openPopupview(Context context, Point p, final int position, final GetPostDataResponse response) {
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) findViewById(R.id.post_popup_view_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.post_pop_view_layout, null);

        /*action button*/
        LinearLayout actionReportPost = (LinearLayout) layout.findViewById(R.id.action_report_post);
        LinearLayout actionHidePost = (LinearLayout) layout.findViewById(R.id.action_hide_post);
        LinearLayout actionBlockUser = (LinearLayout) layout.findViewById(R.id.action_block_user);
        TextView userName = (TextView) actionBlockUser.findViewById(R.id.popup_user_handle);

        userName.setText("Block @" + response.getAuthor().getProfile().getHandle());

        // Creating the PopupWindow
        post_popup = new PopupWindow(context);
        post_popup.setContentView(layout);

        post_popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        post_popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int OFFSET_X = -500;
        int OFFSET_Y = 70;

        //Clear the default translucent background
        post_popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        post_popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);


        actionHidePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doHidePost(response.get_id(), position);
                hidePost(position);
                post_popup.dismiss();
            }
        });

        actionReportPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doPostReport(response.get_id());
                post_popup.dismiss();
            }
        });

        actionBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionPresenter.doBlockUser(response.getAuthor().get_id(),
                        "@" + response.getAuthor().getProfile().getHandle());
                post_popup.dismiss();
            }
        });


    }

    @Override
    public void sharePost(String postid) {

        SharePostRequest request = new SharePostRequest();
        request.setCaption("test sharing post");
        request.setPost_id(postid);
        request.setCordinates(new String[]{"0", "0"});
        iActionPresenter.doSharePost(request);
    }

    @Override
    public void addComment(String postid, String comment_txt, int position) {
        iActionPresenter.doAddComment(postid, comment_txt, position,"post");
    }


    private void hidePost(int position) {
        postDatas.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, postDatas.size());
    }

}
