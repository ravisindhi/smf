package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesRequest;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.AllTribesResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model.search_tribe.SearchTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.view.ITribesHomeView;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.model.CreateTribeResponse;
import com.smf.daffodil.smfapp.slide_menu_options.tribes.tribe_detail.model.JoinTribeRequest;

/**
 * Created by daffodil on 11/2/17.
 */

public class ITribesHomeCompl implements ITribesHomePresenter{

    ITribesHomeView iTribesHomeView;
    Context context;

    public ITribesHomeCompl(ITribesHomeView iTribesHomeView, Context context) {
        this.iTribesHomeView = iTribesHomeView;
        this.context = context;
    }


    @Override
    public void getAllTribes(AllTribesRequest allTribesRequest, final boolean isLoadMore) {


        if(!isLoadMore)
            iTribesHomeView.showLoader(context.getString(R.string.please_wait_message));

        if(iTribesHomeView.onCheckNetworkConnection())
        {



            SMFApplication.getRestClient().getAllTribes(new StringRequestCallback<AllTribesResponse>() {

                @Override
                public void onRestResponse(Exception e, AllTribesResponse result) {
                    if(e==null && result !=null)
                    {
                        if(!isLoadMore)
                        {

                            iTribesHomeView.onGetTribes(result);
                        }else
                        {
                            iTribesHomeView.onAppendMoreData(result);
                        }
                        iTribesHomeView.hideLoader();
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                   try{
                       iTribesHomeView.hideLoader();
                       e.printStackTrace();
                       iTribesHomeView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                   }catch (Exception ex)
                   {
                       e.printStackTrace();
                       iTribesHomeView.onError(IBaseView.NETWORK_ERROR,"Internal Error please try again !!");
                   }

                }
            },allTribesRequest);
        }else
        {
            iTribesHomeView.hideLoader();
            iTribesHomeView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void searchTribesMember(String name) {
        if(iTribesHomeView.onCheckNetworkConnection())
        {



            SMFApplication.getRestClient().searchTribeMemberByName(new StringRequestCallback<SearchTribeResponse>() {

                @Override
                public void onRestResponse(Exception e, SearchTribeResponse result) {
                    if(e==null && result !=null)
                    {
                        iTribesHomeView.onGetSearchTribes(result);
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    iTribesHomeView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                }
            },name);
        }else
        {
            iTribesHomeView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }

    @Override
    public void joinTribe(JoinTribeRequest joinTribeRequest) {

        iTribesHomeView.showLoader(context.getString(R.string.please_wait_message));
        if (iTribesHomeView.onCheckNetworkConnection()) {

            SMFApplication.getRestClient().joinTribe(new StringRequestCallback<CreateTribeResponse>() {
                @Override
                public void onRestResponse(Exception e, CreateTribeResponse result) {
                    iTribesHomeView.hideLoader();
                    iTribesHomeView.onSuccessFullJoinTribe();
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                    e.printStackTrace();
                    iTribesHomeView.hideLoader();
                    iTribesHomeView.onError(IBaseView.SERVER_ERROR, result.getMessage());

                }
            },joinTribeRequest);

        }else
        {
            iTribesHomeView.hideLoader();
            iTribesHomeView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));

        }
    }

    @Override
    public void searchTribes(AllTribesRequest allTribesRequest, final boolean isLoadMore) {

        if(!isLoadMore)
            iTribesHomeView.showLoader(context.getString(R.string.please_wait_message));

        if(iTribesHomeView.onCheckNetworkConnection())
        {



            SMFApplication.getRestClient().searchTribes(new StringRequestCallback<AllTribesResponse>() {

                @Override
                public void onRestResponse(Exception e, AllTribesResponse result) {
                    if(e==null && result !=null)
                    {
                        if(!isLoadMore)
                        {

                            iTribesHomeView.onGetSearchResult(result);
                        }else
                        {
                            iTribesHomeView.onAppendMoreData(result);
                        }
                        iTribesHomeView.hideLoader();
                    }
                }
                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {

                    try{
                        iTribesHomeView.hideLoader();
                        e.printStackTrace();
                        iTribesHomeView.onError(IBaseView.NETWORK_ERROR,result.getMessage());
                    }catch (Exception ex)
                    {
                        e.printStackTrace();
                        iTribesHomeView.onError(IBaseView.NETWORK_ERROR,"Internal Error please try again !!");
                    }

                }
            },allTribesRequest);
        }else
        {
            iTribesHomeView.hideLoader();
            iTribesHomeView.onError(IBaseView.NETWORK_ERROR,context.getString(R.string.internet_error_message));
        }
    }
}
