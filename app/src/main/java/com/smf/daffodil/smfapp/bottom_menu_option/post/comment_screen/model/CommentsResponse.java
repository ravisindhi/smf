
package com.smf.daffodil.smfapp.bottom_menu_option.post.comment_screen.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostAuthor;

public class CommentsResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("author")
    @Expose
    private PostAuthor author;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("newsFeedPost")
    @Expose
    private String newsFeedPost;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("peaceBy")
    @Expose
    private List<String> peaceBy = null;
    @SerializedName("dislikedBy")
    @Expose
    private List<String> dislikedBy = null;
    @SerializedName("likedBy")
    @Expose
    private List<String> likedBy = null;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public PostAuthor getAuthor() {
        return author;
    }

    public void setAuthor(PostAuthor author) {
        this.author = author;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getNewsFeedPost() {
        return newsFeedPost;
    }

    public void setNewsFeedPost(String newsFeedPost) {
        this.newsFeedPost = newsFeedPost;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public List<String> getPeaceBy() {
        return peaceBy;
    }

    public void setPeaceBy(List<String> peaceBy) {
        this.peaceBy = peaceBy;
    }

    public List<String> getDislikedBy() {
        return dislikedBy;
    }

    public void setDislikedBy(List<String> dislikedBy) {
        this.dislikedBy = dislikedBy;
    }

    public List<String> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(List<String> likedBy) {
        this.likedBy = likedBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
