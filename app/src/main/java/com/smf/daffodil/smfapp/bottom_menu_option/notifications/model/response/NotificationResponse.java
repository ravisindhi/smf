
package com.smf.daffodil.smfapp.bottom_menu_option.notifications.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("notifications")
    @Expose
    private List<Notification> notifications = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

}
