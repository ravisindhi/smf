package com.smf.daffodil.smfapp.authentication.terms_condition;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smf.daffodil.smfapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TermsAndCondtionsFragment extends Fragment {



    @BindView(R.id.header_text)
    TextView textViewHeader;

    public TermsAndCondtionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_terms_and_condtions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        textViewHeader.setText(R.string.terms);
    }
}
