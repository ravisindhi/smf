package com.smf.daffodil.smfapp.bottom_menu_option.post.presenter;

import android.content.Context;

import com.smf.daffodil.smfapp.R;
import com.smf.daffodil.smfapp.application.SMFApplication;
import com.smf.daffodil.smfapp.base.view.IBaseView;
import com.smf.daffodil.smfapp.bottom_menu_option.members.user_profile.model.BlockUserResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.PostData;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.request.GetPostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.GetPostDataResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostActionResponse;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.response.PostDataResult;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.share_post.SharePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.model.update_post.UpdatePostRequest;
import com.smf.daffodil.smfapp.bottom_menu_option.post.view.IAllPostView;
import com.smf.daffodil.smfapp.rest_api.ErrorModel;
import com.smf.daffodil.smfapp.rest_api.StringRequestCallback;

import java.util.Arrays;

/**
 * Created by daffodil on 3/2/17.
 */

public class IAllPostCompl implements IAllPostPresenter {

    private Context context;
    private IAllPostView iAllPostView;

    public IAllPostCompl(Context context, IAllPostView iAllPostView) {
        this.context = context;
        this.iAllPostView = iAllPostView;
    }


    @Override
    public void getAllPosts(GetPostRequest request) {
        startGetAllPostsService(request);
    }


    private void startGetAllPostsService(final GetPostRequest request) {



        if (iAllPostView.onCheckNetworkConnection()) {
            SMFApplication.getRestClient().getAllPost(new StringRequestCallback<PostDataResult>() {
                @Override
                public void onRestResponse(Exception e, PostDataResult result) {
                    if (e == null && result != null) {

                        if (request.getLimit().equals("10")) {

                            if (result.getResult().size() > 0)
                                iAllPostView.onSuccess(result);
                            else
                                iAllPostView.onError(IAllPostView.NODATA, "No data found !!");

                        } else {
                            iAllPostView.onAppendData(result);
                        }

                    }
                }

                @Override
                public void onErrorResponse(Exception e, ErrorModel result) {
                        e.printStackTrace();
                        iAllPostView.hideLoader();
                    if (result.getMessage() != null && result.getMessage().length() > 0)
                        iAllPostView.onError(IBaseView.SERVER_ERROR, result.getMessage());
                    else
                        iAllPostView.onError(IBaseView.SERVER_ERROR, "Server Error !!");
                }
            }, request);
        } else {
            iAllPostView.hideLoader();
            iAllPostView.onError(IBaseView.NETWORK_ERROR, context.getString(R.string.internet_error_message));
        }
    }
}
