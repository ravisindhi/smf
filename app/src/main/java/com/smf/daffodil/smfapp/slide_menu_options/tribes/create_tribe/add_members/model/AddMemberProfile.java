package com.smf.daffodil.smfapp.slide_menu_options.tribes.create_tribe.add_members.model;

import java.io.Serializable;

/**
 * Created by daffodil on 22/2/17.
 */

public class AddMemberProfile implements Serializable{

    private String handle;

    private String profileImage;

    private String fullName;

    public String getHandle ()
    {
        return handle;
    }

    public void setHandle (String handle)
    {
        this.handle = handle;
    }

    public String getProfileImage ()
    {
        return profileImage;
    }

    public void setProfileImage (String profileImage)
    {
        this.profileImage = profileImage;
    }

    public String getFullName ()
    {
        return fullName;
    }

    public void setFullName (String fullName)
    {
        this.fullName = fullName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [handle = "+handle+", profileImage = "+profileImage+", fullName = "+fullName+"]";
    }
}
