package com.smf.daffodil.smfapp.slide_menu_options.tribes.all_tribes.model;

/**
 * Created by daffodil on 11/2/17.
 */

public class TribesEmails {

    private String _id;

    private String address;

    private String verified;

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getVerified ()
    {
        return verified;
    }

    public void setVerified (String verified)
    {
        this.verified = verified;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [_id = "+_id+", address = "+address+", verified = "+verified+"]";
    }
}
